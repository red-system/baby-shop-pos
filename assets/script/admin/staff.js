"use strict";

// Class Definition
var KTStaffGeneral = function() {
    var getUrl = window.location;
    var base_url = $("#base_url").val();
    var list_url = $("#list_url").val();
    var showErrorMsg = function(form, type, msg) {
        var alert = $('<div class="kt-alert kt-alert--outline alert alert-' + type + ' alert-dismissible" role="alert">\
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\
			<span></span>\
		</div>');

        form.find('.alert').remove();
        alert.prependTo(form);
        //alert.animateClass('fadeIn animated');
        KTUtil.animateClass(alert[0], 'fadeIn animated');
        alert.find('span').html(msg);
    }
    var StafForm = function(){
        $('#mulai_bekerja').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            autoclose: true,
            format: 'yyyy-mm-dd',
        });
    }
    
    var KTStaffDataTable = function() {
        
        var datatable = $('.kt-datatable').KTDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: list_url,
                        // sample custom headers
                        headers: {'x-my-custokt-header': 'some value', 'x-test-header': 'the value'},
                        map: function(raw) {
                            // // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                
            },

            // layout definition
            layout: {
                scroll: false,
                footer: false,
            },

            // column sorting
            sortable: true,

            pagination: true,

            search: {
                input: $('#generalSearch'),
            },
            // columns definition
            columns: [
               {
                    field: 'no',
                    title: '#',
                    sortable: 'asc',
                    width: 30,
                    type: 'number',
                    selector: false,
                    textAlign: 'center',
               },{
                    field: 'staff_nama',
                    title: 'Nama'
               },{
                    field: 'staff_alamat',
                    title: 'Alamat'
               },{
                    field: 'staff_phone_number',
                    title: 'No. Telepon'
               },{
                    field: 'staff_email',
                    title: 'Email'
               }, {
                    field: 'staff_status',
                    title: 'Status'
               }, {
                    field: 'lama_bekerja',
                    title: 'Lama Bekerja'

               },{
                    field: 'Actions',
                    title: 'Actions',
                    sortable: false,
                    width: 110,
                    overflow: 'visible',
                    autoHide: false,
                    template: function(data) {

                        return '\
                        <textarea style="display:none">'+JSON.stringify(data)+'</textarea>\
                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-sm view-btn" title="Details">\
                            <i class="flaticon-visible"></i>\
                        </a>\
                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-sm edit-btn" title="Edit">\
                            <i class="flaticon2-paper"></i>\
                        </a>\
                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-sm delete-btn" data-route="'+data.delete_url+'" title="Delete">\
                            <i class="flaticon2-trash"></i>\
                        </a>\
                    ';
                    },
                }],

        });
        $("#child_data_ajax").on('click','.view-btn',function(){
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            $.each(object,function(key,value){
                $('#kt_modal_detail [name="' + key + '"]').val(value);
            })
            $("#kt_modal_detail").modal("show");
        })
        $("#child_data_ajax").on('click','.edit-btn',function(){
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            $.each(object,function(key,value){
                $('#kt_modal_edit [name="' + key + '"]').val(value);
            })
            $("#kt_modal_edit").modal("show");
        })
        $("#child_data_ajax").on('click','.delete-btn',function(){
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            $.each(object,function(key,value){
                $('#kt_modal_delete [name="' + key + '"]').val(value);
            })
            var route = object.delete_url;
            swal.fire({
            title: "Perhatian ...",
            text: "Yakin hapus data ini ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Ya, yakin",
            cancelButtonText: "Batal",
            }).then(function (e) {
                if (e.value) {
                    $.ajax({
                        url: route,
                        type: "delete",
                        data:{"id":object.row_id},
                        success: function (response) {
                            var data = jQuery.parseJSON(response);
                            if (data.success){
                                datatable.reload();
                                swal.fire({
                                    type: 'success',
                                    text:data.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                            }else {
                               swal.fire({
                                    type: 'error',
                                    text:data.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                            }
                        },
                        error: function(request) {
                            swal.fire({
                                title: "Ada yang Salah",
                                html: request.responseJSON.message,
                                type: "warning"
                            });
                        }
                    });
                }
            });
        })
        $('#kt_add_staff_submit').click(function(e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            form.validate({
                rules: {
                    nik: {
                        required: true
                    },
                    staff_nama: {
                        required: true
                    },
                    tempat_tanggal_lahir: {
                        required: true
                    },
                    staff_alamat: {
                        required: true
                    },
                    staff_status: {
                        required: true
                    },
                    staff_kelamin: {
                        required: true
                    },
                    mulai_bekerja: {
                        required: true
                    },
                    staff_email: {
                        required: true,
                        email: true
                    },
                    staff_phone_number: {
                        required: true
                    }
                }
            });
            if (!form.valid()) {
                return;
            }
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            form.ajaxSubmit({
                url: form.attr("action"),
                success: function(response, status, xhr, $form) {
                    var data = jQuery.parseJSON(response);
                     btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    if (data.success) {
                        form.clearForm();
                        form.validate().resetForm();
                        $("#kt_modal_add").modal("hide");
                        datatable.reload()
                        swal.fire({
                            type: 'success',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    } else {
                        swal.fire({
                            type: 'error',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    } 
                }
            });
        });
        $('#kt_edit_submit').click(function(e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            form.validate({
                rules: {
                    nik: {
                        required: true
                    },
                    staff_nama: {
                        required: true
                    },
                    tempat_tanggal_lahir: {
                        required: true
                    },
                    staff_alamat: {
                        required: true
                    },
                    staff_status: {
                        required: true
                    },
                    staff_kelamin: {
                        required: true
                    },
                    mulai_bekerja: {
                        required: true
                    },
                    staff_email: {
                        required: true,
                        email: true
                    },
                    staff_phone_number: {
                        required: true
                    }
                }
            });
            if (!form.valid()) {
                return;
            }
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            form.ajaxSubmit({
                url: form.attr("action"),
                success: function(response, status, xhr, $form) {
                    var data = jQuery.parseJSON(response);
                     btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    if (data.success) {
                        form.clearForm();
                        form.validate().resetForm();
                        $("#kt_modal_edit").modal("hide");
                        datatable.reload()
                        swal.fire({
                            type: 'success',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    } else {
                        swal.fire({
                            type: 'error',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    } 
                }
            });
        });

    }
    // Public Functions
    return {
        // public functions
        init: function() {
            StafForm();
            KTStaffDataTable();

        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTStaffGeneral.init();
});