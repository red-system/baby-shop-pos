"use strict";
// Class Definition
var KTPOS = function() {
    var general = function(){
        var base_url = $("#base_url").val();
        var noCount = $("#no_item").length>0 ? $("#no_item").val():1;
        var noChose = 0;
        var transaksi = {};
        var textTrans = {};
        var produk_url = $("#produk_url").val();
        $('.modal').on('hidden.bs.modal', function () {
			$("#kt_add_submit").css('display','block');
		})
		$('.modal').on('show.bs.modal', function () {
			$("#kt_add_submit").css('display','none');
		})
        var produk_table = $("#produk-table").DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ordering: false,
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
            pageLength: 10,
            ajax: produk_url,
            columns:[{data:"produk_kode"},{data:'produk_nama'},{data:'jenis_produk_nama'},{data:'aksi'}],
            columnDefs:[{
                targets: -1,
                responsivePriority: 1,
                title: 'Actions',
                orderable: false,
                render: function(data, type, full, meta) {
                    var temp = '\
                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" aria-label="First group" style="width:100%">\
                    <textarea style="display:none">'+JSON.stringify(full)+'</textarea>\
                    <a href="javascript:;" class="btn btn-success btn-icon btn-icon-sm chose-btn" title="Pilih" >\
                    <i class="fa fa-calendar-check"></i>&nbsp; Pilih\
                    </a>\
                    '
                    temp += '</div>'
                    return temp;
                }
            }]

        });
        var produk_table_edit = $("#produk-table_edit").DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ordering: false,
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
            pageLength: 10,
            ajax: produk_url,
            columns:[{data:"produk_kode"},{data:'produk_nama'},{data:'jenis_produk_nama'},{data:'aksi'}],
            columnDefs:[{
                targets: -1,
                responsivePriority: 1,
                title: 'Actions',
                orderable: false,
                render: function(data, type, full, meta) {
                    var temp = '\
                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm" role="group" aria-label="First group" style="width:100%">\
                    <textarea style="display:none">'+JSON.stringify(full)+'</textarea>\
                    <a href="javascript:;" class="btn btn-success btn-icon btn-icon-sm chose-btn" title="Pilih" >\
                    <i class="fa fa-calendar-check"></i>&nbsp; Pilih\
                    </a>\
                    '
                    temp += '</div>'
                    return temp;
                }
            }]

        });
        $("#generalSearch").on('keyup', function(e){
			produk_table.search($("#generalSearch").val());
			var params = {};
			$('.searchInput').each(function() {

				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val(); 
				}
				else {
					params[i] = $(this).val(); 
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				produk_table.column(i).search(val ? val : '', false, false);
			});
			produk_table.table().draw();
		})
		$("#generalSearch_edit").on('keyup', function(e){
            produk_table_edit.search($("#generalSearch_edit").val());
            var params = {};
            $('.searchInput').each(function() {

                var i = $(this).data('col-index');
                if (params[i]) {
                    params[i] += '|' + $(this).val(); 
                }
                else {
                    params[i] = $(this).val(); 
                }
            });
            $.each(params, function(i, val) {
                // apply search params to datatable
                produk_table_edit.column(i).search(val ? val : '', false, false);
            });
            produk_table_edit.table().draw();
        })
        $(".textSearch").keyup(function(){
            $(this).trigger('change');
        })

		$(".searchInput").change(function(){
			produk_table.search($("#generalSearch").val());
			var params = {};
			$('.searchInput').each(function() {

				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}

			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				produk_table.column(i).search(val ? val : '', false, false);
			});
			produk_table.table().draw();
		})
        $(".searchInput").change(function(){
            produk_table.search($("#generalSearch_edit").val());
            var params = {};
            $('.searchInput').each(function() {

                var i = $(this).data('col-index');
                if (params[i]) {
                    params[i] += '|' + $(this).val();
                }
                else {
                    params[i] = $(this).val();
                }

            });
            $.each(params, function(i, val) {
                // apply search params to datatable
                produk_table.column(i).search(val ? val : '', false, false);
            });
            produk_table.table().draw();
        })
		$("#barcode").keypress(function(event){
			var keycode = (event.keyCode ? event.keyCode : event.which);
			if(keycode == 13){
				var barcode = $(this).val();
				$.ajax({
					type: "POST",
					url: produk_url+"?stock_produk_seri="+barcode,
					cache: false,
					success: function(response){
						var result = jQuery.parseJSON(response);
						var object = result.aaData[0]
						$("#produk_nama_"+noChose).html(object.produk_nama);
						if(transaksi["trans"+noChose].tipe_penjualan == "eceran"){
							$("#harga_"+noChose).html(object.harga_eceran);
						} else{
							$("#harga_"+noChose).html(object.harga_grosir);
						}
						$("#kode_"+noChose).val(object.produk_kode);
						transaksi["trans"+noChose].produk_nama = object.produk_nama;
						transaksi["trans"+noChose].produk_kode = object.produk_kode;
                        transaksi["trans"+noChose].satuan_id = object.produk_satuan_id;
                        transaksi["trans"+noChose].satuan_nama = object.satuan_nama;
						transaksi["trans"+noChose].produk_id = object.produk_id;
						transaksi["trans"+noChose].harga_eceran = object.harga_eceran;
						transaksi["trans"+noChose].harga_grosir = object.harga_grosir;
						transaksi["trans"+noChose].ar_minimal_pembelian = object.ar_minimal_pembelian;
						transaksi["trans"+noChose].ar_harga = object.ar_harga;
						transaksi["trans"+noChose].hpp = object.hpp;
						transaksi["trans"+noChose].stock_produk_id = object.stock_produk_id;
                        transaksi["trans"+noChose].stock_produk_qty = object.stock_produk_qty;
						transaksi["trans"+noChose].lokasi_id = object.lokasi_id;
						$("#kt_modal_search_produk").modal("hide");
						getSubtotal(noChose);
                        $("#jumlah_"+noChose).focus();
                        $("#jumlah_"+noChose).val("");
					},
					error: function (xhr, ajaxOptions, thrownError) {
						console.log(xhr.status);
						console.log(xhr.responseText);
						console.log(thrownError);
					}
				});
			}

		})
        $("#item_child").on('click','.item-search',function(){
            noChose = $(this).data('no');
            $("#kt_modal_search_produk").modal("show");
			$( "#barcode" ).focus();
        })
        $("#item_child").on('click','.item-search-edit',function(){
            noChose = $(this).data('no');
            $("#kt_modal_search_produk_edit").modal("show");
            $( "#barcode_edit" ).focus();
        })
        $("#produk_child").on('click','.chose-btn',function(){
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            console.log(object)
            var id = object.produk_id;
            console.log(id)
            var nilai_def = 0;
            $("#produk_nama_"+noChose).html(object.produk_nama);
            $("#satuan_nama_"+noChose).html('<option data-idsatuanunit="'+object.produk_satuan_id+'" data-unit_id="'+nilai_def+'" value="'+nilai_def+'" >'+object.satuan_nama+'</option>');
            $("#idsatuanunit_"+noChose).html('<input type="text" class="form-control" value="'+object.produk_satuan_id+'">')
            $("#kode_"+noChose).val(object.produk_kode);
            
            $.ajax({
                url : base_url+"assembly/get-unit-satuan",
                method : "POST",
                data : {id: id},
                async : true,
                dataType : 'json',
                success: function(data){
                    var html = '';
                    var i;
                    for(i=0; i<data.length; i++){
                        html += '<option data-idsatuanunit="'+data[i].satuan_id+'" data-unit_id="'+data[i].unit_id+'" value='+data[i].unit_id+'>'+data[i].satuan_nama+'</option>';
                    }
                    $("#satuan_nama_"+noChose).append(html);
                }
            });
            
            transaksi["trans"+noChose].produk_nama = object.produk_nama;
            transaksi["trans"+noChose].produk_kode = object.produk_kode;
            transaksi["trans"+noChose].satuan_nama = object.satuan_nama;
            transaksi["trans"+noChose].satuan_id = object.produk_satuan_id;
            transaksi["trans"+noChose].produk_id = object.produk_id;
            transaksi["trans"+noChose].harga_eceran = object.harga_eceran;
            transaksi["trans"+noChose].harga_grosir = object.harga_grosir;
            transaksi["trans"+noChose].ar_minimal_pembelian = object.ar_minimal_pembelian;
            transaksi["trans"+noChose].ar_harga = object.ar_harga;
            transaksi["trans"+noChose].hpp = object.hpp;
            transaksi["trans"+noChose].stock_produk_id = object.stock_produk_id;
            transaksi["trans"+noChose].stock_produk_qty = object.stock_produk_qty;
            transaksi["trans"+noChose].lokasi_id = object.lokasi_id;
            transaksi["trans"+noChose].unit_id = nilai_def;
            transaksi["trans"+noChose].satuan_id_unit = object.produk_satuan_id;
            $("#kt_modal_search_produk").modal("hide");
            getSubtotal(noChose);
        })
        $("#produk_child_edit").on('click','.chose-btn',function(){
            var json = $(this).siblings('textarea').val();
            var object = JSON.parse(json);
            var id = object.produk_id;
            var nilai_def = 0;
            $("#produk_id_edit_"+noChose).val(object.produk_id);
            $("#produk_kode_edit_"+noChose).val(object.produk_kode);
            $("#produk_nama_edit_"+noChose).html(object.produk_nama);
            $("#satuan_nama_edit_"+noChose).html('<option data-idsatuanunit="'+object.produk_satuan_id+'" data-unit_id="'+nilai_def+'" value="'+nilai_def+'" >'+object.satuan_nama+'</option>');
            $("#idsatuanunit_edit_"+noChose).val(object.produk_satuan_id);
            $("#jumlah_edit_"+noChose).val(nilai_def);
            
            $.ajax({
                url : base_url+"assembly/get-unit-satuan",
                method : "POST",
                data : {id: id},
                async : true,
                dataType : 'json',
                success: function(data){
                    var html = '';
                    var i;
                    for(i=0; i<data.length; i++){
                        html += '<option data-idsatuanunit="'+data[i].satuan_id+'" data-unit_id="'+data[i].unit_id+'" value='+data[i].unit_id+'>'+data[i].satuan_nama+'</option>';
                    }
                    $("#satuan_nama_edit_"+noChose).append(html);
                }
            });
            
            $("#kt_modal_search_produk_edit").modal("hide");
        })
        
        $("#button_add_trans").click(function(){
            var item = {produk_id:null,produk_nama:null,satuan_nama:null,produk_kode:null,jumlah:0}
            transaksi["trans"+noCount] = item;
            var tr = itemRow();
            $("#item_child").append(tr);
            new Cleave($("#jumlah_"+noCount), {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralDecimalScale: 4
            })
			noChose = noCount;
			$("#kt_modal_search_produk").modal("show");
			$( "#barcode" ).focus();
            noCount++;
        })

        var itemRow = function(){
            var text = '<tr id="row_'+noCount+'">';
            text +=  '<td width="150"><div class="input-group col-12">'+
            '<input type="text" class="form-control" id="kode_'+noCount+'" readonly="">'+
            '<div class="input-group-append"><button class="btn btn-primary item-search" type="button" data-no="'+noCount+'"><i class="flaticon-search"></i></button></div>'+
            '</div></td>';
            text +=  '<td width="150"><span id="produk_nama_'+noCount+'"></span></td>';
            text +=  '<td width="150">'+
                        '<select class="col-12 m-select form-control sel-satuan" name="" data-no="'+noCount+'" id="satuan_nama_'+noCount+'">'+
                        
                        '</select>'+
                        '<div class="col-2" style="display:none;" id="idsatuanunit_'+noCount+'">'+
                        '</div>'+
                     '</td>';
            text +=  '<td width="80"><input type="text" class="form-control input-numeral" data-no="'+noCount+'" value="0" id="jumlah_'+noCount+'"></td>';
            text +=  '<td width="30"><button class="btn btn-danger delete-btn" data-no="'+noCount+'"><i class="flaticon2-trash"></i></button></td>';
            text += '</tr>';
            return text;
        }
        $("#item_child").on('change','.sel-satuan',function(){
            var idsatuanunit = ($(this).find(':selected').data('idsatuanunit'));
            var unit_id = ($(this).find(':selected').data('unit_id'));
            var no = $(this).data('no');
            
            $("#idsatuanunit_"+no).html('<input type="text" class="form-control" value="'+idsatuanunit+'">');
            transaksi["trans"+noChose].unit_id = unit_id;
            transaksi["trans"+noChose].satuan_id_unit = idsatuanunit;
            getSubtotal(no);
            getTotal();
        })
        $("#item_child").on('change','.sel-satuan-edit',function(){
            var idsatuanunit = ($(this).find(':selected').data('idsatuanunit'));
            var unit_id = ($(this).find(':selected').data('unit_id'));
            var no = $(this).data('no');
            $("#idsatuanunit_edit_"+no).val(idsatuanunit);
        })
        $('#item_child').on('click','.delete-btn',function(){
            var no = $(this).data("no");
            $("#row_"+no).remove();
            delete transaksi["trans"+no];
            getTotal();
        })
        $("#item_child").on('click','.item-delete',function () {
            var no = $(this).data('no');
            $("#row_edit_"+no).remove();
        })
        $('#item_child').on('keyup','.input-numeral',function(){
            if($(this).val() == ""){
                $(this).val("0");
            }
            var element = $(this);
            var id = $(this).attr('id');
            var no = id.replace("jumlah_","");
            
            transaksi["trans"+no].jumlah = intVal($(this).val());
            getSubtotal(no);

        })
        $('#item_child').on('keyup','.edit-numeral',function(){
            if($(this).val() == ""){
                $(this).val("0");
            }
            var element = $(this);
            var id = $(this).attr('id');
            var no = id.replace("jumlah_edit_","");
            
        })
        var getSubtotal = function(index){
            var total = 0;
            transaksi["trans"+index].subtotal =  KTUtil.numberString(total.toFixed(0))
            $("#subtotal_"+index).html(transaksi["trans"+index].subtotal);
            getTotal(index);
        }
        var intVal = function(i) {
            return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
        };
        var getTotal = function(index){
            textTrans = {item:transaksi}
            $("#textTransaksi").html(JSON.stringify(textTrans));
        }
        $("#kt_add_submit").click(function(){
            var btn = $(this);
            var form = $("#kt_add")
            var is_zero = false;
            var is_zero_edit = false;
            var base_url = $("#base_url").val();
            $("#item_child .input-numeral").each(function(){
                if($(this).val()==0){
                    is_zero = true;
                }
            })
            $("#item_child .edit-numeral").each(function(){
                if($(this).val()==0){
                    is_zero_edit = true;
                }
            })
            if(is_zero||is_zero_edit){
                swal.fire({
                    type: 'error',
                    title:'Peringatan',
                    text:'Tidak boleh ada jumlah maupun harga yang bernilai 0',
                    showConfirmButton: false,
                    timer: 1500
                });
                return false;                 
            }
            if($("#item_child").children().length == 0 ){
                swal.fire({
                    type: 'error',
                    title:'Peringatan',
                    text:'Item harus memiliki setidaknya satu produk',
                    showConfirmButton: false,
                    timer: 1500
                });                 
                return false;                
            }            
            form.validate({})
            if(!form.valid()){
                return false;
            }
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            form.ajaxSubmit({
                url: form.attr("action"),
                beforeSend: function () {
                    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
                },
                success: function(response, status, xhr, $form) {
                    console.log(response);
                    var data = jQuery.parseJSON(response);
                    if(data.success){
                        window.location.href = base_url+"produk";
                    } else {
                        $('.wrapper-loading').fadeOut().addClass('hidden');
                        btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        swal.fire({
                            type: 'error',
                            text:data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });                        
                    }
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                },error: function (xhr, ajaxOptions, thrownError) {
                    $('.wrapper-loading').fadeOut().addClass('hidden');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });

        });

    }
    return {
        // public functions
        init: function() {
            general();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTPOS.init();
});
document.addEventListener('DOMContentLoaded', () => {
    $('.input-numeral').toArray().forEach(function(field){
        new Cleave(field, {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand',
            numeralDecimalScale: 4
        })
    });
    $('.edit-numeral').toArray().forEach(function(field){
        new Cleave(field, {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand',
            numeralDecimalScale: 4
      })
    });
    $('.input-decimal').toArray().forEach(function(field){
        new Cleave(field, {
            numeral: true,
            numeralDecimalScale: 4
        })

    });
});
