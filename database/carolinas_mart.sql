-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 21, 2020 at 08:35 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.1.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `carolinas_mart`
--

-- --------------------------------------------------------

--
-- Table structure for table `arus_stock_produk`
--

CREATE TABLE `arus_stock_produk` (
  `id` int(10) UNSIGNED NOT NULL,
  `tanggal` date DEFAULT NULL,
  `table_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `penjualan_id` int(11) DEFAULT NULL,
  `penjualan_produk_id` int(11) DEFAULT NULL,
  `distribusi_id` int(11) DEFAULT NULL,
  `distribusi_detail_id` int(11) DEFAULT NULL,
  `produksi_id` int(11) DEFAULT NULL,
  `progress_produksi_id` int(11) DEFAULT NULL,
  `stock_produk_id` int(11) DEFAULT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `history_penyesuaian_produk_id` int(11) DEFAULT NULL,
  `stock_in` double DEFAULT NULL,
  `stock_out` double DEFAULT NULL,
  `last_stock` double DEFAULT NULL,
  `last_stock_total` double DEFAULT NULL,
  `method` enum('insert','update','delete') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `arus_stock_produk`
--

INSERT INTO `arus_stock_produk` (`id`, `tanggal`, `table_name`, `penjualan_id`, `penjualan_produk_id`, `distribusi_id`, `distribusi_detail_id`, `produksi_id`, `progress_produksi_id`, `stock_produk_id`, `produk_id`, `history_penyesuaian_produk_id`, `stock_in`, `stock_out`, `last_stock`, `last_stock_total`, `method`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, '2019-11-01', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 0, 200, 200, 'insert', 'Insert stok produk', '2019-11-01 04:24:50', '2019-11-01 04:24:50'),
(2, '2019-11-01', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 199, 199, 'delete', 'Penjualan produk', '2019-11-01 04:53:14', '2019-11-01 04:53:14'),
(3, '2019-11-29', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 70, 129, 129, 'update', 'Transfer stok produk', '2019-11-29 02:29:30', '2019-11-29 02:29:30'),
(4, '2019-11-29', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, NULL, 70, 0, 199, 199, 'insert', 'Penerimaan transfer stok', '2019-11-29 02:30:17', '2019-11-29 02:30:17'),
(5, '2019-11-29', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, NULL, 0, 10, 189, 189, 'update', 'Penyesuaian stok produk', '2019-11-29 02:33:05', '2019-11-29 02:33:05'),
(6, '2019-12-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, NULL, 10, 0, 199, 199, 'update', 'Order Produk', '2019-12-03 09:42:11', '2019-12-03 09:42:11'),
(7, '2019-12-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 3, 2, NULL, 10, 0, 10, 209, 'insert', 'Order Produk', '2019-12-03 09:42:11', '2019-12-03 09:42:11'),
(8, '2019-12-04', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, NULL, 10, 0, 209, 219, 'update', 'Order Produk', '2019-12-04 02:34:40', '2019-12-04 02:34:40'),
(9, '2019-12-11', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 4, 5, NULL, 0, 0, 10, 229, 'insert', 'Insert stok produk', '2019-12-11 07:24:26', '2019-12-11 07:24:26'),
(10, '2019-12-11', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 4, 5, NULL, 0, NULL, NULL, 229, 'delete', 'Delete stok produk', '2019-12-11 07:24:34', '2019-12-11 07:24:34'),
(11, '2019-12-11', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 3, 2, NULL, 0, NULL, NULL, 229, 'delete', 'Delete stok produk', '2019-12-11 07:24:53', '2019-12-11 07:24:53'),
(12, '2019-12-11', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, NULL, 0, NULL, 129, 229, 'delete', 'Delete stok produk', '2019-12-11 07:25:17', '2019-12-11 07:25:17'),
(13, '2019-12-12', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 5, 2, NULL, 0, 0, 130, 359, 'insert', 'Insert stok produk', '2019-12-12 06:42:25', '2019-12-12 06:42:25'),
(14, '2019-12-12', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 6, 2, NULL, 0, 0, 140, 369, 'insert', 'Insert stok produk', '2019-12-12 08:52:21', '2019-12-12 08:52:21'),
(15, '2019-12-13', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 7, 5, NULL, 0, 0, 10, 379, 'insert', 'Insert stok produk', '2019-12-13 03:05:07', '2019-12-13 03:05:07'),
(16, '2019-12-13', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 8, 5, NULL, 0, 0, 20, 389, 'insert', 'Insert stok produk', '2019-12-13 06:37:59', '2019-12-13 06:37:59'),
(17, '2019-12-13', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 9, 2, NULL, 0, 0, 150, 399, 'insert', 'Insert stok produk', '2019-12-13 08:23:21', '2019-12-13 08:23:21'),
(18, '2019-12-23', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 10, 4, NULL, 0, 0, 20, 419, 'insert', 'Insert stok produk', '2019-12-23 03:37:35', '2019-12-23 03:37:35'),
(19, '2019-12-23', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 11, 4, NULL, 0, 0, 40, 439, 'insert', 'Insert stok produk', '2019-12-23 03:37:45', '2019-12-23 03:37:45'),
(20, '2019-12-23', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 12, 5, NULL, 40, 0, 60, 479, 'insert', 'Insert stok produk', '2019-12-23 08:20:25', '2019-12-23 08:20:25'),
(21, '2019-12-26', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 13, 4, NULL, 40, 0, 80, 519, 'insert', 'Insert stok produk dari produk assembly', '2019-12-26 03:00:48', '2019-12-26 03:00:48'),
(22, '2019-12-26', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 14, 4, NULL, 40, 0, 120, 559, 'insert', 'Insert stok produk dari produk assembly', '2019-12-26 03:17:52', '2019-12-26 03:17:52'),
(23, '2019-12-26', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 15, 4, NULL, 200, 0, 320, 759, 'insert', 'Insert stok produk dari produk assembly', '2019-12-26 03:17:53', '2019-12-26 03:17:53'),
(24, '2019-12-26', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 16, 4, NULL, 80, 0, 400, 839, 'insert', 'Insert stok produk dari produk assembly', '2019-12-26 03:17:53', '2019-12-26 03:17:53'),
(25, '2019-12-26', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 17, 4, NULL, 400, 0, 800, 1239, 'insert', 'Insert stok produk dari produk assembly', '2019-12-26 03:17:53', '2019-12-26 03:17:53'),
(26, '2019-12-26', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 18, 4, NULL, 40, 0, 840, 1279, 'insert', 'Insert stok produk dari produk assembly', '2019-12-26 03:17:53', '2019-12-26 03:17:53'),
(27, '2019-12-26', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 19, 4, NULL, 200, 0, 1040, 1479, 'insert', 'Insert stok produk dari produk assembly', '2019-12-26 03:17:53', '2019-12-26 03:17:53'),
(28, '2019-12-26', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 20, 7, NULL, 0, 0, 100, 100, 'insert', 'Insert stok produk', '2019-12-26 03:22:31', '2019-12-26 03:22:31'),
(29, '2019-12-26', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 21, 7, NULL, 0, 0, 200, 200, 'insert', 'Insert stok produk', '2019-12-26 03:22:41', '2019-12-26 03:22:41'),
(30, '2019-12-26', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 22, 6, NULL, 0, 0, 100, 300, 'insert', 'Insert stok produk', '2019-12-26 03:24:24', '2019-12-26 03:24:24'),
(31, '2019-12-26', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 23, 6, NULL, 0, 0, 200, 400, 'insert', 'Insert stok produk', '2019-12-26 03:24:40', '2019-12-26 03:24:40'),
(32, '2019-12-26', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 24, 7, NULL, 0, 0, 300, 500, 'insert', 'Insert stok produk', '2019-12-26 03:33:32', '2019-12-26 03:33:32'),
(33, '2019-12-26', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 24, 7, NULL, 0, NULL, 200, 500, 'delete', 'Delete stok produk', '2019-12-26 03:33:41', '2019-12-26 03:33:41'),
(34, '2019-12-26', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 25, 8, NULL, 0, 0, 10, 510, 'insert', 'Insert stok produk', '2019-12-26 06:34:18', '2019-12-26 06:34:18'),
(35, '2019-12-26', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 25, 8, NULL, 0, NULL, NULL, 510, 'delete', 'Delete stok produk', '2019-12-26 06:35:08', '2019-12-26 06:35:08'),
(36, '2019-12-26', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 26, 6, NULL, 0, 100, NULL, 610, 'update', 'Pengurangan stok produk untuk produk assembly', '2019-12-26 07:24:27', '2019-12-26 07:24:27'),
(37, '2019-12-26', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 27, 7, NULL, 0, 100, NULL, 710, 'update', 'Pengurangan stok produk untuk produk assembly', '2019-12-26 07:24:27', '2019-12-26 07:24:27'),
(38, '2019-12-26', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 28, 8, NULL, 0, 0, 50, 760, 'insert', 'Insert stok produk', '2019-12-26 07:24:27', '2019-12-26 07:24:27'),
(39, '2019-12-26', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 29, 10, NULL, 0, 0, 100, 100, 'insert', 'Insert stok produk', '2019-12-26 08:12:50', '2019-12-26 08:12:50'),
(40, '2019-12-26', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 30, 9, NULL, 0, 0, 100, 200, 'insert', 'Insert stok produk', '2019-12-26 08:13:14', '2019-12-26 08:13:14'),
(41, '2019-12-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 30, 11, NULL, 0, NULL, NULL, 200, 'delete', 'Delete stok produk', '2019-12-27 03:50:27', '2019-12-27 03:50:27'),
(42, '2019-12-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 31, 9, NULL, 0, 0, 100, 300, 'insert', 'Insert stok produk', '2019-12-27 03:51:53', '2019-12-27 03:51:53'),
(43, '2019-12-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 31, 11, NULL, 0, NULL, NULL, 300, 'delete', 'Delete stok produk', '2019-12-27 03:52:21', '2019-12-27 03:52:21'),
(44, '2019-12-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 29, 11, NULL, 0, NULL, NULL, 300, 'delete', 'Delete stok produk', '2019-12-27 04:00:35', '2019-12-27 04:00:35'),
(45, '2019-12-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 32, 11, NULL, 0, 0, 10, 310, 'insert', 'Insert stok produk', '2019-12-27 04:02:26', '2019-12-27 04:02:26'),
(46, '2019-12-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 33, 10, NULL, 0, 0, 100, 410, 'insert', 'Insert stok produk', '2019-12-27 05:51:19', '2019-12-27 05:51:19'),
(47, '2019-12-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 34, 9, NULL, 0, 0, 100, 510, 'insert', 'Insert stok produk', '2019-12-27 05:51:43', '2019-12-27 05:51:43'),
(48, '2019-12-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 32, 11, NULL, 0, NULL, NULL, 510, 'delete', 'Delete stok produk', '2019-12-27 06:04:07', '2019-12-27 06:04:07'),
(49, '2019-12-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 34, 9, NULL, 0, -20, NULL, 510, 'delete', 'Delete stok produk', '2019-12-27 06:10:32', '2019-12-27 06:10:32'),
(50, '2019-12-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 33, 10, NULL, 0, -20, NULL, 510, 'delete', 'Delete stok produk', '2019-12-27 06:17:03', '2019-12-27 06:17:03'),
(51, '2019-12-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 35, 10, NULL, 0, 0, 100, 610, 'insert', 'Insert stok produk', '2019-12-27 07:19:49', '2019-12-27 07:19:49'),
(52, '2019-12-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 36, 9, NULL, 0, 0, 100, 710, 'insert', 'Insert stok produk', '2019-12-27 07:20:17', '2019-12-27 07:20:17'),
(53, '2019-12-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 36, 9, NULL, 0, 0, NULL, 710, 'delete', 'Delete stok produk', '2019-12-27 07:21:53', '2019-12-27 07:21:53'),
(54, '2019-12-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 37, 9, NULL, 0, 0, 100, 810, 'insert', 'Insert stok produk', '2019-12-27 07:36:39', '2019-12-27 07:36:39'),
(55, '2019-12-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 37, 9, NULL, 0, 0, NULL, 810, 'delete', 'Delete stok produk', '2019-12-27 08:19:29', '2019-12-27 08:19:29'),
(56, '2019-12-30', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 38, 9, NULL, 0, 0, 100, 910, 'insert', 'Insert stok produk', '2019-12-30 02:19:44', '2019-12-30 02:19:44'),
(57, '2019-12-30', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 38, 9, NULL, 0, 20, NULL, 910, 'delete', 'Delete stok produk', '2019-12-30 02:27:46', '2019-12-30 02:27:46'),
(58, '2019-12-30', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 39, 9, NULL, 0, 0, 100, 1010, 'insert', 'Insert stok produk', '2019-12-30 02:53:16', '2019-12-30 02:53:16'),
(59, '2019-12-30', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 39, 9, NULL, 0, 20, NULL, 1010, 'delete', 'Delete stok produk', '2019-12-30 02:55:50', '2019-12-30 02:55:50'),
(60, '2019-12-30', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 40, 9, NULL, 0, 0, 100, 1110, 'insert', 'Insert stok produk', '2019-12-30 03:03:14', '2019-12-30 03:03:14'),
(61, '2019-12-30', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 40, 9, NULL, 0, 200, NULL, 1110, 'delete', 'Delete stok produk', '2019-12-30 03:03:50', '2019-12-30 03:03:50'),
(62, '2019-12-30', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 35, 10, NULL, 0, 200, NULL, 1110, 'delete', 'Delete stok produk', '2019-12-30 03:03:50', '2019-12-30 03:03:50'),
(63, '2019-12-30', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 41, 11, NULL, 0, 0, 100, 1210, 'insert', 'Insert stok produk', '2019-12-30 03:03:50', '2019-12-30 03:03:50'),
(64, '2019-12-30', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 41, 11, NULL, 0, NULL, NULL, 1210, 'delete', 'Delete stok produk', '2019-12-30 03:51:11', '2019-12-30 03:51:11'),
(65, '2019-12-30', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 42, 10, NULL, 0, 0, 100, 1310, 'insert', 'Insert stok produk', '2019-12-30 03:51:36', '2019-12-30 03:51:36'),
(66, '2019-12-30', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 43, 9, NULL, 0, 0, 100, 1410, 'insert', 'Insert stok produk', '2019-12-30 03:52:34', '2019-12-30 03:52:34'),
(67, '2019-12-30', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 43, 9, NULL, 0, 20, NULL, 1410, 'delete', 'Delete stok produk', '2019-12-30 03:55:10', '2019-12-30 03:55:10'),
(68, '2019-12-30', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 42, 10, NULL, 0, 20, NULL, 1410, 'delete', 'Delete stok produk', '2019-12-30 03:55:10', '2019-12-30 03:55:10'),
(69, '2019-12-30', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 44, 11, NULL, 0, 0, 10, 1420, 'insert', 'Insert stok produk', '2019-12-30 03:55:10', '2019-12-30 03:55:10'),
(70, '2019-12-30', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 45, 10, NULL, 0, 0, 20, 1440, 'insert', 'Insert stok produk', '2019-12-30 06:20:12', '2019-12-30 06:20:12'),
(71, '2019-12-30', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 45, 10, NULL, 0, NULL, NULL, 1440, 'delete', 'Delete stok produk', '2019-12-30 06:20:30', '2019-12-30 06:20:30'),
(72, '2019-12-30', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 46, 10, NULL, 0, 0, 100, 1540, 'insert', 'Insert stok produk', '2019-12-30 06:20:45', '2019-12-30 06:20:45'),
(73, '2019-12-30', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 47, 9, NULL, 0, 0, 100, 1640, 'insert', 'Insert stok produk', '2019-12-30 06:21:15', '2019-12-30 06:21:15'),
(74, '2019-12-30', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 48, 9, NULL, 0, 0, 100, 1740, 'insert', 'Insert stok produk', '2019-12-30 06:25:15', '2019-12-30 06:25:15'),
(75, '2019-12-30', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 49, 9, NULL, 0, 0, 100, 1840, 'insert', 'Insert stok produk', '2019-12-30 06:29:45', '2019-12-30 06:29:45'),
(76, '2019-12-30', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 49, 9, NULL, 40, 0, NULL, 1840, 'delete', 'Delete stok produk', '2019-12-30 07:19:43', '2019-12-30 07:19:43'),
(77, '2019-12-30', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 46, 10, NULL, 40, 0, NULL, 1840, 'delete', 'Delete stok produk', '2019-12-30 07:19:43', '2019-12-30 07:19:43'),
(78, '2019-12-30', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 50, 11, NULL, 0, 0, 30, 1860, 'insert', 'Insert stok produk', '2019-12-30 07:19:43', '2019-12-30 07:19:43'),
(79, '2019-12-30', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 51, 10, NULL, 0, 0, 100, 1960, 'insert', 'Insert stok produk', '2019-12-30 07:56:55', '2019-12-30 07:56:55'),
(80, '2019-12-30', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 52, 9, NULL, 0, 0, 100, 2060, 'insert', 'Insert stok produk', '2019-12-30 07:57:22', '2019-12-30 07:57:22'),
(81, '2019-12-30', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 52, 9, NULL, 0, 200, NULL, 2060, 'update', 'Update stok produk', '2019-12-30 07:57:45', '2019-12-30 07:57:45'),
(82, '2019-12-30', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 51, 10, NULL, 0, 200, NULL, 2060, 'update', 'Update stok produk', '2019-12-30 07:57:45', '2019-12-30 07:57:45'),
(83, '2019-12-30', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 53, 11, NULL, 0, 0, 130, 2160, 'insert', 'Insert stok produk', '2019-12-30 07:57:46', '2019-12-30 07:57:46'),
(84, '2019-12-30', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 54, 10, NULL, 0, 0, 100, 2260, 'insert', 'Insert stok produk', '2019-12-30 07:58:48', '2019-12-30 07:58:48'),
(85, '2019-12-30', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 55, 9, NULL, 0, 0, 100, 2360, 'insert', 'Insert stok produk', '2019-12-30 07:59:06', '2019-12-30 07:59:06'),
(86, '2019-12-30', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 55, 9, NULL, 0, 10, 100, 2360, 'update', 'Update stok produk', '2019-12-30 08:16:11', '2019-12-30 08:16:11'),
(87, '2019-12-30', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 54, 10, NULL, 0, 10, 100, 2360, 'update', 'Update stok produk', '2019-12-30 08:16:11', '2019-12-30 08:16:11'),
(88, '2019-12-30', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 56, 11, NULL, 0, 0, 135, 2365, 'insert', 'Insert stok produk', '2019-12-30 08:16:11', '2019-12-30 08:16:11'),
(89, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 55, 9, NULL, 0, 20, NULL, 2365, 'update', 'Update stok produk', '2020-01-02 02:18:50', '2020-01-02 02:18:50'),
(90, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 57, 9, NULL, -20, 0, -20, 2345, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 02:18:51', '2020-01-02 02:18:51'),
(91, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 54, 10, NULL, 0, 20, NULL, 2345, 'update', 'Update stok produk', '2020-01-02 02:18:51', '2020-01-02 02:18:51'),
(92, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 58, 10, NULL, -20, 0, -20, 2325, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 02:18:51', '2020-01-02 02:18:51'),
(93, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 59, 11, NULL, 0, 0, 145, 2335, 'insert', 'Insert stok produk', '2020-01-02 02:18:51', '2020-01-02 02:18:51'),
(94, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 60, 13, NULL, 0, 0, 100, 100, 'insert', 'Insert stok produk', '2020-01-02 02:44:07', '2020-01-02 02:44:07'),
(95, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 61, 12, NULL, 0, 0, 100, 200, 'insert', 'Insert stok produk', '2020-01-02 02:44:27', '2020-01-02 02:44:27'),
(96, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 61, 12, NULL, 0, 20, NULL, 200, 'update', 'Update stok produk', '2020-01-02 02:47:17', '2020-01-02 02:47:17'),
(97, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 62, 12, NULL, 20, 0, 20, 220, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 02:47:17', '2020-01-02 02:47:17'),
(98, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 60, 13, NULL, 0, 20, NULL, 220, 'update', 'Update stok produk', '2020-01-02 02:47:17', '2020-01-02 02:47:17'),
(99, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 63, 13, NULL, 20, 0, 20, 240, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 02:47:17', '2020-01-02 02:47:17'),
(100, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 64, 14, NULL, 0, 0, 10, 250, 'insert', 'Insert stok produk', '2020-01-02 02:47:18', '2020-01-02 02:47:18'),
(101, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 65, 15, NULL, 0, 0, 100, 100, 'insert', 'Insert stok produk', '2020-01-02 03:06:37', '2020-01-02 03:06:37'),
(102, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 66, 16, NULL, 0, 0, 100, 200, 'insert', 'Insert stok produk', '2020-01-02 03:06:59', '2020-01-02 03:06:59'),
(103, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 65, 15, NULL, 0, 0, NULL, 200, 'update', 'Update stok produk', '2020-01-02 03:07:21', '2020-01-02 03:07:21'),
(104, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 67, 15, NULL, 0, 0, 100, 300, 'insert', 'Insert stok produk', '2020-01-02 03:10:24', '2020-01-02 03:10:24'),
(105, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 67, 15, NULL, 0, 20, NULL, 300, 'update', 'Update stok produk', '2020-01-02 03:10:51', '2020-01-02 03:10:51'),
(106, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 68, 15, NULL, 120, 0, 120, 420, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 03:10:51', '2020-01-02 03:10:51'),
(107, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 66, 16, NULL, 0, 20, NULL, 420, 'update', 'Update stok produk', '2020-01-02 03:10:51', '2020-01-02 03:10:51'),
(108, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 69, 16, NULL, 120, 0, 120, 540, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 03:10:52', '2020-01-02 03:10:52'),
(109, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 70, 17, NULL, 0, 0, 10, 550, 'insert', 'Insert stok produk', '2020-01-02 03:10:52', '2020-01-02 03:10:52'),
(110, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 68, 15, NULL, 0, 20, NULL, 550, 'update', 'Update stok produk', '2020-01-02 03:25:16', '2020-01-02 03:25:16'),
(111, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 71, 15, NULL, 140, 0, 140, 690, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 03:25:16', '2020-01-02 03:25:16'),
(112, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 69, 16, NULL, 0, 20, NULL, 690, 'update', 'Update stok produk', '2020-01-02 03:25:16', '2020-01-02 03:25:16'),
(113, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 72, 16, NULL, 140, 0, 140, 830, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 03:25:16', '2020-01-02 03:25:16'),
(114, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 73, 17, NULL, 0, 0, 20, 840, 'insert', 'Insert stok produk', '2020-01-02 03:25:16', '2020-01-02 03:25:16'),
(115, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 71, 15, NULL, 0, 20, NULL, 840, 'delete', 'Delete stok produk', '2020-01-02 05:26:18', '2020-01-02 05:26:18'),
(116, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 74, 15, NULL, 120, 0, 120, 960, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 05:26:18', '2020-01-02 05:26:18'),
(117, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 72, 16, NULL, 0, 20, NULL, 960, 'delete', 'Delete stok produk', '2020-01-02 05:26:19', '2020-01-02 05:26:19'),
(118, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 75, 16, NULL, 120, 0, 120, 1080, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 05:26:19', '2020-01-02 05:26:19'),
(119, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 76, 17, NULL, 0, 0, 30, 1090, 'insert', 'Insert stok produk', '2020-01-02 05:26:19', '2020-01-02 05:26:19'),
(120, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 74, 15, NULL, 0, 20, NULL, 1090, 'delete', 'Delete stok produk', '2020-01-02 05:32:39', '2020-01-02 05:32:39'),
(121, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 77, 15, NULL, 100, 0, 100, 1190, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 05:32:39', '2020-01-02 05:32:39'),
(122, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 75, 16, NULL, 0, 20, NULL, 1190, 'delete', 'Delete stok produk', '2020-01-02 05:32:40', '2020-01-02 05:32:40'),
(123, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 78, 16, NULL, 100, 0, 100, 1290, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 05:32:40', '2020-01-02 05:32:40'),
(124, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 79, 17, NULL, 0, 0, 40, 1300, 'insert', 'Insert stok produk', '2020-01-02 05:32:40', '2020-01-02 05:32:40'),
(125, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 77, 15, NULL, 0, 0, NULL, 1300, 'delete', 'Delete stok produk', '2020-01-02 05:38:39', '2020-01-02 05:38:39'),
(126, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 80, 15, NULL, 0, 0, 0, 1300, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 05:40:55', '2020-01-02 05:40:55'),
(127, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 78, 16, NULL, 0, 0, NULL, 1300, 'delete', 'Delete stok produk', '2020-01-02 05:40:55', '2020-01-02 05:40:55'),
(128, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 81, 16, NULL, 0, 0, 0, 1300, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 05:40:55', '2020-01-02 05:40:55'),
(129, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 82, 17, NULL, 0, 0, 50, 1310, 'insert', 'Insert stok produk', '2020-01-02 05:40:55', '2020-01-02 05:40:55'),
(130, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 80, 15, NULL, 0, 20, NULL, 1310, 'delete', 'Delete stok produk', '2020-01-02 05:43:56', '2020-01-02 05:43:56'),
(131, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 83, 15, NULL, -20, 0, -20, 1290, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 05:43:56', '2020-01-02 05:43:56'),
(132, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 81, 16, NULL, 0, 20, NULL, 1290, 'delete', 'Delete stok produk', '2020-01-02 05:43:56', '2020-01-02 05:43:56'),
(133, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 84, 16, NULL, -20, 0, -20, 1270, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 05:43:56', '2020-01-02 05:43:56'),
(134, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 85, 17, NULL, 0, 0, 60, 1280, 'insert', 'Insert stok produk', '2020-01-02 05:43:56', '2020-01-02 05:43:56'),
(135, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 86, 18, NULL, 0, 0, 100, 100, 'insert', 'Insert stok produk', '2020-01-02 05:51:09', '2020-01-02 05:51:09'),
(136, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 87, 19, NULL, 0, 0, 100, 200, 'insert', 'Insert stok produk', '2020-01-02 05:51:30', '2020-01-02 05:51:30'),
(137, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 86, 18, NULL, 0, 20, NULL, 200, 'delete', 'Delete stok produk', '2020-01-02 05:52:00', '2020-01-02 05:52:00'),
(138, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 88, 18, NULL, 80, 0, 80, 280, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 05:52:00', '2020-01-02 05:52:00'),
(139, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 87, 19, NULL, 0, 20, NULL, 280, 'delete', 'Delete stok produk', '2020-01-02 05:52:00', '2020-01-02 05:52:00'),
(140, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 89, 19, NULL, 80, 0, 80, 360, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 05:52:00', '2020-01-02 05:52:00'),
(141, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 90, 20, NULL, 0, 0, 10, 370, 'insert', 'Insert stok produk', '2020-01-02 05:52:00', '2020-01-02 05:52:00'),
(142, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 88, 18, NULL, 0, 0, NULL, 370, 'delete', 'Delete stok produk', '2020-01-02 06:09:17', '2020-01-02 06:09:17'),
(143, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 91, 18, NULL, 20, 0, 20, 390, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 06:09:18', '2020-01-02 06:09:18'),
(144, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 89, 19, NULL, 0, 0, NULL, 390, 'delete', 'Delete stok produk', '2020-01-02 06:09:18', '2020-01-02 06:09:18'),
(145, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 92, 19, NULL, 20, 0, 20, 410, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 06:09:18', '2020-01-02 06:09:18'),
(146, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 93, 20, NULL, 0, 0, 20, 420, 'insert', 'Insert stok produk', '2020-01-02 06:09:18', '2020-01-02 06:09:18'),
(147, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 94, 22, NULL, 0, 0, 100, 100, 'insert', 'Insert stok produk', '2020-01-02 06:30:52', '2020-01-02 06:30:52'),
(148, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 95, 21, NULL, 0, 0, 100, 200, 'insert', 'Insert stok produk', '2020-01-02 06:50:31', '2020-01-02 06:50:31'),
(149, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 95, 21, NULL, 0, 80, NULL, 200, 'delete', 'Delete stok produk', '2020-01-02 07:02:49', '2020-01-02 07:02:49'),
(150, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 96, 21, NULL, 0, 0, 0, 200, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 07:02:49', '2020-01-02 07:02:49'),
(151, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 94, 22, NULL, 0, 80, NULL, 200, 'delete', 'Delete stok produk', '2020-01-02 07:02:49', '2020-01-02 07:02:49'),
(152, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 97, 22, NULL, 0, 0, 0, 200, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 07:02:49', '2020-01-02 07:02:49'),
(153, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 98, 24, NULL, 0, 0, 10, 210, 'insert', 'Insert stok produk', '2020-01-02 07:02:50', '2020-01-02 07:02:50'),
(154, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 99, 22, NULL, 0, 0, 100, 310, 'insert', 'Insert stok produk', '2020-01-02 07:41:44', '2020-01-02 07:41:44'),
(155, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 100, 21, NULL, 0, 0, 100, 410, 'insert', 'Insert stok produk', '2020-01-02 07:42:44', '2020-01-02 07:42:44'),
(156, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 96, 21, NULL, 0, 20, 100, 410, 'delete', 'Delete stok produk', '2020-01-02 07:43:17', '2020-01-02 07:43:17'),
(157, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 101, 21, NULL, -20, 0, 80, 390, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 07:43:17', '2020-01-02 07:43:17'),
(158, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 97, 22, NULL, 0, 20, 100, 390, 'delete', 'Delete stok produk', '2020-01-02 07:43:17', '2020-01-02 07:43:17'),
(159, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 102, 22, NULL, -20, 0, 80, 370, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 07:43:17', '2020-01-02 07:43:17'),
(160, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 103, 24, NULL, 0, 0, 20, 380, 'insert', 'Insert stok produk', '2020-01-02 07:43:17', '2020-01-02 07:43:17'),
(161, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 100, 21, NULL, 0, 100, -20, 380, 'delete', 'Delete stok produk', '2020-01-02 07:44:32', '2020-01-02 07:44:32'),
(162, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 101, 21, NULL, 0, 40, NULL, 380, 'delete', 'Delete stok produk', '2020-01-02 07:44:32', '2020-01-02 07:44:32'),
(163, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 104, 21, NULL, -40, 0, -40, 340, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 07:44:32', '2020-01-02 07:44:32'),
(164, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 99, 22, NULL, 0, 100, -20, 340, 'delete', 'Delete stok produk', '2020-01-02 07:44:32', '2020-01-02 07:44:32'),
(165, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 102, 22, NULL, 0, 40, NULL, 340, 'delete', 'Delete stok produk', '2020-01-02 07:44:32', '2020-01-02 07:44:32'),
(166, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 105, 22, NULL, -40, 0, -40, 300, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 07:44:32', '2020-01-02 07:44:32'),
(167, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 106, 24, NULL, 0, 0, 30, 310, 'insert', 'Insert stok produk', '2020-01-02 07:44:32', '2020-01-02 07:44:32'),
(168, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 107, 25, NULL, 0, 0, 100, 100, 'insert', 'Insert stok produk', '2020-01-02 07:52:22', '2020-01-02 07:52:22'),
(169, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 108, 26, NULL, 0, 0, 100, 200, 'insert', 'Insert stok produk', '2020-01-02 07:52:44', '2020-01-02 07:52:44'),
(170, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 107, 25, NULL, 0, 100, NULL, 200, 'delete', 'Delete stok produk', '2020-01-02 07:57:55', '2020-01-02 07:57:55'),
(171, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 109, 25, NULL, 0, 0, 0, 200, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 07:57:55', '2020-01-02 07:57:55'),
(172, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 108, 26, NULL, 0, 100, NULL, 200, 'delete', 'Delete stok produk', '2020-01-02 07:57:55', '2020-01-02 07:57:55'),
(173, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 110, 26, NULL, 0, 0, 0, 200, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 07:57:55', '2020-01-02 07:57:55'),
(174, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 111, 27, NULL, 0, 0, 10, 210, 'insert', 'Insert stok produk', '2020-01-02 07:57:56', '2020-01-02 07:57:56'),
(175, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 112, 26, NULL, 0, 0, 100, 310, 'insert', 'Insert stok produk', '2020-01-02 08:02:16', '2020-01-02 08:02:16'),
(176, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 113, 25, NULL, 0, 0, 100, 410, 'insert', 'Insert stok produk', '2020-01-02 08:02:32', '2020-01-02 08:02:32'),
(177, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 109, 25, NULL, 0, 20, 100, 410, 'delete', 'Delete stok produk', '2020-01-02 08:03:47', '2020-01-02 08:03:47'),
(178, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 113, 25, NULL, 0, 100, NULL, 410, 'delete', 'Delete stok produk', '2020-01-02 08:03:47', '2020-01-02 08:03:47'),
(179, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 114, 25, NULL, 0, 0, 0, 410, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 08:03:47', '2020-01-02 08:03:47'),
(180, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 110, 26, NULL, 0, 20, 100, 410, 'delete', 'Delete stok produk', '2020-01-02 08:03:47', '2020-01-02 08:03:47'),
(181, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 112, 26, NULL, 0, 100, NULL, 410, 'delete', 'Delete stok produk', '2020-01-02 08:03:48', '2020-01-02 08:03:48'),
(182, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 115, 26, NULL, 0, 0, 0, 410, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 08:03:48', '2020-01-02 08:03:48'),
(183, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 116, 27, NULL, 0, 0, 20, 420, 'insert', 'Insert stok produk', '2020-01-02 08:03:48', '2020-01-02 08:03:48'),
(184, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 117, 26, NULL, 0, 0, 100, 520, 'insert', 'Insert stok produk', '2020-01-02 08:24:04', '2020-01-02 08:24:04'),
(185, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 118, 25, NULL, 0, 0, 100, 620, 'insert', 'Insert stok produk', '2020-01-02 08:25:23', '2020-01-02 08:25:23'),
(186, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 114, 25, NULL, 0, 0, 100, 620, 'delete', 'Delete stok produk', '2020-01-02 08:29:15', '2020-01-02 08:29:15'),
(187, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 119, 25, NULL, -20, 0, 80, 600, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 08:29:15', '2020-01-02 08:29:15'),
(188, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 115, 26, NULL, 0, 0, 100, 600, 'delete', 'Delete stok produk', '2020-01-02 08:29:15', '2020-01-02 08:29:15'),
(189, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 120, 26, NULL, -20, 0, 80, 580, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 08:29:15', '2020-01-02 08:29:15'),
(190, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 121, 27, NULL, 0, 0, 30, 590, 'insert', 'Insert stok produk', '2020-01-02 08:29:15', '2020-01-02 08:29:15'),
(191, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 122, 28, NULL, 0, 0, 100, 100, 'insert', 'Insert stok produk', '2020-01-02 08:42:30', '2020-01-02 08:42:30'),
(192, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 123, 29, NULL, 0, 0, 100, 200, 'insert', 'Insert stok produk', '2020-01-02 08:42:52', '2020-01-02 08:42:52'),
(193, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 122, 28, NULL, 0, 100, NULL, 200, 'delete', 'Delete stok produk', '2020-01-02 08:44:47', '2020-01-02 08:44:47'),
(194, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 124, 28, NULL, 0, 0, 0, 200, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 08:44:47', '2020-01-02 08:44:47'),
(195, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 123, 29, NULL, 0, 100, NULL, 200, 'delete', 'Delete stok produk', '2020-01-02 08:44:47', '2020-01-02 08:44:47'),
(196, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 125, 29, NULL, 0, 0, 0, 200, 'insert', 'Insert stok produk sisa assembly', '2020-01-02 08:44:48', '2020-01-02 08:44:48'),
(197, '2020-01-02', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 126, 30, NULL, 0, 0, 10, 210, 'insert', 'Insert stok produk', '2020-01-02 08:44:48', '2020-01-02 08:44:48'),
(198, '2020-01-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 127, 32, NULL, 0, 0, 100, 100, 'insert', 'Insert stok produk', '2020-01-03 03:04:11', '2020-01-03 03:04:11'),
(199, '2020-01-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 128, 31, NULL, 0, 0, 100, 200, 'insert', 'Insert stok produk', '2020-01-03 03:04:29', '2020-01-03 03:04:29'),
(200, '2020-01-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 128, 31, NULL, 0, 20, NULL, 200, 'delete', 'Delete stok produk', '2020-01-03 03:06:38', '2020-01-03 03:06:38'),
(201, '2020-01-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 129, 31, NULL, 80, 0, 80, 280, 'insert', 'Insert stok produk sisa assembly', '2020-01-03 03:06:38', '2020-01-03 03:06:38'),
(202, '2020-01-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 127, 32, NULL, 0, 20, NULL, 280, 'delete', 'Delete stok produk', '2020-01-03 03:06:39', '2020-01-03 03:06:39'),
(203, '2020-01-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 130, 32, NULL, 80, 0, 80, 360, 'insert', 'Insert stok produk sisa assembly', '2020-01-03 03:06:39', '2020-01-03 03:06:39'),
(204, '2020-01-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 131, 33, NULL, 0, 0, 10, 370, 'insert', 'Insert stok produk', '2020-01-03 03:06:39', '2020-01-03 03:06:39'),
(205, '2020-01-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 129, 31, NULL, 0, 20, NULL, 370, 'delete', 'Delete stok produk', '2020-01-03 03:25:57', '2020-01-03 03:25:57'),
(206, '2020-01-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 132, 31, NULL, 60, 0, 60, 430, 'insert', 'Insert stok produk sisa assembly', '2020-01-03 03:25:58', '2020-01-03 03:25:58'),
(207, '2020-01-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 130, 32, NULL, 0, 20, NULL, 430, 'delete', 'Delete stok produk', '2020-01-03 03:25:58', '2020-01-03 03:25:58'),
(208, '2020-01-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 133, 32, NULL, 60, 0, 60, 490, 'insert', 'Insert stok produk sisa assembly', '2020-01-03 03:25:58', '2020-01-03 03:25:58'),
(209, '2020-01-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 134, 33, NULL, 0, 0, 20, 500, 'insert', 'Insert stok produk', '2020-01-03 03:25:58', '2020-01-03 03:25:58'),
(210, '2020-01-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 135, 33, NULL, 20, 0, 40, 520, 'insert', 'Insert stok produk dari produk assembly', '2020-01-03 03:41:26', '2020-01-03 03:41:26'),
(211, '2020-01-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 136, 33, NULL, 20, 0, 60, 540, 'insert', 'Insert stok produk dari produk assembly', '2020-01-03 03:41:26', '2020-01-03 03:41:26'),
(212, '2020-01-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 137, 33, NULL, 20, 0, 80, 560, 'insert', 'Insert stok produk dari produk assembly', '2020-01-03 03:41:26', '2020-01-03 03:41:26'),
(213, '2020-01-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 138, 33, NULL, 20, 0, 100, 580, 'insert', 'Insert stok produk dari produk assembly', '2020-01-03 03:41:26', '2020-01-03 03:41:26'),
(214, '2020-01-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 139, 31, NULL, 20, 0, 80, 600, 'insert', 'Insert stok produk dari produk assembly', '2020-01-03 06:23:23', '2020-01-03 06:23:23'),
(215, '2020-01-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 140, 31, NULL, 20, 0, 100, 620, 'insert', 'Insert stok produk dari produk assembly', '2020-01-03 06:23:23', '2020-01-03 06:23:23'),
(216, '2020-01-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 141, 31, NULL, 40, 0, 140, 660, 'insert', 'Insert stok produk dari produk assembly', '2020-01-03 06:23:23', '2020-01-03 06:23:23'),
(217, '2020-01-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 142, 31, NULL, 40, 0, 180, 700, 'insert', 'Insert stok produk dari produk assembly', '2020-01-03 06:23:23', '2020-01-03 06:23:23'),
(218, '2020-01-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 143, 31, NULL, 40, 0, 220, 740, 'insert', 'Insert stok produk dari produk assembly', '2020-01-03 06:23:23', '2020-01-03 06:23:23'),
(219, '2020-01-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 144, 31, NULL, 40, 0, 260, 780, 'insert', 'Insert stok produk dari produk assembly', '2020-01-03 06:23:23', '2020-01-03 06:23:23'),
(220, '2020-01-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 145, 34, NULL, 100, 0, 100, 100, 'insert', 'Insert stok produk', '2020-01-03 06:43:29', '2020-01-03 06:43:29'),
(221, '2020-01-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 146, 35, NULL, 100, 0, 100, 200, 'insert', 'Insert stok produk', '2020-01-03 06:43:50', '2020-01-03 06:43:50'),
(222, '2020-01-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 145, 34, NULL, 0, 20, NULL, 200, 'delete', 'Delete stok produk', '2020-01-03 06:44:53', '2020-01-03 06:44:53'),
(223, '2020-01-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 147, 34, NULL, 80, 0, 80, 280, 'insert', 'Insert stok produk sisa assembly', '2020-01-03 06:44:53', '2020-01-03 06:44:53'),
(224, '2020-01-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 146, 35, NULL, 0, 20, NULL, 280, 'delete', 'Delete stok produk', '2020-01-03 06:44:53', '2020-01-03 06:44:53'),
(225, '2020-01-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 148, 35, NULL, 80, 0, 80, 360, 'insert', 'Insert stok produk sisa assembly', '2020-01-03 06:44:53', '2020-01-03 06:44:53'),
(226, '2020-01-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 149, 36, NULL, 0, 0, 10, 370, 'insert', 'Insert stok produk', '2020-01-03 06:44:53', '2020-01-03 06:44:53'),
(227, '2020-01-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 150, 34, NULL, 20, 0, 100, 390, 'insert', 'Insert stok produk dari produk assembly', '2020-01-03 06:53:36', '2020-01-03 06:53:36'),
(228, '2020-01-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 151, 35, NULL, 20, 0, 100, 410, 'insert', 'Insert stok produk dari produk assembly', '2020-01-03 06:53:36', '2020-01-03 06:53:36'),
(229, '2020-01-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 149, 36, NULL, 0, NULL, NULL, 420, 'delete', 'Delete stok produk', '2020-01-03 06:53:36', '2020-01-03 06:53:36'),
(230, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 147, 34, NULL, 0, 10, 20, 420, 'delete', 'Delete stok produk', '2020-01-06 05:30:02', '2020-01-06 05:30:02'),
(231, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 152, 34, NULL, 70, 0, 90, 490, 'insert', 'Insert stok produk sisa assembly', '2020-01-06 05:30:02', '2020-01-06 05:30:02'),
(232, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 153, 36, NULL, 0, 0, 10, 500, 'insert', 'Insert stok produk', '2020-01-06 05:30:02', '2020-01-06 05:30:02'),
(233, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 154, 34, NULL, 10, 0, 100, 510, 'insert', 'Insert stok produk dari produk assembly', '2020-01-06 05:30:31', '2020-01-06 05:30:31'),
(234, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 153, 36, NULL, 0, 10, NULL, 510, 'delete', 'Delete stok produk', '2020-01-06 05:30:31', '2020-01-06 05:30:31'),
(235, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 150, 34, NULL, 0, 20, 80, 510, 'delete', 'Delete stok produk', '2020-01-06 05:47:53', '2020-01-06 05:47:53'),
(236, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 155, 34, NULL, 0, 0, 80, 510, 'insert', 'Insert stok produk sisa assembly', '2020-01-06 05:47:54', '2020-01-06 05:47:54'),
(237, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 148, 35, NULL, 0, 20, 20, 510, 'delete', 'Delete stok produk', '2020-01-06 05:47:54', '2020-01-06 05:47:54'),
(238, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 156, 35, NULL, 60, 0, 80, 570, 'insert', 'Insert stok produk sisa assembly', '2020-01-06 05:47:54', '2020-01-06 05:47:54'),
(239, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 157, 36, NULL, 0, 0, 20, 590, 'insert', 'Insert stok produk', '2020-01-06 05:47:54', '2020-01-06 05:47:54'),
(240, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 158, 34, NULL, 20, 0, 100, 610, 'insert', 'Insert stok produk dari produk assembly', '2020-01-06 05:48:10', '2020-01-06 05:48:10'),
(241, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 159, 35, NULL, 20, 0, 100, 630, 'insert', 'Insert stok produk dari produk assembly', '2020-01-06 05:48:10', '2020-01-06 05:48:10'),
(242, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 157, 36, NULL, 0, 20, NULL, 630, 'delete', 'Delete stok produk', '2020-01-06 05:48:10', '2020-01-06 05:48:10'),
(243, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 152, 34, NULL, 0, 10, 30, 630, 'delete', 'Delete stok produk', '2020-01-06 06:02:18', '2020-01-06 06:02:18'),
(244, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 160, 34, NULL, 60, 0, 90, 690, 'insert', 'Insert stok produk sisa assembly', '2020-01-06 06:02:18', '2020-01-06 06:02:18'),
(245, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 151, 35, NULL, 0, 10, 80, 690, 'delete', 'Delete stok produk', '2020-01-06 06:02:18', '2020-01-06 06:02:18'),
(246, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 161, 35, NULL, 10, 0, 90, 700, 'insert', 'Insert stok produk sisa assembly', '2020-01-06 06:02:19', '2020-01-06 06:02:19'),
(247, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 162, 36, NULL, 0, 0, 10, 710, 'insert', 'Insert stok produk', '2020-01-06 06:02:19', '2020-01-06 06:02:19'),
(248, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 163, 34, NULL, 10, 0, 100, 720, 'insert', 'Insert stok produk dari produk assembly', '2020-01-06 06:09:43', '2020-01-06 06:09:43'),
(249, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 164, 35, NULL, 10, 0, 100, 730, 'insert', 'Insert stok produk dari produk assembly', '2020-01-06 06:09:43', '2020-01-06 06:09:43'),
(250, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 162, 36, NULL, 0, 10, NULL, 730, 'delete', 'Delete stok produk', '2020-01-06 06:09:43', '2020-01-06 06:09:43'),
(251, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 154, 34, NULL, 0, 10, 90, 730, 'delete', 'Delete stok produk', '2020-01-06 06:42:48', '2020-01-06 06:42:48'),
(252, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 155, 34, NULL, 0, 0, 90, 730, 'delete', 'Delete stok produk', '2020-01-06 06:42:48', '2020-01-06 06:42:48'),
(253, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 158, 34, NULL, 0, 20, 70, 730, 'delete', 'Delete stok produk', '2020-01-06 06:42:49', '2020-01-06 06:42:49'),
(254, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 160, 34, NULL, 0, 10, 10, 730, 'delete', 'Delete stok produk', '2020-01-06 06:42:49', '2020-01-06 06:42:49'),
(255, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 165, 34, NULL, 50, 0, 60, 780, 'insert', 'Insert stok produk sisa assembly', '2020-01-06 06:42:49', '2020-01-06 06:42:49'),
(256, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 156, 35, NULL, 0, 40, 40, 780, 'delete', 'Delete stok produk', '2020-01-06 06:42:49', '2020-01-06 06:42:49'),
(257, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 166, 35, NULL, 20, 0, 60, 800, 'insert', 'Insert stok produk sisa assembly', '2020-01-06 06:42:49', '2020-01-06 06:42:49'),
(258, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 167, 37, NULL, 0, 0, 20, 820, 'insert', 'Insert stok produk', '2020-01-06 06:42:49', '2020-01-06 06:42:49'),
(259, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 168, 34, NULL, 40, 0, 100, 860, 'insert', 'Insert stok produk dari produk assembly', '2020-01-06 06:43:47', '2020-01-06 06:43:47'),
(260, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 169, 35, NULL, 40, 0, 100, 900, 'insert', 'Insert stok produk dari produk assembly', '2020-01-06 06:43:47', '2020-01-06 06:43:47'),
(261, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 167, 37, NULL, 0, 20, NULL, 920, 'delete', 'Delete stok produk', '2020-01-06 06:43:47', '2020-01-06 06:43:47'),
(262, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 163, 34, NULL, 0, 10, 90, 920, 'delete', 'Delete stok produk', '2020-01-06 06:47:39', '2020-01-06 06:47:39'),
(263, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 165, 34, NULL, 0, 10, 40, 920, 'delete', 'Delete stok produk', '2020-01-06 06:47:39', '2020-01-06 06:47:39'),
(264, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 170, 34, NULL, 40, 0, 80, 960, 'insert', 'Insert stok produk sisa assembly', '2020-01-06 06:47:39', '2020-01-06 06:47:39'),
(265, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 159, 35, NULL, 0, 20, 80, 960, 'delete', 'Delete stok produk', '2020-01-06 06:47:39', '2020-01-06 06:47:39'),
(266, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 171, 35, NULL, 0, 0, 80, 960, 'insert', 'Insert stok produk sisa assembly', '2020-01-06 06:47:39', '2020-01-06 06:47:39'),
(267, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 172, 38, NULL, 0, 0, 10, 970, 'insert', 'Insert stok produk', '2020-01-06 06:47:39', '2020-01-06 06:47:39'),
(268, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 173, 34, NULL, 20, 0, 100, 990, 'insert', 'Insert stok produk dari produk assembly', '2020-01-06 06:48:14', '2020-01-06 06:48:14');
INSERT INTO `arus_stock_produk` (`id`, `tanggal`, `table_name`, `penjualan_id`, `penjualan_produk_id`, `distribusi_id`, `distribusi_detail_id`, `produksi_id`, `progress_produksi_id`, `stock_produk_id`, `produk_id`, `history_penyesuaian_produk_id`, `stock_in`, `stock_out`, `last_stock`, `last_stock_total`, `method`, `keterangan`, `created_at`, `updated_at`) VALUES
(269, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 174, 35, NULL, 20, 0, 100, 1010, 'insert', 'Insert stok produk dari produk assembly', '2020-01-06 06:48:15', '2020-01-06 06:48:15'),
(270, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 172, 38, NULL, 0, 10, NULL, 1020, 'delete', 'Delete stok produk', '2020-01-06 06:48:15', '2020-01-06 06:48:15'),
(271, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 168, 34, NULL, 0, 20, 60, 1020, 'delete', 'Delete stok produk', '2020-01-06 06:56:23', '2020-01-06 06:56:23'),
(272, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 175, 34, NULL, 20, 0, 80, 1040, 'insert', 'Insert stok produk sisa assembly', '2020-01-06 06:56:23', '2020-01-06 06:56:23'),
(273, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 161, 35, NULL, 0, 10, 90, 1040, 'delete', 'Delete stok produk', '2020-01-06 06:56:23', '2020-01-06 06:56:23'),
(274, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 164, 35, NULL, 0, 10, 80, 1040, 'delete', 'Delete stok produk', '2020-01-06 06:56:23', '2020-01-06 06:56:23'),
(275, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 176, 35, NULL, 0, 0, 80, 1040, 'insert', 'Insert stok produk sisa assembly', '2020-01-06 06:56:23', '2020-01-06 06:56:23'),
(276, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 177, 39, NULL, 0, 0, 10, 1050, 'insert', 'Insert stok produk', '2020-01-06 06:56:23', '2020-01-06 06:56:23'),
(277, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 178, 34, NULL, 20, 0, 100, 1070, 'insert', 'Insert stok produk dari produk assembly', '2020-01-06 06:58:06', '2020-01-06 06:58:06'),
(278, '2020-01-06', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 179, 35, NULL, 20, 0, 100, 1090, 'insert', 'Insert stok produk dari produk assembly', '2020-01-06 06:58:06', '2020-01-06 06:58:06'),
(279, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 170, 34, NULL, 0, 40, 60, 1080, 'delete', 'Delete stok produk', '2020-01-07 02:03:49', '2020-01-07 02:03:49'),
(280, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 173, 34, NULL, 0, 20, 40, 1080, 'delete', 'Delete stok produk', '2020-01-07 02:03:49', '2020-01-07 02:03:49'),
(281, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 175, 34, NULL, 0, 20, 20, 1080, 'delete', 'Delete stok produk', '2020-01-07 02:03:49', '2020-01-07 02:03:49'),
(282, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 178, 34, NULL, 0, 20, NULL, 1080, 'delete', 'Delete stok produk', '2020-01-07 02:03:49', '2020-01-07 02:03:49'),
(283, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 180, 34, NULL, 0, 0, 0, 1080, 'insert', 'Insert stok produk sisa assembly', '2020-01-07 02:03:49', '2020-01-07 02:03:49'),
(284, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 166, 35, NULL, 0, 20, 80, 1080, 'delete', 'Delete stok produk', '2020-01-07 02:03:49', '2020-01-07 02:03:49'),
(285, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 169, 35, NULL, 0, 40, 40, 1080, 'delete', 'Delete stok produk', '2020-01-07 02:03:49', '2020-01-07 02:03:49'),
(286, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 171, 35, NULL, 0, 0, 40, 1080, 'delete', 'Delete stok produk', '2020-01-07 02:03:50', '2020-01-07 02:03:50'),
(287, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 174, 35, NULL, 0, 20, 20, 1080, 'delete', 'Delete stok produk', '2020-01-07 02:03:50', '2020-01-07 02:03:50'),
(288, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 176, 35, NULL, 0, 0, 20, 1080, 'delete', 'Delete stok produk', '2020-01-07 02:03:50', '2020-01-07 02:03:50'),
(289, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 179, 35, NULL, 0, 20, NULL, 1080, 'delete', 'Delete stok produk', '2020-01-07 02:03:50', '2020-01-07 02:03:50'),
(290, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 181, 35, NULL, 0, 0, 0, 1080, 'insert', 'Insert stok produk sisa assembly', '2020-01-07 02:03:50', '2020-01-07 02:03:50'),
(291, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 182, 40, NULL, 0, 0, 13, 1093, 'insert', 'Insert stok produk', '2020-01-07 02:03:50', '2020-01-07 02:03:50'),
(292, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 183, 34, NULL, 130, 0, 130, 1223, 'insert', 'Insert stok produk dari produk assembly', '2020-01-07 02:05:21', '2020-01-07 02:05:21'),
(293, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 184, 35, NULL, 130, 0, 130, 1353, 'insert', 'Insert stok produk dari produk assembly', '2020-01-07 02:05:21', '2020-01-07 02:05:21'),
(294, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 185, 41, NULL, 100, 0, 100, 100, 'insert', 'Insert stok produk', '2020-01-07 02:08:25', '2020-01-07 02:08:25'),
(295, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 186, 42, NULL, 100, 0, 100, 200, 'insert', 'Insert stok produk', '2020-01-07 02:08:47', '2020-01-07 02:08:47'),
(296, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 185, 41, NULL, 0, 100, NULL, 200, 'delete', 'Delete stok produk', '2020-01-07 02:09:41', '2020-01-07 02:09:41'),
(297, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 187, 41, NULL, 0, 0, 0, 200, 'insert', 'Insert stok produk sisa assembly', '2020-01-07 02:09:41', '2020-01-07 02:09:41'),
(298, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 186, 42, NULL, 0, 100, NULL, 200, 'delete', 'Delete stok produk', '2020-01-07 02:09:41', '2020-01-07 02:09:41'),
(299, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 188, 42, NULL, 0, 0, 0, 200, 'insert', 'Insert stok produk sisa assembly', '2020-01-07 02:09:41', '2020-01-07 02:09:41'),
(300, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 189, 43, NULL, 0, 0, 12, 212, 'insert', 'Insert stok produk', '2020-01-07 02:09:42', '2020-01-07 02:09:42'),
(301, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 190, 41, NULL, 120, 0, 120, 332, 'insert', 'Insert stok produk dari produk assembly', '2020-01-07 02:11:41', '2020-01-07 02:11:41'),
(302, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 191, 42, NULL, 120, 0, 120, 452, 'insert', 'Insert stok produk dari produk assembly', '2020-01-07 02:11:41', '2020-01-07 02:11:41'),
(303, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 187, 41, NULL, 0, 0, 120, 440, 'delete', 'Delete stok produk', '2020-01-07 03:01:09', '2020-01-07 03:01:09'),
(304, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 190, 41, NULL, 0, 50, NULL, 440, 'delete', 'Delete stok produk', '2020-01-07 03:01:10', '2020-01-07 03:01:10'),
(305, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 192, 41, NULL, 70, 0, 70, 510, 'insert', 'Insert stok produk sisa assembly', '2020-01-07 03:01:10', '2020-01-07 03:01:10'),
(306, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 188, 42, NULL, 0, 0, 120, 510, 'delete', 'Delete stok produk', '2020-01-07 03:01:10', '2020-01-07 03:01:10'),
(307, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 191, 42, NULL, 0, 50, NULL, 510, 'delete', 'Delete stok produk', '2020-01-07 03:01:10', '2020-01-07 03:01:10'),
(308, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 193, 42, NULL, 70, 0, 70, 580, 'insert', 'Insert stok produk sisa assembly', '2020-01-07 03:01:10', '2020-01-07 03:01:10'),
(309, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 194, 44, NULL, 0, 0, 5, 585, 'insert', 'Insert stok produk', '2020-01-07 03:01:10', '2020-01-07 03:01:10'),
(310, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 194, 44, NULL, 0, NULL, NULL, 585, 'delete', 'Delete stok produk', '2020-01-07 03:42:19', '2020-01-07 03:42:19'),
(311, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 192, 41, NULL, 0, 30, NULL, 585, 'delete', 'Delete stok produk', '2020-01-07 03:51:38', '2020-01-07 03:51:38'),
(312, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 195, 41, NULL, 40, 0, 40, 625, 'insert', 'Insert stok produk sisa assembly', '2020-01-07 03:51:38', '2020-01-07 03:51:38'),
(313, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 193, 42, NULL, 0, 30, NULL, 625, 'delete', 'Delete stok produk', '2020-01-07 03:51:38', '2020-01-07 03:51:38'),
(314, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 196, 42, NULL, 40, 0, 40, 665, 'insert', 'Insert stok produk sisa assembly', '2020-01-07 03:51:38', '2020-01-07 03:51:38'),
(315, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 197, 44, NULL, 0, 0, 3, 668, 'insert', 'Insert stok produk', '2020-01-07 03:51:38', '2020-01-07 03:51:38'),
(316, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 198, 41, NULL, 30, 0, 70, 698, 'insert', 'Insert stok produk dari produk assembly', '2020-01-07 03:51:55', '2020-01-07 03:51:55'),
(317, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 199, 42, NULL, 30, 0, 70, 728, 'insert', 'Insert stok produk dari produk assembly', '2020-01-07 03:51:55', '2020-01-07 03:51:55'),
(318, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 195, 41, NULL, 0, 10, 30, 720, 'delete', 'Delete stok produk', '2020-01-07 06:00:25', '2020-01-07 06:00:25'),
(319, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 200, 41, NULL, 30, 0, 60, 750, 'insert', 'Insert stok produk sisa assembly', '2020-01-07 06:00:25', '2020-01-07 06:00:25'),
(320, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 196, 42, NULL, 0, 10, 30, 750, 'delete', 'Delete stok produk', '2020-01-07 06:00:25', '2020-01-07 06:00:25'),
(321, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 201, 42, NULL, 30, 0, 60, 780, 'insert', 'Insert stok produk sisa assembly', '2020-01-07 06:00:25', '2020-01-07 06:00:25'),
(322, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 202, 45, NULL, 0, 0, 1, 781, 'insert', 'Insert stok produk', '2020-01-07 06:00:25', '2020-01-07 06:00:25'),
(323, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 198, 41, NULL, 0, 10, 30, 781, 'delete', 'Delete stok produk', '2020-01-07 06:00:49', '2020-01-07 06:00:49'),
(324, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 203, 41, NULL, 20, 0, 50, 801, 'insert', 'Insert stok produk sisa assembly', '2020-01-07 06:00:49', '2020-01-07 06:00:49'),
(325, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 199, 42, NULL, 0, 10, 30, 801, 'delete', 'Delete stok produk', '2020-01-07 06:00:49', '2020-01-07 06:00:49'),
(326, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 204, 42, NULL, 20, 0, 50, 821, 'insert', 'Insert stok produk sisa assembly', '2020-01-07 06:00:49', '2020-01-07 06:00:49'),
(327, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 205, 45, NULL, 0, 0, 2, 822, 'insert', 'Insert stok produk', '2020-01-07 06:00:50', '2020-01-07 06:00:50'),
(328, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 200, 41, NULL, 0, 0, 20, 822, 'delete', 'Delete stok produk', '2020-01-07 06:39:03', '2020-01-07 06:39:03'),
(329, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 206, 41, NULL, 30, 0, 50, 852, 'insert', 'Insert stok produk sisa assembly', '2020-01-07 06:39:03', '2020-01-07 06:39:03'),
(330, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 201, 42, NULL, 0, 0, 20, 852, 'delete', 'Delete stok produk', '2020-01-07 06:39:03', '2020-01-07 06:39:03'),
(331, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 207, 42, NULL, 30, 0, 50, 882, 'insert', 'Insert stok produk sisa assembly', '2020-01-07 06:39:03', '2020-01-07 06:39:03'),
(332, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 208, 45, NULL, 0, 0, 2, 882, 'insert', 'Insert stok produk', '2020-01-07 06:39:03', '2020-01-07 06:39:03'),
(333, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 204, 42, NULL, 0, NULL, 30, 882, 'delete', 'Delete stok produk', '2020-01-07 06:41:29', '2020-01-07 06:41:29'),
(334, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 203, 41, NULL, 0, 20, 30, 882, 'delete', 'Delete stok produk', '2020-01-07 06:42:12', '2020-01-07 06:42:12'),
(335, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 205, 45, NULL, 0, NULL, 1, 882, 'delete', 'Delete stok produk', '2020-01-07 06:44:41', '2020-01-07 06:44:41'),
(336, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 202, 45, NULL, 0, NULL, 0, 882, 'delete', 'Delete stok produk', '2020-01-07 06:46:07', '2020-01-07 06:46:07'),
(337, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 206, 41, NULL, 0, 10, NULL, 882, 'delete', 'Delete stok produk', '2020-01-07 06:47:58', '2020-01-07 06:47:58'),
(338, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 209, 41, NULL, 20, 0, 20, 902, 'insert', 'Insert stok produk sisa assembly', '2020-01-07 06:47:58', '2020-01-07 06:47:58'),
(339, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 207, 42, NULL, 0, 10, NULL, 902, 'delete', 'Delete stok produk', '2020-01-07 06:47:58', '2020-01-07 06:47:58'),
(340, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 210, 42, NULL, 20, 0, 20, 922, 'insert', 'Insert stok produk sisa assembly', '2020-01-07 06:47:58', '2020-01-07 06:47:58'),
(341, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 211, 45, NULL, 0, 0, 1, 923, 'insert', 'Insert stok produk', '2020-01-07 06:47:59', '2020-01-07 06:47:59'),
(342, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 211, 45, NULL, 0, 1, 0, 923, 'delete', 'Delete stok produk', '2020-01-07 06:48:15', '2020-01-07 06:48:15'),
(343, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 209, 41, NULL, 0, 10, NULL, 923, 'delete', 'Delete stok produk', '2020-01-07 06:50:23', '2020-01-07 06:50:23'),
(344, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 212, 41, NULL, 10, 0, 10, 933, 'insert', 'Insert stok produk sisa assembly', '2020-01-07 06:50:23', '2020-01-07 06:50:23'),
(345, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 210, 42, NULL, 0, 10, NULL, 933, 'delete', 'Delete stok produk', '2020-01-07 06:50:23', '2020-01-07 06:50:23'),
(346, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 213, 42, NULL, 10, 0, 10, 943, 'insert', 'Insert stok produk sisa assembly', '2020-01-07 06:50:23', '2020-01-07 06:50:23'),
(347, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 214, 45, NULL, 0, 0, 1, 944, 'insert', 'Insert stok produk', '2020-01-07 06:50:23', '2020-01-07 06:50:23'),
(348, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 215, 41, NULL, 10, 0, 20, 954, 'insert', 'Insert stok produk dari produk assembly', '2020-01-07 06:52:25', '2020-01-07 06:52:25'),
(349, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 216, 42, NULL, 10, 0, 20, 964, 'insert', 'Insert stok produk dari produk assembly', '2020-01-07 06:52:25', '2020-01-07 06:52:25'),
(350, '2020-01-07', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 214, 45, NULL, 0, 1, 0, 973, 'delete', 'Delete stok produk', '2020-01-07 06:52:25', '2020-01-07 06:52:25'),
(351, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 213, 42, NULL, 0, 2, 18, 971, 'delete', 'Penjualan produk', '2020-01-08 02:00:02', '2020-01-08 02:00:02'),
(352, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 217, 45, NULL, 0, 0, 2, 973, 'insert', 'Insert stok produk', '2020-01-08 07:01:28', '2020-01-08 07:01:28'),
(353, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 218, 41, NULL, 20, 0, 40, 993, 'insert', 'Insert stok produk dari produk assembly', '2020-01-08 07:04:32', '2020-01-08 07:04:32'),
(354, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 219, 42, NULL, 20, 0, 38, 1013, 'insert', 'Insert stok produk dari produk assembly', '2020-01-08 07:04:32', '2020-01-08 07:04:32'),
(355, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 217, 45, NULL, 0, 2, 0, 1031, 'delete', 'Delete stok produk', '2020-01-08 07:04:32', '2020-01-08 07:04:32'),
(356, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 220, 41, NULL, 0, 0, 40, 1031, 'insert', 'Insert stok produk sisa assembly', '2020-01-08 07:04:53', '2020-01-08 07:04:53'),
(357, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 221, 42, NULL, 0, 0, 38, 1031, 'insert', 'Insert stok produk sisa assembly', '2020-01-08 07:04:53', '2020-01-08 07:04:53'),
(358, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 222, 45, NULL, 0, 0, 0, 1031, 'insert', 'Insert stok produk', '2020-01-08 07:04:53', '2020-01-08 07:04:53'),
(359, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 223, 45, NULL, 0, 0, 4, 1035, 'insert', 'Insert stok produk', '2020-01-08 07:13:42', '2020-01-08 07:13:42'),
(360, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 212, 41, NULL, 0, 10, 30, 1035, 'delete', 'Delete stok produk', '2020-01-08 07:19:11', '2020-01-08 07:19:11'),
(361, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 224, 41, NULL, 0, 0, 30, 1035, 'insert', 'Insert stok produk sisa assembly', '2020-01-08 07:19:11', '2020-01-08 07:19:11'),
(362, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 213, 42, NULL, 0, 8, 30, 1035, 'delete', 'Delete stok produk', '2020-01-08 07:19:12', '2020-01-08 07:19:12'),
(363, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 216, 42, NULL, 0, 2, 20, 1035, 'delete', 'Delete stok produk', '2020-01-08 07:19:12', '2020-01-08 07:19:12'),
(364, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 225, 42, NULL, 8, 0, 28, 1043, 'insert', 'Insert stok produk sisa assembly', '2020-01-08 07:19:12', '2020-01-08 07:19:12'),
(365, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 215, 41, NULL, 0, 10, 20, 1043, 'delete', 'Delete stok produk', '2020-01-08 07:20:19', '2020-01-08 07:20:19'),
(366, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 218, 41, NULL, 0, 10, 0, 1043, 'delete', 'Delete stok produk', '2020-01-08 07:20:19', '2020-01-08 07:20:19'),
(367, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 226, 41, NULL, 10, 0, 10, 1053, 'insert', 'Insert stok produk sisa assembly', '2020-01-08 07:20:19', '2020-01-08 07:20:19'),
(368, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 227, 41, NULL, 40, 0, 50, 1093, 'insert', 'Insert stok produk dari produk assembly', '2020-01-08 07:26:19', '2020-01-08 07:26:19'),
(369, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 228, 42, NULL, 40, 0, 68, 1133, 'insert', 'Insert stok produk dari produk assembly', '2020-01-08 07:26:19', '2020-01-08 07:26:19'),
(370, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 229, 46, NULL, 100, 0, 100, 100, 'insert', 'Insert stok produk', '2020-01-08 07:28:39', '2020-01-08 07:28:39'),
(371, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 230, 47, NULL, 100, 0, 100, 200, 'insert', 'Insert stok produk', '2020-01-08 07:28:56', '2020-01-08 07:28:56'),
(372, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 230, 47, NULL, 0, 100, NULL, 200, 'delete', 'Delete stok produk', '2020-01-08 07:29:03', '2020-01-08 07:29:03'),
(373, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 231, 47, NULL, 80, 0, 80, 280, 'insert', 'Insert stok produk', '2020-01-08 07:29:12', '2020-01-08 07:29:12'),
(374, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 229, 46, NULL, 0, 80, NULL, 280, 'delete', 'Delete stok produk', '2020-01-08 07:30:05', '2020-01-08 07:30:05'),
(375, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 232, 46, NULL, 20, 0, 20, 300, 'insert', 'Insert stok produk sisa assembly', '2020-01-08 07:30:05', '2020-01-08 07:30:05'),
(376, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 233, 50, NULL, 100, 0, 100, 100, 'insert', 'Insert stok produk', '2020-01-08 07:39:27', '2020-01-08 07:39:27'),
(377, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 234, 49, NULL, 100, 0, 100, 200, 'insert', 'Insert stok produk', '2020-01-08 07:39:44', '2020-01-08 07:39:44'),
(378, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 234, 49, NULL, 0, 100, NULL, 200, 'delete', 'Delete stok produk', '2020-01-08 07:40:13', '2020-01-08 07:40:13'),
(379, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 235, 49, NULL, 0, 0, 0, 200, 'insert', 'Insert stok produk sisa assembly', '2020-01-08 07:40:13', '2020-01-08 07:40:13'),
(380, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 236, 49, NULL, 100, 0, 100, 300, 'insert', 'Insert stok produk', '2020-01-08 07:45:23', '2020-01-08 07:45:23'),
(381, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 233, 50, NULL, 0, 100, NULL, 300, 'delete', 'Delete stok produk', '2020-01-08 08:20:16', '2020-01-08 08:20:16'),
(382, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 237, 50, NULL, 80, 0, 80, 380, 'insert', 'Insert stok produk', '2020-01-08 08:20:26', '2020-01-08 08:20:26'),
(383, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 235, 49, NULL, 0, 0, 100, 380, 'delete', 'Delete stok produk', '2020-01-08 08:27:35', '2020-01-08 08:27:35'),
(384, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 236, 49, NULL, 0, 80, NULL, 380, 'delete', 'Delete stok produk', '2020-01-08 08:27:35', '2020-01-08 08:27:35'),
(385, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 238, 49, NULL, 20, 0, 20, 400, 'insert', 'Insert stok produk sisa assembly', '2020-01-08 08:27:35', '2020-01-08 08:27:35'),
(386, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 237, 50, NULL, 0, 80, NULL, 400, 'delete', 'Delete stok produk', '2020-01-08 08:27:35', '2020-01-08 08:27:35'),
(387, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 239, 50, NULL, 0, 0, 0, 400, 'insert', 'Insert stok produk sisa assembly', '2020-01-08 08:27:35', '2020-01-08 08:27:35'),
(388, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 240, 51, NULL, 0, 0, 40, 440, 'insert', 'Insert stok produk', '2020-01-08 08:27:35', '2020-01-08 08:27:35'),
(389, '2020-01-08', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 241, 50, NULL, 50, 0, 50, 490, 'insert', 'Insert stok produk', '2020-01-08 08:30:11', '2020-01-08 08:30:11'),
(390, '2020-01-10', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 240, 51, NULL, 0, 1, 39, 489, 'delete', 'Penjualan produk', '2020-01-10 02:15:04', '2020-01-10 02:15:04'),
(391, '2020-01-13', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 238, 49, NULL, 0, 5, NULL, 489, 'delete', 'Delete stok produk', '2020-01-13 05:50:11', '2020-01-13 05:50:11'),
(392, '2020-01-13', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 242, 49, NULL, 15, 0, 15, 504, 'insert', 'Insert stok produk sisa assembly', '2020-01-13 05:50:12', '2020-01-13 05:50:12'),
(393, '2020-01-13', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 243, 52, NULL, 0, 0, 1, 505, 'insert', 'Insert stok produk', '2020-01-13 05:50:12', '2020-01-13 05:50:12'),
(394, '2020-01-13', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 244, 49, NULL, 5, 0, 20, 510, 'insert', 'Insert stok produk dari produk assembly', '2020-01-13 05:53:41', '2020-01-13 05:53:41'),
(395, '2020-01-13', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 239, 50, NULL, 0, 0, 50, 509, 'delete', 'Delete stok produk', '2020-01-13 06:07:14', '2020-01-13 06:07:14'),
(396, '2020-01-13', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 241, 50, NULL, 0, 2, NULL, 509, 'delete', 'Delete stok produk', '2020-01-13 06:07:14', '2020-01-13 06:07:14'),
(397, '2020-01-13', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 245, 50, NULL, 48, 0, 48, 557, 'insert', 'Insert stok produk sisa assembly', '2020-01-13 06:07:14', '2020-01-13 06:07:14'),
(398, '2020-01-13', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 246, 53, NULL, 0, 0, 1, 558, 'insert', 'Insert stok produk', '2020-01-13 06:07:14', '2020-01-13 06:07:14'),
(399, '2020-01-13', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 247, 50, NULL, 2, 0, 50, 560, 'insert', 'Insert stok produk dari produk assembly', '2020-01-13 06:07:33', '2020-01-13 06:07:33'),
(400, '2020-01-13', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 246, 53, NULL, 0, 1, NULL, 561, 'delete', 'Delete stok produk', '2020-01-13 06:07:33', '2020-01-13 06:07:33'),
(401, '2020-01-13', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 244, 49, NULL, 0, 5, 15, 556, 'delete', 'Penjualan produk', '2020-01-13 06:43:54', '2020-01-13 06:43:54'),
(402, '2020-01-13', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 247, 50, NULL, 0, 2, 48, 554, 'delete', 'Penjualan produk', '2020-01-13 06:55:04', '2020-01-13 06:55:04'),
(403, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 248, 54, NULL, 1000000, 0, 1000000, 1000000, 'insert', 'Insert stok produk', '2020-01-14 05:18:41', '2020-01-14 05:18:41'),
(404, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 249, 55, NULL, 1000000, 0, 1000000, 2000000, 'insert', 'Insert stok produk', '2020-01-14 05:20:28', '2020-01-14 05:20:28'),
(405, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 248, 54, NULL, 0, 500000, NULL, 2000000, 'delete', 'Delete stok produk', '2020-01-14 05:23:24', '2020-01-14 05:23:24'),
(406, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 250, 54, NULL, 500000, 0, 500000, 2500000, 'insert', 'Insert stok produk sisa assembly', '2020-01-14 05:23:24', '2020-01-14 05:23:24'),
(407, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 249, 55, NULL, 0, 500000, NULL, 2500000, 'delete', 'Delete stok produk', '2020-01-14 05:23:24', '2020-01-14 05:23:24'),
(408, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 251, 55, NULL, 500000, 0, 500000, 3000000, 'insert', 'Insert stok produk sisa assembly', '2020-01-14 05:23:24', '2020-01-14 05:23:24'),
(409, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 252, 56, NULL, 0, 0, 100000, 3100000, 'insert', 'Insert stok produk', '2020-01-14 05:23:24', '2020-01-14 05:23:24'),
(410, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 253, 54, NULL, 500000, 0, 1000000, 3600000, 'insert', 'Insert stok produk dari produk assembly', '2020-01-14 06:10:38', '2020-01-14 06:10:38'),
(411, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 254, 55, NULL, 500000, 0, 1000000, 4100000, 'insert', 'Insert stok produk dari produk assembly', '2020-01-14 06:10:38', '2020-01-14 06:10:38'),
(412, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 250, 54, NULL, 0, 1000, 500000, 4000000, 'delete', 'Delete stok produk', '2020-01-14 07:10:22', '2020-01-14 07:10:22'),
(413, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 255, 54, NULL, 499000, 0, 999000, 4499000, 'insert', 'Insert stok produk sisa assembly', '2020-01-14 07:10:22', '2020-01-14 07:10:22'),
(414, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 251, 55, NULL, 0, 1000, 500000, 4499000, 'delete', 'Delete stok produk', '2020-01-14 07:10:22', '2020-01-14 07:10:22'),
(415, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 256, 55, NULL, 499000, 0, 999000, 4998000, 'insert', 'Insert stok produk sisa assembly', '2020-01-14 07:10:22', '2020-01-14 07:10:22'),
(416, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 257, 57, NULL, 0, 0, 1000, 4999000, 'insert', 'Insert stok produk', '2020-01-14 07:10:22', '2020-01-14 07:10:22'),
(417, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 253, 54, NULL, 0, 100, 499000, 4999000, 'delete', 'Delete stok produk', '2020-01-14 07:12:02', '2020-01-14 07:12:02'),
(418, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 258, 54, NULL, 499900, 0, 998900, 5498900, 'insert', 'Insert stok produk sisa assembly', '2020-01-14 07:12:02', '2020-01-14 07:12:02'),
(419, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 254, 55, NULL, 0, 100, 499000, 5498900, 'delete', 'Delete stok produk', '2020-01-14 07:12:02', '2020-01-14 07:12:02'),
(420, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 259, 55, NULL, 499900, 0, 998900, 5998800, 'insert', 'Insert stok produk sisa assembly', '2020-01-14 07:12:02', '2020-01-14 07:12:02'),
(421, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 260, 57, NULL, 0, 0, 1100, 5998900, 'insert', 'Insert stok produk', '2020-01-14 07:12:03', '2020-01-14 07:12:03'),
(422, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 261, 54, NULL, 100, 0, 999000, 5999000, 'insert', 'Insert stok produk dari produk assembly', '2020-01-14 07:24:49', '2020-01-14 07:24:49'),
(423, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 262, 55, NULL, 100, 0, 999000, 5999100, 'insert', 'Insert stok produk dari produk assembly', '2020-01-14 07:24:49', '2020-01-14 07:24:49'),
(424, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 260, 57, NULL, 0, 100, 1000, 5999100, 'delete', 'Delete stok produk', '2020-01-14 07:24:49', '2020-01-14 07:24:49'),
(425, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 263, 54, NULL, 1000, 0, 1000000, 6000100, 'insert', 'Insert stok produk dari produk assembly', '2020-01-14 07:24:52', '2020-01-14 07:24:52'),
(426, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 264, 55, NULL, 1000, 0, 1000000, 6001100, 'insert', 'Insert stok produk dari produk assembly', '2020-01-14 07:24:52', '2020-01-14 07:24:52'),
(427, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 257, 57, NULL, 0, 1000, NULL, 6001100, 'delete', 'Delete stok produk', '2020-01-14 07:24:52', '2020-01-14 07:24:52'),
(428, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 265, 55, NULL, 100000, 0, 1100000, 6101100, 'insert', 'Insert stok produk', '2020-01-14 07:26:20', '2020-01-14 07:26:20'),
(429, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 255, 54, NULL, 0, 499000, 501000, 6101100, 'delete', 'Delete stok produk', '2020-01-14 07:28:32', '2020-01-14 07:28:32'),
(430, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 258, 54, NULL, 0, 499900, 1100, 6101100, 'delete', 'Delete stok produk', '2020-01-14 07:28:32', '2020-01-14 07:28:32'),
(431, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 261, 54, NULL, 0, 100, 1000, 6101100, 'delete', 'Delete stok produk', '2020-01-14 07:28:32', '2020-01-14 07:28:32'),
(432, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 263, 54, NULL, 0, 1000, NULL, 6101100, 'delete', 'Delete stok produk', '2020-01-14 07:28:32', '2020-01-14 07:28:32'),
(433, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 266, 54, NULL, 0, 0, 0, 6101100, 'insert', 'Insert stok produk sisa assembly', '2020-01-14 07:28:32', '2020-01-14 07:28:32'),
(434, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 256, 55, NULL, 0, 499000, 601000, 6101100, 'delete', 'Delete stok produk', '2020-01-14 07:28:33', '2020-01-14 07:28:33'),
(435, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 259, 55, NULL, 0, 499900, 101100, 6101100, 'delete', 'Delete stok produk', '2020-01-14 07:28:33', '2020-01-14 07:28:33'),
(436, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 262, 55, NULL, 0, 100, 101000, 6101100, 'delete', 'Delete stok produk', '2020-01-14 07:28:33', '2020-01-14 07:28:33'),
(437, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 264, 55, NULL, 0, 1000, 100000, 6101100, 'delete', 'Delete stok produk', '2020-01-14 07:28:33', '2020-01-14 07:28:33'),
(438, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 267, 55, NULL, 0, 0, 100000, 6101100, 'insert', 'Insert stok produk sisa assembly', '2020-01-14 07:28:33', '2020-01-14 07:28:33'),
(439, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 268, 57, NULL, 0, 0, 1000, 6102100, 'insert', 'Insert stok produk', '2020-01-14 07:28:34', '2020-01-14 07:28:34'),
(440, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 269, 54, NULL, 1000, 0, 1000, 6103100, 'insert', 'Insert stok produk dari produk assembly', '2020-01-14 07:40:35', '2020-01-14 07:40:35'),
(441, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 270, 55, NULL, 1000, 0, 101000, 6104100, 'insert', 'Insert stok produk dari produk assembly', '2020-01-14 07:40:36', '2020-01-14 07:40:36'),
(442, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 268, 57, NULL, 0, 1000, NULL, 6104100, 'delete', 'Delete stok produk', '2020-01-14 07:40:36', '2020-01-14 07:40:36'),
(443, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 270, 55, NULL, 0, 1000, 100000, 6104100, 'delete', 'Delete stok produk', '2020-01-14 07:42:33', '2020-01-14 07:42:33'),
(444, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 265, 55, NULL, 0, 100000, 0, 6104100, 'delete', 'Delete stok produk', '2020-01-14 07:42:36', '2020-01-14 07:42:36'),
(445, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 269, 54, NULL, 0, 1000, 0, 6104100, 'delete', 'Delete stok produk', '2020-01-14 07:42:52', '2020-01-14 07:42:52'),
(446, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 271, 55, NULL, 1000000, 0, 1000000, 7104100, 'insert', 'Insert stok produk', '2020-01-14 07:43:18', '2020-01-14 07:43:18'),
(447, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 272, 54, NULL, 1000000, 0, 1000000, 8104100, 'insert', 'Insert stok produk', '2020-01-14 07:43:56', '2020-01-14 07:43:56'),
(448, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 266, 54, NULL, 0, 0, 1000000, 8104100, 'delete', 'Delete stok produk', '2020-01-14 07:45:17', '2020-01-14 07:45:17'),
(449, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 272, 54, NULL, 0, 1000000, NULL, 8104100, 'delete', 'Delete stok produk', '2020-01-14 07:45:17', '2020-01-14 07:45:17'),
(450, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 273, 54, NULL, 0, 0, 0, 8104100, 'insert', 'Insert stok produk sisa assembly', '2020-01-14 07:45:17', '2020-01-14 07:45:17'),
(451, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 267, 55, NULL, 0, 0, 1000000, 8104100, 'delete', 'Delete stok produk', '2020-01-14 07:45:18', '2020-01-14 07:45:18'),
(452, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 271, 55, NULL, 0, 1000000, NULL, 8104100, 'delete', 'Delete stok produk', '2020-01-14 07:45:18', '2020-01-14 07:45:18'),
(453, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 274, 55, NULL, 0, 0, 0, 8104100, 'insert', 'Insert stok produk sisa assembly', '2020-01-14 07:45:18', '2020-01-14 07:45:18'),
(454, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 275, 57, NULL, 0, 0, 1000, 8105100, 'insert', 'Insert stok produk', '2020-01-14 07:45:18', '2020-01-14 07:45:18'),
(455, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 276, 54, NULL, 1000000, 0, 1000000, 9105100, 'insert', 'Insert stok produk dari produk assembly', '2020-01-14 07:48:25', '2020-01-14 07:48:25'),
(456, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 277, 55, NULL, 1000000, 0, 1000000, 10105100, 'insert', 'Insert stok produk dari produk assembly', '2020-01-14 07:48:25', '2020-01-14 07:48:25'),
(457, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 275, 57, NULL, 0, 1000, NULL, 11104100, 'delete', 'Delete stok produk', '2020-01-14 07:48:25', '2020-01-14 07:48:25'),
(458, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 273, 54, NULL, 0, 0, 1000000, 11104100, 'delete', 'Delete stok produk', '2020-01-14 07:57:35', '2020-01-14 07:57:35'),
(459, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 276, 54, NULL, 0, 1000000, NULL, 11104100, 'delete', 'Delete stok produk', '2020-01-14 07:57:35', '2020-01-14 07:57:35'),
(460, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 278, 54, NULL, 0, 0, 0, 11104100, 'insert', 'Insert stok produk sisa assembly', '2020-01-14 07:57:35', '2020-01-14 07:57:35'),
(461, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 274, 55, NULL, 0, 0, 1000000, 11104100, 'delete', 'Delete stok produk', '2020-01-14 07:57:35', '2020-01-14 07:57:35'),
(462, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 277, 55, NULL, 0, 1000000, NULL, 11104100, 'delete', 'Delete stok produk', '2020-01-14 07:57:35', '2020-01-14 07:57:35'),
(463, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 279, 55, NULL, 0, 0, 0, 11104100, 'insert', 'Insert stok produk sisa assembly', '2020-01-14 07:57:36', '2020-01-14 07:57:36'),
(464, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 280, 57, NULL, 0, 0, 1000, 11105100, 'insert', 'Insert stok produk', '2020-01-14 07:57:36', '2020-01-14 07:57:36'),
(465, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 281, 54, NULL, 1000, 0, 1000, 11106100, 'insert', 'Insert stok produk dari produk assembly', '2020-01-14 07:59:10', '2020-01-14 07:59:10'),
(466, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 282, 55, NULL, 1000, 0, 1000, 11107100, 'insert', 'Insert stok produk dari produk assembly', '2020-01-14 07:59:10', '2020-01-14 07:59:10'),
(467, '2020-01-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 280, 57, NULL, 0, 1000, NULL, 11107100, 'delete', 'Delete stok produk', '2020-01-14 07:59:10', '2020-01-14 07:59:10'),
(468, '2020-01-16', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 1, 58, NULL, 1000000, 0, 1000000, 1000000, 'insert', 'Insert stok produk', '2020-01-16 10:32:24', '2020-01-16 10:32:24'),
(469, '2020-01-16', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 2, 59, NULL, 1000000, 0, 1000000, 2000000, 'insert', 'Insert stok produk', '2020-01-16 10:32:55', '2020-01-16 10:32:55'),
(470, '2020-01-16', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 1, 58, NULL, 0, 0, NULL, 2000000, 'delete', 'Delete stok produk', '2020-01-16 10:45:49', '2020-01-16 10:45:49'),
(471, '2020-01-16', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 3, 58, NULL, 1000000, 0, 1000000, 3000000, 'insert', 'Insert stok produk sisa assembly', '2020-01-16 10:45:49', '2020-01-16 10:45:49'),
(472, '2020-01-16', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 2, 59, NULL, 0, 1000, NULL, 3000000, 'delete', 'Delete stok produk', '2020-01-16 10:45:49', '2020-01-16 10:45:49'),
(473, '2020-01-16', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 4, 59, NULL, 999000, 0, 999000, 3999000, 'insert', 'Insert stok produk sisa assembly', '2020-01-16 10:45:49', '2020-01-16 10:45:49'),
(474, '2020-01-16', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 5, 63, NULL, 0, 0, 1, 3999001, 'insert', 'Insert stok produk', '2020-01-16 10:45:49', '2020-01-16 10:45:49'),
(475, '2020-01-16', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 6, 58, NULL, 1, 0, 1000001, 3999002, 'insert', 'Insert stok produk dari produk assembly', '2020-01-16 10:47:10', '2020-01-16 10:47:10'),
(476, '2020-01-16', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 7, 59, NULL, 1, 0, 999001, 3999003, 'insert', 'Insert stok produk dari produk assembly', '2020-01-16 10:47:11', '2020-01-16 10:47:11'),
(477, '2020-01-16', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 5, 63, NULL, 0, 1, NULL, 3999003, 'delete', 'Delete stok produk', '2020-01-16 10:47:11', '2020-01-16 10:47:11'),
(478, '2020-01-16', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 7, 59, NULL, 0, 1, 999000, 3999003, 'delete', 'Delete stok produk', '2020-01-16 10:47:46', '2020-01-16 10:47:46'),
(479, '2020-01-16', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 4, 59, NULL, 0, 999000, NULL, 3999003, 'delete', 'Delete stok produk', '2020-01-16 10:47:49', '2020-01-16 10:47:49'),
(480, '2020-01-16', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 8, 59, NULL, 1000000, 0, 1000000, 4999003, 'insert', 'Insert stok produk', '2020-01-16 10:48:03', '2020-01-16 10:48:03'),
(481, '2020-01-16', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 6, 58, NULL, 0, 1, 1000000, 4999003, 'delete', 'Delete stok produk', '2020-01-16 10:48:19', '2020-01-16 10:48:19'),
(482, '2020-01-16', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 3, 58, NULL, 0, 0, NULL, 4999003, 'delete', 'Delete stok produk', '2020-01-16 10:49:00', '2020-01-16 10:49:00'),
(483, '2020-01-16', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 9, 58, NULL, 1000000, 0, 1000000, 5999003, 'insert', 'Insert stok produk sisa assembly', '2020-01-16 10:49:00', '2020-01-16 10:49:00'),
(484, '2020-01-16', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 8, 59, NULL, 0, 1000, NULL, 5999003, 'delete', 'Delete stok produk', '2020-01-16 10:49:00', '2020-01-16 10:49:00'),
(485, '2020-01-16', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 10, 59, NULL, 999000, 0, 999000, 6998003, 'insert', 'Insert stok produk sisa assembly', '2020-01-16 10:49:00', '2020-01-16 10:49:00'),
(486, '2020-01-16', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 11, 63, NULL, 0, 0, 1, 6998004, 'insert', 'Insert stok produk', '2020-01-16 10:49:01', '2020-01-16 10:49:01'),
(487, '2020-01-16', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 12, 58, NULL, 1, 0, 1000001, 6998005, 'insert', 'Insert stok produk dari produk assembly', '2020-01-16 10:53:26', '2020-01-16 10:53:26'),
(488, '2020-01-16', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 13, 59, NULL, 1, 0, 999001, 6998006, 'insert', 'Insert stok produk dari produk assembly', '2020-01-16 10:53:26', '2020-01-16 10:53:26'),
(489, '2020-01-16', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 11, 63, NULL, 0, 1, NULL, 6998006, 'delete', 'Delete stok produk', '2020-01-16 10:53:26', '2020-01-16 10:53:26'),
(490, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 13, 59, NULL, 0, 1, 999000, 6998006, 'delete', 'Delete stok produk', '2020-01-17 02:14:38', '2020-01-17 02:14:38'),
(491, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 14, 59, NULL, 1000, 0, 1000000, 6999006, 'insert', 'Insert stok produk', '2020-01-17 02:14:53', '2020-01-17 02:14:53'),
(492, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 12, 58, NULL, 0, 1, 1000000, 6999006, 'delete', 'Delete stok produk', '2020-01-17 02:15:09', '2020-01-17 02:15:09'),
(493, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 9, 58, NULL, 0, 100, NULL, 6999006, 'delete', 'Delete stok produk', '2020-01-17 02:16:33', '2020-01-17 02:16:33'),
(494, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 15, 58, NULL, 999900, 0, 999900, 7998906, 'insert', 'Insert stok produk sisa assembly', '2020-01-17 02:16:33', '2020-01-17 02:16:33'),
(495, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 10, 59, NULL, 0, 100000, 1000, 7998906, 'delete', 'Delete stok produk', '2020-01-17 02:16:33', '2020-01-17 02:16:33'),
(496, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 16, 59, NULL, 899000, 0, 900000, 8897906, 'insert', 'Insert stok produk sisa assembly', '2020-01-17 02:16:33', '2020-01-17 02:16:33'),
(497, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 17, 63, NULL, 0.10000000149011612, 0, 100, 8898006, 'insert', 'Insert stok produk', '2020-01-17 02:16:33', '2020-01-17 02:16:33'),
(498, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 18, 58, NULL, 100, 0, 1000000, 8898106, 'insert', 'Insert stok produk dari produk assembly', '2020-01-17 02:20:37', '2020-01-17 02:20:37'),
(499, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 19, 59, NULL, 100, 0, 900100, 8898206, 'insert', 'Insert stok produk dari produk assembly', '2020-01-17 02:20:37', '2020-01-17 02:20:37'),
(500, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 17, 63, NULL, 0, 100, NULL, 8898206, 'delete', 'Delete stok produk', '2020-01-17 02:20:38', '2020-01-17 02:20:38'),
(501, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 20, 59, NULL, 900, 0, 901000, 8899106, 'insert', 'Insert stok produk', '2020-01-17 02:22:01', '2020-01-17 02:22:01'),
(502, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 20, 59, NULL, 0, 900, 900100, 8899106, 'delete', 'Delete stok produk', '2020-01-17 02:22:33', '2020-01-17 02:22:33'),
(503, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 19, 59, NULL, 0, 100, 900000, 8899106, 'delete', 'Delete stok produk', '2020-01-17 02:22:35', '2020-01-17 02:22:35'),
(504, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 16, 59, NULL, 0, 899000, 1000, 8899106, 'delete', 'Delete stok produk', '2020-01-17 02:22:37', '2020-01-17 02:22:37'),
(505, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 14, 59, NULL, 0, 1000, NULL, 8899106, 'delete', 'Delete stok produk', '2020-01-17 02:22:40', '2020-01-17 02:22:40'),
(506, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 21, 59, NULL, 1000000, 0, 1000000, 9899106, 'insert', 'Insert stok produk', '2020-01-17 02:22:51', '2020-01-17 02:22:51'),
(507, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 15, 58, NULL, 0, 100, 100, 9899106, 'delete', 'Delete stok produk', '2020-01-17 02:24:42', '2020-01-17 02:24:42'),
(508, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 22, 58, NULL, 999800, 0, 999900, 10898906, 'insert', 'Insert stok produk sisa assembly', '2020-01-17 02:24:42', '2020-01-17 02:24:42'),
(509, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 21, 59, NULL, 0, 100000, NULL, 10898906, 'delete', 'Delete stok produk', '2020-01-17 02:24:42', '2020-01-17 02:24:42'),
(510, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 23, 59, NULL, 900000, 0, 900000, 11798906, 'insert', 'Insert stok produk sisa assembly', '2020-01-17 02:24:42', '2020-01-17 02:24:42'),
(511, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 24, 63, NULL, 0, 0, 100, 11799006, 'insert', 'Insert stok produk', '2020-01-17 02:24:42', '2020-01-17 02:24:42'),
(512, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 25, 58, NULL, 0, 0, 999900, 11799006, 'insert', 'Insert stok produk dari produk assembly', '2020-01-17 02:25:01', '2020-01-17 02:25:01'),
(513, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 26, 59, NULL, 100000, 0, 1000000, 11899006, 'insert', 'Insert stok produk dari produk assembly', '2020-01-17 02:25:01', '2020-01-17 02:25:01'),
(514, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 24, 63, NULL, 0, 100, NULL, 11998906, 'delete', 'Delete stok produk', '2020-01-17 02:25:01', '2020-01-17 02:25:01'),
(515, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 27, 58, NULL, 100, 0, 1000000, 11999006, 'insert', 'Insert stok produk', '2020-01-17 02:32:39', '2020-01-17 02:32:39'),
(516, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 18, 58, NULL, 0, 100, 999900, 11999006, 'delete', 'Delete stok produk', '2020-01-17 02:33:11', '2020-01-17 02:33:11'),
(517, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 22, 58, NULL, 0, 99900, 100, 11999006, 'delete', 'Delete stok produk', '2020-01-17 02:33:11', '2020-01-17 02:33:11'),
(518, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 28, 58, NULL, 899900, 0, 900000, 12898906, 'insert', 'Insert stok produk sisa assembly', '2020-01-17 02:33:11', '2020-01-17 02:33:11'),
(519, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 23, 59, NULL, 0, 100, 100000, 12898906, 'delete', 'Delete stok produk', '2020-01-17 02:33:11', '2020-01-17 02:33:11'),
(520, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 29, 59, NULL, 899900, 0, 999900, 13798806, 'insert', 'Insert stok produk sisa assembly', '2020-01-17 02:33:11', '2020-01-17 02:33:11'),
(521, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 30, 63, NULL, 0, 0, 100, 13798906, 'insert', 'Insert stok produk', '2020-01-17 02:33:11', '2020-01-17 02:33:11'),
(522, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 31, 58, NULL, 100000, 0, 1000000, 13898906, 'insert', 'Insert stok produk dari produk assembly', '2020-01-17 02:33:37', '2020-01-17 02:33:37'),
(523, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 32, 59, NULL, 100, 0, 1000000, 13899006, 'insert', 'Insert stok produk dari produk assembly', '2020-01-17 02:33:37', '2020-01-17 02:33:37'),
(524, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 30, 63, NULL, 0, 100, NULL, 13899006, 'delete', 'Delete stok produk', '2020-01-17 02:33:37', '2020-01-17 02:33:37'),
(525, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 25, 58, NULL, 0, 0, 1000000, 13899006, 'delete', 'Delete stok produk', '2020-01-17 02:40:25', '2020-01-17 02:40:25'),
(526, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 27, 58, NULL, 0, 100, 999900, 13899006, 'delete', 'Delete stok produk', '2020-01-17 02:40:25', '2020-01-17 02:40:25'),
(527, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 28, 58, NULL, 0, 100, 100000, 13899006, 'delete', 'Delete stok produk', '2020-01-17 02:40:25', '2020-01-17 02:40:25'),
(528, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 33, 58, NULL, 899800, 0, 999800, 14798806, 'insert', 'Insert stok produk sisa assembly', '2020-01-17 02:40:25', '2020-01-17 02:40:25');
INSERT INTO `arus_stock_produk` (`id`, `tanggal`, `table_name`, `penjualan_id`, `penjualan_produk_id`, `distribusi_id`, `distribusi_detail_id`, `produksi_id`, `progress_produksi_id`, `stock_produk_id`, `produk_id`, `history_penyesuaian_produk_id`, `stock_in`, `stock_out`, `last_stock`, `last_stock_total`, `method`, `keterangan`, `created_at`, `updated_at`) VALUES
(529, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 26, 59, NULL, 0, 100000, 900000, 14798806, 'delete', 'Delete stok produk', '2020-01-17 02:40:26', '2020-01-17 02:40:26'),
(530, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 29, 59, NULL, 0, 100000, 100, 14798806, 'delete', 'Delete stok produk', '2020-01-17 02:40:26', '2020-01-17 02:40:26'),
(531, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 34, 59, NULL, 799900, 0, 800000, 15598706, 'insert', 'Insert stok produk sisa assembly', '2020-01-17 02:40:26', '2020-01-17 02:40:26'),
(532, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 35, 63, NULL, 0, 0, 100, 15598806, 'insert', 'Insert stok produk', '2020-01-17 02:40:26', '2020-01-17 02:40:26'),
(533, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 36, 58, NULL, 200, 0, 1000000, 15599006, 'insert', 'Insert stok produk dari produk assembly', '2020-01-17 02:46:16', '2020-01-17 02:46:16'),
(534, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 37, 59, NULL, 200, 0, 800200, 15599206, 'insert', 'Insert stok produk dari produk assembly', '2020-01-17 02:46:16', '2020-01-17 02:46:16'),
(535, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 35, 63, NULL, 0, 100, NULL, 15599306, 'delete', 'Delete stok produk', '2020-01-17 02:46:16', '2020-01-17 02:46:16'),
(536, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 31, 58, NULL, 0, 200, 900000, 15599306, 'delete', 'Delete stok produk', '2020-01-17 02:47:00', '2020-01-17 02:47:00'),
(537, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 38, 58, NULL, 99800, 0, 999800, 15699106, 'insert', 'Insert stok produk sisa assembly', '2020-01-17 02:47:00', '2020-01-17 02:47:00'),
(538, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 32, 59, NULL, 0, 100, 800100, 15699106, 'delete', 'Delete stok produk', '2020-01-17 02:47:00', '2020-01-17 02:47:00'),
(539, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 34, 59, NULL, 0, 199900, 200, 15699106, 'delete', 'Delete stok produk', '2020-01-17 02:47:00', '2020-01-17 02:47:00'),
(540, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 39, 59, NULL, 600000, 0, 600200, 16299106, 'insert', 'Insert stok produk sisa assembly', '2020-01-17 02:47:00', '2020-01-17 02:47:00'),
(541, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 40, 63, NULL, 0, 0, 100, 16299206, 'insert', 'Insert stok produk', '2020-01-17 02:47:00', '2020-01-17 02:47:00'),
(542, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 41, 58, NULL, 200, 0, 1000000, 16299406, 'insert', 'Insert stok produk dari produk assembly', '2020-01-17 02:47:31', '2020-01-17 02:47:31'),
(543, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 42, 59, NULL, 200000, 0, 800200, 16499406, 'insert', 'Insert stok produk dari produk assembly', '2020-01-17 02:47:31', '2020-01-17 02:47:31'),
(544, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 40, 63, NULL, 0, 100, NULL, 16699306, 'delete', 'Delete stok produk', '2020-01-17 02:47:31', '2020-01-17 02:47:31'),
(545, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 33, 58, NULL, 0, 100, 100200, 16699306, 'delete', 'Delete stok produk', '2020-01-17 02:48:10', '2020-01-17 02:48:10'),
(546, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 43, 58, NULL, 899700, 0, 999900, 17599006, 'insert', 'Insert stok produk sisa assembly', '2020-01-17 02:48:10', '2020-01-17 02:48:10'),
(547, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 37, 59, NULL, 0, 200, 800000, 17599006, 'delete', 'Delete stok produk', '2020-01-17 02:48:10', '2020-01-17 02:48:10'),
(548, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 39, 59, NULL, 0, 99800, 200000, 17599006, 'delete', 'Delete stok produk', '2020-01-17 02:48:10', '2020-01-17 02:48:10'),
(549, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 44, 59, NULL, 500200, 0, 700200, 18099206, 'insert', 'Insert stok produk sisa assembly', '2020-01-17 02:48:10', '2020-01-17 02:48:10'),
(550, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 45, 63, NULL, 0, 0, 100, 18099306, 'insert', 'Insert stok produk', '2020-01-17 02:48:10', '2020-01-17 02:48:10'),
(551, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 46, 58, NULL, 100, 0, 1000000, 18099406, 'insert', 'Insert stok produk dari produk assembly', '2020-01-17 02:48:55', '2020-01-17 02:48:55'),
(552, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 47, 59, NULL, 100, 0, 700300, 18099506, 'insert', 'Insert stok produk dari produk assembly', '2020-01-17 02:48:55', '2020-01-17 02:48:55'),
(553, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 45, 63, NULL, 0, 100, NULL, 18099506, 'delete', 'Delete stok produk', '2020-01-17 02:48:55', '2020-01-17 02:48:55'),
(554, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 36, 58, NULL, 0, 100, 999800, 18099506, 'delete', 'Delete stok produk', '2020-01-17 02:57:29', '2020-01-17 02:57:29'),
(555, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 48, 58, NULL, 100, 0, 999900, 18099606, 'insert', 'Insert stok produk sisa assembly', '2020-01-17 02:57:29', '2020-01-17 02:57:29'),
(556, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 42, 59, NULL, 0, 100000, 500300, 18099606, 'delete', 'Delete stok produk', '2020-01-17 02:57:29', '2020-01-17 02:57:29'),
(557, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 49, 59, NULL, 100000, 0, 600300, 18199606, 'insert', 'Insert stok produk sisa assembly', '2020-01-17 02:57:29', '2020-01-17 02:57:29'),
(558, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 50, 63, NULL, 0, 0, 100, 18199706, 'insert', 'Insert stok produk', '2020-01-17 02:57:29', '2020-01-17 02:57:29'),
(559, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 51, 58, NULL, 100, 0, 1000000, 18199806, 'insert', 'Insert stok produk dari produk assembly', '2020-01-17 02:58:08', '2020-01-17 02:58:08'),
(560, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 52, 59, NULL, 100, 0, 600400, 18199906, 'insert', 'Insert stok produk dari produk assembly', '2020-01-17 02:58:08', '2020-01-17 02:58:08'),
(561, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 50, 63, NULL, 0, 100, NULL, 18199906, 'delete', 'Delete stok produk', '2020-01-17 02:58:08', '2020-01-17 02:58:08'),
(562, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 38, 58, NULL, 0, 100, 900200, 18199906, 'delete', 'Delete stok produk', '2020-01-17 02:59:43', '2020-01-17 02:59:43'),
(563, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 53, 58, NULL, 99700, 0, 999900, 18299606, 'insert', 'Insert stok produk sisa assembly', '2020-01-17 02:59:43', '2020-01-17 02:59:43'),
(564, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 44, 59, NULL, 0, 100000, 100200, 18299606, 'delete', 'Delete stok produk', '2020-01-17 02:59:43', '2020-01-17 02:59:43'),
(565, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 54, 59, NULL, 400200, 0, 500400, 18699806, 'insert', 'Insert stok produk sisa assembly', '2020-01-17 02:59:43', '2020-01-17 02:59:43'),
(566, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 55, 63, NULL, 0, 0, 100, 18699906, 'insert', 'Insert stok produk', '2020-01-17 02:59:43', '2020-01-17 02:59:43'),
(567, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 56, 58, NULL, 100, 0, 1000000, 18700006, 'insert', 'Insert stok produk dari produk assembly', '2020-01-17 03:00:13', '2020-01-17 03:00:13'),
(568, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 57, 59, NULL, 100, 0, 500500, 18700106, 'insert', 'Insert stok produk dari produk assembly', '2020-01-17 03:00:13', '2020-01-17 03:00:13'),
(569, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 55, 63, NULL, 0, 100, NULL, 18700106, 'delete', 'Delete stok produk', '2020-01-17 03:00:13', '2020-01-17 03:00:13'),
(570, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 41, 58, NULL, 0, 100, 999800, 18700106, 'delete', 'Delete stok produk', '2020-01-17 03:05:15', '2020-01-17 03:05:15'),
(571, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 58, 58, NULL, 100, 0, 999900, 18700206, 'insert', 'Insert stok produk sisa assembly', '2020-01-17 03:05:16', '2020-01-17 03:05:16'),
(572, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 47, 59, NULL, 0, 100, 500400, 18700206, 'delete', 'Delete stok produk', '2020-01-17 03:05:16', '2020-01-17 03:05:16'),
(573, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 49, 59, NULL, 0, 99900, 400400, 18700206, 'delete', 'Delete stok produk', '2020-01-17 03:05:16', '2020-01-17 03:05:16'),
(574, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 59, 59, NULL, 100, 0, 400500, 18700306, 'insert', 'Insert stok produk sisa assembly', '2020-01-17 03:05:16', '2020-01-17 03:05:16'),
(575, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 60, 63, NULL, 0, 0, 100, 18700406, 'insert', 'Insert stok produk', '2020-01-17 03:05:16', '2020-01-17 03:05:16'),
(576, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 61, 58, NULL, 100, 0, 1000000, 18700506, 'insert', 'Insert stok produk dari produk assembly', '2020-01-17 03:07:51', '2020-01-17 03:07:51'),
(577, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 62, 59, NULL, 100000, 0, 500500, 18800506, 'insert', 'Insert stok produk dari produk assembly', '2020-01-17 03:07:51', '2020-01-17 03:07:51'),
(578, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 60, 63, NULL, 0, 100, NULL, 18900406, 'delete', 'Delete stok produk', '2020-01-17 03:07:52', '2020-01-17 03:07:52'),
(579, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 59, 59, NULL, 0, 50, 500450, 18900356, 'update', 'Penyesuaian stok produk', '2020-01-17 03:38:04', '2020-01-17 03:38:04'),
(580, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 43, 58, NULL, 0, 100, 100300, 18900356, 'delete', 'Delete stok produk', '2020-01-17 05:33:01', '2020-01-17 05:33:01'),
(581, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 63, 58, NULL, 899600, 0, 999900, 19799956, 'insert', 'Insert stok produk sisa assembly', '2020-01-17 05:33:01', '2020-01-17 05:33:01'),
(582, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 52, 59, NULL, 0, 100, 500350, 19799956, 'delete', 'Delete stok produk', '2020-01-17 05:33:01', '2020-01-17 05:33:01'),
(583, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 54, 59, NULL, 0, 99900, 100150, 19799956, 'delete', 'Delete stok produk', '2020-01-17 05:33:01', '2020-01-17 05:33:01'),
(584, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 64, 59, NULL, 300300, 0, 400450, 20100256, 'insert', 'Insert stok produk sisa assembly', '2020-01-17 05:33:01', '2020-01-17 05:33:01'),
(585, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 65, 63, NULL, 0, 0, 100, 20100356, 'insert', 'Insert stok produk', '2020-01-17 05:33:01', '2020-01-17 05:33:01'),
(586, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 59, 59, NULL, 50, 0, 400500, 20100406, 'update', 'Penyesuaian stok produk', '2020-01-17 05:47:38', '2020-01-17 05:47:38'),
(587, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 46, 58, NULL, 0, 100, 999800, 20100406, 'delete', 'Delete stok produk', '2020-01-17 06:32:16', '2020-01-17 06:32:16'),
(588, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 48, 58, NULL, 0, 100, 999700, 20100406, 'delete', 'Delete stok produk', '2020-01-17 06:33:19', '2020-01-17 06:33:19'),
(589, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 66, 58, NULL, 0, 0, 999700, 20100406, 'insert', 'Insert stok produk sisa assembly', '2020-01-17 06:33:19', '2020-01-17 06:33:19'),
(590, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 57, 59, NULL, 0, 100, 400400, 20100406, 'delete', 'Delete stok produk', '2020-01-17 06:33:19', '2020-01-17 06:33:19'),
(591, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 59, 59, NULL, 0, 100, 400300, 20100406, 'delete', 'Delete stok produk', '2020-01-17 06:33:19', '2020-01-17 06:33:19'),
(592, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 62, 59, NULL, 0, 99800, 300300, 20100406, 'delete', 'Delete stok produk', '2020-01-17 06:33:19', '2020-01-17 06:33:19'),
(593, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 67, 59, NULL, 200, 0, 300500, 20100606, 'insert', 'Insert stok produk sisa assembly', '2020-01-17 06:33:19', '2020-01-17 06:33:19'),
(594, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 65, 63, NULL, 99800, 0, 100, 20100606, 'update', 'Penyesuaian stok produk', '2020-01-17 06:33:20', '2020-01-17 06:33:20'),
(595, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 51, 58, NULL, 0, 100, 999600, 20100706, 'delete', 'Delete stok produk', '2020-01-17 06:34:03', '2020-01-17 06:34:03'),
(596, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 68, 58, NULL, 0, 0, 999600, 20100706, 'insert', 'Insert stok produk sisa assembly', '2020-01-17 06:34:03', '2020-01-17 06:34:03'),
(597, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 64, 59, NULL, 0, 100000, 200, 20100706, 'delete', 'Delete stok produk', '2020-01-17 06:34:03', '2020-01-17 06:34:03'),
(598, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 69, 59, NULL, 200300, 0, 200500, 20301006, 'insert', 'Insert stok produk sisa assembly', '2020-01-17 06:34:03', '2020-01-17 06:34:03'),
(599, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 65, 63, NULL, 300000, 0, 200, 20301006, 'update', 'Penyesuaian stok produk', '2020-01-17 06:34:04', '2020-01-17 06:34:04'),
(600, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 70, 58, NULL, 100, 0, 999700, 20301206, 'insert', 'Insert stok produk dari produk assembly', '2020-01-17 07:31:25', '2020-01-17 07:31:25'),
(601, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 71, 59, NULL, 100000, 0, 300500, 20401206, 'insert', 'Insert stok produk dari produk assembly', '2020-01-17 07:31:25', '2020-01-17 07:31:25'),
(602, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 65, 63, NULL, 0, 100, 300, 20401206, 'update', 'Penyesuaian stok produk', '2020-01-17 07:31:26', '2020-01-17 07:31:26'),
(603, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 53, 58, NULL, 0, 100, 900000, 20401106, 'delete', 'Delete stok produk', '2020-01-17 07:36:15', '2020-01-17 07:36:15'),
(604, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 72, 58, NULL, 99600, 0, 999600, 20500706, 'insert', 'Insert stok produk sisa assembly', '2020-01-17 07:36:15', '2020-01-17 07:36:15'),
(605, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 67, 59, NULL, 0, 200, 300300, 20500706, 'delete', 'Delete stok produk', '2020-01-17 07:36:15', '2020-01-17 07:36:15'),
(606, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 69, 59, NULL, 0, 99800, 100000, 20500706, 'delete', 'Delete stok produk', '2020-01-17 07:36:15', '2020-01-17 07:36:15'),
(607, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 73, 59, NULL, 100500, 0, 200500, 20601206, 'insert', 'Insert stok produk sisa assembly', '2020-01-17 07:36:15', '2020-01-17 07:36:15'),
(608, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 56, 58, NULL, 0, 100, 999500, 20601206, 'delete', 'Delete stok produk', '2020-01-17 07:36:45', '2020-01-17 07:36:45'),
(609, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 74, 58, NULL, 0, 0, 999500, 20601206, 'insert', 'Insert stok produk sisa assembly', '2020-01-17 07:36:46', '2020-01-17 07:36:46'),
(610, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 71, 59, NULL, 0, 100000, 100500, 20601206, 'delete', 'Delete stok produk', '2020-01-17 07:36:46', '2020-01-17 07:36:46'),
(611, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 75, 59, NULL, 0, 0, 100500, 20601206, 'insert', 'Insert stok produk sisa assembly', '2020-01-17 07:36:46', '2020-01-17 07:36:46'),
(612, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 76, 58, NULL, 100, 0, 999600, 20601306, 'insert', 'Insert stok produk dari produk assembly', '2020-01-17 07:41:48', '2020-01-17 07:41:48'),
(613, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 77, 59, NULL, 100000, 0, 200500, 20701306, 'insert', 'Insert stok produk dari produk assembly', '2020-01-17 07:41:48', '2020-01-17 07:41:48'),
(614, '2020-01-17', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 65, 63, NULL, 0, 100, 200, 20701306, 'update', 'Penyesuaian stok produk', '2020-01-17 07:41:48', '2020-01-17 07:41:48'),
(615, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 58, 58, NULL, 0, 1, 999500, 20701206, 'delete', 'Delete stok produk', '2020-01-20 06:08:23', '2020-01-20 06:08:23'),
(616, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 78, 58, NULL, 99, 0, 999599, 20701304, 'insert', 'Insert stok produk sisa assembly', '2020-01-20 06:08:23', '2020-01-20 06:08:23'),
(617, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 73, 59, NULL, 0, 0.0010000000474974513, 100000, 20701304, 'delete', 'Delete stok produk', '2020-01-20 06:08:23', '2020-01-20 06:08:23'),
(618, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 79, 59, NULL, 100500, 0, 200500, 20801804, 'insert', 'Insert stok produk sisa assembly', '2020-01-20 06:08:23', '2020-01-20 06:08:23'),
(619, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 80, 63, NULL, 0, 0, 101, 20801806, 'insert', 'Insert stok produk', '2020-01-20 06:08:23', '2020-01-20 06:08:23'),
(620, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 81, 58, NULL, 1, 0, 999600, 20801808, 'insert', 'Insert stok produk dari produk assembly', '2020-01-20 06:12:07', '2020-01-20 06:12:07'),
(621, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 82, 59, NULL, 0.0010000000474974513, 0, 200500, 20801808, 'insert', 'Insert stok produk dari produk assembly', '2020-01-20 06:12:07', '2020-01-20 06:12:07'),
(622, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 80, 63, NULL, 0, 1, 100, 20801806, 'delete', 'Delete stok produk', '2020-01-20 06:12:07', '2020-01-20 06:12:07'),
(623, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 61, 58, NULL, 0, 1, 999500, 20801806, 'delete', 'Delete stok produk', '2020-01-20 06:13:45', '2020-01-20 06:13:45'),
(624, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 83, 58, NULL, 99, 0, 999599, 20801906, 'insert', 'Insert stok produk sisa assembly', '2020-01-20 06:13:45', '2020-01-20 06:13:45'),
(625, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 75, 59, NULL, 0, 0, 200500, 20801906, 'delete', 'Delete stok produk', '2020-01-20 06:13:45', '2020-01-20 06:13:45'),
(626, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 77, 59, NULL, 0, 0.0010000000474974513, 100500, 20801906, 'delete', 'Delete stok produk', '2020-01-20 06:13:45', '2020-01-20 06:13:45'),
(627, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 84, 59, NULL, 100000, 0, 200500, 20901906, 'insert', 'Insert stok produk sisa assembly', '2020-01-20 06:13:45', '2020-01-20 06:13:45'),
(628, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 85, 63, NULL, 0, 0, 101, 20901906, 'insert', 'Insert stok produk', '2020-01-20 06:13:45', '2020-01-20 06:13:45'),
(629, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 86, 58, NULL, 1, 0, 999600, 20901908, 'insert', 'Insert stok produk dari produk assembly', '2020-01-20 06:14:47', '2020-01-20 06:14:47'),
(630, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 87, 59, NULL, 0.0010000000474974513, 0, 200500, 20901908, 'insert', 'Insert stok produk dari produk assembly', '2020-01-20 06:14:47', '2020-01-20 06:14:47'),
(631, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 85, 63, NULL, 0, 1, 100, 20901906, 'delete', 'Delete stok produk', '2020-01-20 06:14:47', '2020-01-20 06:14:47'),
(632, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 63, 58, NULL, 0, 100, 100000, 20901906, 'delete', 'Delete stok produk', '2020-01-20 06:18:39', '2020-01-20 06:18:39'),
(633, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 88, 58, NULL, 899500, 0, 999500, 21801406, 'insert', 'Insert stok produk sisa assembly', '2020-01-20 06:18:39', '2020-01-20 06:18:39'),
(634, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 79, 59, NULL, 0, 0.10000000149011612, 100000, 21801406, 'delete', 'Delete stok produk', '2020-01-20 06:18:40', '2020-01-20 06:18:40'),
(635, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 89, 59, NULL, 100499.8984375, 0, 200499.90625, 21901906, 'insert', 'Insert stok produk sisa assembly', '2020-01-20 06:18:40', '2020-01-20 06:18:40'),
(636, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 90, 63, NULL, 0, 0, 200, 21902006, 'insert', 'Insert stok produk', '2020-01-20 06:18:40', '2020-01-20 06:18:40'),
(637, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 91, 58, NULL, 100, 0, 999600, 21902106, 'insert', 'Insert stok produk dari produk assembly', '2020-01-20 06:20:07', '2020-01-20 06:20:07'),
(638, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 92, 59, NULL, 100000, 0, 300499.90625, 22002106, 'insert', 'Insert stok produk dari produk assembly', '2020-01-20 06:20:07', '2020-01-20 06:20:07'),
(639, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 90, 63, NULL, 0, 100, 100, 22102006, 'delete', 'Delete stok produk', '2020-01-20 06:20:08', '2020-01-20 06:20:08'),
(640, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 65, 63, NULL, 0, 0.10000000149011612, 0, 22101906, 'delete', 'Penjualan produk', '2020-01-20 08:44:03', '2020-01-20 08:44:03'),
(641, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 84, 59, NULL, 0, 3, 297499.90625, 22098906, 'delete', 'Penjualan produk', '2020-01-20 08:57:26', '2020-01-20 08:57:26'),
(642, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 93, 65, NULL, 1000000, 0, 1000000, 1000000, 'insert', 'Insert stok produk', '2020-01-20 09:19:49', '2020-01-20 09:19:49'),
(643, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 94, 64, NULL, 1000000, 0, 1000000, 2000000, 'insert', 'Insert stok produk', '2020-01-20 09:20:09', '2020-01-20 09:20:09'),
(644, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 94, 64, NULL, 0, 200000, NULL, 2000000, 'delete', 'Delete stok produk', '2020-01-20 09:22:28', '2020-01-20 09:22:28'),
(645, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 95, 64, NULL, 800000, 0, 800000, 2800000, 'insert', 'Insert stok produk sisa assembly', '2020-01-20 09:22:28', '2020-01-20 09:22:28'),
(646, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 93, 65, NULL, 0, 1000, NULL, 2800000, 'delete', 'Delete stok produk', '2020-01-20 09:22:28', '2020-01-20 09:22:28'),
(647, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 96, 65, NULL, 999000, 0, 999000, 3799000, 'insert', 'Insert stok produk sisa assembly', '2020-01-20 09:22:28', '2020-01-20 09:22:28'),
(648, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 97, 66, NULL, 0, 0, 100000, 3899000, 'insert', 'Insert stok produk', '2020-01-20 09:22:28', '2020-01-20 09:22:28'),
(649, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 97, 66, NULL, 0, 2, 99998, 3898998, 'delete', 'Penjualan produk', '2020-01-20 09:42:21', '2020-01-20 09:42:21'),
(650, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 96, 65, NULL, 0, 2.5, 999000, 3898998, 'delete', 'Penjualan produk', '2020-01-20 09:42:21', '2020-01-20 09:42:21'),
(651, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 97, 66, NULL, 0, 1, 99997, 3898997, 'delete', 'Penjualan produk', '2020-01-20 09:48:12', '2020-01-20 09:48:12'),
(652, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 96, 65, NULL, 0, 10, 999000, 3898997, 'delete', 'Penjualan produk', '2020-01-20 09:48:12', '2020-01-20 09:48:12'),
(653, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 97, 66, NULL, 0, 1, 99996, 3898996, 'delete', 'Penjualan produk', '2020-01-20 09:49:42', '2020-01-20 09:49:42'),
(654, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 96, 65, NULL, 0, 1.5, 999000, 3898996, 'delete', 'Penjualan produk', '2020-01-20 09:49:42', '2020-01-20 09:49:42'),
(655, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 97, 66, NULL, 0, 1, 99995, 3898995, 'delete', 'Penjualan produk', '2020-01-20 10:28:28', '2020-01-20 10:28:28'),
(656, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 96, 65, NULL, 0, 0.0024999999441206455, 999000, 3898995, 'delete', 'Penjualan produk', '2020-01-20 10:28:28', '2020-01-20 10:28:28'),
(657, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 97, 66, NULL, 0, 1, 99994, 3898994, 'delete', 'Penjualan produk', '2020-01-20 10:30:47', '2020-01-20 10:30:47'),
(658, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 96, 65, NULL, 0, 1, 998999, 3898993, 'delete', 'Penjualan produk', '2020-01-20 10:30:47', '2020-01-20 10:30:47'),
(659, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 97, 66, NULL, 0, 11, 99983, 3898982, 'delete', 'Penjualan produk', '2020-01-20 10:51:05', '2020-01-20 10:51:05'),
(660, '2020-01-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 96, 65, NULL, 0, 0.08749999850988388, 998998.9375, 3898982, 'delete', 'Penjualan produk', '2020-01-20 10:51:05', '2020-01-20 10:51:05'),
(661, '2020-01-21', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 97, 66, NULL, 0, 2, 99981, 3898980, 'delete', 'Penjualan produk', '2020-01-21 02:26:34', '2020-01-21 02:26:34'),
(662, '2020-01-21', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 96, 65, NULL, 0, 2, 998997, 3898978, 'delete', 'Penjualan produk', '2020-01-21 02:26:34', '2020-01-21 02:26:34'),
(663, '2020-01-21', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 97, 66, NULL, 0, 3, 99978, 3898975, 'delete', 'Penjualan produk', '2020-01-21 02:34:20', '2020-01-21 02:34:20'),
(664, '2020-01-21', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 96, 65, NULL, 0, 0.010499999858438969, 998997, 3898975, 'delete', 'Penjualan produk', '2020-01-21 02:34:20', '2020-01-21 02:34:20'),
(665, '2020-01-21', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 96, 65, NULL, 0, 0.0010000000474974513, 998997, 3898975, 'delete', 'Penjualan produk', '2020-01-21 02:54:21', '2020-01-21 02:54:21'),
(666, '2020-01-21', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 96, 65, NULL, 0, 0.0010000000474974513, 998997, 3898975, 'delete', 'Penjualan produk', '2020-01-21 03:22:53', '2020-01-21 03:22:53'),
(667, '2020-01-22', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 96, 65, NULL, 0, 0.010499999858438969, 998997, 3898975, 'delete', 'Penjualan produk', '2020-01-22 02:31:38', '2020-01-22 02:31:38'),
(668, '2020-01-22', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 96, 65, NULL, 0, 0.0105, 998996.9895, 3898974.9895, 'delete', 'Penjualan produk', '2020-01-22 02:52:12', '2020-01-22 02:52:12'),
(669, '2020-01-22', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 98, 64, NULL, 199956, 0, 999956, 4098930.9895, 'insert', 'Insert stok produk dari produk assembly', '2020-01-22 08:51:14', '2020-01-22 08:51:14'),
(670, '2020-01-22', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 99, 65, NULL, 999.78, 0, 999996.7695, 4099930.7695, 'insert', 'Insert stok produk dari produk assembly', '2020-01-22 08:51:14', '2020-01-22 08:51:14'),
(671, '2020-01-22', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 97, 66, NULL, 0, 99978, NULL, 4000952.5494999997, 'delete', 'Delete stok produk', '2020-01-22 08:51:14', '2020-01-22 08:51:14'),
(672, '2020-01-22', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 95, 64, NULL, 0, 210, 199956, 4000952.5494999997, 'delete', 'Delete stok produk', '2020-01-22 08:52:54', '2020-01-22 08:52:54'),
(673, '2020-01-22', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 100, 64, NULL, 799790, 0, 999746, 4800742.5495, 'insert', 'Insert stok produk sisa assembly', '2020-01-22 08:52:54', '2020-01-22 08:52:54'),
(674, '2020-01-22', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 96, 65, NULL, 0, 1.05, 999.78, 4800742.5495, 'delete', 'Delete stok produk', '2020-01-22 08:52:54', '2020-01-22 08:52:54'),
(675, '2020-01-22', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 101, 65, NULL, 998995.9395, 0, 999995.7195, 5799738.489, 'insert', 'Insert stok produk sisa assembly', '2020-01-22 08:52:54', '2020-01-22 08:52:54'),
(676, '2020-01-22', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 102, 66, NULL, 0, 0, 105, 5799843.489, 'insert', 'Insert stok produk', '2020-01-22 08:52:54', '2020-01-22 08:52:54'),
(677, '2020-01-22', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 98, 64, NULL, 0, 181.58, 799790, 5799843.489, 'delete', 'Delete stok produk', '2020-01-22 08:59:08', '2020-01-22 08:59:08'),
(678, '2020-01-22', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 103, 64, NULL, 199774.42, 0, 999564.42, 5999617.909, 'insert', 'Insert stok produk sisa assembly', '2020-01-22 08:59:08', '2020-01-22 08:59:08'),
(679, '2020-01-22', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 99, 65, NULL, 0, 0.9079, 998995.9395, 5999617.909, 'delete', 'Delete stok produk', '2020-01-22 08:59:09', '2020-01-22 08:59:09'),
(680, '2020-01-22', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 104, 65, NULL, 998.8721, 0, 999994.8116, 6000616.7811, 'insert', 'Insert stok produk sisa assembly', '2020-01-22 08:59:09', '2020-01-22 08:59:09'),
(681, '2020-01-22', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 105, 66, NULL, NULL, 0, 195.79000000000002, 6000707.5711, 'insert', 'Insert stok produk', '2020-01-22 08:59:09', '2020-01-22 08:59:09'),
(682, '2020-01-22', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 106, 67, NULL, 1, 0, 1, 1, 'insert', 'Insert stok produk', '2020-01-22 09:31:09', '2020-01-22 09:31:09'),
(683, '2020-01-22', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 107, 67, NULL, 1, 0, 2, 2, 'insert', 'Insert stok produk', '2020-01-22 09:35:54', '2020-01-22 09:35:54'),
(684, '2020-01-23', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 107, 67, NULL, 0, 1, 1, 2, 'delete', 'Delete stok produk', '2020-01-23 02:12:12', '2020-01-23 02:12:12'),
(685, '2020-01-23', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 106, 67, NULL, 0, 1, NULL, 2, 'delete', 'Delete stok produk', '2020-01-23 02:12:15', '2020-01-23 02:12:15'),
(686, '2020-01-23', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 108, 67, NULL, 1000.5, 0, 1000.5, 1002.5, 'insert', 'Insert stok produk', '2020-01-23 02:25:24', '2020-01-23 02:25:24'),
(687, '2020-01-23', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 109, 67, NULL, 1000.5, 0, 2001, 2003, 'insert', 'Insert stok produk', '2020-01-23 02:26:37', '2020-01-23 02:26:37'),
(688, '2020-01-23', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 109, 67, NULL, 0, 500.5, 1500.5, 1502.5, 'update', 'Transfer stok produk', '2020-01-23 03:06:53', '2020-01-23 03:06:53'),
(689, '2020-01-23', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 108, 67, NULL, 0, 1000.5, 500, 502, 'update', 'Transfer stok produk', '2020-01-23 03:57:32', '2020-01-23 03:57:32'),
(690, '2020-01-23', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 110, 67, NULL, 1000.5, 0, 1500.5, 1502.5, 'insert', 'Penerimaan transfer stok', '2020-01-23 04:00:13', '2020-01-23 04:00:13'),
(691, '2020-01-23', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 109, 67, NULL, 500.5, 0, 2001, 2003, 'update', 'Penyesuaian stok produk', '2020-01-23 06:14:47', '2020-01-23 06:14:47'),
(692, '2020-01-23', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 109, 67, NULL, 0, 991.5, 1009.5, 1011.5, 'update', 'Penyesuaian stok produk', '2020-01-23 06:53:12', '2020-01-23 06:53:12'),
(693, '2020-01-23', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 110, 67, NULL, 0, 1000.27, 9.23, 11.23, 'update', 'Penyesuaian stok produk', '2020-01-23 06:53:33', '2020-01-23 06:53:33'),
(694, '2020-01-23', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 110, 67, NULL, 1.27, 0, 10.5, 12.5, 'update', 'Penyesuaian stok produk', '2020-01-23 08:03:57', '2020-01-23 08:03:57'),
(695, '2020-01-23', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 111, 67, NULL, 80, 0, 90.5, 92.5, 'insert', 'Insert stok produk', '2020-01-23 08:05:08', '2020-01-23 08:05:08'),
(696, '2020-01-23', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 110, 67, NULL, 98, 0, 188.5, 190.5, 'update', 'Penyesuaian stok produk', '2020-01-23 08:10:54', '2020-01-23 08:10:54'),
(697, '2020-01-23', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 111, 67, NULL, 0, 50, 138.5, 140.5, 'delete', 'Penjualan produk', '2020-01-23 09:06:54', '2020-01-23 09:06:54'),
(698, '2020-01-23', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 110, 67, NULL, 0, 99.5, 39, 140.5, 'delete', 'Delete stok produk', '2020-01-23 09:31:25', '2020-01-23 09:31:25'),
(699, '2020-01-23', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 112, 67, NULL, 30, 0, 69, 170.5, 'insert', 'Insert stok produk', '2020-01-23 09:31:36', '2020-01-23 09:31:36'),
(700, '2020-01-23', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 113, 67, NULL, 100, 0, 169, 270.5, 'insert', 'Insert stok produk', '2020-01-23 09:32:10', '2020-01-23 09:32:10'),
(701, '2020-01-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 114, 68, NULL, 50, 0, 50, 320.5, 'insert', 'Insert stok produk', '2020-01-27 03:03:02', '2020-01-27 03:03:02'),
(702, '2020-01-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 113, 67, NULL, 0, 100, 69, 320.5, 'delete', 'Delete stok produk', '2020-01-27 07:28:52', '2020-01-27 07:28:52'),
(703, '2020-01-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 115, 68, NULL, 10000, 0, 10050, 10320.5, 'insert', 'Insert stok produk', '2020-01-27 09:51:27', '2020-01-27 09:51:27'),
(704, '2020-01-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 116, 67, NULL, 10000, 0, 10069, 20320.5, 'insert', 'Insert stok produk', '2020-01-27 09:52:33', '2020-01-27 09:52:33'),
(705, '2020-01-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 108, 67, NULL, 0, 0, 10069, 20320.5, 'delete', 'Delete stok produk', '2020-01-27 09:52:49', '2020-01-27 09:52:49'),
(706, '2020-01-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 109, 67, NULL, 0, 9, 10060, 20320.5, 'delete', 'Delete stok produk', '2020-01-27 09:52:49', '2020-01-27 09:52:49'),
(707, '2020-01-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 111, 67, NULL, 0, 30, 10030, 20320.5, 'delete', 'Delete stok produk', '2020-01-27 09:52:49', '2020-01-27 09:52:49'),
(708, '2020-01-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 116, 67, NULL, 0, 761, 30, 20320.5, 'delete', 'Delete stok produk', '2020-01-27 09:52:49', '2020-01-27 09:52:49'),
(709, '2020-01-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 117, 67, NULL, 9239, 0, 9269, 29559.5, 'insert', 'Insert stok produk sisa assembly', '2020-01-27 09:52:49', '2020-01-27 09:52:49'),
(710, '2020-01-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 118, 70, NULL, 0, 0, 2, 29561.5, 'insert', 'Insert stok produk', '2020-01-27 09:52:49', '2020-01-27 09:52:49'),
(711, '2020-01-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 118, 70, NULL, 0, 2, 0, 29559.5, 'delete', 'Penjualan produk', '2020-01-27 10:00:49', '2020-01-27 10:00:49'),
(712, '2020-01-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 117, 67, NULL, 0, 800, 30, 29559.5, 'delete', 'Delete stok produk', '2020-01-27 10:02:40', '2020-01-27 10:02:40'),
(713, '2020-01-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 119, 67, NULL, 8439, 0, 8469, 37998.5, 'insert', 'Insert stok produk sisa assembly', '2020-01-27 10:02:40', '2020-01-27 10:02:40'),
(714, '2020-01-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 120, 70, NULL, 0, 0, 2, 38000.5, 'insert', 'Insert stok produk', '2020-01-27 10:02:40', '2020-01-27 10:02:40'),
(715, '2020-01-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 121, 71, NULL, 10, 0, 10, 38010.5, 'insert', 'Insert stok produk', '2020-01-27 10:08:29', '2020-01-27 10:08:29'),
(716, '2020-01-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 121, 71, NULL, 0, 6, 4, 38004.5, 'delete', 'Penjualan produk', '2020-01-27 10:10:10', '2020-01-27 10:10:10'),
(717, '2020-01-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 121, 71, NULL, 0, 3, 1, 38001.5, 'delete', 'Penjualan produk', '2020-01-27 10:11:07', '2020-01-27 10:11:07'),
(718, '2020-01-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 122, 73, NULL, 20, 0, 20, 38021.5, 'insert', 'Insert stok produk', '2020-01-27 10:16:18', '2020-01-27 10:16:18'),
(719, '2020-01-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 123, 72, NULL, 100, 0, 100, 38121.5, 'insert', 'Insert stok produk', '2020-01-27 10:17:23', '2020-01-27 10:17:23'),
(720, '2020-01-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 123, 72, NULL, 0, 6, NULL, 38121.5, 'delete', 'Delete stok produk', '2020-01-27 10:18:43', '2020-01-27 10:18:43'),
(721, '2020-01-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 124, 72, NULL, 94, 0, 94, 38215.5, 'insert', 'Insert stok produk sisa assembly', '2020-01-27 10:18:43', '2020-01-27 10:18:43'),
(722, '2020-01-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 122, 73, NULL, 0, 9, NULL, 38215.5, 'delete', 'Delete stok produk', '2020-01-27 10:18:43', '2020-01-27 10:18:43'),
(723, '2020-01-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 125, 73, NULL, 11, 0, 11, 38226.5, 'insert', 'Insert stok produk sisa assembly', '2020-01-27 10:18:43', '2020-01-27 10:18:43'),
(724, '2020-01-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 126, 74, NULL, 0, 0, 3, 38229.5, 'insert', 'Insert stok produk', '2020-01-27 10:18:43', '2020-01-27 10:18:43'),
(725, '2020-01-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 126, 74, NULL, 0, 1, 2, 38228.5, 'delete', 'Penjualan produk', '2020-01-27 10:19:41', '2020-01-27 10:19:41'),
(726, '2020-01-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 127, 72, NULL, 4, 0, 98, 38232.5, 'insert', 'Insert stok produk dari produk assembly', '2020-01-27 10:21:29', '2020-01-27 10:21:29'),
(727, '2020-01-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 128, 73, NULL, 6, 0, 17, 38238.5, 'insert', 'Insert stok produk dari produk assembly', '2020-01-27 10:21:29', '2020-01-27 10:21:29'),
(728, '2020-01-27', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 126, 74, NULL, 0, 2, NULL, 38242.5, 'delete', 'Delete stok produk', '2020-01-27 10:21:29', '2020-01-27 10:21:29'),
(729, '2020-01-29', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 125, 73, NULL, 0, 2, 15, 38240.5, 'delete', 'Penjualan produk', '2020-01-29 02:19:23', '2020-01-29 02:19:23'),
(730, '2020-01-29', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 129, 75, NULL, 12, 0, 12, 38252.5, 'insert', 'Insert stok produk', '2020-01-29 02:51:13', '2020-01-29 02:51:13'),
(731, '2020-01-29', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 130, 75, NULL, 12, 0, 24, 38264.5, 'insert', 'Insert stok produk', '2020-01-29 02:52:31', '2020-01-29 02:52:31'),
(732, '2020-01-29', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 131, 76, NULL, 36, 0, 36, 38300.5, 'insert', 'Insert stok produk', '2020-01-29 06:29:16', '2020-01-29 06:29:16'),
(733, '2020-01-29', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 131, 76, NULL, 0, 8, 28, 38292.5, 'delete', 'Penjualan produk', '2020-01-29 06:34:42', '2020-01-29 06:34:42'),
(734, '2020-01-29', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 131, 76, NULL, 0, 24, 4, 38268.5, 'delete', 'Penjualan produk', '2020-01-29 06:35:35', '2020-01-29 06:35:35'),
(735, '2020-01-29', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 118, 70, NULL, 0, 0, 2, 38268.5, 'delete', 'Delete stok produk', '2020-01-29 06:55:02', '2020-01-29 06:55:02'),
(736, '2020-01-29', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 120, 70, NULL, 0, 1.6, NULL, 38268.5, 'delete', 'Delete stok produk', '2020-01-29 06:55:02', '2020-01-29 06:55:02'),
(737, '2020-01-29', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 132, 70, NULL, 0.4, 0, 0.4, 38268.9, 'insert', 'Insert stok produk sisa assembly', '2020-01-29 06:55:02', '2020-01-29 06:55:02'),
(738, '2020-01-29', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 131, 76, NULL, 0, 2, NULL, 38268.9, 'delete', 'Delete stok produk', '2020-01-29 06:55:02', '2020-01-29 06:55:02'),
(739, '2020-01-29', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 133, 76, NULL, 2, 0, 2, 38270.9, 'insert', 'Insert stok produk sisa assembly', '2020-01-29 06:55:02', '2020-01-29 06:55:02'),
(740, '2020-01-29', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 134, 77, NULL, 0, 0, 2, 38272.9, 'insert', 'Insert stok produk', '2020-01-29 06:55:02', '2020-01-29 06:55:02'),
(741, '2020-01-29', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 133, 76, NULL, 0, 2, NULL, 38272.9, 'delete', 'Delete stok produk', '2020-01-29 06:59:42', '2020-01-29 06:59:42'),
(742, '2020-01-29', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 135, 76, NULL, 0, 0, 0, 38272.9, 'insert', 'Insert stok produk sisa assembly', '2020-01-29 06:59:42', '2020-01-29 06:59:42'),
(743, '2020-01-29', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 136, 78, NULL, 0, 0, 1, 38273.9, 'insert', 'Insert stok produk', '2020-01-29 06:59:42', '2020-01-29 06:59:42'),
(744, '2020-02-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 124, 72, NULL, 0, 3, 95, 38270.9, 'delete', 'Penjualan produk', '2020-02-03 05:11:32', '2020-02-03 05:11:32'),
(745, '2020-02-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 130, 75, NULL, 0, 6, 18, 38264.9, 'update', 'Transfer stok produk', '2020-02-03 05:52:40', '2020-02-03 05:52:40'),
(746, '2020-02-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 137, 75, NULL, 6, 0, 24, 38270.9, 'insert', 'Penerimaan transfer stok', '2020-02-03 05:52:58', '2020-02-03 05:52:58'),
(747, '2020-02-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 130, 75, NULL, 0, 0, 24, 38270.9, 'update', 'Stok yang tidak diterima', '2020-02-03 05:52:58', '2020-02-03 05:52:58'),
(748, '2020-02-03', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 137, 75, NULL, 0, 5, 19, 38265.9, 'update', 'Penyesuaian stok produk', '2020-02-03 05:54:03', '2020-02-03 05:54:03'),
(749, '2020-02-10', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 136, 78, NULL, 0, 1, 0, 38264.9, 'delete', 'Penjualan produk', '2020-02-10 01:47:56', '2020-02-10 01:47:56'),
(750, '2020-02-11', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 138, 71, NULL, 6, 0, 7, 38270.9, 'insert', 'Insert stok produk', '2020-02-11 03:26:25', '2020-02-11 03:26:25'),
(751, '2020-02-11', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 139, 76, NULL, 7, 0, 7, 38277.9, 'insert', 'Insert stok produk', '2020-02-11 03:32:46', '2020-02-11 03:32:46'),
(752, '2020-02-11', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 135, 76, NULL, 0, 0, 7, 38277.9, 'delete', 'Delete stok produk', '2020-02-11 03:33:14', '2020-02-11 03:33:14'),
(753, '2020-02-11', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 139, 76, NULL, 0, 6, NULL, 38277.9, 'delete', 'Delete stok produk', '2020-02-11 03:33:14', '2020-02-11 03:33:14'),
(754, '2020-02-11', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 140, 76, NULL, 1, 0, 1, 38278.9, 'insert', 'Insert stok produk sisa assembly', '2020-02-11 03:33:14', '2020-02-11 03:33:14'),
(755, '2020-02-11', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 125, 73, NULL, 0, 4, 6, 38278.9, 'delete', 'Delete stok produk', '2020-02-11 03:33:14', '2020-02-11 03:33:14'),
(756, '2020-02-11', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 141, 73, NULL, 5, 0, 11, 38283.9, 'insert', 'Insert stok produk sisa assembly', '2020-02-11 03:33:14', '2020-02-11 03:33:14'),
(757, '2020-02-11', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 142, 80, NULL, 0, 0, 2, 38285.9, 'insert', 'Insert stok produk', '2020-02-11 03:33:14', '2020-02-11 03:33:14'),
(758, '2020-02-11', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 143, 76, NULL, 6, 0, 7, 38291.9, 'insert', 'Insert stok produk dari produk assembly', '2020-02-11 04:58:42', '2020-02-11 04:58:42'),
(759, '2020-02-11', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 144, 73, NULL, 4, 0, 15, 38295.9, 'insert', 'Insert stok produk dari produk assembly', '2020-02-11 04:58:42', '2020-02-11 04:58:42'),
(760, '2020-02-11', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 143, 76, NULL, 0, 4, 3, 38289.9, 'delete', 'Penjualan produk', '2020-02-11 05:03:53', '2020-02-11 05:03:53'),
(761, '2020-02-11', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 145, 76, NULL, 1000, 0, 1003, 39289.9, 'insert', 'Insert stok produk', '2020-02-11 08:47:24', '2020-02-11 08:47:24'),
(762, '2020-02-11', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 146, 82, NULL, 100, 0, 100, 39389.9, 'insert', 'Insert stok produk', '2020-02-11 08:55:53', '2020-02-11 08:55:53'),
(763, '2020-02-11', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 146, 82, NULL, 0, 20, 80, 39369.9, 'delete', 'Penjualan produk', '2020-02-11 08:59:18', '2020-02-11 08:59:18'),
(764, '2020-02-14', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 146, 82, NULL, 0, 10, 70, 39359.9, 'delete', 'Penjualan produk', '2020-02-14 01:33:46', '2020-02-14 01:33:46'),
(765, '2020-03-19', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 147, 83, NULL, 50, 0, 50, 39409.9, 'insert', 'Insert stok produk', '2020-03-18 15:59:51', '0000-00-00 00:00:00'),
(766, '2020-03-19', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 147, 83, NULL, 0, 0.4, 49.6, 39409.5, 'delete', 'Penjualan produk', '2020-03-18 16:06:07', '0000-00-00 00:00:00'),
(767, '2020-03-19', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 147, 83, NULL, 0, 15, NULL, 39409.5, 'delete', 'Delete stok produk', '2020-03-18 16:15:46', '0000-00-00 00:00:00'),
(768, '2020-03-19', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 148, 83, NULL, 34.6, 0, 34.6, 39444.1, 'insert', 'Insert stok produk sisa assembly', '2020-03-18 16:15:46', '0000-00-00 00:00:00'),
(769, '2020-03-19', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 149, 84, NULL, 0, 0, 5, 39449.1, 'insert', 'Insert stok produk', '2020-03-18 16:15:46', '0000-00-00 00:00:00'),
(770, '2020-03-19', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 149, 84, NULL, 0, 3, 2, 39446.1, 'delete', 'Penjualan produk', '2020-03-18 16:16:22', '0000-00-00 00:00:00'),
(771, '2020-03-19', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 146, 82, NULL, 0, 40, 30, 39406.1, 'delete', 'Penjualan produk', '2020-03-19 02:44:57', '0000-00-00 00:00:00'),
(772, '2020-03-19', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 137, 75, NULL, 0, 1, 18, 39406.1, 'delete', 'Delete stok produk', '2020-03-19 07:44:18', '0000-00-00 00:00:00'),
(773, '2020-03-19', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 150, 75, NULL, 0, 0, 18, 39406.1, 'insert', 'Insert stok produk sisa assembly', '2020-03-19 07:44:18', '0000-00-00 00:00:00'),
(774, '2020-03-19', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 128, 73, NULL, 0, 1, 9, 39406.1, 'delete', 'Delete stok produk', '2020-03-19 07:44:18', '0000-00-00 00:00:00'),
(775, '2020-03-19', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 151, 73, NULL, 5, 0, 14, 39411.1, 'insert', 'Insert stok produk sisa assembly', '2020-03-19 07:44:18', '0000-00-00 00:00:00'),
(776, '2020-03-19', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 152, 79, NULL, 0, 0, 1, 39412.1, 'insert', 'Insert stok produk', '2020-03-19 07:44:18', '0000-00-00 00:00:00'),
(777, '2020-03-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 153, 85, NULL, 1, 0, 1, 39413.1, 'insert', 'Insert stok produk', '2020-03-20 02:30:00', '0000-00-00 00:00:00'),
(778, '2020-03-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 154, 85, NULL, 1, 0, 2, 39414.1, 'insert', 'Insert stok produk', '2020-03-20 02:30:28', '0000-00-00 00:00:00'),
(779, '2020-03-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 155, 85, NULL, 1, 0, 3, 39415.1, 'insert', 'Insert stok produk', '2020-03-20 02:31:04', '0000-00-00 00:00:00'),
(781, '2020-03-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 156, 85, NULL, 10, 0, 13, 39425.1, 'insert', 'Insert stok produk', '2020-03-20 04:05:59', '0000-00-00 00:00:00'),
(782, '2020-03-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 153, 85, NULL, 0, 1, 12, 39424.1, 'update', 'Penjualan Produk', '2020-03-20 04:06:34', '0000-00-00 00:00:00'),
(783, '2020-03-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 155, 85, NULL, 0, 1, 11, 39423.1, 'update', 'Penjualan Produk', '2020-03-20 04:06:34', '0000-00-00 00:00:00'),
(784, '2020-03-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 156, 85, NULL, 0, 9, 2, 39414.1, 'update', 'Penjualan Produk', '2020-03-20 04:06:34', '0000-00-00 00:00:00'),
(785, '2020-03-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 156, 85, NULL, 0, 1, 1, 39413.1, 'update', 'Penjualan Produk', '2020-03-20 04:29:07', '0000-00-00 00:00:00'),
(786, '2020-03-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 148, 83, NULL, 0, 1, 33.6, 39412.1, 'update', 'Penjualan Produk', '2020-03-20 04:29:07', '0000-00-00 00:00:00');
INSERT INTO `arus_stock_produk` (`id`, `tanggal`, `table_name`, `penjualan_id`, `penjualan_produk_id`, `distribusi_id`, `distribusi_detail_id`, `produksi_id`, `progress_produksi_id`, `stock_produk_id`, `produk_id`, `history_penyesuaian_produk_id`, `stock_in`, `stock_out`, `last_stock`, `last_stock_total`, `method`, `keterangan`, `created_at`, `updated_at`) VALUES
(787, '2020-03-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 149, 84, NULL, 0, 1, 1, 39411.1, 'update', 'Penjualan Produk', '2020-03-20 04:29:07', '0000-00-00 00:00:00'),
(788, '2020-03-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 154, 85, NULL, 0, 1, 0, 39410.1, 'update', 'Penjualan Produk', '2020-03-20 06:24:01', '0000-00-00 00:00:00'),
(789, '2020-03-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 148, 83, NULL, 0, 0.2, 33.4, 39409.9, 'update', 'Penjualan Produk', '2020-03-20 06:27:36', '0000-00-00 00:00:00'),
(790, '2020-03-20', 'stock_produk', NULL, NULL, NULL, NULL, NULL, NULL, 146, 82, NULL, 0, 20, 10, 39389.9, 'update', 'Penjualan Produk', '2020-03-20 06:28:45', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `assembly`
--

CREATE TABLE `assembly` (
  `assembly_id` int(11) NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assembly`
--

INSERT INTO `assembly` (`assembly_id`, `produk_id`, `created_at`, `updated_at`) VALUES
(2, 70, '2020-01-27 17:50:00', '2020-01-27 09:50:00'),
(3, 74, '2020-01-27 18:15:48', '2020-01-27 10:15:48'),
(4, 77, '2020-01-29 14:48:00', '2020-01-29 06:48:00'),
(5, 78, '2020-01-29 14:59:31', '2020-01-29 06:59:31'),
(6, 79, '2020-02-07 13:58:54', '2020-02-07 05:58:54'),
(8, 81, '2020-02-11 16:45:22', '2020-02-11 08:45:22'),
(9, 84, NULL, '2020-03-18 16:14:44');

-- --------------------------------------------------------

--
-- Table structure for table `assembly_item`
--

CREATE TABLE `assembly_item` (
  `assembly_item_id` int(11) NOT NULL,
  `assembly_id` int(11) DEFAULT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `jumlah` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `satuan_id_unit` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assembly_item`
--

INSERT INTO `assembly_item` (`assembly_item_id`, `assembly_id`, `produk_id`, `jumlah`, `unit_id`, `satuan_id_unit`, `created_at`, `updated_at`) VALUES
(1, 2, 67, 400, 0, 1, '2020-01-27 17:50:00', '2020-01-27 09:50:00'),
(4, 4, 76, 1, 0, 4, '2020-01-29 14:48:00', '2020-01-29 06:48:00'),
(5, 4, 70, 20, 8, 1, '2020-01-29 14:48:00', '2020-01-29 06:48:00'),
(6, 5, 76, 2, 0, 4, '2020-01-29 14:59:31', '2020-01-29 06:59:31'),
(28, 8, 76, 5, 0, 4, '2020-02-11 16:47:59', '2020-02-11 08:47:59'),
(29, 8, 75, 5, 0, 4, '2020-02-11 16:47:59', '2020-02-11 08:47:59'),
(30, 9, 83, 3, 0, 6, NULL, '2020-03-18 16:14:44'),
(31, 6, 75, 1, 0, 4, NULL, '2020-03-19 07:43:30'),
(32, 6, 73, 1, 0, 4, NULL, '2020-03-19 07:43:30');

-- --------------------------------------------------------

--
-- Table structure for table `bahan`
--

CREATE TABLE `bahan` (
  `bahan_id` int(11) NOT NULL,
  `bahan_jenis_id` int(11) DEFAULT NULL,
  `bahan_satuan_id` int(11) DEFAULT NULL,
  `bahan_suplier_id` int(11) DEFAULT NULL,
  `bahan_kode` varchar(10) DEFAULT NULL,
  `bahan_nama` varchar(50) DEFAULT NULL,
  `bahan_minimal_stock` int(11) DEFAULT NULL,
  `bahan_qty` bigint(20) DEFAULT '0',
  `bahan_last_stock` bigint(20) DEFAULT NULL,
  `bahan_harga` bigint(20) DEFAULT NULL,
  `bahan_keterangan` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



-- --------------------------------------------------------

--
-- Table structure for table `forget_request`
--

CREATE TABLE `forget_request` (
  `forget_request_id` int(11) NOT NULL,
  `forget_request_email` varchar(50) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `timeout` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `guest`
--

CREATE TABLE `guest` (
  `guest_id` int(11) NOT NULL,
  `guest_nama` varchar(50) DEFAULT NULL,
  `guest_alamat` varchar(50) DEFAULT NULL,
  `guest_telepon` varchar(50) DEFAULT NULL,
  `kewarganegaraan` varchar(50) DEFAULT NULL,
  `perusahaan` varchar(50) DEFAULT NULL,
  `lokasi_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guest`
--

INSERT INTO `guest` (`guest_id`, `guest_nama`, `guest_alamat`, `guest_telepon`, `kewarganegaraan`, `perusahaan`, `lokasi_id`, `tanggal`) VALUES
(1, 'Agus Arimbawa', 'Jalan Dalung Permai', '0987665421', 'Indonesia', 'Red System', 1, '2020-02-11 10:49:51'),
(2, 'Kadek Bondan Noviada', 'Dusun Ancak, Desa Bungkulan, Kecamatan Sawan', '087863210360', 'Indonesia', 'Red System', 1, '2020-02-11 10:58:29');

-- --------------------------------------------------------

--
-- Table structure for table `harga_produk`
--

CREATE TABLE `harga_produk` (
  `harga_produk_id` int(11) NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `minimal_pembelian` int(11) DEFAULT NULL,
  `harga` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `harga_produk`
--

INSERT INTO `harga_produk` (`harga_produk_id`, `produk_id`, `minimal_pembelian`, `harga`, `created_at`, `updated_at`) VALUES
(2, 82, 10, 900, '2020-02-11 17:05:23', '2020-02-11 09:05:23'),
(3, 82, 100, 800, '2020-02-11 17:05:45', '2020-02-11 09:05:45');

-- --------------------------------------------------------

--
-- Table structure for table `history_penyesuaian_produk`
--

CREATE TABLE `history_penyesuaian_produk` (
  `history_penyesuaian_produk_id` int(10) UNSIGNED NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `jenis_produk_id` int(11) DEFAULT NULL,
  `lokasi_id` int(11) DEFAULT NULL,
  `stock_produk_id` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `produk_kode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `produk_nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `produk_seri` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_produk_nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lokasi_nama` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty_awal` double DEFAULT NULL,
  `qty_akhir` double DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `history_penyesuaian_produk`
--

INSERT INTO `history_penyesuaian_produk` (`history_penyesuaian_produk_id`, `produk_id`, `jenis_produk_id`, `lokasi_id`, `stock_produk_id`, `tanggal`, `produk_kode`, `produk_nama`, `produk_seri`, `jenis_produk_nama`, `lokasi_nama`, `qty_awal`, `qty_akhir`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 2, 2, '2019-11-29', '01', 'Pupuk B-26', NULL, 'An-Organik', 'Gudang 2', 70, 60, 'Barang Rusak 10', '2019-11-29 02:33:05', '2019-11-29 02:33:05'),
(2, 59, 1, 1, 59, '2020-01-17', 'pd002', 'Produk B', NULL, 'Organik', 'Gudang', 100, 50, 'iseng aja', '2020-01-17 03:38:05', '2020-01-17 03:38:05'),
(3, 59, 1, 1, 59, '2020-01-17', 'pd002', 'Produk B', NULL, 'Organik', 'Gudang', 50, 100, 'ditemukan pada lemari', '2020-01-17 05:47:38', '2020-01-17 05:47:38'),
(4, 63, 1, 1, 65, '2020-01-17', 'pdass001', 'Produk Assembly AB', NULL, 'Organik', 'Gudang', 200, 100, 'salah input', '2020-01-17 07:41:48', '2020-01-17 07:41:48'),
(5, 67, 1, 1, 109, '2020-01-23', 'pd001', 'Produk Super A', NULL, 'Organik', 'Gudang', 500, 1000.5, 'salah input', '2020-01-23 06:14:47', '2020-01-23 06:14:47'),
(6, 67, 1, 1, 109, '2020-01-23', 'pd001', 'Produk Super A', NULL, 'Organik', 'Gudang', 1000.5, 9, 'salah input', '2020-01-23 06:53:12', '2020-01-23 06:53:12'),
(7, 67, 1, 2, 110, '2020-01-23', 'pd001', 'Produk Super A', NULL, 'Organik', 'Gudang 2', 1000.5, 0.23, 'salah input', '2020-01-23 06:53:33', '2020-01-23 06:53:33'),
(8, 67, 1, 2, 110, '2020-01-23', 'pd001', 'Produk Super A', NULL, 'Organik', 'Gudang 2', 0.23, 1.5, 'salah input', '2020-01-23 08:03:57', '2020-01-23 08:03:57'),
(9, 67, 1, 2, 110, '2020-01-23', 'pd001', 'Produk Super A', NULL, 'Organik', 'Gudang 2', 1.5, 99.5, 'salah input', '2020-01-23 08:10:54', '2020-01-23 08:10:54'),
(10, 75, 2, 1, 137, '2020-02-03', '010', 'Pensil', NULL, 'Plastik', 'Market', 6, 1, 'hilang', '2020-02-03 05:54:03', '2020-02-03 05:54:03');

-- --------------------------------------------------------

--
-- Table structure for table `history_transfer_produk`
--

CREATE TABLE `history_transfer_produk` (
  `history_transfer_produk_id` int(10) UNSIGNED NOT NULL,
  `tanggal` date DEFAULT NULL,
  `tanggal_konfirmasi` date DEFAULT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `stock_produk_id` int(11) DEFAULT NULL,
  `produk_kode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `produk_nama` text COLLATE utf8mb4_unicode_ci,
  `produk_seri` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `history_transfer_qty` double DEFAULT NULL,
  `qty_terima` double DEFAULT '0',
  `histori_lokasi_awal_id` int(11) DEFAULT NULL,
  `histori_lokasi_tujuan_id` int(11) DEFAULT NULL,
  `dari` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tujuan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_stock` double DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `history_transfer_produk`
--

INSERT INTO `history_transfer_produk` (`history_transfer_produk_id`, `tanggal`, `tanggal_konfirmasi`, `produk_id`, `stock_produk_id`, `produk_kode`, `produk_nama`, `produk_seri`, `history_transfer_qty`, `qty_terima`, `histori_lokasi_awal_id`, `histori_lokasi_tujuan_id`, `dari`, `tujuan`, `last_stock`, `status`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, '2019-11-29', '2019-11-29', 1, 1, '01', 'Pupuk B-26', '11192021', 70, 70, 1, 2, 'Gudang', 'Gudang 2', 129, 'Diterima Semua', 'Sampai', '2019-11-29 02:29:30', '2019-11-29 02:30:17'),
(2, '2020-01-23', NULL, 67, 109, 'pd001', 'Produk Super A', '01201022', 500.5, 0, 1, 2, 'Gudang', 'Gudang 2', 1500.5, 'Menunggu Konfirmasi', NULL, '2020-01-23 03:06:53', '2020-01-23 03:06:53'),
(3, '2020-01-23', '2020-01-23', 67, 108, 'pd001', 'Produk Super A', '01201022', 1000.5, 1000.5, 1, 2, 'Gudang', 'Gudang 2', 500, 'Diterima Semua', 'semua baik', '2020-01-23 03:57:32', '2020-01-23 04:00:13'),
(4, '2020-02-03', '2020-02-03', 75, 130, '010', 'Pensil', '0120002032', 6, 6, 2, 1, 'Gudang Penyimpanan', 'Market', 18, 'Diterima Sebagian', 'ok', '2020-02-03 05:52:40', '2020-02-03 05:52:58');

-- --------------------------------------------------------

--
-- Table structure for table `hutang`
--

CREATE TABLE `hutang` (
  `hutang_id` int(11) NOT NULL,
  `po_bahan_id` int(11) DEFAULT NULL,
  `po_produk_id` int(11) DEFAULT NULL,
  `tenggat_pelunasan` date DEFAULT NULL,
  `status_pembayaran` enum('Belum Lunas','Lunas') DEFAULT 'Belum Lunas',
  `status_hutang` enum('Buka','Tutup') DEFAULT 'Buka',
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hutang`
--

INSERT INTO `hutang` (`hutang_id`, `po_bahan_id`, `po_produk_id`, `tenggat_pelunasan`, `status_pembayaran`, `status_hutang`, `created_at`, `updated_at`) VALUES
(7, NULL, 8, '2020-03-19', 'Lunas', 'Tutup', '2020-03-19 12:14:53', '2020-03-19 06:58:40');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_produk`
--

CREATE TABLE `jenis_produk` (
  `jenis_produk_id` int(11) NOT NULL,
  `jenis_produk_kode` varchar(10) DEFAULT NULL,
  `jenis_produk_nama` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_produk`
--

INSERT INTO `jenis_produk` (`jenis_produk_id`, `jenis_produk_kode`, `jenis_produk_nama`, `created_at`, `updated_at`) VALUES
(1, '001', 'Sayur dan Buah', '2019-11-01 12:23:34', '2020-01-29 03:32:42'),
(2, '002', 'Plastik', '2019-11-01 12:23:47', '2020-01-29 03:32:32'),
(3, '003', 'Makanan', '2020-01-29 10:32:12', '2020-01-29 02:32:12'),
(4, '004', 'Minuman', '2020-01-29 10:32:18', '2020-01-29 02:32:18'),
(5, '005', 'ATK', '2020-02-07 13:55:53', '2020-02-07 05:55:53');

-- --------------------------------------------------------

--
-- Table structure for table `konversi_satuan`
--

CREATE TABLE `konversi_satuan` (
  `konversi_satuan_id` int(11) NOT NULL,
  `id_satuan_awal` int(11) DEFAULT NULL,
  `id_satuan_akhir` int(11) DEFAULT NULL,
  `jumlah_konversi` double DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konversi_satuan`
--

INSERT INTO `konversi_satuan` (`konversi_satuan_id`, `id_satuan_awal`, `id_satuan_akhir`, `jumlah_konversi`, `created_at`, `updated_at`) VALUES
(9, 4, 1, 100, '2019-11-05 17:43:45', '2019-11-05 17:44:56');

-- --------------------------------------------------------

--
-- Table structure for table `log_aktivitas`
--

CREATE TABLE `log_aktivitas` (
  `log_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `nama_menu` varchar(0) DEFAULT NULL,
  `aktivitas` text,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `log_kasir`
--

CREATE TABLE `log_kasir` (
  `log_kasir_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `waktu_buka` datetime DEFAULT NULL,
  `waktu_tutup` datetime DEFAULT NULL,
  `kas_awal` bigint(20) DEFAULT NULL,
  `kas_akhir` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_kasir`
--

INSERT INTO `log_kasir` (`log_kasir_id`, `user_id`, `waktu_buka`, `waktu_tutup`, `kas_awal`, `kas_akhir`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-11-01 12:52:37', NULL, 200000, NULL, '2019-11-01 12:52:37', NULL),
(2, 1, '2019-11-06 11:01:36', NULL, 300000, NULL, '2019-11-06 11:01:36', NULL),
(3, 1, '2019-11-25 13:17:51', NULL, 0, NULL, '2019-11-25 13:17:51', NULL),
(4, 1, '2019-12-02 15:34:38', NULL, 0, NULL, '2019-12-02 15:34:38', NULL),
(5, 1, '2020-01-08 09:54:15', NULL, 200000, NULL, '2020-01-08 09:54:15', NULL),
(6, 1, '2020-01-08 09:54:23', '2020-01-08 10:02:09', 200000, 400000, '2020-01-08 09:54:23', '2020-01-08 10:02:09'),
(7, 1, '2020-01-08 10:06:17', NULL, 200000, NULL, '2020-01-08 10:06:17', NULL),
(8, 1, '2020-01-08 10:06:17', NULL, 200000, NULL, '2020-01-08 10:06:17', NULL),
(9, 1, '2020-01-08 13:35:50', NULL, 100000, NULL, '2020-01-08 13:35:50', NULL),
(10, 1, '2020-01-09 09:43:15', NULL, 200000, NULL, '2020-01-09 09:43:15', NULL),
(11, 1, '2020-01-10 10:13:52', NULL, 200000, NULL, '2020-01-10 10:13:52', NULL),
(12, 1, '2020-01-13 14:42:46', '2020-01-13 14:54:12', 1000000, 1500000, '2020-01-13 14:42:46', '2020-01-13 14:54:12'),
(13, 1, '2020-01-13 14:54:33', '2020-01-13 14:55:32', 1000000, 1200000, '2020-01-13 14:54:33', '2020-01-13 14:55:32'),
(14, 1, '2020-01-15 10:46:20', NULL, 1000000, NULL, '2020-01-15 10:46:20', NULL),
(15, 1, '2020-01-17 15:48:03', NULL, 1000000, NULL, '2020-01-17 15:48:03', NULL),
(16, 1, '2020-01-17 15:48:06', NULL, 1000000, NULL, '2020-01-17 15:48:06', NULL),
(17, 1, '2020-01-20 09:46:05', '2020-01-20 18:46:14', 1000000, 2615000, '2020-01-20 09:46:05', '2020-01-20 18:46:14'),
(18, 1, '2020-01-20 18:47:07', NULL, 10000000, NULL, '2020-01-20 18:47:07', NULL),
(19, 1, '2020-01-21 10:12:34', NULL, 100000000, NULL, '2020-01-21 10:12:34', NULL),
(20, 1, '2020-01-22 10:10:45', NULL, 100000000000, NULL, '2020-01-22 10:10:45', NULL),
(21, 1, '2020-01-22 10:10:48', NULL, 100000000000, NULL, '2020-01-22 10:10:48', NULL),
(22, 1, '2020-01-23 17:05:51', NULL, 10000000, NULL, '2020-01-23 17:05:51', NULL),
(23, 1, '2020-01-27 18:45:32', '2020-01-27 18:45:44', 40000, 40000, '2020-01-27 18:45:32', '2020-01-27 18:45:44'),
(24, 1, '2020-01-27 18:45:55', '2020-01-27 18:46:00', 6, 6, '2020-01-27 18:45:55', '2020-01-27 18:46:00'),
(25, 1, '2020-01-27 18:55:49', NULL, 3000, NULL, '2020-01-27 18:55:49', NULL),
(26, 1, '2020-01-27 18:55:52', NULL, 0, NULL, '2020-01-27 18:55:52', NULL),
(27, 1, '2020-01-28 13:18:06', NULL, 0, NULL, '2020-01-28 13:18:06', NULL),
(28, 2, '2020-01-29 11:18:40', '2020-01-29 11:20:08', 0, 12000, '2020-01-29 11:18:40', '2020-01-29 11:20:08'),
(29, 1, '2020-01-29 11:54:22', NULL, 0, NULL, '2020-01-29 11:54:22', NULL),
(30, 1, '2020-01-29 14:43:03', '2020-01-29 14:45:23', 0, 0, '2020-01-29 14:43:03', '2020-01-29 14:45:23'),
(31, 1, '2020-01-29 15:31:32', NULL, 0, NULL, '2020-01-29 15:31:32', NULL),
(32, 1, '2020-02-03 14:11:06', '2020-02-03 14:12:18', 0, 9000, '2020-02-03 14:11:06', '2020-02-03 14:12:18'),
(33, 1, '2020-02-03 14:59:17', NULL, 0, NULL, '2020-02-03 14:59:17', NULL),
(34, 1, '2020-02-04 13:33:29', NULL, 0, NULL, '2020-02-04 13:33:29', NULL),
(35, 1, '2020-02-04 16:19:57', NULL, 0, NULL, '2020-02-04 16:19:57', NULL),
(36, 1, '2020-02-10 10:47:21', NULL, 0, NULL, '2020-02-10 10:47:21', NULL),
(37, 1, '2020-02-11 14:01:39', NULL, 0, NULL, '2020-02-11 14:01:39', NULL),
(38, 1, '2020-02-11 14:02:59', NULL, 0, NULL, '2020-02-11 14:02:59', NULL),
(39, 1, '2020-02-11 17:52:36', NULL, 0, NULL, '2020-02-11 17:52:36', NULL),
(40, 1, '2020-02-14 10:32:32', '2020-02-14 10:35:35', 0, 10000, '2020-02-14 10:32:32', '2020-02-14 10:35:35'),
(41, 1, '2020-03-12 15:01:39', NULL, 0, NULL, '2020-03-12 15:01:39', NULL),
(42, 1, '2020-03-19 07:58:01', NULL, 0, NULL, '2020-03-19 07:58:01', NULL),
(43, 1, '2020-03-19 10:44:07', NULL, 0, NULL, '2020-03-19 10:44:07', NULL),
(44, 1, '2020-03-20 10:30:40', NULL, 0, NULL, '2020-03-20 10:30:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lokasi`
--

CREATE TABLE `lokasi` (
  `lokasi_id` int(11) NOT NULL,
  `lokasi_kode` varchar(10) DEFAULT NULL,
  `lokasi_nama` varchar(50) DEFAULT NULL,
  `lokasi_tipe` varchar(20) DEFAULT NULL,
  `lokasi_alamat` text,
  `lokasi_telepon` varchar(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lokasi`
--

INSERT INTO `lokasi` (`lokasi_id`, `lokasi_kode`, `lokasi_nama`, `lokasi_tipe`, `lokasi_alamat`, `lokasi_telepon`, `created_at`, `updated_at`) VALUES
(1, '02', 'Market', 'Toko', 'Gang nusa kambanagan', '0361 223456', '2019-05-05 15:58:24', '2020-01-29 02:14:54'),
(2, '03', 'Gudang Penyimpanan', 'Gudang', 'Jalan gudang 2', '0987654', '2019-11-29 10:29:14', '2020-01-29 02:14:40');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL,
  `menu_kode` varchar(50) DEFAULT NULL,
  `menu_nama` varchar(50) DEFAULT NULL,
  `action` text,
  `url` varchar(100) DEFAULT NULL,
  `keterangan` text,
  `icon` varchar(50) DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menu_id`, `menu_kode`, `menu_nama`, `action`, `url`, `keterangan`, `icon`, `urutan`, `created_at`, `updated_at`) VALUES
(1, 'pos', 'POS', NULL, 'pos', NULL, 'cart', 1, '2019-06-18 03:13:50', '2019-06-17 19:13:50'),
(2, 'master_data', 'Master Data', NULL, '#', NULL, 'master', 3, '2019-06-18 03:13:50', '2019-06-17 19:13:50'),
(3, 'guest', 'Guest', 'list|add|edit|delete', 'guest', NULL, 'address-book', 4, '2019-06-18 03:13:50', '2019-06-17 19:13:50'),
(4, 'inventori', 'Inventori', '', '#', NULL, 'commode', 5, '2019-06-18 03:13:50', '2019-06-17 19:13:50'),
(5, 'produksi', 'produksi', '', '#', NULL, 'hammers', 7, '2019-06-18 03:13:50', '2019-10-10 08:00:10'),
(6, 'laporan', 'Laporan', NULL, '#', NULL, 'clipboards', 9, '2019-06-18 03:13:50', '2019-06-17 19:13:50'),
(7, 'dashboard', 'Dashboard', NULL, 'home', NULL, 'layers', 2, '2019-06-18 04:12:56', '2019-06-17 20:12:56'),
(8, 'order_produk', 'Order Produk', 'list|add|edit|delete|view', 'po-produk', NULL, 'order', 6, '2019-06-29 11:14:43', '2019-12-03 07:36:44'),
(9, 'hutang_piutang', 'Hutang/Piutang', NULL, '#', NULL, 'debt', 8, '2019-07-08 10:58:16', '2019-07-08 02:58:45');

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_hutang`
--

CREATE TABLE `pembayaran_hutang` (
  `pembayaran_hutang_id` int(11) NOT NULL,
  `hutang_id` int(11) DEFAULT NULL,
  `tipe_pembayaran_id` int(11) DEFAULT NULL,
  `jumlah` bigint(20) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `keterangan` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembayaran_hutang`
--

INSERT INTO `pembayaran_hutang` (`pembayaran_hutang_id`, `hutang_id`, `tipe_pembayaran_id`, `jumlah`, `tanggal`, `keterangan`, `created_at`, `updated_at`) VALUES
(3, 7, 6, 2000000, '2020-03-19', NULL, '2020-03-19 14:46:01', '2020-03-19 06:46:01');

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_piutang`
--

CREATE TABLE `pembayaran_piutang` (
  `pembayaran_piutang_id` int(11) NOT NULL,
  `piutang_id` int(11) DEFAULT NULL,
  `tipe_pembayaran_id` int(11) DEFAULT NULL,
  `jumlah` bigint(20) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `keterangan` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembayaran_piutang`
--

INSERT INTO `pembayaran_piutang` (`pembayaran_piutang_id`, `piutang_id`, `tipe_pembayaran_id`, `jumlah`, `tanggal`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 1, 6, 1000000, '2020-02-27', NULL, '2020-02-27 11:05:26', '2020-02-27 03:05:26'),
(2, 1, 6, 1000000, '2020-03-05', NULL, '2020-02-27 11:06:11', '2020-02-27 03:06:11'),
(3, 6, 6, 6000, '2020-03-19', NULL, '2020-03-19 11:49:52', '2020-03-19 03:49:52');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE `penjualan` (
  `penjualan_id` int(11) NOT NULL,
  `ekspedisi_id` int(11) DEFAULT NULL,
  `lokasi_id` int(11) DEFAULT NULL,
  `tipe_pembayaran_id` int(11) DEFAULT NULL,
  `log_kasir_id` int(11) DEFAULT NULL,
  `nama_pelanggan` varchar(50) DEFAULT NULL,
  `alamat_pelanggan` varchar(100) DEFAULT NULL,
  `telepon_pelanggan` varchar(20) DEFAULT NULL,
  `urutan` bigint(20) DEFAULT NULL,
  `no_faktur` varchar(100) DEFAULT NULL,
  `status_pembayaran` varchar(50) DEFAULT NULL,
  `status_distribusi` enum('not_yet','done') DEFAULT 'not_yet',
  `tanggal` date DEFAULT NULL,
  `jenis_transaksi` enum('retail','distributor') DEFAULT 'distributor',
  `pengiriman` enum('ya','tidak') DEFAULT NULL,
  `total` float DEFAULT NULL,
  `biaya_tambahan` float DEFAULT NULL,
  `potongan_akhir` float DEFAULT NULL,
  `grand_total` float DEFAULT NULL,
  `terbayar` float DEFAULT NULL,
  `sisa_pembayaran` float DEFAULT NULL,
  `kembalian` float DEFAULT NULL,
  `additional_no` varchar(50) DEFAULT NULL,
  `additional_tanggal` date DEFAULT NULL,
  `additional_keterangan` text,
  `status_distribusi_done_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan`
--

INSERT INTO `penjualan` (`penjualan_id`, `ekspedisi_id`, `lokasi_id`, `tipe_pembayaran_id`, `log_kasir_id`, `nama_pelanggan`, `alamat_pelanggan`, `telepon_pelanggan`, `urutan`, `no_faktur`, `status_pembayaran`, `status_distribusi`, `tanggal`, `jenis_transaksi`, `pengiriman`, `total`, `biaya_tambahan`, `potongan_akhir`, `grand_total`, `terbayar`, `sisa_pembayaran`, `kembalian`, `additional_no`, `additional_tanggal`, `additional_keterangan`, `status_distribusi_done_at`, `created_at`, `updated_at`) VALUES
(45, NULL, 1, 6, 44, 'Guest', '', '', 1, 'PL-200320/00/1', 'Lunas', 'not_yet', '2020-03-20', 'distributor', 'tidak', 66000, 0, 0, 66000, 70000, 0, 4000, '', '0000-00-00', '', NULL, NULL, '2020-03-20 04:06:34'),
(46, NULL, 1, 6, 44, 'Guest', '', '', 2, 'PL-200320/00/2', 'Lunas', 'not_yet', '2020-03-20', 'distributor', 'tidak', 146000, 0, 0, 146000, 150000, 0, 4000, '', '0000-00-00', '', NULL, NULL, '2020-03-20 04:29:07'),
(47, NULL, 2, 6, 44, 'Guest', '', '', 3, 'PL-200320/00/3', 'Lunas', 'not_yet', '2020-03-20', 'distributor', 'tidak', 6000, 0, 0, 6000, 10000, 0, 4000, '', '0000-00-00', '', NULL, NULL, '2020-03-20 06:24:01'),
(48, NULL, 1, 6, 44, 'Guest', '', '', 4, 'PL-200320/00/4', 'Lunas', 'not_yet', '2020-03-20', 'distributor', 'tidak', 5000, 0, 0, 5000, 5000, 0, 0, '', '0000-00-00', '', NULL, NULL, '2020-03-20 06:27:36'),
(49, NULL, 1, 6, 44, 'Guest', '', '', 5, 'PL-200320/00/5', 'Lunas', 'not_yet', '2020-03-20', 'distributor', 'tidak', 3000, 0, 0, 3000, 3000, 0, 0, '', '0000-00-00', '', NULL, NULL, '2020-03-20 06:28:45');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_produk`
--

CREATE TABLE `penjualan_produk` (
  `penjualan_produk_id` int(11) NOT NULL,
  `penjualan_id` int(11) DEFAULT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `satuan_id` int(11) NOT NULL,
  `satuan_nama` varchar(100) DEFAULT NULL,
  `produk_custom_id` int(11) DEFAULT NULL,
  `qty` double DEFAULT NULL,
  `harga` float DEFAULT NULL,
  `hpp` float DEFAULT NULL,
  `potongan` float DEFAULT NULL,
  `ppn_persen` float DEFAULT NULL,
  `ppn_nominal` float DEFAULT NULL,
  `harga_net` float DEFAULT NULL,
  `stock_produk_id` int(11) DEFAULT NULL,
  `sub_total` float DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan_produk`
--

INSERT INTO `penjualan_produk` (`penjualan_produk_id`, `penjualan_id`, `produk_id`, `satuan_id`, `satuan_nama`, `produk_custom_id`, `qty`, `harga`, `hpp`, `potongan`, `ppn_persen`, `ppn_nominal`, `harga_net`, `stock_produk_id`, `sub_total`, `created_at`, `updated_at`) VALUES
(45, 45, 85, 0, NULL, NULL, 11, 6000, 4000, NULL, 0.1, NULL, NULL, NULL, 66000, NULL, '2020-03-20 04:06:34'),
(46, 46, 85, 0, NULL, NULL, 1, 6000, 4000, NULL, 0.1, NULL, NULL, NULL, 6000, NULL, '2020-03-20 04:29:07'),
(47, 46, 83, 0, NULL, NULL, 1, 50000, 4850, NULL, 0.1, NULL, NULL, NULL, 50000, NULL, '2020-03-20 04:29:07'),
(48, 46, 84, 0, NULL, NULL, 1, 90000, 88500, NULL, 0.1, NULL, NULL, NULL, 90000, NULL, '2020-03-20 04:29:07'),
(49, 47, 85, 0, NULL, NULL, 1, 6000, 4000, NULL, 0.1, NULL, NULL, NULL, 6000, NULL, '2020-03-20 06:24:01'),
(50, 48, 83, 0, '', NULL, 1, 5000, 48500, NULL, 0.1, NULL, NULL, NULL, 5000, NULL, '2020-03-20 06:27:36'),
(51, 49, 82, 0, 'Paket', NULL, 1, 3000, 850, NULL, 0.1, NULL, NULL, NULL, 3000, NULL, '2020-03-20 06:28:45');

-- --------------------------------------------------------

--
-- Table structure for table `piutang`
--

CREATE TABLE `piutang` (
  `piutang_id` int(11) NOT NULL,
  `penjualan_id` int(11) DEFAULT NULL,
  `tenggat_pelunasan` date DEFAULT NULL,
  `status_pembayaran` enum('Belum Lunas','Lunas') DEFAULT 'Belum Lunas',
  `keterangan` text,
  `status_piutang` enum('Buka','Tutup') DEFAULT 'Buka',
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `piutang`
--

INSERT INTO `piutang` (`piutang_id`, `penjualan_id`, `tenggat_pelunasan`, `status_pembayaran`, `keterangan`, `status_piutang`, `created_at`, `updated_at`) VALUES
(1, 4, '2020-02-08', 'Lunas', NULL, 'Tutup', '2020-02-11 17:53:53', '2020-02-27 05:49:47'),
(2, 30, '2020-02-24', 'Belum Lunas', NULL, 'Buka', '2020-02-27 17:39:33', '2020-02-27 09:39:33'),
(3, 31, '2020-03-20', 'Belum Lunas', NULL, 'Buka', '2020-02-27 17:39:57', '2020-02-27 09:39:57'),
(4, 32, '2020-02-28', 'Belum Lunas', NULL, 'Buka', '2020-02-27 18:08:17', '2020-02-27 10:08:17'),
(5, 33, '2020-02-27', 'Belum Lunas', NULL, 'Buka', '2020-02-27 18:10:23', '2020-02-27 10:10:23'),
(6, 34, '2020-03-01', 'Lunas', NULL, 'Tutup', '2020-02-27 18:11:19', '2020-03-19 03:50:07');

-- --------------------------------------------------------

--
-- Table structure for table `po_bahan`
--

CREATE TABLE `po_bahan` (
  `po_bahan_id` int(11) NOT NULL,
  `po_bahan_no` varchar(50) DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `suplier_id` int(11) DEFAULT NULL,
  `tipe_pembayaran` int(11) DEFAULT NULL,
  `tipe_pembayaran_nama` varchar(50) DEFAULT NULL,
  `tipe_pembayaran_no` varchar(50) DEFAULT NULL,
  `tipe_pembayaran_keterangan` varchar(100) DEFAULT NULL,
  `pengiriman` varchar(50) DEFAULT NULL,
  `tanggal_penerimaan` date DEFAULT NULL,
  `tanggal_pemesanan` date DEFAULT NULL,
  `total` bigint(20) DEFAULT '0',
  `tambahan` bigint(20) DEFAULT '0',
  `potongan` bigint(20) DEFAULT '0',
  `grand_total` bigint(20) DEFAULT '0',
  `status_pembayaran` varchar(50) DEFAULT NULL,
  `jenis_pembayaran` varchar(50) DEFAULT NULL,
  `lokasi_penerimaan_id` int(11) DEFAULT NULL,
  `status_penerimaan` varchar(50) DEFAULT NULL,
  `keterangan` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `po_bahan_detail`
--

CREATE TABLE `po_bahan_detail` (
  `po_bahan_detail_id` int(11) NOT NULL,
  `po_bahan_id` int(11) DEFAULT NULL,
  `bahan_id` int(11) DEFAULT NULL,
  `jumlah` bigint(20) DEFAULT NULL,
  `harga` bigint(20) DEFAULT NULL,
  `tambahan` bigint(20) DEFAULT '0',
  `potongan` bigint(20) DEFAULT '0',
  `sub_total` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `po_produk`
--

CREATE TABLE `po_produk` (
  `po_produk_id` int(11) NOT NULL,
  `po_produk_no` varchar(50) DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `suplier_id` int(11) DEFAULT NULL,
  `tipe_pembayaran` int(11) DEFAULT NULL,
  `tipe_pembayaran_nama` varchar(50) DEFAULT NULL,
  `tipe_pembayaran_no` varchar(50) DEFAULT NULL,
  `tipe_pembayaran_keterangan` varchar(100) DEFAULT NULL,
  `pengiriman` varchar(50) DEFAULT NULL,
  `tanggal_penerimaan` date DEFAULT NULL,
  `tanggal_pemesanan` date DEFAULT NULL,
  `total` bigint(20) DEFAULT '0',
  `tambahan` bigint(20) DEFAULT '0',
  `potongan` bigint(20) DEFAULT '0',
  `grand_total` bigint(20) DEFAULT '0',
  `status_pembayaran` varchar(50) DEFAULT NULL,
  `jenis_pembayaran` varchar(50) DEFAULT NULL,
  `lokasi_penerimaan_id` int(11) DEFAULT NULL,
  `status_penerimaan` varchar(50) DEFAULT NULL,
  `keterangan` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `po_produk`
--

INSERT INTO `po_produk` (`po_produk_id`, `po_produk_no`, `urutan`, `suplier_id`, `tipe_pembayaran`, `tipe_pembayaran_nama`, `tipe_pembayaran_no`, `tipe_pembayaran_keterangan`, `pengiriman`, `tanggal_penerimaan`, `tanggal_pemesanan`, `total`, `tambahan`, `potongan`, `grand_total`, `status_pembayaran`, `jenis_pembayaran`, `lokasi_penerimaan_id`, `status_penerimaan`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 'POB/1219/1', 1, 1, 6, NULL, '0987', 'order', NULL, '2019-12-03', '2019-12-03', 2000000, 50000, 20000, 1980000, 'Lunas', 'kas', 2, 'Diterima', 'order produk', '2019-12-03 15:41:43', '2019-12-03 09:42:11'),
(2, 'POB/1219/2', 2, 1, 6, NULL, '', '', NULL, '2019-12-04', '2019-12-04', 1000000, 0, 0, 1000000, 'Lunas', 'kas', 2, 'Diterima', 'order produk', '2019-12-04 10:33:09', '2019-12-04 02:34:40'),
(4, 'POB/1219/3', 3, 1, 6, NULL, '0987', 'order', NULL, NULL, '2019-12-04', 1000000, 0, 0, 1000000, 'Lunas', 'kas', NULL, 'Belum Diterima', 'order produk', '2019-12-04 10:42:54', '2019-12-04 02:42:54'),
(5, 'POB/0220/1', 1, 2, 6, NULL, '', '', NULL, NULL, '2020-02-12', 100000, 0, 0, 100000, 'Lunas', 'kas', NULL, 'Belum Diterima', '0', '2020-02-12 09:34:46', '2020-02-12 01:34:46'),
(8, 'POB/0320/1', 1, 1, NULL, 'Kredit', NULL, NULL, NULL, NULL, '2020-03-19', 2000000, 0, 0, 2000000, 'Lunas', 'kredit', NULL, 'Belum Diterima', '', NULL, '2020-03-19 06:46:01');

-- --------------------------------------------------------

--
-- Table structure for table `po_produk_detail`
--

CREATE TABLE `po_produk_detail` (
  `po_produk_detail_id` int(11) NOT NULL,
  `po_produk_id` int(11) DEFAULT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `jumlah` double DEFAULT NULL,
  `harga` bigint(20) DEFAULT NULL,
  `tambahan` bigint(20) DEFAULT '0',
  `potongan` bigint(20) DEFAULT '0',
  `sub_total` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `po_produk_detail`
--

INSERT INTO `po_produk_detail` (`po_produk_detail_id`, `po_produk_id`, `produk_id`, `jumlah`, `harga`, `tambahan`, `potongan`, `sub_total`, `created_at`, `updated_at`) VALUES
(2, 1, 2, 10, 100000, 0, 0, 1000000, '2019-12-03 16:00:04', '2019-12-03 08:00:04'),
(3, 1, 1, 10, 100000, 0, 0, 1000000, '2019-12-03 16:00:04', '2019-12-03 08:00:04'),
(4, 2, 1, 10, 100000, 0, 0, 1000000, '2019-12-04 10:33:09', '2019-12-04 02:33:09'),
(6, 4, 1, 10, 100000, 0, 0, 1000000, '2019-12-04 10:42:54', '2019-12-04 02:42:54'),
(7, 5, 77, 10, 10000, 0, 0, 100000, '2020-02-12 09:34:46', '2020-02-12 01:34:46'),
(10, 8, 82, 5000, 400, 0, 0, 2000000, NULL, '2020-03-19 04:14:53');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `produk_id` int(11) NOT NULL,
  `barcode` varchar(100) DEFAULT NULL,
  `produk_nama` varchar(100) DEFAULT NULL,
  `produk_satuan_id` int(11) DEFAULT NULL,
  `produk_jenis_id` int(11) NOT NULL,
  `produk_kode` varchar(100) NOT NULL,
  `fabric_id` int(11) DEFAULT NULL,
  `patern_id` int(11) DEFAULT NULL,
  `color_id` int(11) DEFAULT NULL,
  `size_id` int(11) DEFAULT NULL,
  `bed_size_id` int(11) DEFAULT NULL,
  `good_size_id` int(11) DEFAULT NULL,
  `weight_pc` int(11) DEFAULT NULL,
  `weight_m` int(11) DEFAULT NULL,
  `style` varchar(100) DEFAULT NULL,
  `file` varchar(100) DEFAULT NULL,
  `harga_eceran` double DEFAULT '0',
  `harga_grosir` double DEFAULT '0',
  `harga_konsinyasi` double DEFAULT '0',
  `produk_minimal_stock` bigint(20) NOT NULL,
  `hpp_global` double DEFAULT '0',
  `disc_persen` double NOT NULL DEFAULT '0',
  `disc_nominal` bigint(20) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`produk_id`, `barcode`, `produk_nama`, `produk_satuan_id`, `produk_jenis_id`, `produk_kode`, `fabric_id`, `patern_id`, `color_id`, `size_id`, `bed_size_id`, `good_size_id`, `weight_pc`, `weight_m`, `style`, `file`, `harga_eceran`, `harga_grosir`, `harga_konsinyasi`, `produk_minimal_stock`, `hpp_global`, `disc_persen`, `disc_nominal`, `created_at`, `updated_at`) VALUES
(67, '12121244', 'Produk Super A', 1, 1, '009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10000, 0, 0, 100, 9800, 0, 0, '2020-01-22 17:30:34', '2020-03-20 04:26:30'),
(68, '44433121', 'Produk Super B', 1, 1, 'pd002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10000, 0, 0, 100, 9800, 0, 0, '2020-01-27 11:02:23', '2020-03-20 04:26:36'),
(70, '22321134', 'beras', 3, 1, '001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30000, 0, 0, 1, 28000, 0, 0, '2020-01-27 17:50:00', '2020-03-20 04:26:42'),
(71, '22345332', 'Pensil', 4, 2, '002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1000, 0, 0, 0, 750, 0, 0, '2020-01-27 18:08:08', '2020-03-20 04:26:47'),
(72, '66332232', 'Shampo', 4, 2, '005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3000, 0, 0, 0, 2750, 0, 0, '2020-01-27 18:13:40', '2020-03-20 04:26:57'),
(73, '332211244', 'Sabun', 4, 2, '008', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6000, 0, 0, 0, 5700, 0, 0, '2020-01-27 18:14:06', '2020-03-20 04:27:02'),
(74, '21245522', 'Paket Hemat Sabun ', 6, 2, '009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8000, 0, 0, 0, 7400, 0, 0, '2020-01-27 18:15:48', '2020-03-20 04:27:07'),
(75, '443321145', 'Pensil', 4, 2, '010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2000, 0, 0, 0, 1850, 0, 0, '2020-01-29 10:48:18', '2020-03-20 04:27:13'),
(76, '2120998088', 'Pulpen', 4, 2, '011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2000, 0, 0, 10, 1200, 0, 0, '2020-01-29 14:28:41', '2020-03-20 02:16:49'),
(77, '2233455242', 'paket tahun baru', 6, 3, '012', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18000, 0, 0, 0, 17000, 0, 0, '2020-01-29 14:48:00', '2020-03-20 04:27:17'),
(78, '22342240098', 'Paket Baru Banget', 6, 2, '013', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30000, 0, 0, 1, 29500, 0, 0, '2020-01-29 14:59:31', '2020-03-20 04:27:28'),
(79, '2003976278', 'Paket Hemat Pulpen Pensil', 6, 5, 'pahepulpen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50000, 0, 0, 100, 49000, 0, 0, '2020-02-07 13:58:54', '2020-03-20 04:27:38'),
(81, '4468229099', 'Paket Chiki', 6, 3, 'pdassm001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 20000, 0, 0, 10, 19000, 0, 0, '2020-02-11 16:45:22', '2020-03-20 04:27:43'),
(82, '4432256789', 'Aqua', 4, 4, 'aqua89', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1000, 0, 0, 10, 850, 0, 0, '2020-02-11 16:55:24', '2020-03-20 04:27:47'),
(83, '33448829280', 'proweb', 6, 5, 'proweb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50000, 0, 0, 10, 48500, 0, 0, NULL, '2020-03-20 04:41:59'),
(84, '309986288377', 'Paket web', 6, 5, 'prowebpaket', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 90000, 0, 0, 20, 88500, 0, 0, NULL, '2020-03-20 04:28:09'),
(85, '12323444222', 'sample', 4, 5, '12123332', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6000, 0, 0, 2, 4000, 0, 0, NULL, '2020-03-20 02:16:38');

-- --------------------------------------------------------

--
-- Table structure for table `satuan`
--

CREATE TABLE `satuan` (
  `satuan_id` int(11) NOT NULL,
  `satuan_kode` varchar(10) DEFAULT NULL,
  `satuan_nama` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `satuan`
--

INSERT INTO `satuan` (`satuan_id`, `satuan_kode`, `satuan_nama`, `created_at`, `updated_at`) VALUES
(1, 'G', 'Gram', '2019-11-01 12:19:23', '2019-11-01 04:19:23'),
(2, 'KG', 'Kilo Gram', '2019-11-01 12:19:36', '2019-11-01 04:19:36'),
(3, 'SK', 'Sak', '2019-11-01 12:19:48', '2019-11-01 04:19:48'),
(4, 'PCS', 'Pieces', '2019-11-01 12:22:42', '2019-11-01 04:22:42'),
(5, '007', 'Ikat', '2020-01-27 18:08:56', '2020-01-27 10:08:56'),
(6, '009', 'Paket', '2020-01-27 18:14:44', '2020-01-27 10:14:44');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `staff_id` int(11) NOT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `staff_nama` varchar(100) DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `staff_alamat` text,
  `staff_status` varchar(35) DEFAULT NULL,
  `staff_kelamin` varchar(15) DEFAULT NULL,
  `mulai_bekerja` date DEFAULT NULL,
  `staff_email` varchar(50) DEFAULT NULL,
  `staff_phone_number` varchar(15) DEFAULT NULL,
  `staff_keterangan` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`staff_id`, `nik`, `staff_nama`, `tempat_lahir`, `tanggal_lahir`, `staff_alamat`, `staff_status`, `staff_kelamin`, `mulai_bekerja`, `staff_email`, `staff_phone_number`, `staff_keterangan`, `created_at`, `updated_at`) VALUES
(1, '1909807889237', 'Astradanta', 'Denpasar', '1970-01-01', 'Gang nuri 2 no 9 Banjarangkan Klungkung', 'Staff', 'Laki-laki', '2019-04-18', 'astra.danta@gmail.com', '085792078364', '', '2019-04-30 14:29:14', '2020-01-29 02:36:24'),
(2, '1234567890', 'Kadek Bondan Noviada', 'Buleleng', '2020-01-30', 'Dalung, Denpasar Utara', 'Kontrak', 'Laki-laki', '2020-01-29', 'noviadarkbondan@gmail.com', '087863210360', 'ok', '2020-01-29 10:13:20', '2020-01-29 02:13:20');

-- --------------------------------------------------------

--
-- Stand-in structure for view `staff_lama_kerja`
-- (See below for the actual view)
--
CREATE TABLE `staff_lama_kerja` (
`staff_id` int(11)
,`lama_bekerja` varchar(30)
);

-- --------------------------------------------------------

--
-- Table structure for table `stock_produk`
--

CREATE TABLE `stock_produk` (
  `stock_produk_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `stock_produk_lokasi_id` int(11) NOT NULL,
  `month` char(4) DEFAULT NULL,
  `year` char(4) DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `stock_produk_seri` varchar(100) NOT NULL,
  `stock_produk_qty` double NOT NULL,
  `hpp` bigint(20) DEFAULT NULL,
  `last_stok` double DEFAULT NULL,
  `stock_produk_keterangan` text,
  `delete_flag` tinyint(4) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_produk`
--

INSERT INTO `stock_produk` (`stock_produk_id`, `produk_id`, `stock_produk_lokasi_id`, `month`, `year`, `urutan`, `stock_produk_seri`, `stock_produk_qty`, `hpp`, `last_stok`, `stock_produk_keterangan`, `delete_flag`, `created_at`, `updated_at`) VALUES
(106, 67, 1, '01', '20', 1, '01201021', 1, 0, NULL, '', 1, '2020-01-22 17:31:09', '2020-01-23 02:12:15'),
(107, 67, 1, '01', '20', 2, '01201022', 1, 0, NULL, '', 1, '2020-01-22 17:35:54', '2020-01-23 02:12:12'),
(108, 67, 1, '01', '20', 2, '01201022', 0, 0, NULL, '', 1, '2020-01-23 10:25:24', '2020-01-27 09:52:49'),
(109, 67, 1, '01', '20', 2, '01201022', 9, 0, NULL, '', 1, '2020-01-23 10:26:37', '2020-01-27 09:52:49'),
(110, 67, 2, NULL, NULL, NULL, '01201022', 99.5, NULL, NULL, NULL, 1, '2020-01-23 12:00:13', '2020-01-23 09:31:25'),
(111, 67, 1, '01', '20', 2, '01201022', 30, 0, NULL, '', 1, '2020-01-23 16:05:08', '2020-01-27 09:52:49'),
(112, 67, 2, '01', '20', 1, '01201031', 30, 0, NULL, '', 0, '2020-01-23 17:31:36', '2020-01-23 09:31:36'),
(113, 67, 1, '01', '20', 2, '01201022', 100, 0, NULL, '', 1, '2020-01-23 17:32:10', '2020-01-27 07:28:52'),
(114, 68, 1, '01', '20', 2, '01201022', 50, 0, NULL, '', 0, '2020-01-27 11:03:02', '2020-01-27 03:03:02'),
(115, 68, 1, '01', '20', 2, '01201022', 10000, 0, NULL, '', 0, '2020-01-27 17:51:27', '2020-01-27 09:51:27'),
(116, 67, 1, '01', '20', 2, '01201022', 10000, 0, NULL, '', 1, '2020-01-27 17:52:33', '2020-01-27 09:52:49'),
(117, 67, 1, '01', '20', 2, '01201022', 9239, 30000, NULL, '', 1, '2020-01-27 17:52:49', '2020-01-27 10:02:40'),
(118, 70, 1, '01', '20', 2, '01201022', 0, 30000, NULL, '', 1, '2020-01-27 17:52:49', '2020-01-29 06:55:02'),
(119, 67, 1, '01', '20', 2, '01201022', 8439, 0, NULL, '', 0, '2020-01-27 18:02:40', '2020-01-27 10:02:40'),
(120, 70, 1, '01', '20', 2, '01201022', 2, 0, NULL, '', 1, '2020-01-27 18:02:40', '2020-01-29 06:55:02'),
(121, 71, 1, '01', '20', 1, '01202021', 1, 9000, NULL, '', 0, '2020-01-27 18:08:29', '2020-01-27 10:11:07'),
(122, 73, 1, '01', '20', 2, '01202022', 20, 3000, NULL, '', 1, '2020-01-27 18:16:18', '2020-01-27 10:18:43'),
(123, 72, 1, '01', '20', 2, '01202022', 100, 4000, NULL, '', 1, '2020-01-27 18:17:23', '2020-01-27 10:18:43'),
(124, 72, 1, '01', '20', 2, '01202022', 91, 8000, NULL, '', 0, '2020-01-27 18:18:43', '2020-02-03 05:11:32'),
(125, 73, 1, '01', '20', 2, '01202022', 9, 8000, NULL, '', 1, '2020-01-27 18:18:43', '2020-02-11 03:33:14'),
(126, 73, 1, '01', '20', 2, '01202022', 6, 0, NULL, '', 1, '2020-01-27 18:18:43', '2020-01-27 10:21:29'),
(127, 72, 1, '01', '20', 2, '01202022', 4, 0, NULL, '', 0, '2020-01-27 18:21:29', '2020-01-27 10:21:29'),
(128, 73, 1, '01', '20', 2, '01202022', 6, 0, NULL, '', 1, '2020-01-27 18:21:29', '2020-03-19 07:44:18'),
(129, 75, 2, '01', '20', 1, '0120002031', 12, 1700, NULL, '', 0, '2020-01-29 10:51:13', '2020-01-29 02:51:13'),
(130, 75, 2, '01', '20', 2, '0120002032', 6, 1700, NULL, '', 0, '2020-01-29 10:52:31', '2020-02-03 05:52:40'),
(131, 76, 1, '01', '20', 1, '0120002021', 4, 1500, NULL, '', 1, '2020-01-29 14:29:16', '2020-01-29 06:55:02'),
(132, 70, 1, '01', '20', 1, '0120001021', 0.4, 30000, NULL, '', 0, '2020-01-29 14:55:02', '2020-01-29 06:55:02'),
(133, 76, 1, '01', '20', 2, '0120002022', 2, 30000, NULL, '', 1, '2020-01-29 14:55:02', '2020-01-29 06:59:42'),
(134, 77, 1, '01', '20', 1, '0120003021', 2, 30000, NULL, '', 0, '2020-01-29 14:55:02', '2020-01-29 06:55:02'),
(135, 76, 1, '01', '20', 2, '0120002022', 0, 0, NULL, '', 1, '2020-01-29 14:59:42', '2020-02-11 03:33:14'),
(136, 78, 1, '01', '20', 2, '0120002022', 0, 0, NULL, '', 0, '2020-01-29 14:59:42', '2020-02-10 01:47:56'),
(137, 75, 1, NULL, NULL, NULL, '0120002032', 1, NULL, NULL, NULL, 1, '2020-02-03 13:52:58', '2020-03-19 07:44:18'),
(138, 71, 1, '02', '20', 1, '0220002021', 6, 5000, NULL, '', 0, '2020-02-11 11:26:25', '2020-02-11 03:26:25'),
(139, 76, 1, '02', '20', 2, '0220002022', 7, 0, NULL, '', 1, '2020-02-11 11:32:46', '2020-02-11 03:33:14'),
(140, 76, 1, '02', '20', 2, '0220002022', 1, 20000, NULL, '', 0, '2020-02-11 11:33:14', '2020-02-11 03:33:14'),
(141, 73, 1, '02', '20', 2, '0220002022', 5, 20000, NULL, '', 0, '2020-02-11 11:33:14', '2020-02-11 03:33:14'),
(143, 76, 1, '02', '20', 2, '0220002022', 2, 0, NULL, '', 0, '2020-02-11 12:58:42', '2020-02-11 05:03:53'),
(144, 73, 1, '02', '20', 2, '0220002022', 4, 0, NULL, '', 0, '2020-02-11 12:58:42', '2020-02-11 04:58:42'),
(145, 76, 1, '02', '20', 2, '0220002022', 1000, 0, NULL, '', 0, '2020-02-11 16:47:24', '2020-02-11 08:47:24'),
(146, 82, 1, '02', '20', 1, '0220004021', 10, 1000, NULL, '', 0, '2020-02-11 16:55:53', '2020-03-20 06:28:45'),
(147, 83, 1, '03', '20', 1, '0320005021', 49.6, 40000, NULL, 'ok', 1, NULL, '2020-03-18 16:15:46'),
(148, 83, 1, '03', '20', 2, '0320005022', 33.4, 8000, NULL, '', 0, NULL, '2020-03-20 06:27:36'),
(149, 84, 1, '03', '20', 2, '0320005022', 1, 8000, NULL, '', 0, NULL, '2020-03-20 04:29:07'),
(150, 75, 1, '03', '20', 1, '0320002021', 0, 5000, NULL, '', 0, NULL, '2020-03-19 07:44:18'),
(151, 73, 1, '03', '20', 2, '0320002022', 5, 5000, NULL, '', 0, NULL, '2020-03-19 07:44:18'),
(152, 79, 1, '03', '20', 2, '0320005022', 1, 5000, NULL, '', 0, NULL, '2020-03-19 07:44:18'),
(153, 85, 1, '03', '20', 2, '0320005022', 0, 0, NULL, '', 0, NULL, '2020-03-20 04:06:34'),
(154, 85, 2, '03', '20', 1, '0320005031', 0, 0, NULL, '', 0, NULL, '2020-03-20 06:24:01'),
(155, 85, 1, '03', '20', 2, '0320005022', 0, 0, NULL, '', 0, NULL, '2020-03-20 04:06:34'),
(156, 85, 1, '03', '20', 2, '0320005022', 0, 0, NULL, '', 0, NULL, '2020-03-20 04:29:07');

-- --------------------------------------------------------

--
-- Table structure for table `sub_menu`
--

CREATE TABLE `sub_menu` (
  `sub_menu_id` int(11) NOT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `sub_menu_kode` varchar(50) DEFAULT NULL,
  `sub_menu_nama` varchar(50) DEFAULT NULL,
  `action` text,
  `data` text,
  `url` varchar(100) DEFAULT NULL,
  `keterangan` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_menu`
--

INSERT INTO `sub_menu` (`sub_menu_id`, `menu_id`, `sub_menu_kode`, `sub_menu_nama`, `action`, `data`, `url`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 1, 'penjualan', 'POS Reguler', 'delete|view', NULL, 'pos', NULL, '2019-06-18 03:17:08', '2019-10-26 03:57:03'),
(3, 2, 'suplier', 'Suplier', 'list|add|edit|delete|view', NULL, 'suplier', NULL, '2019-06-18 03:25:43', '2019-06-17 19:25:43'),
(4, 2, 'satuan', 'Satuan', 'list|add|edit|delete|view|konversi_satuan', NULL, 'satuan', NULL, '2019-06-18 03:25:43', '2019-11-05 06:58:08'),
(5, 2, 'jenis-produk', 'Jenis Produk', 'list|add|edit|delete|view', NULL, 'jenis-produk', NULL, '2019-06-18 03:25:43', '2019-06-17 19:25:43'),
(7, 2, 'lokasi', 'Lokasi', 'list|add|edit|delete|view', NULL, 'lokasi', NULL, '2019-06-18 03:25:43', '2019-06-17 19:25:43'),
(8, 2, 'user', 'User', 'list|add|edit|delete|view', NULL, 'user', NULL, '2019-06-18 03:25:43', '2019-06-17 19:25:43'),
(9, 2, 'user-role', 'User Role', '', NULL, 'user-role', NULL, '2019-06-18 03:25:43', '2019-06-17 19:25:43'),
(10, 2, 'tipe-pembayaran', 'Tipe Pembayaran', 'list|add|edit|delete|view', NULL, 'tipe-pembayaran', NULL, '2019-06-18 03:25:43', '2019-06-17 19:25:43'),
(11, 2, 'staff', 'Staf', 'list|add|edit|delete|view', NULL, 'staff', NULL, '2019-06-18 03:25:43', '2019-06-17 19:25:43'),
(13, 4, 'produk', 'Produk', 'list|add|edit|delete|view|stock|price|unit|assembly', 'hpp', 'produk', NULL, '2019-06-18 03:51:52', '2019-11-08 03:06:58'),
(14, 6, 'produk-by-location', 'Produk per Lokasi', 'list', NULL, 'produk-by-location', NULL, '2019-06-18 03:51:52', '2019-06-17 19:51:52'),
(16, 6, 'bahan-by-location', 'Bahan per Lokasi', 'list', NULL, 'bahan-by-location', NULL, '2019-06-28 15:26:43', '2019-06-28 07:26:43'),
(18, 4, 'transfer-produk', 'Transfer Stok Produk', 'list|transfer_produk|konfirmasi_transfer_produk|history_transfer_produk', NULL, 'transfer-produk', NULL, '2019-06-18 03:51:52', '2019-06-17 19:51:52'),
(20, 4, 'penyesuaian-produk', 'Penyesuaian Stok Produk', 'list|pesnyesuaian_produk|history_penyesuaian_produk', NULL, 'penyesuaian-produk', NULL, '2019-06-18 03:51:52', '2019-06-17 19:51:52'),
(22, 4, 'low-stock-produk', 'Low Stock Produk', 'list', NULL, 'low-stock-produk', NULL, '2019-06-18 03:51:52', '2019-06-17 19:51:52'),
(24, 4, 'kartu-stock-produk', 'Kartu Stok Produk', 'list', NULL, 'kartu-stock-produk', NULL, '2019-06-18 03:51:52', '2019-06-17 19:51:52'),
(26, 6, 'laporan-stock-produk', 'Stok Produk', 'list', NULL, 'laporan/laporan-stock-produk', NULL, '2019-06-18 04:00:08', '2019-06-17 20:00:08'),
(28, 6, 'laporan-penjualan', 'Penjualan', 'list', NULL, 'laporan/laporan-penjualan', NULL, '2019-06-18 04:00:08', '2019-06-17 20:00:08'),
(29, 6, 'laporan-produksi', 'Produksi', 'list', NULL, 'laporan/laporan-produksi', NULL, '2019-06-18 04:00:08', '2019-06-17 20:00:08'),
(30, 6, 'laporan-hutang', 'Hutang', 'list', NULL, 'laporan/laporan-hutang', NULL, '2019-06-18 04:00:08', '2019-06-17 20:00:08'),
(31, 6, 'laporan-piutang', 'Piutang', 'list', NULL, 'laporan/laporan-piutang', NULL, '2019-06-18 04:00:08', '2019-06-17 20:00:08'),
(34, 9, 'hutang', 'Hutang', 'list|pembayaran|posting', NULL, 'hutang', NULL, '2019-07-08 11:19:09', '2020-03-19 03:11:04'),
(35, 9, 'piutang', 'Piutang', 'list|pembayaran|posting', NULL, 'piutang', NULL, '2019-07-08 11:19:18', '2020-03-19 03:11:06'),
(36, 6, 'laporan-staff', 'Staff', 'list', NULL, 'laporan/laporan-staff', NULL, '2019-07-18 16:18:06', '2019-07-18 08:18:06'),
(37, 6, 'laporan-customer', 'Customer', 'list', NULL, 'laporan/laporan-customer', NULL, '2019-07-18 16:18:42', '2019-07-18 08:18:42'),
(38, 6, 'laporan-po-produk', 'Order Produk', 'list', NULL, 'laporan/laporan-po-produk', NULL, '2020-02-12 17:26:10', '2020-02-12 09:26:10');

-- --------------------------------------------------------

--
-- Table structure for table `suplier`
--

CREATE TABLE `suplier` (
  `suplier_id` int(11) NOT NULL,
  `suplier_kode` varchar(10) DEFAULT NULL,
  `suplier_nama` varchar(100) DEFAULT NULL,
  `suplier_alamat` text,
  `suplier_telepon` varchar(20) DEFAULT NULL,
  `suplier_email` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suplier`
--

INSERT INTO `suplier` (`suplier_id`, `suplier_kode`, `suplier_nama`, `suplier_alamat`, `suplier_telepon`, `suplier_email`, `created_at`, `updated_at`) VALUES
(1, '001-supfff', 'Suplier A', 'Jalan Ratna', '08917771777', 'sup-a@gmail.com', '2019-11-25 13:00:54', '2019-11-25 05:01:07'),
(2, NULL, 'tsubasa amami', NULL, NULL, NULL, '2020-02-12 09:34:46', '2020-02-12 01:34:46');

-- --------------------------------------------------------

--
-- Table structure for table `tipe_pembayaran`
--

CREATE TABLE `tipe_pembayaran` (
  `tipe_pembayaran_id` int(11) NOT NULL,
  `tipe_pembayaran_kode` varchar(20) DEFAULT NULL,
  `tipe_pembayaran_nama` varchar(50) DEFAULT NULL,
  `no_akun` varchar(50) DEFAULT NULL,
  `jenis_pembayaran` varchar(50) DEFAULT NULL,
  `additional` tinyint(1) DEFAULT NULL,
  `kembalian` tinyint(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipe_pembayaran`
--

INSERT INTO `tipe_pembayaran` (`tipe_pembayaran_id`, `tipe_pembayaran_kode`, `tipe_pembayaran_nama`, `no_akun`, `jenis_pembayaran`, `additional`, `kembalian`, `created_at`, `updated_at`) VALUES
(6, '01', 'Tunai', '', 'kas', NULL, 1, '2019-10-03 12:02:07', '2019-10-03 04:02:07'),
(7, '02', 'Visa', '', 'kas', 1, 0, '2019-10-20 15:30:11', '2019-10-20 08:14:35'),
(8, '03', 'Master Card', '', 'kas', 1, 0, '2019-10-20 15:30:22', '2019-10-20 08:14:43'),
(9, '04', 'American Expres (Amex)', '', 'kas', 1, 0, '2019-10-20 15:30:47', '2019-10-20 08:14:52'),
(10, '05', 'Debet', '', 'kas', 1, 0, '2019-10-20 15:31:45', '2019-10-20 08:14:55'),
(11, '06', 'Cek', '', 'kas', 1, 0, '2019-10-20 15:32:13', '2019-10-20 08:15:06'),
(12, '07', 'Bilyet Giro (BG)', '', 'kas', 1, 0, '2019-10-20 15:32:43', '2019-10-20 08:18:21'),
(13, '08', 'Piutang', '', 'kredit', NULL, 0, '2019-10-20 15:51:54', '2019-10-20 07:51:58');

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE `unit` (
  `unit_id` int(11) NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `satuan_id` int(11) DEFAULT NULL,
  `jumlah_satuan_unit` double DEFAULT NULL,
  `barcode` varchar(50) DEFAULT NULL,
  `harga_jual` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unit`
--

INSERT INTO `unit` (`unit_id`, `produk_id`, `satuan_id`, `jumlah_satuan_unit`, `barcode`, `harga_jual`, `created_at`, `updated_at`) VALUES
(1, 59, 2, 1000, '', 100000, '2020-01-15 11:46:08', '2020-01-20 14:19:35'),
(3, 58, 2, 1000, '', 100000, '2020-01-15 11:48:04', '2020-01-20 14:19:59'),
(5, 63, 2, 1000, '', 1500000, '2020-01-20 13:20:38', '2020-01-20 14:19:16'),
(6, 65, 1, 0.001, '', 10000, '2020-01-20 17:17:02', NULL),
(7, 64, 1, 0.001, '', 10000, '2020-01-20 17:17:28', NULL),
(8, 70, 1, 0.04, '', 1000, '2020-01-27 18:58:44', '2020-01-27 18:59:44'),
(9, 71, 5, 3, '', 2500, '2020-01-27 19:09:19', '2020-01-27 19:09:36'),
(10, 75, 5, 12, '', 10000, '2020-01-29 11:53:39', NULL),
(11, 76, 5, 12, '', 10000, '2020-01-29 15:29:50', '2020-01-29 15:32:24'),
(12, 76, 6, 24, '', 50000, '2020-02-11 13:59:35', '2020-02-11 14:00:18'),
(13, 76, 6, 2, '', 3000, '2020-02-11 14:01:25', '2020-02-11 14:01:32'),
(14, 81, 4, 10, '', 0, '2020-02-11 17:51:02', NULL),
(15, 76, 6, 10, '', 10000, '2020-02-11 17:52:22', NULL),
(17, 82, 6, 20, '', 3000, '2020-03-19 07:57:37', NULL),
(18, 83, 4, 0.2, '', 5000, '2020-03-19 08:03:41', NULL),
(19, 85, 5, 1, '', 6000, '2020-03-20 14:21:18', '2020-03-20 14:22:33');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_staff_id` int(11) DEFAULT NULL,
  `password` varchar(100) NOT NULL,
  `user_role_id` int(11) NOT NULL,
  `avatar` varchar(200) NOT NULL,
  `lokasi_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_staff_id`, `password`, `user_role_id`, `avatar`, `lokasi_id`, `created_at`, `updated_at`) VALUES
(1, 1, '$2y$10$sIfZ5LEefpTjmSN9B0Wl6.gZ44N.km9zp34lIaTt8CIdIBs4g4uAS', 1, 'assets/media/users/1556714265JXOGF.jpg', NULL, '2019-05-01 18:06:31', '2019-05-01 12:37:45'),
(2, 2, '$2y$10$Q3SXdb1f29tRI1WUyk.vr.2eHdwLNelmgYpE53PUgNmgV4mrfEXQC', 3, 'assets/media/users/15802677395Le6b.png', 1, '2020-01-29 10:15:39', '2020-01-29 02:15:39');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `user_role_id` int(11) NOT NULL,
  `user_role_name` varchar(50) NOT NULL,
  `user_role_akses` text,
  `keterangan` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`user_role_id`, `user_role_name`, `user_role_akses`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', '{\"pos\":{\"akses_menu\":true,\"penjualan\":{\"akses_menu\":true,\"delete\":false,\"view\":true}},\"dashboard\":{\"akses_menu\":true},\"master_data\":{\"akses_menu\":true,\"suplier\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"satuan\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true,\"konversi_satuan\":true},\"jenis-produk\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"lokasi\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"user\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"user-role\":{\"akses_menu\":true},\"tipe-pembayaran\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"staff\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true}},\"guest\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true},\"inventori\":{\"akses_menu\":true,\"produk\":{\"akses_menu\":true,\"data\":{\"hpp\":true},\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true,\"stock\":true,\"price\":true,\"unit\":true,\"assembly\":true},\"transfer-produk\":{\"akses_menu\":true,\"list\":true,\"transfer_produk\":true,\"konfirmasi_transfer_produk\":true,\"history_transfer_produk\":true},\"penyesuaian-produk\":{\"akses_menu\":true,\"list\":true,\"pesnyesuaian_produk\":true,\"history_penyesuaian_produk\":true},\"low-stock-produk\":{\"akses_menu\":true,\"list\":true},\"kartu-stock-produk\":{\"akses_menu\":true,\"list\":true}},\"order_produk\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"produksi\":{\"akses_menu\":false},\"hutang_piutang\":{\"akses_menu\":true,\"hutang\":{\"akses_menu\":true,\"list\":true,\"pembayaran\":true,\"posting\":true},\"piutang\":{\"akses_menu\":true,\"list\":true,\"pembayaran\":true,\"posting\":true}},\"laporan\":{\"akses_menu\":true,\"produk-by-location\":{\"akses_menu\":true,\"list\":true},\"bahan-by-location\":{\"akses_menu\":false,\"list\":false},\"laporan-stock-produk\":{\"akses_menu\":true,\"list\":true},\"laporan-penjualan\":{\"akses_menu\":true,\"list\":true},\"laporan-produksi\":{\"akses_menu\":false,\"list\":false},\"laporan-hutang\":{\"akses_menu\":true,\"list\":true},\"laporan-piutang\":{\"akses_menu\":true,\"list\":true},\"laporan-staff\":{\"akses_menu\":true,\"list\":true},\"laporan-customer\":{\"akses_menu\":true,\"list\":true},\"laporan-po-produk\":{\"akses_menu\":true,\"list\":true}}}', NULL, '2019-04-26 00:00:00', '2020-03-19 03:11:24'),
(2, 'admin', '{\"pos\":{\"akses_menu\":true,\"penjualan\":{\"akses_menu\":true,\"delete\":false,\"view\":true}},\"dashboard\":{\"akses_menu\":false},\"master_data\":{\"akses_menu\":true,\"suplier\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"satuan\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true,\"konversi_satuan\":false},\"jenis-produk\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"lokasi\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"user\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"user-role\":{\"akses_menu\":false},\"tipe-pembayaran\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"staff\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true}},\"guest\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true},\"inventori\":{\"akses_menu\":true,\"produk\":{\"akses_menu\":true,\"data\":{\"hpp\":true},\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true,\"stock\":true,\"price\":true,\"unit\":true,\"assembly\":true},\"transfer-produk\":{\"akses_menu\":true,\"list\":true,\"transfer_produk\":true,\"konfirmasi_transfer_produk\":true,\"history_transfer_produk\":true},\"penyesuaian-produk\":{\"akses_menu\":true,\"list\":true,\"pesnyesuaian_produk\":true,\"history_penyesuaian_produk\":true},\"low-stock-produk\":{\"akses_menu\":true,\"list\":true},\"kartu-stock-produk\":{\"akses_menu\":true,\"list\":true}},\"order_produk\":{\"akses_menu\":true,\"list\":true,\"add\":true,\"edit\":true,\"delete\":true,\"view\":true},\"produksi\":{\"akses_menu\":true},\"hutang_piutang\":{\"akses_menu\":true,\"hutang\":{\"akses_menu\":true},\"piutang\":{\"akses_menu\":true}},\"laporan\":{\"akses_menu\":true,\"produk-by-location\":{\"akses_menu\":true,\"list\":true},\"bahan-by-location\":{\"akses_menu\":false,\"list\":false},\"laporan-stock-produk\":{\"akses_menu\":true,\"list\":true},\"laporan-penjualan\":{\"akses_menu\":true,\"list\":true},\"laporan-produksi\":{\"akses_menu\":true,\"list\":true},\"laporan-hutang\":{\"akses_menu\":true,\"list\":true},\"laporan-piutang\":{\"akses_menu\":true,\"list\":true},\"laporan-staff\":{\"akses_menu\":true,\"list\":true},\"laporan-customer\":{\"akses_menu\":true,\"list\":true},\"laporan-po-produk\":{\"akses_menu\":true,\"list\":true}}}', '', '2019-11-01 12:05:04', '2020-02-12 09:29:09'),
(3, 'POS', '{\"pos\":{\"akses_menu\":true,\"penjualan\":{\"akses_menu\":true,\"delete\":true,\"view\":true}},\"dashboard\":{\"akses_menu\":true},\"master_data\":{\"akses_menu\":false,\"suplier\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"satuan\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false,\"konversi_satuan\":false},\"jenis-produk\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"lokasi\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"user\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"user-role\":{\"akses_menu\":false},\"tipe-pembayaran\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"staff\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false}},\"guest\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false},\"inventori\":{\"akses_menu\":false,\"produk\":{\"akses_menu\":false,\"data\":{\"hpp\":false},\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false,\"stock\":false,\"price\":false,\"unit\":false,\"assembly\":false},\"transfer-produk\":{\"akses_menu\":false,\"list\":false,\"transfer_produk\":false,\"konfirmasi_transfer_produk\":false,\"history_transfer_produk\":false},\"penyesuaian-produk\":{\"akses_menu\":false,\"list\":false,\"pesnyesuaian_produk\":false,\"history_penyesuaian_produk\":false},\"low-stock-produk\":{\"akses_menu\":false,\"list\":false},\"kartu-stock-produk\":{\"akses_menu\":false,\"list\":false}},\"order_produk\":{\"akses_menu\":false,\"list\":false,\"add\":false,\"edit\":false,\"delete\":false,\"view\":false},\"produksi\":{\"akses_menu\":false},\"hutang_piutang\":{\"akses_menu\":false,\"hutang\":{\"akses_menu\":false},\"piutang\":{\"akses_menu\":false}},\"laporan\":{\"akses_menu\":false,\"produk-by-location\":{\"akses_menu\":false,\"list\":false},\"bahan-by-location\":{\"akses_menu\":false,\"list\":false},\"laporan-stock-produk\":{\"akses_menu\":false,\"list\":false},\"laporan-penjualan\":{\"akses_menu\":false,\"list\":false},\"laporan-produksi\":{\"akses_menu\":false,\"list\":false},\"laporan-hutang\":{\"akses_menu\":false,\"list\":false},\"laporan-piutang\":{\"akses_menu\":false,\"list\":false},\"laporan-staff\":{\"akses_menu\":false,\"list\":false},\"laporan-customer\":{\"akses_menu\":false,\"list\":false}}}', 'Melakukan Point of Sales\r\n', '2020-01-29 10:08:31', '2020-01-29 02:17:12');


--
-- Indexes for dumped tables
--

--
-- Indexes for table `arus_stock_produk`
--
ALTER TABLE `arus_stock_produk`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `assembly`
--
ALTER TABLE `assembly`
  ADD PRIMARY KEY (`assembly_id`) USING BTREE;

--
-- Indexes for table `assembly_item`
--
ALTER TABLE `assembly_item`
  ADD PRIMARY KEY (`assembly_item_id`) USING BTREE;

--
-- Indexes for table `bahan`
--
ALTER TABLE `bahan`
  ADD PRIMARY KEY (`bahan_id`) USING BTREE;

--
-- Indexes for table `forget_request`
--
ALTER TABLE `forget_request`
  ADD PRIMARY KEY (`forget_request_id`) USING BTREE;

--
-- Indexes for table `guest`
--
ALTER TABLE `guest`
  ADD PRIMARY KEY (`guest_id`) USING BTREE;

--
-- Indexes for table `harga_produk`
--
ALTER TABLE `harga_produk`
  ADD PRIMARY KEY (`harga_produk_id`) USING BTREE;

--
-- Indexes for table `history_penyesuaian_produk`
--
ALTER TABLE `history_penyesuaian_produk`
  ADD PRIMARY KEY (`history_penyesuaian_produk_id`) USING BTREE;

--
-- Indexes for table `history_transfer_produk`
--
ALTER TABLE `history_transfer_produk`
  ADD PRIMARY KEY (`history_transfer_produk_id`) USING BTREE;

--
-- Indexes for table `hutang`
--
ALTER TABLE `hutang`
  ADD PRIMARY KEY (`hutang_id`) USING BTREE;

--
-- Indexes for table `jenis_produk`
--
ALTER TABLE `jenis_produk`
  ADD PRIMARY KEY (`jenis_produk_id`) USING BTREE;

--
-- Indexes for table `konversi_satuan`
--
ALTER TABLE `konversi_satuan`
  ADD PRIMARY KEY (`konversi_satuan_id`) USING BTREE;

--
-- Indexes for table `log_aktivitas`
--
ALTER TABLE `log_aktivitas`
  ADD PRIMARY KEY (`log_id`) USING BTREE;

--
-- Indexes for table `log_kasir`
--
ALTER TABLE `log_kasir`
  ADD PRIMARY KEY (`log_kasir_id`) USING BTREE;

--
-- Indexes for table `lokasi`
--
ALTER TABLE `lokasi`
  ADD PRIMARY KEY (`lokasi_id`) USING BTREE,
  ADD UNIQUE KEY `lokasi_kode` (`lokasi_kode`) USING BTREE;

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_id`) USING BTREE;

--
-- Indexes for table `pembayaran_hutang`
--
ALTER TABLE `pembayaran_hutang`
  ADD PRIMARY KEY (`pembayaran_hutang_id`) USING BTREE;

--
-- Indexes for table `pembayaran_piutang`
--
ALTER TABLE `pembayaran_piutang`
  ADD PRIMARY KEY (`pembayaran_piutang_id`) USING BTREE;

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`penjualan_id`) USING BTREE;

--
-- Indexes for table `penjualan_produk`
--
ALTER TABLE `penjualan_produk`
  ADD PRIMARY KEY (`penjualan_produk_id`) USING BTREE;

--
-- Indexes for table `piutang`
--
ALTER TABLE `piutang`
  ADD PRIMARY KEY (`piutang_id`) USING BTREE;

--
-- Indexes for table `po_bahan`
--
ALTER TABLE `po_bahan`
  ADD PRIMARY KEY (`po_bahan_id`) USING BTREE;

--
-- Indexes for table `po_bahan_detail`
--
ALTER TABLE `po_bahan_detail`
  ADD PRIMARY KEY (`po_bahan_detail_id`) USING BTREE,
  ADD KEY `po_bahan_constraint` (`po_bahan_id`) USING BTREE;

--
-- Indexes for table `po_produk`
--
ALTER TABLE `po_produk`
  ADD PRIMARY KEY (`po_produk_id`) USING BTREE;

--
-- Indexes for table `po_produk_detail`
--
ALTER TABLE `po_produk_detail`
  ADD PRIMARY KEY (`po_produk_detail_id`) USING BTREE,
  ADD KEY `po_produk_constraint` (`po_produk_id`) USING BTREE;

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`produk_id`) USING BTREE;

--
-- Indexes for table `satuan`
--
ALTER TABLE `satuan`
  ADD PRIMARY KEY (`satuan_id`) USING BTREE;

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`staff_id`) USING BTREE;

--
-- Indexes for table `stock_produk`
--
ALTER TABLE `stock_produk`
  ADD PRIMARY KEY (`stock_produk_id`) USING BTREE,
  ADD KEY `stock_produk_constraint` (`produk_id`) USING BTREE;

--
-- Indexes for table `sub_menu`
--
ALTER TABLE `sub_menu`
  ADD PRIMARY KEY (`sub_menu_id`) USING BTREE;

--
-- Indexes for table `suplier`
--
ALTER TABLE `suplier`
  ADD PRIMARY KEY (`suplier_id`) USING BTREE;

--
-- Indexes for table `tipe_pembayaran`
--
ALTER TABLE `tipe_pembayaran`
  ADD PRIMARY KEY (`tipe_pembayaran_id`) USING BTREE;

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`unit_id`) USING BTREE;

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`) USING BTREE;

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`user_role_id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `arus_stock_produk`
--
ALTER TABLE `arus_stock_produk`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=791;

--
-- AUTO_INCREMENT for table `assembly`
--
ALTER TABLE `assembly`
  MODIFY `assembly_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `assembly_item`
--
ALTER TABLE `assembly_item`
  MODIFY `assembly_item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `bahan`
--
ALTER TABLE `bahan`
  MODIFY `bahan_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `forget_request`
--
ALTER TABLE `forget_request`
  MODIFY `forget_request_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `guest`
--
ALTER TABLE `guest`
  MODIFY `guest_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `harga_produk`
--
ALTER TABLE `harga_produk`
  MODIFY `harga_produk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `history_penyesuaian_produk`
--
ALTER TABLE `history_penyesuaian_produk`
  MODIFY `history_penyesuaian_produk_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `history_transfer_produk`
--
ALTER TABLE `history_transfer_produk`
  MODIFY `history_transfer_produk_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `hutang`
--
ALTER TABLE `hutang`
  MODIFY `hutang_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `jenis_produk`
--
ALTER TABLE `jenis_produk`
  MODIFY `jenis_produk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `konversi_satuan`
--
ALTER TABLE `konversi_satuan`
  MODIFY `konversi_satuan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `log_aktivitas`
--
ALTER TABLE `log_aktivitas`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `log_kasir`
--
ALTER TABLE `log_kasir`
  MODIFY `log_kasir_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `lokasi`
--
ALTER TABLE `lokasi`
  MODIFY `lokasi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pembayaran_hutang`
--
ALTER TABLE `pembayaran_hutang`
  MODIFY `pembayaran_hutang_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pembayaran_piutang`
--
ALTER TABLE `pembayaran_piutang`
  MODIFY `pembayaran_piutang_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `penjualan`
--
ALTER TABLE `penjualan`
  MODIFY `penjualan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `penjualan_produk`
--
ALTER TABLE `penjualan_produk`
  MODIFY `penjualan_produk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `piutang`
--
ALTER TABLE `piutang`
  MODIFY `piutang_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `po_bahan`
--
ALTER TABLE `po_bahan`
  MODIFY `po_bahan_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `po_bahan_detail`
--
ALTER TABLE `po_bahan_detail`
  MODIFY `po_bahan_detail_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `po_produk`
--
ALTER TABLE `po_produk`
  MODIFY `po_produk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `po_produk_detail`
--
ALTER TABLE `po_produk_detail`
  MODIFY `po_produk_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `produk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT for table `satuan`
--
ALTER TABLE `satuan`
  MODIFY `satuan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `staff_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `stock_produk`
--
ALTER TABLE `stock_produk`
  MODIFY `stock_produk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=157;

--
-- AUTO_INCREMENT for table `sub_menu`
--
ALTER TABLE `sub_menu`
  MODIFY `sub_menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `suplier`
--
ALTER TABLE `suplier`
  MODIFY `suplier_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tipe_pembayaran`
--
ALTER TABLE `tipe_pembayaran`
  MODIFY `tipe_pembayaran_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `unit`
--
ALTER TABLE `unit`
  MODIFY `unit_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `user_role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `po_bahan_detail`
--
ALTER TABLE `po_bahan_detail`
  ADD CONSTRAINT `po_bahan_detail_ibfk_1` FOREIGN KEY (`po_bahan_id`) REFERENCES `po_bahan` (`po_bahan_id`) ON DELETE CASCADE;

--
-- Constraints for table `po_produk_detail`
--
ALTER TABLE `po_produk_detail`
  ADD CONSTRAINT `po_produk_detail_ibfk_1` FOREIGN KEY (`po_produk_id`) REFERENCES `po_produk` (`po_produk_id`) ON DELETE CASCADE;

--
-- Constraints for table `stock_produk`
--
ALTER TABLE `stock_produk`
  ADD CONSTRAINT `stock_produk_ibfk_1` FOREIGN KEY (`produk_id`) REFERENCES `produk` (`produk_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
