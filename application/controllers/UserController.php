<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserController extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user','',true);
		$this->load->model('staff','',true);
		$this->load->model('lokasi','',true);
		$this->load->helper('string');
		$this->load->library('upload');
		$this->load->library('image_lib');
	}
	public function index()
	{

		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/select2/dist/js/select2.full.js");
		array_push($this->js, "script/app.js");
		array_push($this->js, "script/admin/user.js");
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->css, "vendors/general/select2/dist/css/select2.css");
		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$data['staff'] = $this->staff->all_list();
		$data['user_role'] = $this->user->all_role();
		$column = array();
		$data["meta_title"] = "User < Master Data < ".$this->config->item('company_name');;
		$data['parrent'] = "master_data";
		$data['page'] = $this->uri->segment(1);
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"staff_nama"));
		array_push($column, array("data"=>"staff_email"));
		array_push($column, array("data"=>"staff_phone_number"));
		array_push($column, array("data"=>"user_role_name"));
		$data["lokasi"] = $this->lokasi->all_list();
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
		$akses_menu = json_decode($this->menu_akses,true);
		$action = array();
		foreach ($akses_menu['master_data']['user'] as $key => $value) {
			if($key != "list" && $key != "akses_menu"){
				$action[$key] = $value;
			}
		}
		$data['action'] = json_encode($action);
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/user');
		$this->load->view('admin/static/footer');
	}
	function list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->user->user_count_all();
		$result['iTotalDisplayRecords'] = $this->user->user_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->user->user_list($start,$length,$query);
		$i = $start+1;
				foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i;
			$i++;
			$key->delete_url = base_url().'user/delete/';
			$key->row_id = $key->user_id;
		}
		$result['aaData'] = $data;		
		echo json_encode($result);
	}
	function add(){
		$result['success'] = false;
		$result['message'] = "Staff ini sudah terdaftar sebagai user";
		$this->form_validation->set_rules('user_staff_id', '', 'required|is_unique[user.user_staff_id]');
		if ($this->form_validation->run() == TRUE) {
			$user_staff_id = $this->input->post('user_staff_id');
			$user_role_id = $this->input->post('user_role_id');
			$password = $this->input->post('password');
			$password = password_hash($password, PASSWORD_BCRYPT);
			$avatar = "";
			$avatar = $this->uploadImage("avatar",$avatar);
			if ($avatar!="failed"){
				$lokasi_id = null;
				if ($this->input->post('lokasi_id')!=""){
					$lokasi_id = $this->input->post('lokasi_id');
				}
				$data = array("user_staff_id"=>$user_staff_id,"user_role_id"=>$user_role_id,"password"=>$password,"avatar"=>$avatar,"lokasi_id"=>$lokasi_id);
				$insert = $this->user->insert($data);
				if($insert){
					$result['success'] = true;
					$result['message'] = "Data berhasil disimpan";
				} else {
					$result['message'] = "Gagal menyimpan data";
				}
			} else {
				$result['message'] = "Gagal mengunggah gambar";
			}
		}
		echo json_encode($result);
	}
	function edit(){
		$result['success'] = false;
		$result['message'] = "Gagal upload data";
		$data = array();
		$data["user_staff_id"] = $this->input->post('user_staff_id');
		$data["user_role_id"] = $this->input->post('user_role_id');
		$data["lokasi_id"] = null;
		if($this->input->post('user_role_id')!="1"){
			$data["lokasi_id"] = $this->input->post('lokasi_id');
		}
		if($this->input->post('password')!=""){
			$data["password"] = password_hash($this->input->post('password'), PASSWORD_BCRYPT);
		}
		$user_id = $this->input->post('user_id');
		$old_data = $this->user->user_by_id($user_id);
		$avatar = $old_data->avatar;
		if(isset($_FILES["avatar"]['name'])&&$_FILES["avatar"]["name"]!=""){
			$avatar = $this->uploadImage("avatar",$avatar);
		}
		if ($avatar!="failed"){
			$data["avatar"] = $avatar;

			$update = $this->user->update_by_id('user_id',$user_id,$data);
			if($update){
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
				if($avatar != $old_data->avatar){
					$temp = $old_data->avatar;
					if (file_exists(FCPATH.$temp)){
						unlink(FCPATH.$temp);
					}
				}
			} else {
				$result['message'] = "Gagal mengubah data";
			}
		} 
		echo json_encode($result);
	}
	function delete(){
		$id = $this->input->input_stream('id');
		$result['success'] = false;
		$result['message'] = "missing parameter";
		if($id != ""){
			$delete = $this->user->delete_by_id("user_id",$id);
			if($delete){
				$result['success'] = true;
				$result['message'] = "Data berhasil dihapus";
			} else {
				$result['message'] = "Gagal menghapus data";
			}
		}
		echo json_encode($result);
	}
	function uploadImage($file,$url){
		$date = new DateTime();
		$config['file_name'] = $date->getTimestamp().random_string('alnum', 5);
		$config['upload_path'] = 'assets/media/users/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']  = 2000;
		
		$this->upload->initialize($config);
		
		 if ( ! $this->upload->do_upload($file))
                {
                        $error = array('error' => $this->upload->display_errors());

                        $url = "failed";
                        return $url;
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
                        $url = $config['upload_path'].$data['upload_data']['orig_name'];
                        $this->resizeImage($url,$data['upload_data']['orig_name'],200);
                }		
        return $url;
	}
	function resizeImage($url,$name,$size){		
		$config['image_library'] = 'gd2';
		$config['source_image'] = FCPATH.$url;
		$config['create_thumb'] = FALSE;
		$config['maintain_ratio'] = FALSE;
		$config['width']         = $size;
		$config['height']       = $size;
		$temp = explode(".", $name);
		$folder = $temp[0];
		$config['new_image'] = FCPATH.$url;	
		$this->image_lib->initialize($config);
		$this->image_lib->resize();		
	}
}

/* End of file User.php */
/* Location: ./application/controllers/User.php */