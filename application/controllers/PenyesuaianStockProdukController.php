<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PenyesuaianStockProdukController extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('produk','',true);
		$this->load->model('stock_produk','',true);
		$this->load->model('history_penyesuaian_produk','',true);
		$this->load->model('assembly','',true);
		$this->load->model('unit','',true);
		$this->load->model('lokasi','',true);
	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		
		array_push($this->js, "script/app.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = " Penyesuaian Stock Produk < Inventori < ".$this->config->item('company_name');;
		$data['parrent'] = "inventori";
		$data['page'] = 'penyesuaian-produk';
		$target = array(0,4);
		$sumColumn = array(4);
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"produk_kode"));
		array_push($column, array("data"=>"produk_nama"));
		array_push($column, array("data"=>"jenis_produk_nama"));
		array_push($column, array("data"=>"stock"));
		if(isset($_SESSION['login']['lokasi_id'])){
			array_push($column, array("data"=>"jumlah_lokasi"));
			array_push($target, 5);
			array_push($sumColumn, 5);
		}		
		$data['sumColumn'] = json_encode($sumColumn);
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>$target));
		$data["list_url"] = base_url()."penyesuaian-produk/list";
		$data["action"] = json_encode(array("stock"=>true,"view"=>false,"edit"=>false,"delete"=>false));
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/penyesuaian_produk');
		$this->load->view('admin/static/footer');
	}
	function list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->produk->produk_count_all();
		$result['iTotalDisplayRecords'] = $this->produk->produk_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->produk->produk_list($start,$length,$query);
		$i = $start+1;
				foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i;
			$key->delete_url = base_url().'produk/delete/';
			$key->row_id = $key->produk_id;
			$key->stok_url = base_url().'penyesuaian-produk/stock/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->produk_id));
			$key->stock = number_format($key->stock,2);
			$i++;
			if(isset($_SESSION['login']['lokasi_id'])){
				$key->jumlah_lokasi = number_format($key->jumlah_lokasi);	
			}			
		}
		$result['aaData'] = $data;			
		echo json_encode($result);
	}
	function stock_index(){
		array_push($this->css,"app/custom/wizard/wizard-v3.default.css");
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		array_push($this->js, "script/app.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Penyesuaian Stok < Inventori < ".$this->config->item('company_name');;
		$data['parrent'] = "inventori";
		$data['page'] = 'penyesuaian-produk';
		$url = str_replace(array("-","_"), array("+","/"), $this->uri->segment(3));
		$id = $this->encryption->decrypt($url);
		$produk = $this->produk->produk_by_id($id);
		$data['id'] = $id;
		if ($produk != null) {
			$data['produk'] = $produk;
			array_push($column, array("data"=>"no"));
			array_push($column, array("data"=>"lokasi_nama"));
			array_push($column, array("data"=>"stock_produk_seri"));
			array_push($column, array("data"=>"stock_produk_qty"));
			$data['sumColumn'] = json_encode(array(3));
			$data['column'] = json_encode($column);
			$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0,3)));
			$data["action"] = json_encode(array("adjust"=>true));
			$data["list_url"] = base_url()."stock-produk/list/".$id;
			$this->load->view('admin/static/header',$data);
			$this->load->view('admin/static/sidebar');
			$this->load->view('admin/static/topbar');
			$this->load->view('admin/penyesuaian_produk');
			$this->load->view('admin/static/footer');
		} else {
			redirect('404_override','refresh');
		}
	}
	function adjust(){
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$data_qty["stock_produk_qty"] = str_replace(",", "", $this->input->post('stock_produk_qty'));
		$stock_produk_id = $this->input->post('stock_produk_id');
		$old_data = $this->stock_produk->row_by_id($stock_produk_id);
		$produk_id = $old_data->produk_id;
		$status = array();
		$nilai_status = 0;
		$exists = $this->assembly->get_exists("produk_id",$produk_id);
		if ($exists == FALSE) {
			$update = $this->stock_produk->update_by_id('stock_produk_id',$stock_produk_id,$data_qty);
			if($update){
					$data = array();
					$diff = $old_data->stock_produk_qty - $data_qty["stock_produk_qty"];
					if ($diff != 0) {
						$data["tanggal"] = date("Y-m-d");
						$data["table_name"] = "stock_produk";
						$data["stock_produk_id"] = $stock_produk_id;
						$data["produk_id"] = $this->input->post('produk_id');
						if ($diff > 0) {
							$data["stock_out"] = abs($diff);
							$data["stock_in"] = 0;
						} else {
							$data["stock_in"] = abs($old_data->stock_produk_qty - $data_qty["stock_produk_qty"]);
							$data["stock_out"] = 0;
						}
						$data["last_stock"] = $this->stock_produk->last_stock($this->input->post('produk_id'))->result;
						$data["last_stock_total"] = $this->stock_produk->stock_total()->result;
						$data["keterangan"] = "Penyesuaian stok produk";
						$data["method"] = "update";
						$this->stock_produk->arus_stock_produk($data);
					}
					$result['success'] = true;
					$result['message'] = "Data berhasil disimpan";
					$data = array();
					$data["tanggal"] = date("Y-m-d");
					$data["produk_id"] = $this->input->post('produk_id');
					$data["jenis_produk_id"] = $this->input->post('jenis_produk_id');
					$data["lokasi_id"] = $this->input->post('stock_produk_lokasi_id');
					$data["stock_produk_id"] = $stock_produk_id;
					$data["produk_kode"] = $this->input->post('produk_kode');
					$data["produk_nama"] = $this->input->post('produk_nama');
					$data["jenis_produk_nama"] = $this->input->post('jenis_produk_nama');
					$data["lokasi_nama"] = $this->input->post('lokasi_nama');
					$data["qty_awal"] = $old_data->stock_produk_qty;
					$data["qty_akhir"] = str_replace(",", "", $this->input->post('stock_produk_qty'));
					$data["keterangan"] = $this->input->post('keterangan');
					$this->history_penyesuaian_produk->insert($data);
			}
		} else {
			$data = array();
			$diff = $old_data->stock_produk_qty - $data_qty["stock_produk_qty"];
			if ($diff != 0) {
				$data["tanggal"] = date("Y-m-d");
				$data["table_name"] = "stock_produk";
				$data["stock_produk_id"] = $stock_produk_id;
				$data["produk_id"] = $this->input->post('produk_id');
				$assembly = $this->assembly->get_id_assembly($produk_id);
				$assembly_id = $assembly['assembly_id'];
				$item_assembly = $this->assembly->assembly_detail_by_id($assembly_id);
				if ($diff > 0) {
					foreach ($item_assembly as $item) {
		                $jml_stok_produk_assembly = abs($diff);
		                $jml_komposisi = $item->jumlah;
		                if ($item->unit_id==0){
		                    $stock_produk_qty = $jml_stok_produk_assembly*$jml_komposisi;
		                } else {
		                    $unit_satuan = $this->unit->row_by_id($item->unit_id);
		                    $konversi_nilai = $unit_satuan->jumlah_satuan_unit;
		                    $stock_produk_qty = $jml_stok_produk_assembly*$jml_komposisi*$konversi_nilai;
		                }
		                $data_stk_in["stock_produk_qty"] = $stock_produk_qty;
		                $data_stk_in["produk_id"] = $item->produk_id;
		                $data_stk_in["stock_produk_lokasi_id"] = $this->input->post('stock_produk_lokasi_id');
		                $data_stk_in["year"] = date("y");
		                $data_stk_in["month"] = date("m");
		                $lokasi_kode = $this->lokasi->row_by_id($data_stk_in["stock_produk_lokasi_id"])->lokasi_kode;
		                $jenis_produk_kode = $this->produk->produk_by_id($data_stk_in["produk_id"])->jenis_produk_kode;
		                $data_stk_in["stock_produk_seri"] = $data_stk_in["month"].$data_stk_in["year"].$jenis_produk_kode.$lokasi_kode;
		                $data_stk_in["urutan"] = $this->stock_produk->urutan_seri($data_stk_in["stock_produk_seri"]);
		                $data_stk_in["stock_produk_seri"] = $data_stk_in["stock_produk_seri"].$data_stk_in["urutan"];
		                $data_stk_in["stock_produk_keterangan"] = '';
		                $data_stk_in["hpp"] = 0;
		                $insert0 = $this->stock_produk->insert($data_stk_in);
		                if($insert0){
		                    $result['success'] = true;
		                    $result['message'] = "Data berhasil disimpan";
		                    $data1 = array();
		                    $data1["tanggal"] = date("Y-m-d");
		                    $data1["table_name"] = "stock_produk";
		                    $data1["stock_produk_id"] = $this->stock_produk->last_id();
		                    $data1["produk_id"] = $item->produk_id;
		                    $data1["stock_out"] = 0;
		                    $data1["stock_in"] = $stock_produk_qty;
		                    $data1["last_stock"] = $this->stock_produk->last_stock($item->produk_id)->result;
		                    $data1["last_stock_total"] = $this->stock_produk->stock_total()->result;
		                    $data1["keterangan"] = "Insert stok produk dari produk assembly";
		                    $data1["method"] = "insert";
		                    $this->stock_produk->arus_stock_produk($data1);
		                } else {
		                    $result['message'] = "Gagal menyimpan data";
		                } 
		            }
					$data["stock_out"] = abs($diff);
					$data["stock_in"] = 0;
					$nilai_status = 0;
				} else {
					foreach ($item_assembly as $key) {
						$stock_produk_qty = abs($diff);
						$jumlah_komposisi = $key->jumlah;
						$stock_produk = $this->stock_produk->produk_by_lokasi($this->input->post('stock_produk_lokasi_id'),$key->produk_id);
						if ($key->unit_id==0){
							$stock_produk_hasil = $stock_produk_qty*$jumlah_komposisi;
						} else {
							$unit_satuan = $this->unit->row_by_id($key->unit_id);
			                $konversi_nilai = $unit_satuan->jumlah_satuan_unit;
			               	$stock_produk_hasil = $stock_produk_qty*$jumlah_komposisi*$konversi_nilai;
						}
						foreach ($item_assembly as $datastock) {
							$stock_produk_item = $this->stock_produk->total_stock_perlokasi($datastock->produk_id,$this->input->post('stock_produk_lokasi_id'));
							if ($stock_produk_item < $stock_produk_hasil){
						      	array_Push($status, 'break');
						    } else {
						    	array_Push($status, 'run');
						    }
						}
						if (in_array('break', $status))
						{
							$nilai_status = 1;
							$result['message'] = "Stock Produk Item Tidak Cukup Untuk Membuat Produk Assembly";
							break;
						}
					}
					if ($nilai_status==0) {
						foreach ($item_assembly as $key) {
							$stock_produk_qty = abs($diff);
							$jumlah_komposisi = $key->jumlah;
							$stock_produk = $this->stock_produk->produk_by_lokasi($this->input->post('stock_produk_lokasi_id'),$key->produk_id);
							if ($key->unit_id==0){
								$stock_produk_hasil = $stock_produk_qty*$jumlah_komposisi;
							} else {
								$unit_satuan = $this->unit->row_by_id($key->unit_id);
			                    $konversi_nilai = $unit_satuan->jumlah_satuan_unit;
			                   	$stock_produk_hasil = $stock_produk_qty*$jumlah_komposisi*$konversi_nilai;
							}
							$stock_produk_sisa = $stock_produk_hasil;
							$sisa = 0;
							foreach ($stock_produk as $stocks) {
								$id= $stocks->stock_produk_id;
								$id_produk = $stocks->produk_id;
								$old_data = $this->stock_produk->row_by_id($id);
								if($id != ""){
									$dataa["delete_flag"] = 1;
									$delete = $this->stock_produk->update_by_id("stock_produk_id",$id,$dataa);
									if($delete){
										$result['success'] = true;
										$result['message'] = "Data berhasil dihapus";
										$stock_keluar = abs($stock_produk_sisa);
										$stock_produk_sisa = $old_data->stock_produk_qty - abs($stock_produk_sisa);
										$data_stk_out = array();
										$data_stk_out["tanggal"] = date("Y-m-d");
										$data_stk_out["table_name"] = "stock_produk";
										$data_stk_out["stock_produk_id"] = $id;
										$data_stk_out["produk_id"] = $id_produk;
										if ($stock_produk_sisa >= 0) {
											$data_stk_out["stock_out"] = $stock_keluar;
											$data_stk_out["stock_in"] = 0;
										} else {
											$data_stk_out["stock_out"] = $old_data->stock_produk_qty;
											$data_stk_out["stock_in"] = 0;
										}
										$data_stk_out["last_stock"] = $this->stock_produk->last_stock($id_produk)->result;
										$data_stk_out["last_stock_total"] = $this->stock_produk->stock_total()->result;
										$data_stk_out["keterangan"] = "Delete stok produk";
										$data_stk_out["method"] = "delete";
										$this->stock_produk->arus_stock_produk($data_stk_out);
									} 
								}
								if ($stock_produk_sisa >= 0)
							    {
							      	$sisa = $stock_produk_sisa;
							      	break;
							    }
							}
							$data_stk_in["stock_produk_qty"] = $sisa;
							$data_stk_in["produk_id"] = $key->produk_id;
							$data_stk_in["stock_produk_lokasi_id"] = $this->input->post('stock_produk_lokasi_id');
							if(isset($_SESSION['login']['lokasi_id'])){
								$data_stk_in["stock_produk_lokasi_id"] = $_SESSION['login']['lokasi_id'];
							}		
							$data_stk_in["year"] = date("y");
							$data_stk_in["month"] = date("m");
							$lokasi_kode = $this->lokasi->row_by_id($data_stk_in["stock_produk_lokasi_id"])->lokasi_kode;
							$jenis_produk_kode = $this->produk->produk_by_id($data_stk_in["produk_id"])->jenis_produk_kode;
							$data_stk_in["stock_produk_seri"] = $data_stk_in["month"].$data_stk_in["year"].$jenis_produk_kode.$lokasi_kode;
							$data_stk_in["urutan"] = $this->stock_produk->urutan_seri($data_stk_in["stock_produk_seri"]);
							$data_stk_in["stock_produk_seri"] = $data_stk_in["stock_produk_seri"].$data_stk_in["urutan"];
							$data_stk_in["stock_produk_keterangan"] = $this->input->post('stock_produk_keterangan');
							$data_stk_in["hpp"] = $this->string_to_number($this->input->post('hpp'));
								$insert = $this->stock_produk->insert($data_stk_in);
								if($insert){
									$result['success'] = true;
									$result['message'] = "Data berhasil disimpan";
									$data_stk_in_arus = array();
									$data_stk_in_arus["tanggal"] = date("Y-m-d");
									$data_stk_in_arus["table_name"] = "stock_produk";
									$data_stk_in_arus["stock_produk_id"] = $this->stock_produk->last_id();
									$data_stk_in_arus["produk_id"] = $key->produk_id;
									$data_stk_in_arus["stock_out"] = 0;
									$data_stk_in_arus["stock_in"] = $sisa;
									$data_stk_in_arus["last_stock"] = $this->stock_produk->last_stock($key->produk_id)->result;
									$data_stk_in_arus["last_stock_total"] = $this->stock_produk->stock_total()->result;
									$data_stk_in_arus["keterangan"] = "Insert stok produk sisa assembly";
									$data_stk_in_arus["method"] = "insert";
									$this->stock_produk->arus_stock_produk($data_stk_in_arus);
								}
						}
					}
					$data["stock_in"] = abs($old_data->stock_produk_qty - $data_qty["stock_produk_qty"]);
					$data["stock_out"] = 0;
				}
				$data["last_stock"] = $this->stock_produk->last_stock($this->input->post('produk_id'))->result;
				$data["last_stock_total"] = $this->stock_produk->stock_total()->result;
				$data["keterangan"] = "Penyesuaian stok produk";
				$data["method"] = "update";
				if ($nilai_status==0) {
					$update = $this->stock_produk->update_by_id('stock_produk_id',$stock_produk_id,$data_qty);
					if($update){
						$this->stock_produk->arus_stock_produk($data);
					}
				}
			}
			$result['success'] = true;
			$result['message'] = "Data berhasil disimpan";
			$data_hist = array();
			$data_hist["tanggal"] = date("Y-m-d");
			$data_hist["produk_id"] = $this->input->post('produk_id');
			$data_hist["jenis_produk_id"] = $this->input->post('jenis_produk_id');
			$data_hist["lokasi_id"] = $this->input->post('stock_produk_lokasi_id');
			$data_hist["stock_produk_id"] = $stock_produk_id;
			$data_hist["produk_kode"] = $this->input->post('produk_kode');
			$data_hist["produk_nama"] = $this->input->post('produk_nama');
			$data_hist["jenis_produk_nama"] = $this->input->post('jenis_produk_nama');
			$data_hist["lokasi_nama"] = $this->input->post('lokasi_nama');
			$data_hist["qty_awal"] = $old_data->stock_produk_qty;
			$data_hist["qty_akhir"] = str_replace(",", "", $this->input->post('stock_produk_qty'));
			$data_hist["keterangan"] = $this->input->post('keterangan');
			$this->history_penyesuaian_produk->insert($data_hist);
		}
		echo json_encode($result);
	}
}

/* End of file PenyesuaianStockProdukController.php */
/* Location: ./application/controllers/PenyesuaianStockProdukController.php */