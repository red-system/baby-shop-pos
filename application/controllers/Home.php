<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('guest','',true);
        $this->load->model('pos','',true);
        $this->load->model('hutang','',true);
        $this->load->model('piutang','',true);
        $this->load->model('lokasi','',true);

    }

    public function index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->js, "vendors/general/raphael/raphael.js");
        array_push($this->js, "vendors/general/morris.js/morris.js");
        array_push($this->css, "vendors/general/morris.js/morris.css");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
        array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
        array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");

        array_push($this->js, "script/admin/home.js");
        $data["meta_title"] = "Dashboard < ".$this->config->item('company_name');;
        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $data["hutang"] = number_format($this->hutang->totalHutang());
        $data["piutang"] = number_format($this->piutang->totalPiutang());
        $data["sales"] = number_format($this->pos->totalPenjualan());
        $data['parrent'] = "dasboard";
        $data['page'] = "dashboard";
        $data["chartData"] = array();
        $data["lokasi"] = $this->lokasi->all_list();
        $min_date = $this->input->get('start_date');
        $max_date = $this->input->get('end_date');
        $lokasi_id = $this->input->get('lokasi_id');
        $data["min_tanggal"] = $min_date == '' ? $this->pos->minTanggalPenjualan() : $min_date;
        $data["max_tanggal"] = $max_date == '' ? date('Y-m-d') : $max_date;
        $data["lokasi_id"] = $lokasi_id == '' ? 'All' : $lokasi_id;
        if(isset($_SESSION['login']['lokasi_id'])){
            $data["lokasi_id"] = $_SESSION['login']['lokasi_id'];
        }
        $search = array("min_tanggal"=>$data["min_tanggal"],"max_tanggal"=>$data["max_tanggal"],"lokasi_id"=>$data["lokasi_id"]);
        $data["salesMonth"] = $this->pos->penjualanByMonth($search);
        foreach ($data["salesMonth"] as $key) {
            $temp = explode(" ", $key->label);
            $temp[0] = $this->monthIdn($temp[0]);
            $key->label = $temp[0]." ".$temp[1];
            array_push($data["chartData"], array("y"=>$key->label,"a"=>$key->sales));
        }
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/home');
        $this->load->view('admin/static/footer');
    }
    function country(){
        $min_date = $this->input->get('start_date');
        $max_date = $this->input->get('end_date');
        $lokasi_id = $this->input->get('lokasi_id');
        $data["min_tanggal"] = $min_date == '' ? $this->pos->minTanggalPenjualan() : $min_date;
        $data["max_tanggal"] = $max_date == '' ? date('Y-m-d') : $max_date;
        $data["lokasi_id"] = $lokasi_id == '' ? 'All' : $lokasi_id;
        if(isset($_SESSION['login']['lokasi_id'])){
            $data["lokasi_id"] = $_SESSION['login']['lokasi_id'];
        }
        $search = array("min_tanggal"=>$data["min_tanggal"],"max_tanggal"=>$data["max_tanggal"],"lokasi_id"=>$data["lokasi_id"]);
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->guest->country_count_all($search);
        $result['iTotalDisplayRecords'] = $this->guest->country_count_filter($query,$search);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->guest->country_list($start,$length,$query,$search);
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function most(){
        $min_date = $this->input->get('start_date');
        $max_date = $this->input->get('end_date');
        $lokasi_id = $this->input->get('lokasi_id');
        $data["min_tanggal"] = $min_date == '' ? $this->pos->minTanggalPenjualan() : $min_date;
        $data["max_tanggal"] = $max_date == '' ? date('Y-m-d') : $max_date;
        $data["lokasi_id"] = $lokasi_id == '' ? 'All' : $lokasi_id;
        if(isset($_SESSION['login']['lokasi_id'])){
            $data["lokasi_id"] = $_SESSION['login']['lokasi_id'];
        }
        $search = array("min_tanggal"=>$data["min_tanggal"],"max_tanggal"=>$data["max_tanggal"],"lokasi_id"=>$data["lokasi_id"]);
        $result['iTotalRecords'] = $this->pos->mostPenjualanDashboardCount($search);
        $result['iTotalDisplayRecords'] = $this->pos->mostPenjualanDashboardCount($search);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data =  $this->pos->mostPenjualanDashboardList($search);
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function least(){
        $min_date = $this->input->get('start_date');
        $max_date = $this->input->get('end_date');
        $lokasi_id = $this->input->get('lokasi_id');
        $data["min_tanggal"] = $min_date == '' ? $this->pos->minTanggalPenjualan() : $min_date;
        $data["max_tanggal"] = $max_date == '' ? date('Y-m-d') : $max_date;
        $data["lokasi_id"] = $lokasi_id == '' ? 'All' : $lokasi_id;
        if(isset($_SESSION['login']['lokasi_id'])){
            $data["lokasi_id"] = $_SESSION['login']['lokasi_id'];
        }
        $search = array("min_tanggal"=>$data["min_tanggal"],"max_tanggal"=>$data["max_tanggal"],"lokasi_id"=>$data["lokasi_id"]);
        $result['iTotalRecords'] = $this->pos->leastPenjualanDashboardCount($search);
        $result['iTotalDisplayRecords'] = $this->pos->leastPenjualanDashboardCount($search);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data =  $this->pos->leastPenjualanDashboardList($search);
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function piutang_akan(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');

        $result['iTotalRecords'] = $this->piutang->piutang_akan_count_all();
        $result['iTotalDisplayRecords'] = $this->piutang->piutang_akan_count_filter($query);
        $data =  $this->piutang->piutang_akan_list($start,$length,$query);

        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->row_id = $key->piutang_id;
            $key->grand_total = number_format($key->grand_total);
            $key->terbayar = number_format($key->terbayar);
            $key->sisa = number_format($key->sisa);
            $key->pembayaran_url = base_url().'piutang/pay/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->piutang_id));
            $key->posting_url = base_url().'piutang/posting/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->piutang_id));
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function piutang_jatuh(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');

        $result['iTotalRecords'] = $this->piutang->piutang_jatuh_count_all();
        $result['iTotalDisplayRecords'] = $this->piutang->piutang_jatuh_count_filter($query);
        $data =  $this->piutang->piutang_jatuh_list($start,$length,$query);

        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->row_id = $key->piutang_id;
            $key->grand_total = number_format($key->grand_total);
            $key->terbayar = number_format($key->terbayar);
            $key->sisa = number_format($key->sisa);
            $key->pembayaran_url = base_url().'piutang/pay/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->piutang_id));
            $key->posting_url = base_url().'piutang/posting/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->piutang_id));
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function hutang_akan(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');

        $result['iTotalRecords'] = $this->hutang->hutang_akan_count_all();
        $result['iTotalDisplayRecords'] = $this->hutang->hutang_akan_count_filter($query);
        $data =  $this->hutang->hutang_akan_list($start,$length,$query);

        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->row_id = $key->hutang_id;
            $key->grand_total = number_format($key->grand_total);
            $key->terbayar = number_format($key->terbayar);
            $key->sisa = number_format($key->sisa);
            $key->pembayaran_url = base_url().'hutang/pay/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->hutang_id));
            $key->posting_url = base_url().'hutang/posting/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->hutang_id));
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function hutang_jatuh(){
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');

        $result['iTotalRecords'] = $this->hutang->hutang_jatuh_count_all();
        $result['iTotalDisplayRecords'] = $this->hutang->hutang_jatuh_count_filter($query);
        $data =  $this->hutang->hutang_jatuh_list($start,$length,$query);

        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->row_id = $key->hutang_id;
            $key->grand_total = number_format($key->grand_total);
            $key->terbayar = number_format($key->terbayar);
            $key->sisa = number_format($key->sisa);
            $key->pembayaran_url = base_url().'hutang/pay/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->hutang_id));
            $key->posting_url = base_url().'hutang/posting/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->hutang_id));
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
}