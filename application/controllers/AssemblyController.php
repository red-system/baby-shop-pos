<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AssemblyController extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('produk','',true);
        $this->load->model('assembly','',true);
        $this->load->model('assembly_item','',true);
        $this->load->model('jenis_produk','',true);
        $this->load->model('stock_produk','',true);
        $this->load->model('lokasi','',true);
        $this->load->model('satuan','',true);
        $this->load->model('unit','',true);
    }

    public function add_index()
    {
        array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
        array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
        array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
        array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
        array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");

        array_push($this->js, "vendors/general/select2/dist/js/select2.full.js");
        array_push($this->css, "vendors/general/select2/dist/css/select2.css");

        array_push($this->js, "script/admin/assembly.js");
        
        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $column = array();
        $data["meta_title"] = "Assembly < Produk < Master Data < ".$this->config->item('company_name');;
        $data['parrent'] = "inventori";
        $data['page'] = "produk";
        $data['satuan'] = $this->satuan->all_list();
        $data['jenis_produk'] = $this->jenis_produk->all_list();
        $data['produk'] = json_encode($this->produk->all_list());
        $data['satuan_item'] = json_encode($this->satuan->all_list());
        $this->load->view('admin/static/header',$data);
        $this->load->view('admin/static/sidebar');
        $this->load->view('admin/static/topbar');
        $this->load->view('admin/assembly/add');
        $this->load->view('admin/static/footer');
    }
    function get_unit_satuan(){
        $idproduk = $this->input->post('id');
        $data = $this->assembly->get_unit_satuan($idproduk);
        echo json_encode($data);
    }
    function assembly_detail_by_id(){
        $assembly_id = $this->input->post('id');
        $data = $this->assembly->assembly_detail_by_id($assembly_id);
        echo json_encode($data);
    }
    function add_save(){
        $result['success'] = false;
        $result['message'] = "Kode ini telah terdaftar";
        $this->form_validation->set_rules('produk_kode', '', 'required|is_unique[produk.produk_kode]');
        $transaksi = json_decode($this->input->post('transaksi'));
        if ($this->form_validation->run() == TRUE) {
            $insert = $this->assembly->insert_assembly();
            if($insert){
                $result['success'] = true;
                $result['message'] = "Data berhasil disimpan";
            } else {
                $result['message'] = "Gagal menyimpan data";
            }
        }
        echo json_encode($result);
    }
    function edit_index(){
        $url = str_replace(array("-","_"), array("+","/"), $this->uri->segment(3));
        $id = $this->encryption->decrypt($url);
        $assembly = $this->assembly->get_id_assembly($id);
        $id_assembly = $assembly['assembly_id'];
        $assembly = $this->assembly->produk_by_id($id);
        if ($assembly != null) {
            $data['assembly_id'] = $id_assembly;
            $data['assembly'] = $assembly;
            $data['assembly_detail']=$this->assembly->assembly_detail_by_id($id_assembly);
            $data['item_assembly'] = json_encode($data['assembly_detail']);
            //print_r($data['item_assembly']);die();
            array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
            array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
            array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
            array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
            array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
            array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
            array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
            array_push($this->js, "vendors/general/typeahead.js/dist/typeahead.bundle.js");
            array_push($this->css, "vendors/general/select2/dist/css/select2.css");
            array_push($this->js, "script/admin/assembly.js");
            $data["css"] = $this->css;
            $data["js"] = $this->js;
            $column = array();
            $data["meta_title"] = "Produk < ".$this->config->item('company_name');;
            $data['parrent'] = "inventori";
            $data['satuan'] = $this->satuan->all_list();
            $data['jenis_produk'] = $this->jenis_produk->all_list();
            $data['produk_items'] = $this->produk->all_list();
            $satuan_unit=array();
            foreach ($data['assembly_detail'] as $key) {
                array_push($satuan_unit,
                    ($this->assembly->get_unit_satuan($key->produk_id)
                    )
                );
            }
            $data['satuan_units']=$satuan_unit;
            $data['produk'] = json_encode($this->produk->all_list());
            $data['satuan_item'] = json_encode($this->satuan->all_list());
            $data['page'] = $this->uri->segment(1);
            $this->load->view('admin/static/header',$data);
            $this->load->view('admin/static/sidebar');
            $this->load->view('admin/static/topbar');
            $this->load->view('admin/assembly/edit');
            $this->load->view('admin/static/footer');
        }else {

            redirect('404_override','refresh');
        }
    }
    function edit_save(){
        $result['success'] = false;
        $result['message'] = "Gagal menyimpan data";
        $produk_id = $this->input->post('produk_id');
        $assembly_id = $this->input->post('assembly_id');
        $stock_perlokasi = $this->assembly->stock_perlokasi($produk_id);
        $item_assembly = $this->assembly->assembly_detail_by_id($assembly_id);
        $exists = $this->produk->get_exist_with_id($produk_id,$this->input->post('produk_kode'));
        $item = $this->input->post('item');
        //print_r(count($item));die();
        if(($exists == TRUE)) {
            $result['success'] = false;
            $result['message'] = "Kode ini telah terdaftar";
            echo json_encode($result); 
        } else {
            if (count($stock_perlokasi)==0){
                $delete_item = $this->assembly_item->delete_by_id("assembly_id",$assembly_id);
                if ($delete_item) {
                    $insert = $this->assembly->edit_assembly();
                    if($insert){
                        $result['success'] = true;
                        $result['message'] = "Berhasil menyimpan data";         
                    }
                }
                echo json_encode($result); 
            } else {
                foreach ($stock_perlokasi as $stock) {
                    foreach ($item_assembly as $item) {
                        $jml_stok_produk_assembly = $stock->stock_produk_qty;
                        $jml_komposisi = $item->jumlah;
                        if ($item->unit_id==0){
                            $stock_produk_qty = $jml_stok_produk_assembly*$jml_komposisi;
                        } else {
                            $unit_satuan = $this->unit->row_by_id($item->unit_id);
                            $konversi_nilai = $unit_satuan->jumlah_satuan_unit;
                            $stock_produk_qty = $jml_stok_produk_assembly*$jml_komposisi*$konversi_nilai;
                        }
                        
                        $data["stock_produk_qty"] = $stock_produk_qty;
                        $data["produk_id"] = $item->produk_id;
                        $data["stock_produk_lokasi_id"] = $stock->stock_produk_lokasi_id;
                        $data["year"] = date("y");
                        $data["month"] = date("m");
                        $lokasi_kode = $this->lokasi->row_by_id($data["stock_produk_lokasi_id"])->lokasi_kode;
                        $jenis_produk_kode = $this->produk->produk_by_id($data["produk_id"])->jenis_produk_kode;
                        $data["stock_produk_seri"] = $data["month"].$data["year"].$jenis_produk_kode.$lokasi_kode;
                        $data["urutan"] = $this->stock_produk->urutan_seri($data["stock_produk_seri"]);
                        $data["stock_produk_seri"] = $data["stock_produk_seri"].$data["urutan"];
                        $data["stock_produk_keterangan"] = '';
                        $data["hpp"] = 0;
                        $insert0 = $this->stock_produk->insert($data);
                        if($insert0){
                            $result['success'] = true;
                            $result['message'] = "Data berhasil disimpan";
                            $data1 = array();
                            $data1["tanggal"] = date("Y-m-d");
                            $data1["table_name"] = "stock_produk";
                            $data1["stock_produk_id"] = $this->stock_produk->last_id();
                            $data1["produk_id"] = $item->produk_id;
                            $data1["stock_out"] = 0;
                            $data1["stock_in"] = $stock_produk_qty;
                            $data1["last_stock"] = $this->stock_produk->last_stock($item->produk_id)->result;
                            $data1["last_stock_total"] = $this->stock_produk->stock_total()->result;
                            $data1["keterangan"] = "Insert stok produk dari produk assembly";
                            $data1["method"] = "insert";
                            $this->stock_produk->arus_stock_produk($data1);
                        } else {
                            $result['message'] = "Gagal menyimpan data";
                        } 

                    }
                    $produk_id = $stock->produk_id;
                    $id = $stock->stock_produk_id;
                    $result['success'] = false;
                    $result['message'] = "missing parameter";
                    $old_data = $this->stock_produk->row_by_id($id);
                    if($id != ""){
                        $data["delete_flag"] = 1;
                        $delete = $this->stock_produk->update_by_id("stock_produk_id",$id,$data);
                        if($delete){
                            $result['success'] = true;
                            $result['message'] = "Data berhasil dihapus";
                            $data = array();
                            $data["tanggal"] = date("Y-m-d");
                            $data["table_name"] = "stock_produk";
                            $data["stock_produk_id"] = $id;
                            $data["produk_id"] = $produk_id;
                            $data["stock_out"] = $old_data->stock_produk_qty;
                            $data["stock_in"] = 0;
                            $data["last_stock"] = $this->stock_produk->last_stock($produk_id)->result;
                            $data["last_stock_total"] = $this->stock_produk->stock_total()->result;
                            $data["keterangan"] = "Delete stok produk";
                            $data["method"] = "delete";
                            $this->stock_produk->arus_stock_produk($data);
                        } else {
                            $result['message'] = "Gagal menghapus data";
                        }
                    }
                }
                $delete_item = $this->assembly_item->delete_by_id("assembly_id",$assembly_id);
                if ($delete_item) {
                    $insert = $this->assembly->edit_assembly();
                    if($insert){
                        $result['success'] = true;
                        $result['message'] = "Berhasil menyimpan data";         
                    }
                }
                echo json_encode($result); 
            }
        }
        
    }
    function delete_assembly_produk(){
        $id = $this->input->input_stream('id');
        $result['success'] = false;
        $result['message'] = "missing parameter";
        if($id != ""){
            $assembly = $this->assembly->get_id_assembly($id);
            $assembly_id = $assembly['assembly_id'];
            $stock_perlokasi = $this->assembly->stock_perlokasi($id);
            $item_assembly = $this->assembly->assembly_detail_by_id($assembly_id);
            if (count($stock_perlokasi)==0){
                $delete_item = $this->assembly_item->delete_by_id("assembly_id",$assembly_id);
                if ($delete_item){
                    $delete_assembly = $this->assembly->delete_by_id("produk_id",$id);
                    if ($delete_assembly) {
                        $delete_produk = $this->produk->delete_by_id("produk_id",$id);
                        if ($delete_produk) {
                            $result['success'] = true;
                            $result['message'] = "Data berhasil dihapus";
                        }
                    }
                } else {
                    $result['message'] = "Gagal menghapus data";
                }
                echo json_encode($result);
            } else {
                foreach ($stock_perlokasi as $stock) {
                    foreach ($item_assembly as $item) {
                        $jml_stok_produk_assembly = $stock->stock_produk_qty;
                        $jml_komposisi = $item->jumlah;
                        if ($item->unit_id==0){
                            $stock_produk_qty = $jml_stok_produk_assembly*$jml_komposisi;
                        } else {
                            $unit_satuan = $this->unit->row_by_id($item->unit_id);
                            $konversi_nilai = $unit_satuan->jumlah_satuan_unit;
                            $stock_produk_qty = $jml_stok_produk_assembly*$jml_komposisi*$konversi_nilai;
                        }
                        $data["stock_produk_qty"] = $stock_produk_qty;
                        $data["produk_id"] = $item->produk_id;
                        $data["stock_produk_lokasi_id"] = $stock->stock_produk_lokasi_id;
                        $data["year"] = date("y");
                        $data["month"] = date("m");
                        $lokasi_kode = $this->lokasi->row_by_id($data["stock_produk_lokasi_id"])->lokasi_kode;
                        $jenis_produk_kode = $this->produk->produk_by_id($data["produk_id"])->jenis_produk_kode;
                        $data["stock_produk_seri"] = $data["month"].$data["year"].$jenis_produk_kode.$lokasi_kode;
                        $data["urutan"] = $this->stock_produk->urutan_seri($data["stock_produk_seri"]);
                        $data["stock_produk_seri"] = $data["stock_produk_seri"].$data["urutan"];
                        $data["stock_produk_keterangan"] = '';
                        $data["hpp"] = 0;
                        $insert_produk = $this->stock_produk->insert($data);
                        if($insert_produk){
                            $result['success'] = true;
                            $result['message'] = "Data berhasil disimpan";
                            $data_stock_in = array();
                            $data_stock_in["tanggal"] = date("Y-m-d");
                            $data_stock_in["table_name"] = "stock_produk";
                            $data_stock_in["stock_produk_id"] = $this->stock_produk->last_id();
                            $data_stock_in["produk_id"] = $item->produk_id;
                            $data_stock_in["stock_out"] = 0;
                            $data_stock_in["stock_in"] = $stock_produk_qty;
                            $data_stock_in["last_stock"] = $this->stock_produk->last_stock($item->produk_id)->result;
                            $data_stock_in["last_stock_total"] = $this->stock_produk->stock_total()->result;
                            $data_stock_in["keterangan"] = "Insert stok produk dari produk assembly";
                            $data_stock_in["method"] = "insert";
                            $this->stock_produk->arus_stock_produk($data_stock_in);
                        } else {
                            $result['message'] = "Gagal menyimpan data";
                        } 
                    }
                }
                $delete_assembly_item = $this->assembly_item->delete_by_id("assembly_id",$assembly_id);
                if ($delete_assembly_item){
                    $delete_assembly = $this->assembly->delete_by_id("produk_id",$id);
                    if ($delete_assembly) {
                        $delete_produk = $this->produk->delete_by_id("produk_id",$id);
                        if ($delete_produk) {
                            $result['success'] = true;
                            $result['message'] = "Data berhasil dihapus";
                        }
                    }
                } else {
                    $result['message'] = "Gagal menghapus data";
                }
                    
                echo json_encode($result);
            }
        }
    }
    function delete_stock_perlokasi(){
        $produk_id = $this->uri->segment(3);
        $id = $this->input->input_stream('id');
        $result['success'] = false;
        $result['message'] = "missing parameter";
        if($id != ""){
            $assembly = $this->assembly->get_id_assembly($produk_id);
            $assembly_id = $assembly['assembly_id'];
            $stock_perlokasi = $this->assembly->stock_perlokasi($id);

            $item_assembly = $this->assembly->assembly_detail_by_id($assembly_id);
            $old_data = $this->stock_produk->row_by_id($id);
            foreach ($item_assembly as $item) {
                $jml_stok_produk_assembly = $old_data->stock_produk_qty;
                $jml_komposisi = $item->jumlah;
                if ($item->unit_id==0){
                    $stock_produk_qty = $jml_stok_produk_assembly*$jml_komposisi;
                } else {
                    $unit_satuan = $this->unit->row_by_id($item->unit_id);
                    $konversi_nilai = $unit_satuan->jumlah_satuan_unit;
                    $stock_produk_qty = $jml_stok_produk_assembly*$jml_komposisi*$konversi_nilai;
                }
                $data["stock_produk_qty"] = $stock_produk_qty;
                $data["produk_id"] = $item->produk_id;
                $data["stock_produk_lokasi_id"] = $old_data->stock_produk_lokasi_id;
                $data["year"] = date("y");
                $data["month"] = date("m");
                $lokasi_kode = $this->lokasi->row_by_id($data["stock_produk_lokasi_id"])->lokasi_kode;
                $jenis_produk_kode = $this->produk->produk_by_id($data["produk_id"])->jenis_produk_kode;
                $data["stock_produk_seri"] = $data["month"].$data["year"].$jenis_produk_kode.$lokasi_kode;
                $data["urutan"] = $this->stock_produk->urutan_seri($data["stock_produk_seri"]);
                $data["stock_produk_seri"] = $data["stock_produk_seri"].$data["urutan"];
                $data["stock_produk_keterangan"] = '';
                $data["hpp"] = 0;
                $insert_produk = $this->stock_produk->insert($data);
                if($insert_produk){
                    $result['success'] = true;
                    $result['message'] = "Data berhasil disimpan";
                    $data_stock_in = array();
                    $data_stock_in["tanggal"] = date("Y-m-d");
                    $data_stock_in["table_name"] = "stock_produk";
                    $data_stock_in["stock_produk_id"] = $this->stock_produk->last_id();
                    $data_stock_in["produk_id"] = $item->produk_id;
                    $data_stock_in["stock_out"] = 0;
                    $data_stock_in["stock_in"] = $stock_produk_qty;
                    $data_stock_in["last_stock"] = $this->stock_produk->last_stock($item->produk_id)->result;
                    $data_stock_in["last_stock_total"] = $this->stock_produk->stock_total()->result;
                    $data_stock_in["keterangan"] = "Insert stok produk dari produk assembly";
                    $data_stock_in["method"] = "insert";
                    $this->stock_produk->arus_stock_produk($data_stock_in);
                } else {
                    $result['message'] = "Gagal menyimpan data";
                } 
            }
            
            $data["delete_flag"] = 1;
            $delete = $this->stock_produk->update_by_id("stock_produk_id",$id,$data);
            if($delete){
                $result['success'] = true;
                $result['message'] = "Data berhasil dihapus";
                $data = array();
                $data["tanggal"] = date("Y-m-d");
                $data["table_name"] = "stock_produk";
                $data["stock_produk_id"] = $id;
                $data["produk_id"] = $produk_id;
                $data["stock_out"] = $old_data->stock_produk_qty;
                $data["stock_in"] = 0;
                $data["last_stock"] = $this->stock_produk->last_stock($produk_id)->result;
                $data["last_stock_total"] = $this->stock_produk->stock_total()->result;
                $data["keterangan"] = "Delete stok produk";
                $data["method"] = "delete";
                $this->stock_produk->arus_stock_produk($data);
            } else {
                $result['message'] = "Gagal menghapus data";
            }
        }
        echo json_encode($result);
    }
    function produk_list(){
        if(isset($_GET["columns"][3]["search"]["value"]) && $_GET["columns"][3]["search"]["value"] != ""){
            $_GET['stock_produk_seri'] = $_GET["columns"][3]["search"]["value"];
        }
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->stock_produk->produk_item_count();
        $result['iTotalDisplayRecords'] = $this->stock_produk->produk_item_filter($query);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $count = $result['iTotalDisplayRecords'];
        $_GET["is_pos"] = 1;
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->stock_produk->produk_item_list($start,$length,$query);
        $i = $start+1;
        foreach ($data as $key) {
            $key->aksi = null;
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i; 
            $key->delete_url = base_url().'stock-bahan/delete/'.$key->produk_id;
            $key->row_id = $key->produk_id;
            $i++;
        }
        $result['aaData'] = $data;              
        echo json_encode($result);      
    }

}

/* End of file AssemblyController.php */
/* Location: ./application/controllers/AssemblyController.php */
