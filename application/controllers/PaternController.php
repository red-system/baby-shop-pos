<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PaternController extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('patern','',true);
		
	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		array_push($this->js, "script/app.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Patern < Master Data < ".$this->config->item('company_name');;
		$data['parrent'] = "master_data";
		$data['page'] = $this->uri->segment(1);
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"patern_nama"));
		array_push($column, array("data"=>"created_at"));
		array_push($column, array("data"=>"updated_at"));
				$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
		$akses_menu = json_decode($this->menu_akses,true);
		$action = array();
		foreach ($akses_menu['master_data']['jenis-bahan'] as $key => $value) {
			if($key != "list" && $key != "akses_menu"){
				$action[$key] = $value;
			}
		}
		$data['action'] = json_encode($action);		
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/patern');
		$this->load->view('admin/static/footer');
	}
	function list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->patern->patern_count_all();
		$result['iTotalDisplayRecords'] = $this->patern->patern_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->patern->patern_list($start,$length,$query);
		$i = $start+1;
				foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i;
			$i++;
			$key->delete_url = base_url().'patern/delete/';
			$key->row_id = $key->patern_id;		
		}
		$result['aaData'] = $data;	
		echo json_encode($result);	
	}
	function options(){
		$data = array();
		$patern = $this->patern->all_list();
		foreach ($patern as $key) {
			array_push($data, strtoupper($key->patern_nama));
		}
		echo json_encode($data);
	}
	function add(){
		$result['success'] = false;
		$result['message'] = "Kode ini telah terdaftar";
			$patern_nama = $this->input->post('patern_nama');
			$data = array("patern_nama"=>$patern_nama);
			$insert = $this->patern->insert($data);
			if($insert){
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
			} else {
				$result['message'] = "Gagal menyimpan data";
			}
		echo json_encode($result);
	}
	function edit(){
		$result['success'] = false;
		$result['message'] = "Kode sudah terdaftar";
		$data = array();
		$data["patern_nama"] = $this->input->post('patern_nama');
		$patern_id = $this->input->post('patern_id');
			$updated_at = date('Y-m-d H:i:s');
			$data['updated_at'] = $updated_at;
			$update = $this->patern->update_by_id('patern_id',$patern_id,$data);
			if($update){
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
			} else {
				$result['message'] = "Gagal menyimpan data";
			}
		echo json_encode($result);
	}
	function delete(){
		$id = $this->input->input_stream('id');
		$result['success'] = false;
		$result['message'] = "missing parameter";
		if($id != ""){
			$delete = $this->patern->delete_by_id("patern_id",$id);
			if($delete){
				$result['success'] = true;
				$result['message'] = "Data berhasil dihapus";
			} else {
				$result['message'] = "Gagal menghapus data";
			}
		}
		echo json_encode($result);
	}

}

/* End of file PaternController.php */
/* Location: ./application/controllers/PaternController.php */
