<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PoProdukController extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('po_produk','',true);
		$this->load->model('lokasi','',true);
		$this->load->model('stock_produk','',true);
		$this->load->model('suplier','',true);
		$this->load->model('tipe_pembayaran','',true);
	}
	
	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");		
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		
		array_push($this->js, "script/app.js");
		array_push($this->js, "script/admin/po_produk.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Order Produk < ".$this->config->item('company_name');;
		$data['parrent'] = "order_produk";
		$data['page'] = "order_produk";
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"po_produk_no"));
		array_push($column, array("data"=>"tanggal_pemesanan"));
		array_push($column, array("data"=>"suplier_nama"));
		array_push($column, array("data"=>"grand_total_lbl"));
		array_push($column, array("data"=>"jenis_pembayaran"));
		array_push($column, array("data"=>"tipe_pembayaran_nama"));
		array_push($column, array("data"=>"status_pembayaran","template"=>"badgeTemplate"));
		array_push($column, array("data"=>"tanggal_penerimaan"));
		array_push($column, array("data"=>"status_penerimaan","template"=>"badgeTemplate"));
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0,4)));
		$akses_menu = json_decode($this->menu_akses,true);
		$action = array();
		foreach ($akses_menu['order_produk'] as $key => $value) {
			if($key != "list" && $key != "akses_menu"){
				$action[$key] = $value;
			}
		}
		$data['lokasi'] = $this->lokasi->all_list();
		$data['action'] = json_encode($action);
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/po_produk');
		$this->load->view('admin/static/footer');
	}
	function list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->po_produk->po_produk_count_all();
		$result['iTotalDisplayRecords'] = $this->po_produk->po_produk_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->po_produk->po_produk_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			if($key->tanggal_pemesanan != null){
				$time = strtotime($key->tanggal_pemesanan);
				$key->tanggal_pemesanan = date('d-m-Y',$time);
			}
			if($key->tanggal_penerimaan != null){
				$time = strtotime($key->tanggal_penerimaan);
				$key->tanggal_penerimaan = date('d-m-Y',$time);
			}
			$key->total = number_format($key->total);
			$key->tambahan = number_format($key->tambahan);
			$key->potongan = number_format($key->potongan);
            $key->grand_total_lbl = $this->idr_currency($key->grand_total);
			$key->grand_total = number_format($key->grand_total);
			$key->penerimaan_status_btn = true;
			$key->edit_url = base_url().'po-produk/edit/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->po_produk_id));
			$key->no = $i;
			if($key->jenis_pembayaran == "kas"){
				$key->tipe_pembayaran_nama = $key->kas_nama." ".$key->no_akun;
			}
			$i++;
			$key->delete_url = base_url().'po-produk/delete/';
			$key->row_id = $key->po_produk_id;
			$key->action = null;
		}
		$result['aaData'] = $data;			
		echo json_encode($result);
	}
	function add(){
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");		
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		array_push($this->js, "vendors/general/typeahead.js/dist/typeahead.bundle.js");
		array_push($this->js, "script/admin/po_produk.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Order Produk < ".$this->config->item('company_name');;
		$data['parrent'] = "order_produk";
		$data['page'] = $this->uri->segment(1);	
		$data['lokasi'] = $this->lokasi->all_list();
		$data['suplier'] = $this->suplier->all_list();	
		$data['tipe_pembayaran'] = $this->tipe_pembayaran->all_kas();
		$data['po_produk_no'] = 	$this->po_produk->get_kode_po_produk();
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/add_po_produk');
		$this->load->view('admin/static/footer');
		unset($_SESSION['po_produk']);
		$_SESSION['po_produk']['lokasi'] = 1;
	}
	function utility(){
		$key = $this->uri->segment(3);
		if($key == "get-po-no"){
			$code = $this->input->post('suplier_kode');
			echo $this->po_produk->get_kode_po_produk($code);
		}
		if($key=="list-produk"){
			$this->list_produk();
		}
		if($key=="sess-produk-add"){
			$no = $this->input->post('key');
			$produk_id = $this->input->post('produk_id');
			$_SESSION['po_produk']['produk']['con_'.$no]['produk_id'] = $produk_id;
			$_SESSION['po_produk']['produk']['con_'.$no]['value'] = 0;
		}
		if($key=="sess-produk-change"){		
			$jumlah = $this->input->post('jumlah');
			$no = $this->input->post('key');
			if(isset($_SESSION['po_produk']['produk']['con_'.$no]['produk_id'])){
				$_SESSION['po_produk']['produk']['con_'.$no]['value'] = $jumlah;
			}
		}
		if($key=="sess-produk-delete"){
			$no = $this->input->post('key');
			unset($_SESSION['po_produk']['produk']['con_'.$no]);
		}
		if($key=="sess-produk-reset"){
			unset($_SESSION['po_produk']['produk']);
		}
		if($key=="sess-lokasi"){
			$_SESSION['po_produk']['lokasi'] = $this->input->post('lokasi_id');
		}				
	}
	function list_produk(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->produk->produk_count_all();
		$result['iTotalDisplayRecords'] = $this->produk->produk_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->produk->produk_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i;
			$i++;
			$key->delete_url = base_url().'produk/delete/';
			$key->action =null;
			$key->row_id = $key->produk_id;
			$key->produk_minimal_stock = number_format($key->produk_minimal_stock);
		}
		$result['aaData'] = $data;				
		echo json_encode($result);		
	}
	function save_add(){
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$_POST["suplier_id"] = $this->suplier->find_suplier_by_name($this->input->post('suplier_nama'));		
		$insert = $this->po_produk->insert_po_produk();
		if($insert){
			$result['success'] = true;
			$result['message'] = "Berhasil menyimpan data";			
		}
		echo json_encode($result);
	}
	function edit(){
		$url = str_replace(array("-","_"), array("+","/"), $this->uri->segment(3));
		$id = $this->encryption->decrypt($url);
		$po_produk = $this->po_produk->po_produk_by_id($id);
		if ($po_produk != null) {
			$data['po_produk'] = $po_produk;
			$data['po_produk_detail']=$this->po_produk->po_produk_detail_by_id($id);
			array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
			array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
			array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
			array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");		
			array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
			array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
			array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
			array_push($this->js, "vendors/general/typeahead.js/dist/typeahead.bundle.js");
			array_push($this->js, "script/admin/po_produk.js");
			$data['lokasi'] = $this->lokasi->all_list();
			$data['suplier'] = $this->suplier->all_list();
			$data['tipe_pembayaran'] = $this->tipe_pembayaran->all_kas();	
			$data["css"] = $this->css;
			$data["js"] = $this->js;
			$column = array();
			$data["meta_title"] = "Produksi < ".$this->config->item('company_name');;
			$data['parrent'] = "master_data";
			$data['page'] = $this->uri->segment(1);		
			$this->load->view('admin/static/header',$data);
			$this->load->view('admin/static/sidebar');
			$this->load->view('admin/static/topbar');
			$this->load->view('admin/edit_po_produk');
			$this->load->view('admin/static/footer');			
		}else {

			redirect('404_override','refresh');
		}
	}
	function save_edit(){
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$_POST["suplier_id"] = $this->suplier->find_suplier_by_name($this->input->post('suplier_nama'));		
		$insert = $this->po_produk->edit_po_produk();
		if($insert){
			$result['success'] = true;
			$result['message'] = "Berhasil menyimpan data";			
		}
		echo json_encode($result);		
	}
	function delete(){
		$id = $this->input->input_stream('id');
		$result['success'] = false;
		$result['message'] = "missing parameter";
		if($id != ""){
			$delete = $this->po_produk->delete_by_id("po_produk_id",$id);
			if($delete){
				$result['success'] = true;
				$result['message'] = "Data berhasil dihapus";
			} else {
				$result['message'] = "Gagal menghapus data";
			}
		}
		echo json_encode($result);
	}
	function detail(){
		$id = $this->input->post('po_produk_id');
		$temp = $this->po_produk->po_produk_by_id($id);
		$no = 0;
		$no_produk = 0;
		$post = array();
		$temp->item = $this->po_produk->po_produk_detail_by_id($id);
		$temp->metode_pembayaran = $temp->tipe_pembayaran_nama." ".$temp->tipe_pembayaran_no;
		if($temp->tanggal_pemesanan != null){
			$x = strtotime($temp->tanggal_pemesanan);
			$temp->tanggal_pemesanan = date("Y-m-d",$x);
		}
		if($temp->tanggal_penerimaan != null){
			$x = strtotime($temp->tanggal_penerimaan);
			$temp->tanggal_penerimaan = date("Y-m-d",$x);
		}
		if($temp->created_at != null){
			$time = strtotime($temp->created_at);
			$temp->created_at = date('d-m-Y H:i:s',$time);
		}
		if($temp->updated_at != null){
			$time = strtotime($temp->updated_at);
			$temp->updated_at = date('d-m-Y H:i:s',$time);
		}
		foreach ($temp->item as $key) {
			$key->harga = number_format($key->harga);
			$key->jumlah = number_format($key->jumlah);
			$key->sub_total = number_format($key->sub_total);
		}
			$temp->total = number_format($temp->total);
			$temp->tambahan = number_format($temp->tambahan);
			$temp->potongan = number_format($temp->potongan);
			$temp->grand_total = number_format($temp->grand_total);		
		echo json_encode($temp);
	}
	function penerimaan_po_produk(){
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$po_produk_id = $this->input->post('po_produk_id');
		$data["status_penerimaan"] = "Diterima";
		$data["tanggal_penerimaan"] = date("Y-m-d",strtotime($this->input->post('tanggal_penerimaan')));
		$lokasi_id = $this->input->post('lokasi_penerimaan_id');
		$data['lokasi_penerimaan_id'] = $lokasi_id;
		$this->po_produk->start_trans();
		$update_po_produk = $this->po_produk->update_by_id('po_produk_id',$po_produk_id,$data);
		if($update_po_produk){
			$po_produk_item = $this->po_produk->po_produk_detail_by_id($po_produk_id);
			foreach ($po_produk_item as $key) {
				$produk_lokasi = $this->stock_produk->produk_by_lokasi($lokasi_id,$key->produk_id);
				if(sizeof($produk_lokasi)>0){
					$produk = $produk_lokasi[0];
					$data = array();
					$data["stock_produk_qty"] = $produk->stock_produk_qty + $key->jumlah;
					$insert = $this->stock_produk->update_by_id('stock_produk_id',$produk->stock_produk_id,$data);
					$arus["stock_produk_id"] = $produk->stock_produk_id;
					$arus["method"] = "update"; 					
				} else {
					$data = array();
					$data["stock_produk_qty"] = $key->jumlah;
					$data["produk_id"] = $key->produk_id;
					$data["stock_produk_lokasi_id"] = $lokasi_id;
					$insert = $this->stock_produk->insert($data);					
					$arus["stock_produk_id"] = $this->stock_produk->last_id();
					$arus["method"] = "insert";
				}
					$arus["tanggal"] = date("Y-m-d");
					$arus["table_name"] = "stock_produk";
					$arus["produk_id"] = $key->produk_id;
					$arus["stock_out"] = 0;
					$arus["stock_in"] = $key->jumlah;
					$arus["last_stock"] = $this->stock_produk->last_stock($key->produk_id)->result;
					$arus["last_stock_total"] = $this->stock_produk->stock_total()->result;
					$arus["keterangan"] = "Order Produk";
					$this->stock_produk->arus_stock_produk($arus);					
			}
		}
		if($this->po_produk->result_trans()){
			$result['success'] = true;
			$result['message'] = "Berhasil menyimpan data";			
		}
		echo json_encode($result);
	}
	function history(){
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");		
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		
		array_push($this->js, "script/app.js");
		array_push($this->js, "script/admin/po_produk.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "History Order Produk < ".$this->config->item('company_name');;
		$data['parrent'] = "order_produk";
		$data['page'] = "order_produk";
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"po_produk_no"));
		array_push($column, array("data"=>"tanggal_pemesanan"));
		array_push($column, array("data"=>"suplier_nama"));
		array_push($column, array("data"=>"grand_total_lbl"));
		array_push($column, array("data"=>"jenis_pembayaran"));
		array_push($column, array("data"=>"tipe_pembayaran_nama"));
		array_push($column, array("data"=>"status_pembayaran","template"=>"badgeTemplate"));
		array_push($column, array("data"=>"tanggal_penerimaan"));
		array_push($column, array("data"=>"status_penerimaan","template"=>"badgeTemplate"));
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0,4)));
		$data['lokasi'] = $this->lokasi->all_list();
		$action = array("view"=>true);
		$data['action'] = json_encode($action);
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/po_produk');
		$this->load->view('admin/static/footer');		
	}
	function history_list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->po_produk->po_produk_count_all_history();
		$result['iTotalDisplayRecords'] = $this->po_produk->po_produk_count_filter_history($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->po_produk->po_produk_list_history($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			if($key->tanggal_pemesanan != null){
				$time = strtotime($key->tanggal_pemesanan);
				$key->tanggal_pemesanan = date('d-m-Y',$time);
			}
			if($key->tanggal_penerimaan != null){
				$time = strtotime($key->tanggal_penerimaan);
				$key->tanggal_penerimaan = date('d-m-Y',$time);
			}
			$key->total = number_format($key->total);
			$key->tambahan = number_format($key->tambahan);
			$key->potongan = number_format($key->potongan);
            $key->grand_total_lbl = $this->idr_currency($key->grand_total);
			$key->grand_total = number_format($key->grand_total);
			
			$key->edit_url = base_url().'po-produk/edit/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->po_produk_id));
			$key->no = $i;
			$i++;
			$key->delete_url = base_url().'po-produk/delete/';
			$key->row_id = $key->po_produk_id;
			$key->action = null;
		}
		$result['aaData'] = $data;			
		echo json_encode($result);
	}	
}

/* End of file PoProdukController.php */
/* Location: ./application/controllers/PoProdukController.php */