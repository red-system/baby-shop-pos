<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProduksiController extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('produksi','',true);
		$this->load->model('lokasi','',true);
		$this->load->model('stock_produk','',true);
		$this->load->model('stock_bahan','',true);
		$this->load->model('konversi_bahan','',true);
	}
	
	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");		
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		
		array_push($this->js, "script/app.js");
		array_push($this->js, "script/admin/produksi.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Produksi < ".$this->config->item('company_name');;
		$data['parrent'] = "produksi";
		$data['page'] = $this->uri->segment(1);
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"produksi_kode"));
		array_push($column, array("data"=>"jumlah_item"));
		array_push($column, array("data"=>"tanggal_mulai"));
		array_push($column, array("data"=>"estimasi_selesai"));
		array_push($column, array("data"=>"status_produksi","template"=>"badgeTemplate"));
		array_push($column, array("data"=>"tanggal_selesai"));
		array_push($column, array("data"=>"keterangan"));
		array_push($column, array("data"=>"status_penerimaan","template"=>"badgeTemplate"));
		array_push($column, array("data"=>"tanggal_penerimaan"));
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
		$akses_menu = json_decode($this->menu_akses,true);
		$action = array();
		foreach ($akses_menu['produksi']['produksi'] as $key => $value) {
			if($key != "list" && $key != "akses_menu"){
				$action[$key] = $value;
			}
		}
		$data['lokasi'] = $this->lokasi->all_list();
		$data['action'] = json_encode($action);
		$data['produksi_kode'] = $this->produksi->get_kode_produksi();
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/produksi');
		$this->load->view('admin/static/footer');
	}
	function list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->produksi->produksi_count_all();
		$result['iTotalDisplayRecords'] = $this->produksi->produksi_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->produksi->produksi_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			if($key->tanggal_mulai != null){
				$time = strtotime($key->tanggal_mulai);
				$key->tanggal_mulai = date('d-m-Y',$time);
			}
			if($key->tanggal_selesai != null){
				$time = strtotime($key->tanggal_selesai);
				$key->tanggal_selesai = date('d-m-Y',$time);
			}
			if($key->estimasi_selesai != null){
				$time = strtotime($key->estimasi_selesai);
				$key->estimasi_selesai = date('d-m-Y',$time);
			}
			if($key->status_produksi == "Selesai"){
				$key->deny_edit = true;
				$key->deny_delete = true;
				if($key->status_penerimaan == "Belum Diterima")	{
					$key->penerimaan_status_btn = true;
				}
			} else{
				$key->produksi_status_btn = true;
			}
			$key->edit_url = base_url().'produksi/edit/'.$this->encrypt_id($key->produksi_id);
			$key->no = $i;
			$i++;
			$key->delete_url = base_url().'produksi/delete/';
			$key->row_id = $key->produksi_id;
			$key->action = null;
		}
		$result['aaData'] = $data;			
		echo json_encode($result);
	}
	function add(){
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");		
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		array_push($this->js, "script/admin/produksi.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Produksi < ".$this->config->item('company_name');;
		$data['parrent'] = "master_data";
		$data['page'] = $this->uri->segment(1);	
		$data['lokasi'] = $this->lokasi->all_list();		
		$data['produksi_kode'] = $this->produksi->get_kode_produksi();
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/add_produksi');
		$this->load->view('admin/static/footer');
		unset($_SESSION['produksi']);
		$_SESSION['produksi']['lokasi'] = 1;
	}
	function utility(){
		$key = $this->uri->segment(3);
		if($key=="list-bahan"){
			$this->list_bahan();
		}
		if($key=="sess-bahan-add"){
			$no = $this->input->post('key');
			$bahan_id = $this->input->post('bahan_id');
			$_SESSION['produksi']['bahan']['con_'.$no]['bahan_id'] = $bahan_id;
			$_SESSION['produksi']['bahan']['con_'.$no]['value'] = 0;
		}
		if($key=="sess-bahan-change"){		
			$jumlah = $this->input->post('jumlah');
			$no = $this->input->post('key');
			if(isset($_SESSION['produksi']['bahan']['con_'.$no]['bahan_id'])){
				$_SESSION['produksi']['bahan']['con_'.$no]['value'] = $jumlah;
			}
		}
		if($key=="sess-bahan-delete"){
			$no = $this->input->post('key');
			unset($_SESSION['produksi']['bahan']['con_'.$no]);
		}
		if($key=="sess-bahan-reset"){
			unset($_SESSION['produksi']['bahan']);
		}
		if($key=="sess-lokasi"){
			$_SESSION['produksi']['lokasi'] = $this->input->post('lokasi_id');
		}				
	}
	function list_bahan(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->bahan->bahan_count_all();
		$result['iTotalDisplayRecords'] = $this->bahan->bahan_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->bahan->bahan_list($start,$length,$query);
		$tempBahan = array();
		if(isset($_SESSION['produksi']['bahan'])){
			foreach ($_SESSION['produksi']['bahan'] as $key) {
				if(!isset($tempBahan["bahan_".$key["bahan_id"]])){
					$tempBahan["bahan_".$key["bahan_id"]] = 0;
				}
				$tempBahan["bahan_".$key["bahan_id"]] += $key["value"];

			}
		}
		
		$i = $start+1;
		foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			if(isset($tempBahan['bahan_'.$key->bahan_id])){
				$key->jumlah_lokasi = $key->jumlah_lokasi - $tempBahan['bahan_'.$key->bahan_id];
			}
			$key->no = $i;
			$i++;
			$key->delete_url = base_url().'bahan/delete/';
			$key->action =null;
			$key->row_id = $key->bahan_id;
			$key->bahan_minimal_stock = number_format($key->bahan_minimal_stock);
			$key->jumlah_lokasi = number_format($key->jumlah_lokasi);
			if(isset($_SESSION['login']['lokasi_id'])){
				$key->jumlah_lokasi = number_format($key->jumlah_lokasi);	
			}
		}
		$result['aaData'] = $data;				
		echo json_encode($result);		
	}
	function save_add(){
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$insert = $this->produksi->insert_produksi();
		if($insert){
			$result['success'] = true;
			$result['message'] = "Berhasil menyimpan data";
		}
		echo json_encode($result);
	}
	function edit(){
		$id = $this->dencrypt_id($this->uri->segment(3));
		$produksi = $this->produksi->row_by_id($id);
		if ($produksi != null) {
			unset($_SESSION['produksi']);
			$_SESSION['produksi']['lokasi'] = 1;
			$temp = $this->produksi->produksi_by_id($id);
			$no = 0;
			$no_bahan = 0;
			$post = array();
			$produkCheck = '';
			foreach ($temp as $key => $value) {			
				$post['produksi_kode'] = $value->produksi_kode;
				$post['tanggal_mulai'] = $value->tanggal_mulai;
				$post['estimasi_selesai'] = $value->estimasi_selesai;
				$post['lokasi_bahan_id'] = $value->lokasi_bahan_id;
				$post['keterangan'] = $value->keterangan;				
				$post['item']['produk_'.$value->produksi_item_id]['item_bahan']['bahan_'.$no_bahan]['bahan_id'] = $value->bahan_id;
				$post['item']['produk_'.$value->produksi_item_id]['item_bahan']['bahan_'.$no_bahan]['bahan_nama'] = $value->bahan_nama;
				$post['item']['produk_'.$value->produksi_item_id]['item_bahan']['bahan_'.$no_bahan]['jumlah'] = $value->jumlah_bahan;
				$post['item']['produk_'.$value->produksi_item_id]['item_bahan']['bahan_'.$no_bahan]['no_bahan'] = $no_bahan;
				$post['item']['produk_'.$value->produksi_item_id]['item_bahan']['bahan_'.$no_bahan]['potong'] = $this->konversi_bahan->konversi_bahan_by_produk($value->produk_id,$value->bahan_id)->jumlah;
				$_SESSION['produksi']['bahan']['con_'.$no_bahan]['bahan_id'] = $value->bahan_id;
				$_SESSION['produksi']['bahan']['con_'.$no_bahan]['value'] = $value->jumlah_bahan;
				$no_bahan++;
				if($produkCheck != $value->produk_nama){
					$post['item']['produk_'.$value->produksi_item_id]['produksi_item_id'] = $value->produksi_item_id;
					$post['item']['produk_'.$value->produksi_item_id]['produk_id'] = $value->produk_id;
					$post['item']['produk_'.$value->produksi_item_id]['produk_nama'] = $value->produk_nama;
					$post['item']['produk_'.$value->produksi_item_id]['jumlah'] = $value->jumlah;
					$post['item']['produk_'.$value->produksi_item_id]['produk_keterangan'] = $value->produk_keterangan;
					$produkCheck = $value->produk_nama;
					$no = $value->produksi_item_id;	
				}				

			}
			$data['no_produk'] = $no;
			$data['no_bahan'] = $no_bahan;
			$data['produksi'] = $post;
			$data['id'] = $id;
			array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
			array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
			array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
			array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");		
			array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
			array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
			array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
			array_push($this->js, "script/admin/produksi.js");
			$data['lokasi'] = $this->lokasi->all_list();
			$data["css"] = $this->css;
			$data["js"] = $this->js;
			$column = array();
			$data["meta_title"] = "Produksi < ".$this->config->item('company_name');;
			$data['parrent'] = "master_data";
			$data['page'] = $this->uri->segment(1);		
			$data['produksi_kode'] = $this->produksi->get_kode_produksi();
			$this->load->view('admin/static/header',$data);
			$this->load->view('admin/static/sidebar');
			$this->load->view('admin/static/topbar');
			$this->load->view('admin/edit_produksi');
			$this->load->view('admin/static/footer');			
		}else {

			redirect('404_override','refresh');
		}
	}
	function save_edit(){
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";		
		$insert = $this->produksi->edit_produksi();
		if($insert){
			$result['success'] = true;
			$result['message'] = "Berhasil menyimpan data";			
		}
		echo json_encode($result);		
	}
	function delete(){
		$id = $this->input->input_stream('id');
		$result['success'] = false;
		$result['message'] = "missing parameter";
		if($id != ""){
			$delete = $this->produksi->delete_by_id("produksi_id",$id);
			if($delete){
				$result['success'] = true;
				$result['message'] = "Data berhasil dihapus";
			} else {
				$result['message'] = "Gagal menghapus data";
			}
		}
		echo json_encode($result);
	}
	function detail(){
		$id = $this->input->post('produksi_id');
		$temp = $this->produksi->produksi_by_id($id);
		$no = 0;
		$no_bahan = 0;
		$post = array();
		$produkCheck = '';
		foreach ($temp as $key => $value) {
			if($value->created_at != null){
				$time = strtotime($value->created_at);
				$value->created_at = date('d-m-Y H:i:s',$time);
			}
			if($value->updated_at != null){
				$time = strtotime($value->updated_at);
				$value->updated_at = date('d-m-Y H:i:s',$time);
			}
			if($value->tanggal_mulai != null){
				$time = strtotime($value->tanggal_mulai);
				$value->tanggal_mulai = date('d-m-Y',$time);
			}
			if($value->tanggal_selesai != null){
				$time = strtotime($value->tanggal_selesai);
				$value->tanggal_selesai = date('d-m-Y',$time);
			}
			if($value->estimasi_selesai != null){
				$time = strtotime($value->estimasi_selesai);
				$value->estimasi_selesai = date('d-m-Y',$time);
			}
			if($value->tanggal_penerimaan != null){
				$time = strtotime($value->tanggal_penerimaan);
				$value->tanggal_penerimaan = date('d-m-Y',$time);
			}							
			$post['status_produksi'] = $value->status_produksi;
			$post['tanggal_selesai'] = $value->tanggal_selesai;
			$post['status_penerimaan'] = $value->status_penerimaan;
			$post['tanggal_penerimaan'] = $value->tanggal_penerimaan;	
			$post['produksi_kode'] = $value->produksi_kode;
			$post['tanggal_mulai'] = $value->tanggal_mulai;
			$post['estimasi_selesai'] = $value->estimasi_selesai;

			$post['keterangan'] = $value->keterangan;				
			$post['item']['produk_'.$value->produksi_item_id]['item_bahan']['bahan_'.$no_bahan]['bahan_id'] = $value->bahan_id;
			$post['item']['produk_'.$value->produksi_item_id]['item_bahan']['bahan_'.$no_bahan]['bahan_nama'] = $value->bahan_nama;
			$post['item']['produk_'.$value->produksi_item_id]['item_bahan']['bahan_'.$no_bahan]['jumlah'] = $value->jumlah_bahan;
			$post['item']['produk_'.$value->produksi_item_id]['item_bahan']['bahan_'.$no_bahan]['no_bahan'] = $no_bahan;
			$_SESSION['produksi']['bahan']['con_'.$no_bahan]['bahan_id'] = $value->bahan_id;
			$_SESSION['produksi']['bahan']['con_'.$no_bahan]['value'] = $value->jumlah_bahan;
			$no_bahan++;
			if($produkCheck != $value->produk_nama){
				if(!isset($post['jumlah_item'])){
					$post['jumlah_item'] = 0;
				}
				$post['jumlah_item'] +=$value->jumlah;
				$post['item']['produk_'.$value->produksi_item_id]['produksi_item_id'] = $value->produksi_item_id;
				$post['item']['produk_'.$value->produksi_item_id]['produk_id'] = $value->produk_id;
				$post['item']['produk_'.$value->produksi_item_id]['produk_nama'] = $value->produk_nama;
				$post['item']['produk_'.$value->produksi_item_id]['jumlah'] = $value->jumlah;
				$post['item']['produk_'.$value->produksi_item_id]['produk_keterangan'] = $value->produk_keterangan;
				$produkCheck = $value->produk_nama;
				$no = $value->produksi_item_id;	
			}				

		}
		$data['produksi'] = $post;
		$data['no_produk'] = $no;
		$data['no_bahan'] = $no_bahan;		
		echo json_encode($data);
	}
	function selesai_produksi(){
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";	
		$data['tanggal_selesai'] =date("Y-m-d",strtotime($this->input->post('tanggal_selesai')));
		$data['status_produksi'] = "Selesai";
		$produksi_id = $this->input->post('produksi_id');
		$this->produksi->start_trans();
		$update = $this->produksi->update_by_id('produksi_id',$produksi_id,$data);
		if($update){
			$lokasi_bahan_id = $this->input->post('lokasi_bahan_id');
			$produksi_item_bahan = $this->produksi->item_bahan_in_produksi($produksi_id);
			foreach ($produksi_item_bahan as $key) {
				$row_stock = $this->stock_bahan->stock_by_location_bahan_id($lokasi_bahan_id,$key->bahan_id);
				$jumlah_bahan = $key->jumlah_bahan;
				foreach ($row_stock as $row) {
					$data = array();
					$jumlah = $row->stock_bahan_qty - $jumlah_bahan;
					$stock_out = $jumlah_bahan;
					if($row->stock_bahan_qty < $jumlah_bahan){
						$jumlah = 0;
						$stock_out = $row->stock_bahan_qty;
					}
					$jumlah_bahan =  abs($jumlah_bahan-$row->stock_bahan_qty);
					$data["stock_bahan_qty"] = $jumlah;
					$stock_bahan_id = $row->stock_bahan_id;
					$updateStok = $this->stock_bahan->update_by_id('stock_bahan_id',$stock_bahan_id,$data);
					if($updateStok){
						$data = array();
						$data["tanggal"] = date("Y-m-d");
						$data["table_name"] = "stock_bahan";
						$data["stock_bahan_id"] = $row->stock_bahan_id;
						$data["bahan_id"] = $key->bahan_id;
						$data["stock_out"] = $stock_out;
						$data["stock_in"] = 0;
						$data["last_stock"] = $this->stock_bahan->last_stock($key->bahan_id)->result;
						$data["last_stock_total"] = $this->stock_bahan->stock_total()->result;
						$data["keterangan"] = "Produksi produk";
						$data["method"] = "update";
						$this->stock_bahan->arus_stock_bahan($data);					
					}
					if($jumlah_bahan == 0){
						break;
					}

				}
			}
		}
		if($this->produksi->result_trans()){
			$result['success'] = true;
			$result['message'] = "Berhasil menyimpan data";			
		}		
		echo json_encode($result);
	}
	function penerimaan_produksi(){
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$produksi_id = $this->input->post('produksi_id');
		$data["status_penerimaan"] = "Diterima";
		$data["tanggal_penerimaan"] = date("Y-m-d",strtotime($this->input->post('tanggal_penerimaan')));
		$lokasi_id = $this->input->post('lokasi_penerimaan_id');
		$hpp = $this->input->post('hpp');
		$data['lokasi_penerimaan_id'] = $lokasi_id;
		$this->produksi->start_trans();
		$update_produksi = $this->produksi->update_by_id('produksi_id',$produksi_id,$data);
		if($update_produksi){
			$produksi_item = $this->produksi->produksi_item_list($produksi_id);
			foreach ($produksi_item as $key) {
				$data = array();
				$data["stock_produk_qty"] = $key->jumlah;
				$data["produk_id"] = $key->produk_id;
				$data["stock_produk_lokasi_id"] = $lokasi_id;	
				$data["year"] = date("y");
				$data["month"] = date("m");
				$lokasi_kode = $this->lokasi->row_by_id($data["stock_produk_lokasi_id"])->lokasi_kode;
				$jenis_produk_kode = $this->produk->produk_by_id($data["produk_id"])->jenis_produk_kode;
				$data["stock_produk_seri"] = $data["month"].$data["year"].$jenis_produk_kode.$lokasi_kode;
				$data["urutan"] = $this->stock_produk->urutan_seri($data["stock_produk_seri"]);
				$data["stock_produk_seri"] = $data["stock_produk_seri"].$data["urutan"];
				$data["hpp"] = $this->string_to_number($hpp['produk_'.$key->produk_id]);
				$insert = $this->stock_produk->insert($data);
				if($insert){
					$data = array();
					$data["tanggal"] = date("Y-m-d");
					$data["table_name"] = "stock_produk";
					$data["stock_produk_id"] = $this->stock_produk->last_id();
					$data["produk_id"] = $key->produk_id;
					$data["stock_out"] = 0;
					$data["stock_in"] = $key->jumlah;
					$data["last_stock"] = $this->stock_produk->last_stock($key->produk_id)->result;
					$data["last_stock_total"] = $this->stock_produk->stock_total()->result;
					$data["keterangan"] = "Produksi produk";
					$data["method"] = "insert";
					$this->stock_produk->arus_stock_produk($data);
				}					
			}
		}
		if($this->produksi->result_trans()){
			$result['success'] = true;
			$result['message'] = "Berhasil menyimpan data";			
		}
		echo json_encode($result);
	}
	function history_index(){
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");		
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		
		array_push($this->js, "script/app.js");
		array_push($this->js, "script/admin/produksi.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Produksi < ".$this->config->item('company_name');;
		$data['parrent'] = "produksi";
		$data['page'] = $this->uri->segment(1);
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"produksi_kode"));
		array_push($column, array("data"=>"jumlah_item"));
		array_push($column, array("data"=>"tanggal_mulai"));
		array_push($column, array("data"=>"estimasi_selesai"));
		array_push($column, array("data"=>"status_produksi","template"=>"badgeTemplate"));
		array_push($column, array("data"=>"tanggal_selesai"));
		array_push($column, array("data"=>"keterangan"));
		array_push($column, array("data"=>"status_penerimaan","template"=>"badgeTemplate"));
		array_push($column, array("data"=>"tanggal_penerimaan"));
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
		$akses_menu = json_decode($this->menu_akses,true);
		$action = array();
		foreach ($akses_menu['produksi'] as $key => $value) {
			if($key != "list" && $key != "akses_menu"){
				$action[$key] = $value;
			}
		}
		$data['lokasi'] = $this->lokasi->all_list();
		$data['action'] = json_encode($action);
		$data['produksi_kode'] = $this->produksi->get_kode_produksi();
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/history_produksi');
		$this->load->view('admin/static/footer');
	}
	function history_list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->produksi->history_produksi_count_all();
		$result['iTotalDisplayRecords'] = $this->produksi->history_produksi_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->produksi->history_produksi_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			if($key->tanggal_mulai != null){
				$time = strtotime($key->tanggal_mulai);
				$key->tanggal_mulai = date('d-m-Y',$time);
			}
			if($key->tanggal_selesai != null){
				$time = strtotime($key->tanggal_selesai);
				$key->tanggal_selesai = date('d-m-Y',$time);
			}
			if($key->estimasi_selesai != null){
				$time = strtotime($key->estimasi_selesai);
				$key->estimasi_selesai = date('d-m-Y',$time);
			}
			if($key->status_produksi == "Selesai"){
				$key->deny_edit = true;
				$key->deny_delete = true;
				if($key->status_penerimaan == "Belum Diterima")	{
					$key->penerimaan_status_btn = true;
				}
			} else{
				$key->produksi_status_btn = true;
			}
			$key->edit_url = base_url().'produksi/edit/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->produksi_id));
			$key->no = $i;
			$i++;
			$key->delete_url = base_url().'produksi/delete/';
			$key->row_id = $key->produksi_id;
			$key->action = null;
		}
		$result['aaData'] = $data;			
		echo json_encode($result);		
	}
	function produk_list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->produk->produk_produksi_count_all();
		$result['iTotalDisplayRecords'] = $this->produk->produk_produksi_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->produk->produk_produksi_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			$key->stock = number_format($key->jumlah);
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}
}

/* End of file ProduksiController.php */
/* Location: ./application/controllers/ProduksiController.php */
