<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class POSController extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('guest','',true);
		$this->load->model('stock_produk','',true);
		$this->load->model('pos','',true);
		$this->load->model('lokasi','',true);
		$this->load->model('harga_produk','',true);
		$this->load->model('tipe_pembayaran','',true);
		$this->load->model('log_kasir','',true);
		$this->load->model('unit','',true);
	}
	public function index(){
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");		
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");		
		array_push($this->js, "script/admin/pos.js");


		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Penjualan < POS < ".$this->config->item('company_name');;
		$data['parrent'] = "pos";
		$data['page'] = 'penjualan';
		$data['lokasi'] = $this->lokasi->all_list();
		unset($_SESSION['pos']);
		$_SESSION['pos']['location_id'] = $data['lokasi'][0]->lokasi_id;
		$data['faktur'] = $this->pos->getFakturCode();
		$data['urutan'] = $this->pos->getUrutan();		
		$data['tipe_pembayaran'] = $this->tipe_pembayaran->all_list();		
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		if (isset($_SESSION['log_pos']['log_kasir_id'])) {
			$this->load->view('admin/penjualan');
		} else {
			$this->load->view('admin/form_pos');
		}
		$this->load->view('admin/static/footer');
	}
	function guest_list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->guest->guest_count_all();
		$result['iTotalDisplayRecords'] = $this->guest->guest_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->guest->guest_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			$key->no = $i;
			$i++;
			$key->aksi = null;
		}
		$result['aaData'] = $data;				
		echo json_encode($result);		
	}
	function save_preaty_cash(){
		$result['success'] = false;
		$result['message'] = 'Gagal menyimpan data';
		$data['user_id'] = $_SESSION['login']['user_id'];
		$data['waktu_buka'] = Date('Y-m-d H:i:s');
		$data['kas_awal'] = $this->string_to_number($this->input->post('kas_awal'));
		$data['created_at'] = Date('Y-m-d H:i:s');
		$insert = $this->log_kasir->insert($data);
		if($insert){
			$result['success'] = true;
			$result['message'] = 'Berhasil menyimpan data';
			$_SESSION['log_pos']['log_kasir_id'] = $this->log_kasir->last_id();
		}
		echo json_encode($result);
	}
	function closing(){
		$result['success'] = true;
		$result['message'] = 'Gagal Merubah data';
		$log_pos_id = $_SESSION['log_pos']['log_kasir_id'];
		$log_pos = $this->log_kasir->row_by_id($log_pos_id);
		$total_penjualan = $this->pos->transaksi_by_log($log_pos_id)->total;
		$total = (int)$total_penjualan + (int)$log_pos->kas_awal;
		$data['kas_akhir'] = $total;
		$data['waktu_tutup'] = Date('Y-m-d H:i:s');
		$data['updated_at'] = Date('Y-m-d H:i:s');
		$update = $this->log_kasir->update_by_id('log_kasir_id',$log_pos_id,$data);
		if($update){
			unset($_SESSION['log_pos']);
			$result['total_kas'] = number_format($total);
			$result['success'] = true;
			$result['message'] = 'Berhasil Merubah data';
		}
		echo json_encode($result);
	}
	function utility(){
		$key = $this->uri->segment(3);
		if($key=="change-location"){
			$_SESSION['pos']['location_id'] = $this->input->post('lokasi_id');
		}
	}
	function produk_list(){
		$lokasi_id = $_SESSION['pos']['location_id'];
		if(isset($_SESSION["login"]['lokasi_id'])){
			$lokasi_id = $_SESSION["login"]['lokasi_id'];
		}
		if(isset($_GET["columns"][3]["search"]["value"]) && $_GET["columns"][3]["search"]["value"] != ""){
			$_GET['stock_produk_seri'] = $_GET["columns"][3]["search"]["value"];
		}
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->stock_produk->pos_produk_count($lokasi_id);
		$result['iTotalDisplayRecords'] = $this->stock_produk->pos_produk_filter($query,$lokasi_id);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		$count = $result['iTotalDisplayRecords'];
		$_GET["is_pos"] = 1;
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->stock_produk->pos_produk_list($start,$length,$query,$lokasi_id);
		$i = $start+1;
		foreach ($data as $key) {
			$key->aksi = null;
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->ar_minimal_pembelian = "";
			$key->ar_harga ="";
			$harga = $this->harga_produk->harga_produk_list($start,$count,"",$key->produk_id);
			foreach ($harga as $each) {
				$key->ar_minimal_pembelian .= $each->minimal_pembelian."|";
				$key->ar_harga .= $each->harga."|";
			}
			$key->ar_minimal_pembelian .= "0";
			$key->ar_harga .= $key->harga_eceran;			
			$key->no = $i; 
			$key->delete_url = base_url().'stock-bahan/delete/'.$key->produk_id;
			$key->row_id = $key->produk_id;
			$key->stock_produk_qty =  $this->number_to_string($key->stock_produk_qty);
			$key->harga_eceran = number_format($key->harga_eceran);
			$key->harga_grosir = number_format($key->harga_grosir);
			$i++;
		}
		$result['aaData'] = $data;				
		echo json_encode($result);		
	}
	function save(){
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$transaksi = json_decode($this->input->post('transaksi'));
		$save = $this->pos->save();
		if($save['result']){
			$transaksi = json_decode($this->input->post('transaksi'));
			$grand_total = $transaksi->grand_total;
			$terbayar = $transaksi->terbayar;
			$kembalian = $terbayar - $grand_total;			
			$result['success'] = true;
			$result['message'] = "Transaksi tersimpan";	
			$result['kembalian'] = number_format($kembalian);
			$id = $save["id"];
			$result["url"] = base_url()."pos/print/".$id;
		}
		echo json_encode($result);
	}
	function print(){
		$id = $this->uri->segment(3);
		$trans = $this->pos->penjualan_by_id($id);
		$penjualan_detail = $this->pos->detailTransaksi($id);
		foreach ($penjualan_detail as $key){
			if($key->produk_nama == null){
				$key->produk_nama = $key->deskripsi;
			}
			if($key->produk_kode == null){
				$key->produk_kode = "custom";
			}
		}
		$trans->detail_transaksi = $penjualan_detail;
		$data["trans"] = $trans;

		$this->load->view("admin/print_struct",$data);
	}
}

/* End of file POSController.php */
/* Location: ./application/controllers/POSController.php */
