<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KonversiBahanController extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('konversi_bahan','',true);
		$this->load->model('produk','',true);
		$this->load->model('bahan','',true);

	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		array_push($this->css, "vendors/general/select2/dist/css/select2.css");
		array_push($this->js, "vendors/general/select2/dist/js/select2.full.js");
		array_push($this->js, "script/app.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Konversi Bahan < Master Data < ".$this->config->item('company_name');;
		$data['parrent'] = "master_data";
		$data['page'] = $this->uri->segment(1);
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"produk_nama"));
		array_push($column, array("data"=>"bahan_nama"));
		array_push($column, array("data"=>"jumlah"));
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
		$akses_menu = json_decode($this->menu_akses,true);
		$action = array();
		foreach ($akses_menu['master_data']['konversi-bahan'] as $key => $value) {
			if($key != "list" && $key != "akses_menu"){
				$action[$key] = $value;
			}
		}
		$data['produk'] = $this->produk->konversi_produk_list();
		$data['bahan'] = $this->bahan->all_list();
		$data['action'] = json_encode($action);
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/konversi_bahan');
		$this->load->view('admin/static/footer');
	}
	function list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->konversi_bahan->konversi_bahan_count_all();
		$result['iTotalDisplayRecords'] = $this->konversi_bahan->konversi_bahan_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->konversi_bahan->konversi_bahan_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i;
			$i++;
			$key->delete_url = base_url().'konversi-bahan/delete/';
			$key->row_id = $key->konversi_bahan_id;
		}
		$result['aaData'] = $data;
		echo json_encode($result);
	}
	function add(){
		$result['success'] = false;
		$result['message'] = "Kode ini telah terdaftar";
			$data["produk_id"] = $this->input->post("produk_id");
			$data["bahan_id"] = $this->input->post("bahan_id");
			$data["jumlah"] = $this->string_to_decimal($this->input->post("jumlah"));
			$data["created_at"] = Date("Y-m-d H:i:s");
			$insert = $this->konversi_bahan->insert($data);
			if($insert){
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
			} else {
				$result['message'] = "Gagal menyimpan data";
			}
		echo json_encode($result);
	}
	function edit(){
		$result['success'] = false;
		$result['message'] = "Kode sudah terdaftar";
		$data = array();
		$data["produk_id"] = $this->input->post("produk_id");
		$data["bahan_id"] = $this->input->post("bahan_id");
		$data["jumlah"] = $this->string_to_decimal($this->input->post("jumlah"));
		$konversi_bahan_id = $this->input->post('konversi_bahan_id');
		$updated_at = date('Y-m-d H:i:s');
		$data['updated_at'] = $updated_at;
		$update = $this->konversi_bahan->update_by_id('konversi_bahan_id',$konversi_bahan_id,$data);
		if($update){
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
		} else {
				$result['message'] = "Gagal menyimpan data";
		}
		echo json_encode($result);
	}
	function delete(){
		$id = $this->input->input_stream('id');
		$result['success'] = false;
		$result['message'] = "missing parameter";
		if($id != ""){
			$delete = $this->konversi_bahan->delete_by_id("konversi_bahan_id",$id);
			if($delete){
				$result['success'] = true;
				$result['message'] = "Data berhasil dihapus";
			} else {
				$result['message'] = "Gagal menghapus data";
			}
		}
		echo json_encode($result);
	}

}

/* End of file KonversiBahanController.php */
/* Location: ./application/controllers/KonversiBahanController.php */
