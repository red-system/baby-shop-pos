<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GuestController extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('guest','',true);
		$this->load->model('lokasi','',true);
		
	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		array_push($this->js, "script/app.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Guest < Master Data < ".$this->config->item('company_name');;
		$data['parrent'] = "guest";
		$data['page'] = $this->uri->segment(1);
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"guest_nama"));
		array_push($column, array("data"=>"guest_alamat"));
		array_push($column, array("data"=>"guest_telepon"));
		array_push($column, array("data"=>"kewarganegaraan"));
		array_push($column, array("data"=>"perusahaan"));
		array_push($column, array("data"=>"lokasi_nama"));
		array_push($column, array("data"=>"tanggal"));
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
		$akses_menu = json_decode($this->menu_akses,true);
		$action = array();
		foreach ($akses_menu['guest'] as $key => $value) {
			if($key != "list" && $key != "akses_menu"){
				$action[$key] = $value;
			}
		}
		$data['action'] = json_encode($action);
		$data['lokasi'] = $this->lokasi->all_list();
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/guest');
		$this->load->view('admin/static/footer');
	}
	function list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		if(isset($_SESSION["login"]["lokasi_id"])){
			$_GET['lokasi_id'] = $_SESSION["login"]["lokasi_id"];
		}
		$result['iTotalRecords'] = $this->guest->guest_count_all();
		$result['iTotalDisplayRecords'] = $this->guest->guest_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->guest->guest_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			$key->no = $i;
			$i++;
			$key->delete_url = base_url().'guest/delete/';
			$key->row_id = $key->guest_id;
			$key->action = null;
			$time = strtotime($key->tanggal);
			$key->tanggal = date("d-m-Y H:i:s");
		}
		$result['aaData'] = $data;	
		echo json_encode($result);	
	}
	function add(){
		$result['success'] = false;
		$result['message'] = "Gagal Menyimpan data";
		$data["guest_nama"] = $this->input->post('guest_nama');
		$data["guest_alamat"] = $this->input->post('guest_alamat');
		$data["guest_telepon"] = $this->input->post('guest_telepon');
		$data["kewarganegaraan"] = $this->input->post('kewarganegaraan');
		$data["perusahaan"] = $this->input->post('perusahaan');
		if(!isset($_SESSION['login']['lokasi_id'])){
			$data['lokasi_id'] = $this->input->post('lokasi_id') ;
		}else {
			$data['lokasi_id'] = $_SESSION['login']['lokasi_id'] ;
		}
		$add = $this->guest->insert($data);
		if($add){
			$result['success'] = true;
			$result['message'] = "Berhasil Menyimpan data";			
		}
		echo json_encode($result);
	}
	function edit(){
		$result['success'] = false;
		$result['message'] = "Gagal Menyimpan data";
		$data = array();
		$data["guest_nama"] = $this->input->post('guest_nama');
		$data["guest_alamat"] = $this->input->post('guest_alamat');
		$data["guest_telepon"] = $this->input->post('guest_telepon');
		$data["kewarganegaraan"] = $this->input->post('kewarganegaraan');
		$data["perusahaan"] = $this->input->post('perusahaan');
		$guest_id = $this->input->post('guest_id');
		if(!isset($_SESSION['login']['lokasi_id'])){
			$data['lokasi_id'] = $this->input->post('lokasi_id') ;
		}else {
			$data['lokasi_id'] = $_SESSION['login']['lokasi_id'] ;
		}	
		$edit = $this->guest->update_by_id("guest_id",$guest_id,$data);
		if($edit){
			$result['success'] = true;
			$result['message'] = "Berhasil Menyimpan data";					
		}
		echo json_encode($result);
	}
	function delete(){
		$id = $this->input->input_stream('id');
		$result['success'] = false;
		$result['message'] = "missing parameter";
		if($id != ""){
			$delete = $this->guest->delete_by_id("guest_id",$id);
			if($delete){
				$result['success'] = true;
				$result['message'] = "Data berhasil dihapus";
			} else {
				$result['message'] = "Gagal menghapus data";
			}
		}
		echo json_encode($result);
	}

}

/* End of file JenisBahanController.php */
/* Location: ./application/controllers/JenisBahanController.php */