<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RekapitulasiPosController extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('pos');
		$this->load->model('stock_produk');
		$this->load->model('custom_produksi');
	}
	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
	
		array_push($this->js, "script/app.js");
		array_push($this->js, "script/admin/rekap_pos.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Rekapitulasi POS < POS < ".$this->config->item('company_name');;
		$data['parrent'] = "pos";
		$data['page'] = "pos";
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"tanggal"));
		array_push($column, array("data"=>"no_faktur"));
		array_push($column, array("data"=>"lokasi_nama"));
		array_push($column, array("data"=>"nama_pelanggan"));
		array_push($column, array("data"=>"total_lbl"));
		array_push($column, array("data"=>"biaya_tambahan_lbl"));
		array_push($column, array("data"=>"potongan_akhir_lbl"));
		array_push($column, array("data"=>"grand_total_lbl"));
        array_push($column, array("data"=>"laba_lbl"));
		array_push($column, array("data"=>"tipe_pembayaran_nama"));
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0,5,6,7,8)));
		$data['sumColumn'] = json_encode(array(5,6,7,8,9));
		$akses_menu = json_decode($this->menu_akses,true);
		$action = array();
		foreach ($akses_menu['pos']['penjualan'] as $key => $value) {
			if($key != "list" && $key != "akses_menu"){
				$action[$key] = $value;
			}
		}
		$data['action'] = json_encode($action);
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/rekapitulasi_pos');
		$this->load->view('admin/static/footer');
	}
	function list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->pos->transaksi_count_all();
		$result['iTotalDisplayRecords'] = $this->pos->transaksi_count_all($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->pos->transaksi_list($start,$length,$query);
		$i = $start+1;
			foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i;
			$i++;
			$key->action = null;
			$key->total_lbl = $this->idr_currency($key->total);
			$key->total = number_format($key->total);
			$key->biaya_tambahan_lbl = $this->idr_currency($key->biaya_tambahan);
			$key->biaya_tambahan = number_format($key->biaya_tambahan);
			$key->potongan_akhir_lbl = $this->idr_currency($key->potongan_akhir);
			$key->potongan_akhir = number_format($key->potongan_akhir);
			$key->delete_url = base_url().'pos/delete/';
			$key->grand_total_lbl = $this->idr_currency($key->grand_total);
			$key->grand_total = number_format($key->grand_total);
			$key->row_id = $key->penjualan_id;
			$key->laba_lbl = $this->idr_currency($key->laba);
			$key->laba = number_format($key->laba);
		}
		$result['aaData'] = $data;				
		echo json_encode($result);
	}
	function detail(){
		$id = $this->uri->segment(3);
		$data = $this->pos->detailTransaksi($id);
		$result['data'] = $data;
		foreach ($result['data'] as $key) {
			$key->harga = $this->idr_currency($key->harga);
			$key->qty = number_format($key->qty);
			$key->sub_total = $this->idr_currency($key->sub_total);
			if($key->produk_nama == null){
				$key->produk_nama = $key->deskripsi;
			}
			if($key->produk_kode == null){
				$key->produk_kode = "custom";
			}
		}
		echo json_encode($result);
	}
	function delete(){
		$id = $this->input->input_stream('id');
		$result['success'] = false;
		$result['message'] = "missing parameter";
		$detail = $this->pos->detailTransaksi($id);
		foreach ($detail as $value){
			$data = array();
			$stock = $this->stock_produk->row_by_id($value->stock_produk_id);
			if(sizeof($stock)>0){
				$data['stock_produk_qty'] = $stock->stock_produk_qty+$value->qty;
				$this->stock_produk->update_by_id('stock_produk_id',$value->stock_produk_id,$data);
			} else{
				$this->custom_produksi->delete_by_id('produk_custom_id',$value->produk_custom_id);
			}
		}
		if($id != ""){
			$delete = $this->pos->delete_by_id("penjualan_id",$id);
			if($delete){
				$result['success'] = true;
				$result['message'] = "Data berhasil dihapus";
			} else {
				$result['message'] = "Gagal menghapus data";
			}
		}
		echo json_encode($result);
	}
}

/* End of file RekapitulasiPosController.php */
/* Location: ./application/controllers/RekapitulasiPosController.php */
