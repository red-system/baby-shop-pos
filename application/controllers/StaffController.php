<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StaffController extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('staff');
		
	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "script/app.js");
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$data['parrent'] = "master_data";
		$data['page'] = $this->uri->segment(1);
		$data["meta_title"] = "Staff < Master Data < ".$this->config->item('company_name');;
		$column = array();
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"staff_nama"));
		array_push($column, array("data"=>"staff_alamat"));
		array_push($column, array("data"=>"staff_phone_number"));
		array_push($column, array("data"=>"staff_email"));	
		array_push($column, array("data"=>"staff_status"));	
		array_push($column, array("data"=>"lama_bekerja"));
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
		$akses_menu = json_decode($this->menu_akses,true);
		$action = array();
		foreach ($akses_menu['master_data']['staff'] as $key => $value) {
			if($key != "list" && $key != "akses_menu"){
				$action[$key] = $value;
			}
		}
		$data['action'] = json_encode($action);
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/staff');
		$this->load->view('admin/static/footer');
	}
	function list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->staff->staff_count_all();
		$result['iTotalDisplayRecords'] = $this->staff->staff_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->staff->staff_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			if($key->tanggal_lahir != null){
				$time = strtotime($key->tanggal_lahir);
				$key->tanggal_lahir = date('d-m-Y',$time);
			}			
			$key->no = $i;
			$i++;
			$key->delete_url = base_url().'staff/delete/';
			$key->row_id = $key->staff_id;
		}
		$result['aaData'] = $data;	
		echo json_encode($result);
	}
	function add(){
		$this->form_validation->set_rules('staff_email', '', 'required|is_unique[staff.staff_email]');
		$result['success'] = false;
		$result['message'] = "missing parameter or the email is allready taken";
		if ($this->form_validation->run() == TRUE) {
			$nik = $this->input->post('nik');
			$staff_nama = $this->input->post('staff_nama');
			$tempat_lahir = $this->input->post('tempat_lahir');
			$tanggal_lahir =date("Y-m-d",strtotime($this->input->post('tanggal_lahir')));
			$staff_alamat = $this->input->post('staff_alamat');
			$staff_status = $this->input->post('staff_status');
			$staff_kelamin = $this->input->post('staff_kelamin');
			$mulai_bekerja = $this->input->post('mulai_bekerja');
			$staff_email = $this->input->post('staff_email');
			$staff_phone_number = $this->input->post('staff_phone_number');
			$staff_keterangan = $this->input->post('staff_keterangan');
			$data = array("nik"=>$nik,"staff_nama"=>$staff_nama,"tempat_lahir"=>$tempat_lahir,"tanggal_lahir"=>$tanggal_lahir,"staff_alamat"=>$staff_alamat,"staff_status"=>$staff_status,"staff_kelamin"=>$staff_kelamin,"mulai_bekerja"=>$mulai_bekerja,"staff_email"=>$staff_email,"staff_phone_number"=>$staff_phone_number,"staff_keterangan"=>$staff_keterangan);
			$insert = $this->staff->insert($data);
			if($insert){
				$result['success'] = true;
				$result['message'] = "Sukses menambahkan data";
			} else {
				$result['message'] = "Gagal menambahkan data";
			}
		}
		echo json_encode($result);
	}
	function edit(){
		$result['success'] = false;
		$result['message'] = "missing parameter or the email is allready taken";
		$staff_id = $this->input->post('staff_id');
		$nik = $this->input->post('nik');
		$staff_nama = $this->input->post('staff_nama');
		$tempat_lahir = $this->input->post('tempat_lahir');
		$tanggal_lahir =date("Y-m-d",strtotime($this->input->post('tanggal_lahir')));
		$staff_alamat = $this->input->post('staff_alamat');
		$staff_status = $this->input->post('staff_status');
		$staff_kelamin = $this->input->post('staff_kelamin');
		$mulai_bekerja = $this->input->post('mulai_bekerja');
		$staff_email = $this->input->post('staff_email');
		$staff_phone_number = $this->input->post('staff_phone_number');
		$staff_keterangan = $this->input->post('staff_keterangan');
		if ($this->staff->is_ready_email($staff_id,$staff_email)){
			$data = array("nik"=>$nik,"staff_nama"=>$staff_nama,"tempat_lahir"=>$tempat_lahir,"tanggal_lahir"=>$tanggal_lahir,"staff_alamat"=>$staff_alamat,"staff_status"=>$staff_status,"staff_kelamin"=>$staff_kelamin,"mulai_bekerja"=>$mulai_bekerja,"staff_email"=>$staff_email,"staff_phone_number"=>$staff_phone_number,"staff_keterangan"=>$staff_keterangan);
			$update = $this->staff->update_by_id('staff_id',$staff_id,$data);
			if($update){
				$result['success'] = true;
				$result['message'] = "Sukses mengubah data";
			} else {
				$result['message'] = "Gagal mengubah data";
			}	
		}
		echo json_encode($result);
	}
	function delete(){
		$id = $this->input->input_stream('id');
		$result['success'] = false;
		$result['message'] = "missing parameter";
		if($id != ""){
			$delete = $this->staff->delete_by_id("staff_id",$id);
			if($delete){
				$result['success'] = true;
				$result['message'] = "Data berhasil dihapus";
			} else {
				$result['message'] = "Gagal menghapus data";
			}
		}
		echo json_encode($result);
	}
}

/* End of file StaffController.php */
/* Location: ./application/controllers/StaffController.php */