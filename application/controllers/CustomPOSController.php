<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CustomPOSController extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('guest', '', true);
		$this->load->model('stock_produk', '', true);
		$this->load->model('pos', '', true);
		$this->load->model('lokasi', '', true);
		$this->load->model('harga_produk', '', true);
		$this->load->model('tipe_pembayaran', '', true);
		$this->load->model('log_kasir', '', true);
		$this->load->model('jenis_produk', '', true);
		$this->load->model('satuan', '', true);
	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		array_push($this->js, "vendors/general/typeahead.js/dist/typeahead.bundle.js");
		array_push($this->js, "script/admin/custom_pos.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Penjualan Custom< POS < ".$this->config->item('company_name');;
		$data['parrent'] = "pos";
		$data['page'] = 'custom-pos';
		$data['lokasi'] = $this->lokasi->all_list();
		$data['satuan'] = $this->satuan->all_list();
		$data["jenis_produk"] = $this->jenis_produk->all_list();
		unset($_SESSION['pos']);
		$_SESSION['pos']['location_id'] = $data['lokasi'][0]->lokasi_id;
		$data['faktur'] = $this->pos->getFakturCode();
		$data['urutan'] = $this->pos->getUrutan();
		$data['tipe_pembayaran'] = $this->tipe_pembayaran->all_list();
		$this->load->view('admin/static/header', $data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		if (isset($_SESSION['log_pos']['log_kasir_id'])) {
			$this->load->view('admin/custom_penjualan');
		} else {
			$this->load->view('admin/form_pos');
		}
		$this->load->view('admin/static/footer');
	}
	function save(){
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$save = $this->pos->custom_save();
		if($save['result']){
			$transaksi = json_decode($this->input->post('transaksi'));
			$grand_total = $transaksi->grand_total;
			$terbayar = $transaksi->terbayar;
			$kembalian = $terbayar - $grand_total;
			$result['success'] = true;
			$result['message'] = "Transaksi tersimpan";
			$result['kembalian'] = number_format($kembalian);
			$id = $save["id"];
			$result["url"] = base_url()."pos/print/".$id;
		}
		echo json_encode($result);
	}
}
