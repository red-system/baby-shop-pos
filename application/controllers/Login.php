<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user','',true);
		$this->load->helper('string');
	}

	public function index()
	{
		if(isset($_SESSION['login'])){
			redirect(base_url(),'refresh');
		} else {
			$this->load->view('admin/login');
		}
	}
	function auth(){
		$result['success'] = false;
		$result['message'] = "missing parameter";
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$auth = $this->user->auth_login($email);
		if($auth != null){
			if (password_verify($password, $auth->password)){
				$result["success"] = true;
				$result["message"] = "Login success";
				$_SESSION["login"]["user_id"] = $auth->user_id;
				$_SESSION["login"]["user_name"] = $auth->staff_nama;
				$_SESSION["login"]["email"] = $auth->staff_email;
				$_SESSION["login"]["user_role_id"] = $auth->user_role_id;
				$_SESSION["login"]["avatar"] = $auth->avatar;
				$_SESSION["login"]["staff_id"] = $auth->user_staff_id;
				if($auth->lokasi_id != null){
					$_SESSION["login"]["lokasi_id"] = $auth->lokasi_id;
					$_SESSION["login"]["lokasi_nama"] = $auth->lokasi_nama;
				}
			} else {
				$result["message"] = "Wrong password";
			}
		} else {
			$result["message"] = "Email not registered";
		}
		echo json_encode($result);
	}
	function logout(){
		$result['success'] = false;
		$result['message'] = 'Gagal logout, silakan lakukan closing pada POS terlebih dahulu';
		if(!isset($_SESSION['log_pos']['log_kasir_id'])){
			unset($_SESSION['login']);
			$result['success'] = true;
			$result['message'] = 'Berhasil logout';
		}
		echo json_encode($result);
	}
	function forget(){
		$result['success'] = false;
		$result['message'] = "missing parameter";
		$email = $this->input->post('email');
		$auth = $this->user->auth_login($email);
		if($auth != null){
			$timeout = time()+(24*3600);
			$code = random_string('alnum', 25);
			$session = $this->user->forgot_request($email,$code,$timeout);
			if($session){
				$config = Array(
						  'protocol' => 'smtp',
						  'smtp_host' => 'smtp.mailtrap.io',
						  'smtp_port' => 2525,
						  'smtp_user' => '8fe5a91a10cc87',
						  'smtp_pass' => '1cd529218bc7b0',
						  'mailtype'  => 'html',
				          'charset'   => 'utf-8',
						  'crlf' => "\r\n",
						  'newline' => "\r\n"
						);
	            $htmlContent = '<h3><strong>Reset Password</strong> </h3>';
	            $htmlContent .= '<p>We have confirmed your request, please click <a href="'.base_url().'forget/'.$email.'/'.$code.'">here</a> to reset your password</p>';
				$this->load->library('email');
				$this->email->initialize($config);
				$this->email->from($this->config->item('company_email'), $this->config->item('company_name'));
				$this->email->to($email);				
				$this->email->subject('Request forget password');
				$this->email->message($htmlContent);
				
				if ($this->email->send()) {
					$result["success"] = true;
					$result["message"] = "Your request has been accepted please check your email";
				} else {
					$result["message"] = "Failed to send link";
				}
			}
			
		} else {
			$result["message"] = "Email not registered";
		}
		echo json_encode($result);
	}
	public function forget_index(){
		$email = $this->uri->segment(2);
		$code = $this->uri->segment(3);
		$check = $this->user->forget_session_check($email,$code);
		if(sizeof($check)){
			$data["email"]  = $email;
			$data["code"]  = $code;
			$this->load->view('admin/reset_password');
		} else {
			redirect(base_url()."404_override",'refresh');
		}
	}
	function reset_password(){
		$result['success'] = false;
		$result['message'] = "missing parameter";
		$email = $this->input->post('email');
		$code = $this->input->post('code');
		$new_password = $this->input->post('new_pass');
		$check = $this->user->forget_session_check($email,$code);
		if(sizeof($check)){
			$password = password_hash($new_password, PASSWORD_BCRYPT);
			$change = $this->user->change_password($email,$password);
			if ($change) {
				$result["success"] = true;
				$result["message"] = "Your password successfully changed";
			} else {
				$result["message"] = "Failed to change password";
			}
		}
		echo json_encode($result);
	}
}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */
