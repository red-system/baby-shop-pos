<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProdukController extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('produk','',true);
		$this->load->model('assembly','',true);
		$this->load->model('jenis_produk','',true);
		$this->load->model('satuan','',true);
		$this->load->model('harga_produk','',true);
		$this->load->model('size','',true);
		$this->load->model('color','',true);
		$this->load->model('patern','',true);
		$this->load->model('fabric','',true);
	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->css, "vendors/general/select2/dist/css/select2.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/select2/dist/js/select2.full.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		
		array_push($this->js, "script/app.js");
		array_push($this->js, "script/admin/produk.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Produk < Inventori < ".$this->config->item('company_name');;
		$data['parrent'] = "inventori";
		$data['page'] = $this->uri->segment(1);
		$target = array(0,4,5,6,7);
		$sumColumn = array(7);
		$data['satuan'] = $this->satuan->all_list();
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"produk_kode"));
		array_push($column, array("data"=>"produk_nama"));
		array_push($column, array("data"=>"jenis_produk_nama"));
		array_push($column, array("data"=>"produk_minimal_stock"));
        array_push($column, array("data"=>"hpp_global_lbl"));
		array_push($column, array("data"=>"harga_eceran_lbl"));
		array_push($column, array("data"=>"stock"));
		array_push($column, array("data"=>"satuan_nama"));
		if(isset($_SESSION['login']['lokasi_id'])){
			array_push($column, array("data"=>"jumlah_lokasi"));
			array_push($target, 10);
			array_push($sumColumn, 9);
		}

		$data['sumColumn'] = json_encode($sumColumn);
		$data['column'] = json_encode($column);

		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>$target));
		$akses_menu = json_decode($this->menu_akses,true);
		$action = array();
		foreach ($akses_menu['inventori']['produk'] as $key => $value) {
			if($key != "list" && $key != "akses_menu"){
				$action[$key] = $value;
			}
		}
		$action['edit_assembly'] = true;
		$data['action'] = json_encode($action);
		$data["jenis_produk"] = $this->jenis_produk->all_list();
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/produk');
		$this->load->view('admin/static/footer');
	}
	function list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->produk->produk_count_all();
		$result['iTotalDisplayRecords'] = $this->produk->produk_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->produk->produk_list($start,$length,$query);
		$i = $start+1;
		foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i;
			$i++;
			$key->row_id = $key->produk_id;
			$exists = $this->assembly->get_exists("produk_id",$key->row_id);
			if(($exists == TRUE)) {
				$key->delete_url = base_url().'assembly/delete-assembly/';
			} else {
				$key->delete_url = base_url().'produk/delete/';
			}
			$key->stok_url = base_url().'stock-produk/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->produk_id));
			$key->price_url = base_url().'harga-produk/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->produk_id));
            $key->unit_url = base_url().'unit/'.$this->encrypt_id($key->produk_id);
			$key->stock =  $this->number_to_string( $key->stock);
			$exists = $this->assembly->get_exists("produk_id",$key->row_id);
			if(($exists == TRUE)) {
				$key->edit_assembly_url = base_url().'assembly/edit-assembly/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->produk_id));
				$key->direct = base_url().'assembly/edit-assembly/'.str_replace(array("+","/"), array("-","_"), $this->encryption->encrypt($key->produk_id));
				$stock_perlokasi = $this->assembly->stock_perlokasi($key->produk_id);
				$key->check = count($stock_perlokasi);
				$key->deny_edit= true;
			} else {
				$key->deny_edit_assembly= true;
			}
			$key->produk_minimal_stock = number_format($key->produk_minimal_stock);
            $key->harga_eceran_lbl = $this->idr_currency($key->harga_eceran);
            $key->hpp_global_lbl = $this->idr_currency($key->hpp_global);
            $key->hpp_global = number_format($key->hpp_global);
            $key->harga_eceran = number_format($key->harga_eceran);
			$key->harga_grosir = number_format($key->harga_grosir);
			$key->satuan_nama = $key->satuan_nama;
			$key->harga_konsinyasi = number_format($key->harga_konsinyasi);
			if(isset($_SESSION['login']['lokasi_id'])){
				$key->jumlah_lokasi = number_format($key->jumlah_lokasi);	
			}			
		}
		$result['aaData'] = $data;			
		echo json_encode($result);
	}
	function add(){
		$result['success'] = false;
		$result['message'] = "Kode ini telah terdaftar";
		$this->form_validation->set_rules('produk_kode', '', 'required|is_unique[produk.produk_kode]');
		if ($this->form_validation->run() == TRUE) {
			$data["produk_kode"] = $this->input->post('produk_kode');
            $data["barcode"] = $this->input->post('barcode');
			$data["produk_nama"] = $this->input->post('produk_nama');
			$data["produk_satuan_id"] = $this->input->post('produk_satuan_id');
			$data["produk_jenis_id"] = $this->input->post('produk_jenis_id');
			$data["produk_minimal_stock"] = $this->string_to_number($this->input->post('produk_minimal_stock'));
			$data["disc_persen"] = $this->string_to_number($this->input->post('disc_persen'));
			$data["harga_eceran"] = $this->string_to_number($this->input->post('harga_eceran'));
			$data["harga_grosir"] = $this->string_to_number($this->input->post('harga_grosir'));
			$data["harga_konsinyasi"] = $this->string_to_number($this->input->post('harga_konsinyasi'));
            $data["hpp_global"] = $this->string_to_number($this->input->post('hpp_global'));
			$data["color_id"] = $this->input->post('color_id');
			$data["size_id"] = $this->input->post('size_id');
			$data["good_size_id"] = $this->input->post('good_size_id');
			$data["bed_size_id"] = $this->input->post('bed_size_id');
			$data["weight_pc"] = $this->input->post('weight_pc');
			$data["weight_m"] = $this->input->post('weight_m');
			$data["fabric_id"] = $this->input->post('fabric_id');
			$data["patern_id"] = $this->input->post('patern_id');
			$data["style"] = $this->input->post('style');
			$data["file"] = $this->input->post('file');	
			$insert = $this->produk->insert($data);
			if($insert){
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
			} else {
				$result['message'] = "Gagal menyimpan data";
			}
		}
		echo json_encode($result);
	}
	function edit(){
		$result['success'] = false;
		$result['message'] = "Gagal menyimpan data";
		$produk_id = $this->input->post('produk_id');
		$exists = $this->produk->get_exist_with_id($produk_id,$this->input->post('produk_kode'));
		if(($exists == TRUE)) {
			$result['success'] = false;
			$result['message'] = "Kode ini telah terdaftar";
		} else {
			$data = array();
			$data["produk_kode"] = $this->input->post('produk_kode');
            $data["barcode"] = $this->input->post('barcode');
	        $data["produk_nama"] = $this->input->post('produk_nama');
			$data["produk_satuan_id"] = $this->input->post('produk_satuan_id');
			$data["produk_jenis_id"] = $this->input->post('produk_jenis_id');
			$data["disc_persen"] = $this->input->post('disc_persen');
			$data["produk_minimal_stock"] = $this->string_to_number($this->input->post('produk_minimal_stock'));
			$data["harga_eceran"] = $this->string_to_number($this->input->post('harga_eceran'));
			$data["harga_grosir"] = $this->string_to_number($this->input->post('harga_grosir'));
			$data["harga_konsinyasi"] = $this->string_to_number($this->input->post('harga_konsinyasi'));
            $data["hpp_global"] = $this->string_to_number($this->input->post('hpp_global'));
			$data["color_id"] = $this->input->post('color_id');
			$data["size_id"] = $this->input->post('size_id');
			$data["good_size_id"] = $this->input->post('good_size_id');
			$data["bed_size_id"] = $this->input->post('bed_size_id');
			$data["weight_pc"] = $this->input->post('weight_pc');
			$data["weight_m"] = $this->input->post('weight_m');
			$data["fabric_id"] = $this->input->post('fabric_id');
			$data["patern_id"] = $this->input->post('patern_id');
			$data["style"] = $this->input->post('style');
			$data["file"] = $this->input->post('file');		
			$updated_at = date('Y-m-d H:i:s');
			$data['updated_at'] = $updated_at;
			$update = $this->produk->update_by_id('produk_id',$produk_id,$data);
			if($update){
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
			} else {
				$result['message'] = "Gagal menyimpan data";
			}

		}
		
		echo json_encode($result);
	}
	function delete(){
		$id = $this->input->input_stream('id');
		$result['success'] = false;
		$result['message'] = "missing parameter";
		if($id != ""){
			$delete = $this->produk->delete_by_id("produk_id",$id);
			if($delete){
				$result['success'] = true;
				$result['message'] = "Data berhasil dihapus";
			} else {
				$result['message'] = "Gagal menghapus data";
			}
		}
		echo json_encode($result);
	}
	function harga(){
		$produk_id = $this->uri->segment(3);
		$count = $this->harga_produk->harga_produk_count($produk_id);
		$start = 0;
		$harga = $this->harga_produk->harga_produk_list($start,$count,"",$produk_id);
		foreach ($harga as $key) {
			$key->minimal_pembelian = number_format($key->minimal_pembelian);
			$key->harga = number_format($key->harga);			
		}
		echo json_encode($harga);
	}
	function item_produk(){
		$produk_id = $this->uri->segment(3);
		$exists = $this->assembly->get_exists("produk_id",$produk_id);
		if(($exists == TRUE)) {
			$assembly = $this->assembly->get_id_assembly($produk_id);
	        $assembly_id = $assembly['assembly_id'];
			$count = $this->assembly->assembly_item_produk_count($assembly_id);
			$start = 0;
			$item = $this->assembly->item_list($start,$count,"",$assembly_id);
			echo json_encode($item);
		} else {
			$item = [];
			echo json_encode($item);
		}

	}

}

/* End of file ProdukController.php */
/* Location: ./application/controllers/ProdukController.php */
