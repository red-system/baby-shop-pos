<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HargaProdukController extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('produk','',true);
		$this->load->model('jenis_produk','',true);
		$this->load->model('harga_produk','',true);
		$this->load->model('lokasi','',true);
		$this->load->model('satuan','',true);
	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		
		array_push($this->js, "script/app.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Harga Produk< Inventori < ".$this->config->item('company_name');;
		$data['parrent'] = "inventori";
		$data['page'] = 'produk';
		$url = str_replace(array("-","_"), array("+","/"), $this->uri->segment(2));
		$id = $this->encryption->decrypt($url);
		$data["lokasi"] = $this->lokasi->all_list();
		$produk = $this->produk->row_by_id($id);
		$data['id'] = $id;
		if ($produk != null) {
			$data['nama_satuan'] = $this->satuan->row_by_id($produk->produk_satuan_id)->satuan_nama;
			$data['produk'] = $produk;
			array_push($column, array("data"=>"no"));
			array_push($column, array("data"=>"minimal_pembelian"));
			array_push($column, array("data"=>"harga"));
			$data['column'] = json_encode($column);
			$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0,1,2)));
			$data["action"] = json_encode(array("edit"=>true,"delete"=>true));
			$this->load->view('admin/static/header',$data);
			$this->load->view('admin/static/sidebar');
			$this->load->view('admin/static/topbar');
			$this->load->view('admin/harga_produk');
			$this->load->view('admin/static/footer');
		} else {
			redirect('404_override','refresh');
		}
		
	}
	function list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->harga_produk->harga_produk_count($this->uri->segment(3));
		$result['iTotalDisplayRecords'] = $this->harga_produk->harga_produk_count_filter($query,$this->uri->segment(3));
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->harga_produk->harga_produk_list($start,$length,$query,$this->uri->segment(3));
		$i = $start+1;
		foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i; 
			$key->delete_url = base_url().'harga-produk/delete/'.$key->produk_id;
			$key->row_id = $key->harga_produk_id;
			$key->minimal_pembelian = number_format($key->minimal_pembelian);
			$key->harga = number_format($key->harga);
			$i++;
		}
		$result['aaData'] = $data;		
		echo json_encode($result);
	}
	function add(){
		$result['success'] = false;
		$data["minimal_pembelian"] = $this->string_to_number($this->input->post('minimal_pembelian'));
		$data["produk_id"] = $this->input->post('produk_id');
		$data["harga"] = $this->string_to_number($this->input->post('harga'));
		$insert = $this->harga_produk->insert($data);
		if($insert){
			$result['success'] = true;
			$result['message'] = "Data berhasil disimpan";
			
		} else {
			$result['message'] = "Gagal menyimpan data";
		}	
		echo json_encode($result);
	}
	function edit(){
		$result['success'] = false;
		$result['message'] = "Tidak dapat menyimpan produk dengan seri produk yang sama dengan data yang ada ";
		$data["minimal_pembelian"] = $this->string_to_number($this->input->post('minimal_pembelian'));
		$data["produk_id"] = $this->input->post('produk_id');
		$data["harga"] = $this->string_to_number($this->input->post('harga'));		
		$harga_produk_id = $this->input->post('harga_produk_id');
		$edit = $this->harga_produk->update_by_id('harga_produk_id',$harga_produk_id,$data);
		if($edit){
			$result['success'] = true;
			$result['message'] = "Data berhasil disimpan";
			
		} else {
			$result['message'] = "Gagal menyimpan data";
		}	
		echo json_encode($result);
	}
	function delete(){
		$id = $this->input->input_stream('id');
		$result['success'] = false;
		$result['message'] = "missing parameter";
		if($id != ""){
			$delete = $this->harga_produk->delete_by_id("harga_produk_id",$id);
			if($delete){
				$result['success'] = true;
				$result['message'] = "Data berhasil dihapus";
			} else {
				$result['message'] = "Gagal menghapus data";
			}
		}
		echo json_encode($result);
	}
}

/* End of file HargaProdukController.php */
/* Location: ./application/controllers/HargaProdukController.php */