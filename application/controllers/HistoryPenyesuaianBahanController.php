<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	require 'vendor/autoload.php';

	use PhpOffice\PhpSpreadsheet\Helper\Sample;
	use PhpOffice\PhpSpreadsheet\IOFactory;
	use PhpOffice\PhpSpreadsheet\Spreadsheet;

class HistoryPenyesuaianBahanController extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('bahan','',true);
		$this->load->model('stock_bahan','',true);
		$this->load->model('lokasi','',true);
		$this->load->model('history_penyesuaian_bahan');
	}

	public function index()
	{
		array_push($this->css,"app/custom/wizard/wizard-v3.default.css");
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");		
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		
		array_push($this->js, "script/app.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = " Penyesuaian Stock Bahan < Inventori < ".$this->config->item('company_name');;
		$data['parrent'] = "inventori";
		$data['page'] = 'penyesuaian-bahan';
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"tanggal"));
		array_push($column, array("data"=>"bahan_kode"));
		array_push($column, array("data"=>"bahan_nama"));
		array_push($column, array("data"=>"jenis_bahan_nama"));
		array_push($column, array("data"=>"qty_awal"));
		array_push($column, array("data"=>"qty_akhir"));
		array_push($column, array("data"=>"keterangan"));
		$data['column'] = json_encode($column);
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0)));
		$data["list_url"] = base_url().'history-penyesuaian-bahan/list';
		$data['first_date'] = $this->history_penyesuaian_bahan->first_date();
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/history_penyesuaian_bahan');
		$this->load->view('admin/static/footer');
	}
	function list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->history_penyesuaian_bahan->history_count();
		$result['iTotalDisplayRecords'] = $this->history_penyesuaian_bahan->history_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->history_penyesuaian_bahan->history_list($start,$length,$query);
		$i = $start+1;
				foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i;
			$key->row_id = $key->bahan_id;
			$i++;
			$time = strtotime($key->tanggal);
			$temp["tanggal"] = date("d-m-Y",$time);
		}
		$result['aaData'] = $data;		
		echo json_encode($result);
	}
	function pdf(){
		$data['first_date'] = $this->history_penyesuaian_bahan->first_date();
		$data["start_date"] = $this->input->get('start_date');
		$data["end_date"] = $this->input->get('end_date');
		if($this->input->get('bahan_id')!="" && $this->input->get('bahan_id')!="all"){
			$data["bahan_nama"] = $this->bahan->row_by_id($this->input->get('bahan_id'))->bahan_nama;
		}
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->history_penyesuaian_bahan->history_count_filter('');
		$list =  $this->history_penyesuaian_bahan->history_list($start,$length,$query);
		$i = $start+1;
		foreach ($list as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i;
			$key->row_id = $key->bahan_id;
			$i++;
			$time = strtotime($key->tanggal);
			$temp["tanggal"] = date("d-m-Y",$time);
		}
		$data['list'] = $list;			
		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
        $html = $this->load->view('admin/pdf/history_penyesuaian_pdf',$data,true);
        $mpdf->WriteHTML($html);
        $date = date("Y-m-d");
        if($this->input->get('start_date')!=""){
        	$date = $this->input->get('start_date')." s.d ".$this->input->get('end_date');
        }
        $mpdf->Output('History Stock Bahan '.$date.".pdf","D");
	}
	function excel(){
		$start = 0;
		$length = $this->history_penyesuaian_bahan->history_count_filter('');
		$list =  $this->history_penyesuaian_bahan->history_list($start,$length,'');
		$i = $start+1;
		foreach ($list as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i;
			$key->row_id = $key->bahan_id;
			$i++;
			$time = strtotime($key->tanggal);
			$temp["tanggal"] = date("d-m-Y",$time);
		}
		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator($this->config->item('company_name'))
		->setLastModifiedBy($_SESSION['login']['user_name'])
		->setTitle('History Penyesuaian Bahan')
		->setSubject('');
		$style = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        )
	    );
	    $right = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
	        )
	    ); 
		$border = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        ),
			'borders' => [
		        'allBorders' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
		        ],
		    ],

	    );
		$data['first_date'] = $this->history_penyesuaian_bahan->first_date();
		$tanggal = $data['first_date']." s/d ".date("d-m-Y");
		if($this->input->get('start_date')!="") $tanggal = $this->input->get('start_date')." s/d ".$this->input->get('end_date');

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A7', 'No')
		->setCellValue('B7', 'Tanggal')
		->setCellValue('C7', 'Kode Bahan')
		->setCellValue('D7', 'Nama Bahan')
		->setCellValue('E7', 'Jenis Bahan')
		->setCellValue('F7', 'Jumlah Awal')
		->setCellValue('G7', 'Jumlah Akhir')
		->setCellValue('H7', 'Keterangan')
		;
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(12);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(12);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(12);
		$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(12);
		$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(18);
		$spreadsheet->getActiveSheet()->getStyle("A7:H7")->applyFromArray($style);				
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$date = date("Y-m-d");
        if($this->input->get('start_date')!=""){
        	$date = $this->input->get('start_date')." s.d ".$this->input->get('end_date');
        }
		$i=8; foreach($list as $key) {

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $key->no)
		->setCellValue('B'.$i, $key->tanggal)
		->setCellValue('C'.$i, $key->bahan_kode)
		->setCellValue('D'.$i, $key->bahan_nama)
		->setCellValue('E'.$i, $key->jenis_bahan_nama)
		->setCellValue('F'.$i, $key->qty_awal)
		->setCellValue('G'.$i, $key->qty_akhir)
		->setCellValue('H'.$i, $key->keterangan);
		$i++;
		}
		$spreadsheet->getActiveSheet()->getStyle("A7:H".$i)->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A7:H7')->getFill()
	    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
	    ->getStartColor()->setARGB('BEBEBE');
	    $spreadsheet->getActiveSheet()->getStyle('H8:H'.$i)->getAlignment()->setWrapText(true);
		// Rename worksheet
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/logo-laporan.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C1',$this->config->item('company_name'));
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C2',$this->config->item('company_address'));
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('C3',$this->config->item('company_phone'));
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('H1','History Penyesuaian Bahan');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('H2',$tanggal);
		$spreadsheet->getActiveSheet()->getStyle("A1:H1")->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle("H1:H4")->applyFromArray($right);
		$spreadsheet->getActiveSheet()->setTitle('History Transfer Stock ');		
		$spreadsheet->getActiveSheet()->setTitle('History Penyesuaian Stock ');
		$spreadsheet->setActiveSheetIndex(0);
		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="History Penyesuaian Bahan '.$date.'.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;		
	}	

}

/* End of file HistoryPenyesuaianBahanController.php */
/* Location: ./application/controllers/HistoryPenyesuaianBahanController.php */