<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
class LowStockProdukController extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('produk','',true);
		$this->load->model('jenis_produk','',true);
		$this->load->model('stock_produk','',true);
		$this->load->model('lokasi','',true);
		$this->load->model('satuan','',true);
		$this->load->model('suplier','',true);
	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		
		array_push($this->js, "script/app.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Produk < Inventori < ".$this->config->item('company_name');;
		$data['parrent'] = "inventori";
		$data['page'] = $this->uri->segment(1);
		array_push($column, array("data"=>"no"));
		array_push($column, array("data"=>"produk_kode"));
		array_push($column, array("data"=>"produk_nama"));
		array_push($column, array("data"=>"jenis_produk_nama"));
		array_push($column, array("data"=>"produk_minimal_stock"));
		array_push($column, array("data"=>"satuan_nama"));
		array_push($column, array("data"=>"lokasi_nama"));
		array_push($column, array("data"=>"jumlah_lokasi"));
		$data['sumColumn'] = json_encode(array(7));
				$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0,7)));
		$data["lokasi"] = $this->lokasi->all_list();
		$data["jenis_produk"] = $this->jenis_produk->all_list();
		$data["satuan"] = $this->satuan->all_list();
		$data["suplier"] = $this->suplier->all_list();
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/produk');
		$this->load->view('admin/static/footer');
	}
	function list(){
		$lokasi_id = null;
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->produk->low_produk_count_all($lokasi_id);
		$result['iTotalDisplayRecords'] = $this->produk->low_produk_count_filter($query,$lokasi_id);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->produk->low_produk_list($start,$length,$query,$lokasi_id);
		$i = $start+1;
		foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i; 
			$key->row_id = $key->produk_id;
			$key->produk_minimal_stock = number_format($key->produk_minimal_stock,2);
			$key->stock = number_format($key->stock,2);
			$i++;
		}
		$result['aaData'] = $data;			
		echo json_encode($result);
	}
	function pdf(){
        $lokasi_id = null;
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $list =  $this->produk->low_produk_list($start,$length,$query,$lokasi_id);
        $i = $start+1;
        foreach ($list as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $key->row_id = $key->produk_id;
            $key->produk_minimal_stock = number_format($key->produk_minimal_stock,2);
            $key->stock = number_format($key->stock,2);
            $i++;
        }
        $data['list'] = $list;
        //echo json_encode($data['list']);
        $mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
        $html = $this->load->view('admin/pdf/low_stock_pdf',$data,true);
        $mpdf->WriteHTML($html);
        $date = date("Y-m-d");
        if($this->input->get('start_date')!=""){
            $date = $this->input->get('start_date')." s.d ".$this->input->get('end_date');
        }
        $mpdf->Output('Laporan Low Stok Produk'.$date.".pdf","D");
    }
    function excel(){
        $lokasi_id = null;
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $list =  $this->produk->low_produk_list($start,$length,$query,$lokasi_id);
        $i = $start+1;
        foreach ($list as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $key->row_id = $key->produk_id;
            $key->produk_minimal_stock = number_format($key->produk_minimal_stock,2);
            $key->stock = number_format($key->stock,2);
            $i++;
        }
        $spreadsheet = new Spreadsheet();

        // Set document properties
        $spreadsheet->getProperties()->setCreator($this->config->item('company_name'))
            ->setLastModifiedBy($_SESSION['login']['user_name'])
            ->setTitle('Laporan Stok Produk')
            ->setSubject('');
        $style = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            )
        );
        $right = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
            )
        );
        $border = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],

        );


        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A7', 'No')
            ->setCellValue('B7', 'Kode Produk')
            ->setCellValue('C7', 'Nama Produk')
            ->setCellValue('D7', 'Jenis Produk')
            ->setCellValue('E7', 'Minimal Stock')
            ->setCellValue('F7', 'Satuan')
            ->setCellValue('G7', 'Lokasi')
            ->setCellValue('H7', 'Stock')
        ;
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(23);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(28);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(28);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(12);
        $spreadsheet->getActiveSheet()->getStyle("A7:F7")->applyFromArray($style);
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $date = date("Y-m-d");
        $sum = 0;
        $i=8; foreach($list as $key) {

            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A'.$i, $key->no)
                ->setCellValue('B'.$i, $key->produk_kode)
                ->setCellValue('C'.$i, $key->produk_nama)
                ->setCellValue('D'.$i, $key->jenis_produk_nama)
                ->setCellValue('E'.$i, $key->produk_minimal_stock)
                ->setCellValue('F'.$i, $key->satuan_nama)
                ->setCellValue('G'.$i, $key->lokasi_nama)
                ->setCellValue('H'.$i, $key->jumlah_lokasi)
            ;
            $i++;
            $sum += str_replace(",", "", $key->stock);
        }
        $spreadsheet->getActiveSheet()->getStyle("A7:H".$i)->applyFromArray($border);
        $spreadsheet->getActiveSheet()->getStyle('A7:H7')->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('BEBEBE');
        $spreadsheet->getActiveSheet()->getStyle('F8:F'.$i)->getAlignment()->setWrapText(true);
        // Rename worksheet
        $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
        $drawing->setName('logo');
        $drawing->setDescription('logo');
        $drawing->setPath("assets/media/logos/logo-laporan.png"); // put your path and image here
        $drawing->setCoordinates('A1');
        $drawing->setOffsetX(1);
        $drawing->setWidth(80);
        
        $drawing->setWorksheet($spreadsheet->getActiveSheet());

        $spreadsheet->setActiveSheetIndex(0)->setCellValue('C1',$this->config->item('company_name'));
        $spreadsheet->setActiveSheetIndex(0)->setCellValue('C2',$this->config->item('company_address'));
        $spreadsheet->setActiveSheetIndex(0)->setCellValue('C3',$this->config->item('company_phone'));
        $spreadsheet->getActiveSheet()->getStyle("A1:F1")->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getStyle("F1:F4")->applyFromArray($right);
        $spreadsheet->getActiveSheet()->setTitle('Laporan Low Stok Produk');
        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Laporan Low Stok Produk'.$date.'.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;

    }

}

/* End of file LowStockProdukController.php */
/* Location: ./application/controllers/LowStockProdukController.php */