<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	require 'vendor/autoload.php';

	use PhpOffice\PhpSpreadsheet\Helper\Sample;
	use PhpOffice\PhpSpreadsheet\IOFactory;
	use PhpOffice\PhpSpreadsheet\Spreadsheet;

class KartuStockProdukController extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('stock_produk','',true);
		$this->load->model('produk','',true);
		$this->load->model('lokasi','',true);
	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
		array_push($this->js, "vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
		array_push($this->js, "script/app.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Kartu stok produk < Inventori < ".$this->config->item('company_name');;
		$data['parrent'] = "inventori";
		$data['page'] = $this->uri->segment(1);
		$data['produk'] = $this->produk->all_list();
		$data['lokasi'] = $this->lokasi->all_list();
		array_push($column, array("data"=>"produk_nama_in"));
		array_push($column, array("data"=>"stock_produk_seri_in"));
		array_push($column, array("data"=>"lokasi_nama_in"));
		array_push($column, array("data"=>"stock_in"));
		array_push($column, array("data"=>"keterangan_in"));
		array_push($column, array("data"=>"tanggal"));
		array_push($column, array("data"=>"produk_nama_out"));
		array_push($column, array("data"=>"stock_produk_seri_out"));
		array_push($column, array("data"=>"lokasi_nama_out"));
		array_push($column, array("data"=>"stock_out"));
		array_push($column, array("data"=>"keterangan_out"));
		array_push($column, array("data"=>"total_stock"));
		$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(3,9,11)));
		$data['first_date'] = $this->stock_produk->first_date();
		$this->load->view('admin/static/header',$data);
		$this->load->view('admin/static/sidebar');
		$this->load->view('admin/static/topbar');
		$this->load->view('admin/kartu_stock_produk');
		$this->load->view('admin/static/footer');
	}
	function list(){

		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->stock_produk->arus_stock_produk_count();
		$result['iTotalDisplayRecords'] = $this->stock_produk->arus_stock_produk_count_filter($query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->stock_produk->arus_stock_produk_list($start,$length,$query);
		$displayData = array();
		$empty = array("produk_nama_in"=>"","stock_produk_seri_in"=>"","lokasi_nama_in"=>"","stock_in"=>"","keterangan_in"=>"","produk_nama_out"=>"","stock_produk_seri_out"=>"","lokasi_nama_out"=>"","stock_out"=>"","keterangan_out"=>"","total_stock"=>"");
				foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$temp = $empty;
			if ($key->stock_in>0){
				$temp["produk_nama_in"] = $key->produk_nama;
				$temp["stock_produk_seri_in"] = $key->stock_produk_seri;
				$temp["lokasi_nama_in"] = $key->lokasi_nama;
				$temp["stock_in"] = number_format($key->stock_in,2);
				$temp["keterangan_in"] = $key->keterangan;
			} else {
				$temp["produk_nama_out"] = $key->produk_nama;
				$temp["stock_produk_seri_out"] = $key->stock_produk_seri;
				$temp["lokasi_nama_out"] = $key->lokasi_nama;
				$temp["stock_out"] = number_format($key->stock_out,2);
				$temp["keterangan_out"] = $key->keterangan;				
			}
						$time = strtotime($key->tanggal);
			$temp["tanggal"] = date("d-m-Y",$time);
			$temp["total_stock"] = number_format($key->last_stock,2);
			array_push($displayData, $temp);
		}
		$result['aaData'] = $displayData;
		echo json_encode($result);
	}
	function pdf(){
		$data['first_date'] = $this->stock_produk->first_date();
		$data["start_date"] = $this->input->get('start_date');
		$data["end_date"] = $this->input->get('end_date');
		if($this->input->get('produk_id')!="" && $this->input->get('produk_id')!="all"){
			$data["produk_nama"] = $this->produk->row_by_id($this->input->get('produk_id'))->produk_nama;
		}
		if($this->input->get('lokasi_id')!="" && $this->input->get('lokasi_id')!="all"){
			$data["lokasi_nama"] = $this->lokasi->row_by_id($this->input->get('lokasi_id'))->lokasi_nama;
		}		
		$start = 0;
		$length = $this->stock_produk->arus_stock_produk_count_filter('');
		$list =  $this->stock_produk->arus_stock_produk_list($start,$length,'');
		$displayData = array();
		$empty = array("produk_nama_in"=>"","stock_produk_seri_in"=>"","lokasi_nama_in"=>"","stock_in"=>"","keterangan_in"=>"","produk_nama_out"=>"","stock_produk_seri_out"=>"","lokasi_nama_out"=>"","stock_out"=>"","keterangan_out"=>"","total_stock"=>"");
		foreach ($list as $key) {
			$temp = $empty;
			if ($key->stock_in > 0){
				$temp["produk_nama_in"] = $key->produk_nama;
				$temp["stock_produk_seri_in"] = $key->stock_produk_seri;
				$temp["lokasi_nama_in"] = $key->lokasi_nama;
				$temp["stock_in"] = number_format($key->stock_in,2);
				$temp["keterangan_in"] = $key->keterangan;
			} else {
				$temp["produk_nama_out"] = $key->produk_nama;
				$temp["stock_produk_seri_out"] = $key->stock_produk_seri;
				$temp["lokasi_nama_out"] = $key->lokasi_nama;
				$temp["stock_out"] = number_format($key->stock_out,2);
				$temp["keterangan_out"] = $key->keterangan;				
			}
						$time = strtotime($key->tanggal);
			$temp["tanggal"] = date("d-m-Y",$time);
			$temp["total_stock"] = number_format($key->last_stock,2);
			array_push($displayData, $temp);
		}
		$css = array();
		array_push($css, base_url()."assets/vendors/custom/datatables/datatables.bundle.css");
		array_push($css, base_url()."assets/demo/default/base/style.bundle.css");
		$data["css"] = $css;
		$data['list'] = $displayData;
		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
        $html = $this->load->view('admin/pdf/pdf_kartu_stock_produk',$data,true);
        $mpdf->WriteHTML($html);
        $date = date("Y-m-d");
        if($this->input->get('start_date')!=""){
        	$date = " ".$this->input->get('start_date')." s.d ".$this->input->get('end_date');
        }
        $mpdf->Output('Kartu Stok Produk '.$date.".pdf","D");
        
	}
	function excel(){
		$start = 0;
		$length = $this->stock_produk->arus_stock_produk_count_filter('');
		$list =  $this->stock_produk->arus_stock_produk_list($start,$length,'');
		$displayData = array();
		$empty = array("produk_nama_in"=>"","stock_produk_seri_in"=>"","lokasi_nama_in"=>"","stock_in"=>"","keterangan_in"=>"","produk_nama_out"=>"","stock_produk_seri_out"=>"","lokasi_nama_out"=>"","stock_out"=>"","keterangan_out"=>"","total_stock"=>"");
		foreach ($list as $key) {
			$temp = $empty;
			if ($key->stock_in > 0){
				$temp["produk_nama_in"] = $key->produk_nama;
				$temp["stock_produk_seri_in"] = $key->stock_produk_seri;
				$temp["lokasi_nama_in"] = $key->lokasi_nama;
				$temp["stock_in"] = number_format($key->stock_in,2);
				$temp["keterangan_in"] = $key->keterangan;
			} else {
				$temp["produk_nama_out"] = $key->produk_nama;
				$temp["stock_produk_seri_out"] = $key->stock_produk_seri;
				$temp["lokasi_nama_out"] = $key->lokasi_nama;
				$temp["stock_out"] = number_format($key->stock_out,2);
				$temp["keterangan_out"] = $key->keterangan;				
			}
			$time = strtotime($key->tanggal);
			$temp["tanggal"] = date("d-m-Y",$time);
			$temp["total_stock"] = number_format($key->last_stock,2);
			array_push($displayData, $temp);
		}
		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator($this->config->item('company_name'))
		->setLastModifiedBy($_SESSION['login']['user_name'])
		->setTitle('Kartu Stock Produk')
		->setSubject('');
		$style = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        )
	    );
	    $right = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
	        )
	    );
		$border = array(
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        ),
			'borders' => [
		        'allBorders' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
		        ],
		    ],

	    );
		// Add some data
		$data['first_date'] = $this->stock_produk->first_date();
		$tanggal =$data['first_date']. " s/d ".date("d-m-Y");
		$produk = "Semua Produk";
		if($this->input->get('produk_id')!="" && $this->input->get('produk_id')!="all"){
			$produk = $this->produk->row_by_id($this->input->get('produk_id'))->produk_nama;
		}
		if($this->input->get('start_date')!="") $tanggal = $this->input->get('start_date')." s/d ".$this->input->get('end_date');
		$lokasi = "Semua Lokasi";
		if($this->input->get('lokasi_id')!="" && $this->input->get('lokasi_id')!="all"){
			$lokasi = $this->lokasi->row_by_id($this->input->get('lokasi_id'))->lokasi_nama;
		}
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('A6','Stok Masuk');
		$spreadsheet->getActiveSheet()->mergeCells('A6:E6');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('G6','Stok Keluar');
		$spreadsheet->getActiveSheet()->mergeCells('G6:K6');
		$spreadsheet->getActiveSheet()->mergeCells('F6:F7');
		$spreadsheet->getActiveSheet()->mergeCells('L6:L7');
		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A7', 'Produk')
		->setCellValue('B7', 'Seri')
		->setCellValue('C7', 'Lokasi')
		->setCellValue('D7', 'Masuk')
		->setCellValue('E7', 'Keterangan')
		->setCellValue('F6', 'Tanggal')
		->setCellValue('G7', 'Produk')
		->setCellValue('H7', 'Seri')
		->setCellValue('I7', 'Lokasi')
		->setCellValue('J7', 'Keluar')
		->setCellValue('K7', 'Keterangan')
		->setCellValue('L6', 'Stok Akhir')
		;
		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(12);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(12);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(12);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(12);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(12);
		$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(12);
		$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(12);
		$spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(12);
		$spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(12);
		$spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(18);
		$spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(12);
		$spreadsheet->getActiveSheet()->getStyle("A6:l7")->applyFromArray($style);
		// Miscellaneous glyphs, UTF-8
		$i=8; foreach($displayData as $key) {

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $key["produk_nama_in"])
		->setCellValue('B'.$i, $key["stock_produk_seri_in"])
		->setCellValue('C'.$i, $key["lokasi_nama_in"])
		->setCellValue('D'.$i, $key["stock_in"])
		->setCellValue('E'.$i, $key["keterangan_in"])
		->setCellValue('F'.$i, $key["tanggal"])
		->setCellValue('G'.$i, $key["produk_nama_out"])
		->setCellValue('H'.$i, $key["stock_produk_seri_out"])
		->setCellValue('I'.$i, $key["lokasi_nama_out"])
		->setCellValue('J'.$i, $key["stock_out"])
		->setCellValue('K'.$i, $key["keterangan_out"])
		->setCellValue('L'.$i, $key["total_stock"]);
		$i++;
		}
		$spreadsheet->getActiveSheet()->getStyle("A6:L".$i)->applyFromArray($border);
		$spreadsheet->getActiveSheet()->getStyle('A6:L7')->getFill()
	    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
	    ->getStartColor()->setARGB('BEBEBE');
	    $spreadsheet->getActiveSheet()->getStyle('E8:E'.$i)->getAlignment()->setWrapText(true);
	    $spreadsheet->getActiveSheet()->getStyle('K8:K'.$i)->getAlignment()->setWrapText(true);	    
		// Rename worksheet
		$spreadsheet->getActiveSheet()->setTitle('Kartu Stok Produk '.date('d-m-Y H'));
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('logo');
		$drawing->setDescription('logo');
		$drawing->setPath("assets/media/logos/carolinas-logo.png"); // put your path and image here
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(1);
		$drawing->setWidth(80);
		$drawing->setHeight(80);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		$spreadsheet->setActiveSheetIndex(0)->setCellValue('B1',$this->config->item('company_name'));
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('B2',$this->config->item('company_address'));
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('B3',$this->config->item('company_phone'));
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('L1','Kartu Stok Produk');
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('L2',$tanggal);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('L3',$produk);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('L4',$lokasi);
		$spreadsheet->getActiveSheet()->getStyle("A1:L1")->getFont()->setBold(true);
		$spreadsheet->getActiveSheet()->getStyle("L1:L4")->applyFromArray($right);
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$spreadsheet->setActiveSheetIndex(0);

		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Kartu Stock Produk.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;
	}	
}

/* End of file KartuStockProdukController.php */
/* Location: ./application/controllers/KartuStockProdukController.php */