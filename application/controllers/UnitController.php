<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UnitController extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('produk', '', true);
        $this->load->model('unit', '', true);
        $this->load->model('satuan', '', true);
    }
    public function index()
    {
        $id = $this->dencrypt_id($this->uri->segment(2));
        $produk = $this->produk->row_by_id($id);
        $satuan = $this->satuan->row_by_id($produk->produk_satuan_id);
        if($produk != null) {
            array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
            array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
            array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
            array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
            array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
            array_push($this->css, "vendors/general/select2/dist/css/select2.css");
            array_push($this->js, "vendors/general/select2/dist/js/select2.full.js");
            array_push($this->js, "script/app.js");
            $data['satuan'] = $satuan;
            $data["css"] = $this->css;
            $data["js"] = $this->js;
            $column = array();
            $data["meta_title"] = "Konversi Satuan < Inventori < " . $this->config->item('company_name');;
            $data['parrent'] = "inventori";
            $data['page'] = "produk";
            $data['id'] = $id;
            $data['satuan_list'] = $this->satuan->all_list();
            array_push($column, array("data" => "no"));
            array_push($column, array("data" => "satuan_nama"));
            array_push($column, array("data" => "jumlah_satuan_unit"));
            array_push($column, array("data" => "satuan_default"));
            array_push($column, array("data" => "barcode"));
            array_push($column, array("data" => "harga_jual"));
            $data['column'] = json_encode($column);
            $data['columnDef'] = json_encode(array("className" => "text__right", "targets" => array(0)));
            $akses_menu = json_decode($this->menu_akses, true);
            $action = array("edit" => true, "delete" => true);
            $data['action'] = json_encode($action);
            $data['produk'] = $produk;
            $this->load->view('admin/static/header', $data);
            $this->load->view('admin/static/sidebar');
            $this->load->view('admin/static/topbar');
            $this->load->view('admin/unit');
            $this->load->view('admin/static/footer');
        } else {
            redirect('404_override','refresh');
        }
    }
    function list(){
        $id = $this->dencrypt_id($this->uri->segment(2));
        $query = $this->input->get('search')["value"];
        $start = $this->input->get('start');
        $length = $this->input->get('length');
        $result['iTotalRecords'] = $this->unit->unit_count_all($id);
        $result['iTotalDisplayRecords'] = $this->unit->unit_count_filter($query,$id);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $produk = $this->produk->row_by_id($id);
        $satuan = $this->satuan->row_by_id($produk->produk_satuan_id);
        if ($length == -1) $length = $result['iTotalDisplayRecords'];
        $data =  $this->unit->unit_list($start,$length,$query,$id);
        $i = $start+1;
        foreach ($data as $key) {
            if($key->created_at != null){
                $time = strtotime($key->created_at);
                $key->created_at = date('d-m-Y H:i:s',$time);
            }
            if($key->updated_at != null){
                $time = strtotime($key->updated_at);
                $key->updated_at = date('d-m-Y H:i:s',$time);
            }
            $key->no = $i;
            $i++;
            $key->jumlah_satuan_unit = $key->jumlah_satuan_unit;
            $key->satuan_default=$satuan->satuan_nama;
            $key->harga_jual=number_format($key->harga_jual);
            $key->delete_url = base_url().'unit-delete';
            $key->row_id = $key->unit_id;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
    function add(){
        $result['success'] = false;
        $result['message'] = "Kode ini telah terdaftar";
        $data["produk_id"] = $this->input->post("produk_id");
        $data["satuan_id"] = $this->input->post("satuan_id");
        $data["barcode"] = $this->input->post("barcode");
        $data["jumlah_satuan_unit"] = $this->string_to_decimal($this->input->post("jumlah_satuan_unit"));
        $data["harga_jual"] = $this->string_to_decimal($this->input->post("harga_jual"));
        $data["created_at"] = Date("Y-m-d H:i:s");
        $insert = $this->unit->insert($data);
        if($insert){
            $result['success'] = true;
            $result['message'] = "Data berhasil disimpan";
        } else {
            $result['message'] = "Gagal menyimpan data";
        }
        echo json_encode($result);
    }
    function delete(){
        $id = $this->input->input_stream('id');
        $result['success'] = false;
        $result['message'] = "missing parameter";
        if($id != ""){
            $delete = $this->unit->delete_by_id("unit_id",$id);
            if($delete){
                $result['success'] = true;
                $result['message'] = "Data berhasil dihapus";
            } else {
                $result['message'] = "Gagal menghapus data";
            }
        }
        echo json_encode($result);
    }
    function edit(){
        $result['success'] = false;
        $result['message'] = "Kode sudah terdaftar";
        $data = array();
        $data["produk_id"] = $this->input->post("produk_id");
        $data["satuan_id"] = $this->input->post("satuan_id");
        $data["barcode"] = $this->input->post("barcode");
        $data["jumlah_satuan_unit"] = $this->string_to_decimal($this->input->post("jumlah_satuan_unit"));
        $data["harga_jual"] = $this->string_to_decimal($this->input->post("harga_jual"));
        $unit_id = $this->input->post('unit_id');
        $updated_at = date('Y-m-d H:i:s');
        $data['updated_at'] = $updated_at;
        $update = $this->unit->update_by_id('unit_id',$unit_id,$data);
        if($update){
            $result['success'] = true;
            $result['message'] = "Data berhasil disimpan";
        } else {
            $result['message'] = "Gagal menyimpan data";
        }
        echo json_encode($result);
    }
}