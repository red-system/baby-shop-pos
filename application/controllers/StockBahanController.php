<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StockBahanController extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('bahan','',true);
		$this->load->model('stock_bahan','',true);
		$this->load->model('lokasi','',true);
		$this->load->model('satuan','',true);
	}

	public function index()
	{
		array_push($this->css, "vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
		array_push($this->js, "vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
		array_push($this->js, "vendors/general/jquery-validation/dist/jquery.validate.js");
		array_push($this->js, "vendors/custom/datatables/datatables.bundle.min.js");
		array_push($this->css, "vendors/custom/datatables/datatables.bundle.css");
			
		array_push($this->js, "script/app.js");

		$data["css"] = $this->css;
		$data["js"] = $this->js;
		$column = array();
		$data["meta_title"] = "Stok Bahan< Inventori < ".$this->config->item('company_name');;
		$data['parrent'] = "inventori";
		$data['page'] = 'bahan';
		$url = str_replace(array("-","_"), array("+","/"), $this->uri->segment(2));
		$id = $this->encryption->decrypt($url);
		$data["lokasi"] = $this->lokasi->all_list();
		$bahan = $this->bahan->row_by_id($id);
		$data['id'] = $id;
		if ($bahan != null) {
			$data['nama_satuan'] = $this->satuan->row_by_id($bahan->bahan_satuan_id)->satuan_nama;
			$data['nama_bahan'] = $bahan->bahan_nama;
			array_push($column, array("data"=>"no"));
			array_push($column, array("data"=>"lokasi_nama"));
			array_push($column, array("data"=>"stock_bahan_seri"));
			array_push($column, array("data"=>"stock_bahan_qty"));
			$data['sumColumn'] = json_encode(array(3));
					$data['column'] = json_encode($column);
		$data['columnDef'] = json_encode(array("className"=>"text__right","targets"=>array(0,3)));
			$data["action"] = json_encode(array("stock"=>false,"view"=>false,"edit"=>true,"delete"=>true));
			
			$this->load->view('admin/static/header',$data);
			$this->load->view('admin/static/sidebar');
			$this->load->view('admin/static/topbar');
			$this->load->view('admin/stock_bahan');
			$this->load->view('admin/static/footer');
		} else {
			redirect('404_override','refresh');
		}
		
	}
	function list(){
		$query = $this->input->get('search')["value"];
		$start = $this->input->get('start');
		$length = $this->input->get('length');
		$result['iTotalRecords'] = $this->stock_bahan->stock_bahan_count($this->uri->segment(3));
		$result['iTotalDisplayRecords'] = $this->stock_bahan->stock_bahan_count_filter($this->uri->segment(3),$query);
		$result['sEcho'] = 0;
		$result['sColumns'] = '';
		if ($length == -1) $length = $result['iTotalDisplayRecords'];
		$data =  $this->stock_bahan->stock_bahan_list($start,$length,$query,$this->uri->segment(3));
		$i = $start+1;
				foreach ($data as $key) {
			if($key->created_at != null){
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y H:i:s',$time);
			}
			if($key->updated_at != null){
				$time = strtotime($key->updated_at);
				$key->updated_at = date('d-m-Y H:i:s',$time);
			}
			$key->no = $i; 
			$key->delete_url = base_url().'stock-bahan/delete/'.$key->bahan_id;
			$key->row_id = $key->stock_bahan_id;
			$key->stock_bahan_qty = number_format($key->stock_bahan_qty);
			$i++;
		}
		$result['aaData'] = $data;			
		echo json_encode($result);
	}
	function add(){
		$result['success'] = false;
		$result['message'] = "Bahan dengan seri yang sama tidak dapat ditambahkan dilokasi yang sama";
		$data["stock_bahan_seri"] = $this->input->post('stock_bahan_seri');
		$data["stock_bahan_qty"] = $this->string_to_number($this->input->post('stock_bahan_qty'));
		$data["bahan_id"] = $this->input->post('bahan_id');
		$data["stock_bahan_lokasi_id"] = $this->input->post('stock_bahan_lokasi_id');
		if(isset($_SESSION['login']['lokasi_id'])){
			$data["stock_bahan_lokasi_id"] = $_SESSION['login']['lokasi_id'];
		}
		$cek = $this->stock_bahan->is_bahan_seri_ready_arr($data["stock_bahan_seri"],$data["bahan_id"],$data["stock_bahan_lokasi_id"]);
		if($cek['status']){
			$insert = $this->stock_bahan->insert($data);
			if($insert){
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
				$data = array();
				$data["tanggal"] = date("Y-m-d");
				$data["table_name"] = "stock_bahan";
				$data["stock_bahan_id"] = $this->stock_bahan->last_id();
				$data["bahan_id"] = $this->input->post('bahan_id');
				$data["stock_out"] = 0;
				$data["stock_in"] = $this->string_to_number($this->input->post('stock_bahan_qty'));
				$data["last_stock"] = $this->stock_bahan->last_stock($this->input->post('bahan_id'))->result;
				$data["last_stock_total"] = $this->stock_bahan->stock_total()->result;
				$data["keterangan"] = "Insert stok bahan";
				$data["method"] = "insert";
				$this->stock_bahan->arus_stock_bahan($data);
			} else {
				$result['message'] = "Gagal menyimpan data";
			}
		} else {
			$cekData = $cek['data'];
			$stock = $cekData->stock_bahan_qty + $this->input->post('stock_bahan_qty');
			$data['stock_bahan_qty'] = $stock;
			$update = $this->stock_bahan->update_by_id('stock_bahan_id',$cekData->stock_bahan_id,$data);
			if ($update){
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
				$data = array();
				$data["tanggal"] = date("Y-m-d");
				$data["table_name"] = "stock_bahan";
				$data["stock_bahan_id"] = $cekData->stock_bahan_id;
				$data["bahan_id"] = $this->input->post('bahan_id');
				$data["stock_out"] = 0;
				$data["stock_in"] = $this->string_to_number($this->input->post('stock_bahan_qty'));
				$data["last_stock"] = $this->stock_bahan->last_stock($this->input->post('bahan_id'))->result;
				$data["last_stock_total"] = $this->stock_bahan->stock_total()->result;
				$data["keterangan"] = "Insert stok bahan";
				$data["method"] = "update";
				$this->stock_bahan->arus_stock_bahan($data);
			} else {
				$result['message'] = "Gagal menyimpan data";
			}	
		}	
		echo json_encode($result);
	}
	function edit(){
		$result['success'] = false;
		$result['message'] = "Tidak dapat menyimpan bahan dengan seri bahan yang sama dengan data yang ada ";
		$data["stock_bahan_seri"] = $this->input->post('stock_bahan_seri');
		$data["stock_bahan_qty"] = $this->string_to_number($this->input->post('stock_bahan_qty'));;
		$data["stock_bahan_lokasi_id"] = $this->input->post('stock_bahan_lokasi_id');
		if(isset($_SESSION['login']['lokasi_id'])){
			$data["stock_bahan_lokasi_id"] = $_SESSION['login']['lokasi_id'];
		}		
		$data["bahan_id"] = $this->input->post('bahan_id');
		$stock_bahan_id = $this->input->post('stock_bahan_id');
		$old_data = $this->stock_bahan->row_by_id($stock_bahan_id);
		if($this->stock_bahan->is_bahan_seri_ready($data["stock_bahan_seri"],$data["bahan_id"],$data["stock_bahan_lokasi_id"],$stock_bahan_id)){
			$insert = $this->stock_bahan->update_by_id('stock_bahan_id',$stock_bahan_id,$data);
			if($insert){
				$data = array();
				$diff = $old_data->stock_bahan_qty -$this->string_to_number($this->input->post('stock_bahan_qty'));;
				if ($diff != 0) {
					$data["tanggal"] = date("Y-m-d");
					$data["table_name"] = "stock_bahan";
					$data["stock_bahan_id"] = $stock_bahan_id;
					$data["bahan_id"] = $this->input->post('bahan_id');
					if ($diff > 0) {
						$data["stock_out"] = abs($diff);
						$data["stock_in"] = 0;
					} else {
						$data["stock_in"] = abs($old_data->stock_bahan_qty - $this->string_to_number($this->input->post('stock_bahan_qty')));
						$data["stock_out"] = 0;
					}
					$data["last_stock"] = $this->stock_bahan->last_stock($this->input->post('bahan_id'))->result;
					$data["last_stock_total"] = $this->stock_bahan->stock_total()->result;
					$data["keterangan"] = "Edit stok bahan";
					$data["method"] = "update";
					$this->stock_bahan->arus_stock_bahan($data);
				}
				$result['success'] = true;
				$result['message'] = "Data berhasil disimpan";
			} else {
				$result['message'] = "Gagal menyimpan data";
			}
		}	
		echo json_encode($result);
	}
	function delete(){
		$bahan_id = $this->uri->segment(3);
		$id = $this->input->input_stream('id');
		$result['success'] = false;
		$result['message'] = "missing parameter";
		$old_data = $this->stock_bahan->row_by_id($id);
		if($id != ""){
			$data["delete_flag"] = 1;
			$delete = $this->stock_bahan->update_by_id("stock_bahan_id",$id,$data);
			if($delete){
				$result['success'] = true;
				$result['message'] = "Data berhasil dihapus";
				$data = array();
				$data["tanggal"] = date("Y-m-d");
				$data["table_name"] = "stock_bahan";
				$data["stock_bahan_id"] = $id;
				$data["bahan_id"] = $bahan_id;
				$data["stock_out"] = $old_data->stock_bahan_qty;
				$data["stock_in"] = 0;
				$data["last_stock"] = $this->stock_bahan->last_stock($bahan_id)->result;
				$data["last_stock_total"] = $this->stock_bahan->stock_total()->result;
				$data["keterangan"] = "Delete stok bahan";
				$data["method"] = "delete";
				$this->stock_bahan->arus_stock_bahan($data);
			} else {
				$result['message'] = "Gagal menghapus data";
			}
		}
		echo json_encode($result);
	}
}

/* End of file StockBahanController.php */
/* Location: ./application/controllers/StockBahanController.php */