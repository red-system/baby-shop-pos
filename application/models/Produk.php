<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name = 'produk';
	}
	function produk_list($start,$length,$query){
		
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id','left');
		$sql = 'produk.*,jenis_produk_nama, display_stock_produk.jumlah as stock, satuan.satuan_nama';
		if(isset($_SESSION['login']['lokasi_id'])){
			$sql .= ', if(a.jumlah is not null, a.jumlah,0) as "jumlah_lokasi"';
			$this->db->join('(select display_stock_produk_lokasi.* from display_stock_produk_lokasi where display_stock_produk_lokasi.stock_produk_lokasi_id = '.$_SESSION['login']['lokasi_id'].') a', 'a.produk_id = produk.produk_id', 'left');
			
		}
		$this->db->select($sql);		
		$this->db->group_start();
			$this->db->like('produk.produk_id', $query, 'BOTH'); 
			$this->db->or_like('produk.produk_nama', $query, 'BOTH'); 
			$this->db->or_like('produk.produk_kode', $query, 'BOTH'); 
			$this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH');
			$this->db->or_like('produk.produk_minimal_stock', $query, 'BOTH'); 
			$this->db->or_like('produk.harga_eceran', $query, 'BOTH'); 
		$this->db->group_end();
		$this->db->group_by('produk.produk_id');
		$this->db->order_by('produk_id', 'desc');
		return $this->db->get('produk', $length, $start)->result();
	}
	function all_list(){
		$this->db->select('produk.*,satuan_nama');
		$this->db->join('jenis_produk', 'jenis_produk.jenis_produk_id = produk.produk_jenis_id');
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
		return $this->db->get('produk')->result();
	}
	function konversi_produk_list(){
		$this->db->select('produk.produk_id,produk.produk_kode');
		$this->db->join('jenis_produk','produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		return $this->db->get('produk')->result();
	}
	function row_by_id($id){
		$this->db->select('produk.*');
		$this->db->join('jenis_produk', 'jenis_produk.jenis_produk_id = produk.produk_jenis_id');
		$this->db->where('produk_id', $id);
		return $this->db->get('produk')->row();
	}
	function produk_count_all(){
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id','left');
		if(isset($_SESSION['login']['lokasi_id'])){
			$this->db->join('(select display_stock_produk_lokasi.* from display_stock_produk_lokasi where display_stock_produk_lokasi.stock_produk_lokasi_id = '.$_SESSION['login']['lokasi_id'].') a', 'a.produk_id = produk.produk_id', 'left');
		}
		$this->db->group_by('produk.produk_id');
		return $this->db->get('produk')->num_rows();
	}
	function produk_count_filter($query){
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id','left');
		if(isset($_SESSION['login']['lokasi_id'])){
			$this->db->join('(select display_stock_produk_lokasi.* from display_stock_produk_lokasi where display_stock_produk_lokasi.stock_produk_lokasi_id = '.$_SESSION['login']['lokasi_id'].') a', 'a.produk_id = produk.produk_id', 'left');
			
		}		
		$this->db->group_start();
			$this->db->like('produk.produk_id', $query, 'BOTH'); 
			$this->db->or_like('produk.produk_nama', $query, 'BOTH'); 
			$this->db->or_like('produk.produk_kode', $query, 'BOTH'); 
			$this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH');
			$this->db->or_like('produk.produk_minimal_stock', $query, 'BOTH'); 
			$this->db->or_like('produk.harga_eceran', $query, 'BOTH');
		$this->db->group_end();
		$this->db->group_by('produk.produk_id');
		return $this->db->get('produk')->num_rows();
	}
    function low_produk_list($start,$length,$query){
        $this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
        $this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
        $this->db->join('display_stock_produk_lokasi', 'display_stock_produk_lokasi.produk_id = produk.produk_id','left');
        $sql = 'produk.*,jenis_produk_nama, display_stock_produk_lokasi.jumlah as stock, satuan_nama';
        if(isset($_SESSION['login']['lokasi_id'])){
            $sql .= ', if(a.jumlah is not null, a.jumlah,0) as "jumlah_lokasi",a.lokasi_nama';
            $this->db->join('(select display_stock_produk_lokasi.*,lokasi.lokasi_nama from display_stock_produk_lokasi inner join lokasi on lokasi.lokasi_id = display_stock_produk_lokasi.stock_produk_lokasi_id  where display_stock_produk_lokasi.stock_produk_lokasi_id = '.$_SESSION['login']['lokasi_id'].') a', 'a.produk_id = produk.produk_id', 'left');

        } else {
            $sql .= ', if(a.jumlah is not null, a.jumlah,0) as "jumlah_lokasi",a.lokasi_nama';
            $this->db->join('(select display_stock_produk_lokasi.*,lokasi.lokasi_nama from display_stock_produk_lokasi inner join lokasi on lokasi.lokasi_id = display_stock_produk_lokasi.stock_produk_lokasi_id) a', 'a.produk_id = produk.produk_id', 'left');
        }
        $this->db->select($sql);
        $this->db->group_start();
        $this->db->like('produk.produk_id', $query, 'BOTH');
        $this->db->or_like('produk.produk_kode', $query, 'BOTH');
        $this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH');
        $this->db->group_end();
        $this->db->group_start();
        $this->db->where('a.jumlah <= produk.produk_minimal_stock','',false);
        $this->db->or_where('a.jumlah is null','',false);
        $this->db->group_end();
        $this->db->group_by('produk.produk_id');
        $this->db->order_by('produk.produk_id', 'desc');

        return $this->db->get('produk', $length, $start)->result();
    }
    function low_produk_count_filter($query){
        $this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
        $this->db->join('display_stock_produk_lokasi', 'display_stock_produk_lokasi.produk_id = produk.produk_id','left');
        if(isset($_SESSION['login']['lokasi_id'])){
            $this->db->join('(select display_stock_produk_lokasi.*,lokasi.lokasi_nama from display_stock_produk_lokasi inner join lokasi on lokasi.lokasi_id = display_stock_produk_lokasi.stock_produk_lokasi_id  where display_stock_produk_lokasi.stock_produk_lokasi_id = '.$_SESSION['login']['lokasi_id'].') a', 'a.produk_id = produk.produk_id', 'left');

        } else {
            $this->db->join('(select display_stock_produk_lokasi.*,lokasi.lokasi_nama from display_stock_produk_lokasi inner join lokasi on lokasi.lokasi_id = display_stock_produk_lokasi.stock_produk_lokasi_id) a', 'a.produk_id = produk.produk_id', 'left');
        }
        $this->db->group_start();
        $this->db->like('produk.produk_id', $query, 'BOTH');
        $this->db->or_like('produk.produk_kode', $query, 'BOTH');
        $this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH');
        $this->db->group_end();
        $this->db->group_start();
        $this->db->where('a.jumlah <= produk.produk_minimal_stock','',false);
        $this->db->or_where('a.jumlah is null','',false);
        $this->db->group_end();
        $this->db->group_by('produk.produk_id');
        $this->db->order_by('produk.produk_id', 'desc');
        return $this->db->get('produk')->num_rows();
    }
    function low_produk_count_all(){
        $this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
        $this->db->join('display_stock_produk_lokasi', 'display_stock_produk_lokasi.produk_id = produk.produk_id','left');
        if(isset($_SESSION['login']['lokasi_id'])){
            $this->db->join('(select display_stock_produk_lokasi.*,lokasi.lokasi_nama from display_stock_produk_lokasi inner join lokasi on lokasi.lokasi_id = display_stock_produk_lokasi.stock_produk_lokasi_id  where display_stock_produk_lokasi.stock_produk_lokasi_id = '.$_SESSION['login']['lokasi_id'].') a', 'a.produk_id = produk.produk_id', 'left');

        } else {
            $this->db->join('(select display_stock_produk_lokasi.*,lokasi.lokasi_nama from display_stock_produk_lokasi inner join lokasi on lokasi.lokasi_id = display_stock_produk_lokasi.stock_produk_lokasi_id) a', 'a.produk_id = produk.produk_id', 'left');
        }
        $this->db->group_start();
        $this->db->where('a.jumlah <= produk.produk_minimal_stock','',false);
        $this->db->or_where('a.jumlah is null','',false);
        $this->db->group_end();
        $this->db->group_by('produk.produk_id');
        $this->db->order_by('produk.produk_id', 'desc');
        return $this->db->get('produk')->num_rows();
    }
	function produk_by_id($produk_id){
		$this->db->select('produk.*,jenis_produk.jenis_produk_nama,jenis_produk.jenis_produk_kode');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->where('produk.produk_id', $produk_id);
		return $this->db->get('produk')->row();
	}
	function laporan_produk_list($start,$length,$query){
		
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id','left');
		$sql = 'produk.*,jenis_produk_nama, display_stock_produk.jumlah as stock, satuan.satuan_nama';
		$this->db->select($sql);		
		$this->db->group_start();
			$this->db->like('produk.produk_id', $query, 'BOTH'); 
			$this->db->or_like('produk.produk_kode', $query, 'BOTH');
			$this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH'); 
			$this->db->or_like('produk.produk_nama', $query, 'BOTH'); 
			$this->db->or_like('satuan.satuan_nama', $query, 'BOTH'); 
		$this->db->group_end();
		if(isset($_GET['jenis_produk_id'])){
			$this->db->where('jenis_produk_id', $this->input->get('jenis_produk_id'));
		}
		if(isset($_GET['satuan_id'])){
			$this->db->where('satuan_id', $this->input->get('satuan_id'));
		}			
		$this->db->group_by('produk.produk_id');
		$this->db->order_by('produk_id', 'desc');
		return $this->db->get('produk', $length, $start)->result();
	}
	function laporan_produk_count_all(){
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id','left');
		$this->db->group_by('produk.produk_id');
		return $this->db->get('produk')->num_rows();
	}
	function laporan_produk_count_filter($query){
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id','left');
		$this->db->group_start();
			$this->db->like('produk.produk_id', $query, 'BOTH'); 
			$this->db->or_like('produk.produk_kode', $query, 'BOTH');
			$this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH'); 
			$this->db->or_like('produk.produk_nama', $query, 'BOTH'); 
			$this->db->or_like('satuan.satuan_nama', $query, 'BOTH'); 
		$this->db->group_end();
		if(isset($_GET['jenis_produk_id'])){
			$this->db->where('jenis_produk_id', $this->input->get('jenis_produk_id'));
		}
		if(isset($_GET['satuan_id'])){
			$this->db->where('satuan_id', $this->input->get('satuan_id'));
		}			
		$this->db->group_by('produk.produk_id');
		return $this->db->get('produk')->num_rows();
	}
	function laporan_produk_count_filter_print($query,$jenis_produk_id,$satuan_id){
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id','left');
		$this->db->group_start();
			$this->db->like('produk.produk_id', $query, 'BOTH'); 
			$this->db->or_like('produk.produk_kode', $query, 'BOTH');
			$this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH'); 
			$this->db->or_like('produk.produk_nama', $query, 'BOTH'); 
			$this->db->or_like('satuan.satuan_nama', $query, 'BOTH'); 
		$this->db->group_end();
		if($jenis_produk_id!=null){
			$this->db->where('jenis_produk.jenis_produk_id', $this->input->get('jenis_produk_id'));
		}
		if($satuan_id!=null){
			$this->db->where('satuan.satuan_id', $this->input->get('satuan_id'));
		}
		$this->db->group_by('produk.produk_id');
		return $this->db->get('produk')->num_rows();
	}
	function laporan_produk_list_print($start,$length,$query,$jenis_produk_id,$satuan_id){
		
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('satuan', 'produk.produk_satuan_id = satuan.satuan_id');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id','left');
		$sql = 'produk.*,jenis_produk_nama, display_stock_produk.jumlah as stock, satuan.satuan_nama';
		$this->db->select($sql);		
		$this->db->group_start();
			$this->db->like('produk.produk_id', $query, 'BOTH'); 
			$this->db->or_like('produk.produk_kode', $query, 'BOTH');
			$this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH'); 
			$this->db->or_like('produk.produk_nama', $query, 'BOTH'); 
			$this->db->or_like('satuan.satuan_nama', $query, 'BOTH'); 
		$this->db->group_end();
		if($jenis_produk_id!=null){
			$this->db->where('jenis_produk.jenis_produk_id', $this->input->get('jenis_produk_id'));
		}
		if($satuan_id!=null){
			$this->db->where('satuan.satuan_id', $this->input->get('satuan_id'));
		}	
		$this->db->group_by('produk.produk_id');
		$this->db->order_by('produk_id', 'desc');
		return $this->db->get('produk', $length, $start)->result();
	}
	function produk_produksi_count_all(){
		$this->db->join('konversi_bahan','produk.produk_id = konversi_bahan.produk_id','left');
		$this->db->join('bahan','konversi_bahan.bahan_id = bahan.bahan_id','left');
		$this->db->group_by('produk.produk_id');
	   	return $this->db->get('produk')->num_rows();
	}
	function produk_produksi_count_filter($query){
		$this->db->join('konversi_bahan','produk.produk_id = konversi_bahan.produk_id','left');
		$this->db->join('bahan','konversi_bahan.bahan_id = bahan.bahan_id','left');
		$this->db->join('jenis_produk','produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->group_start();
			$this->db->or_like('jenis_produk_nama',$query);
		$this->db->group_end();
		$this->db->group_by('produk.produk_id');
		return $this->db->get('produk')->num_rows();
	}
	function produk_produksi_list($start,$length,$query){
		$this->db->select('produk.produk_id,produk.produk_kode,produk.produk_nama,display_stock_produk.jumlah,group_concat(konversi_bahan.jumlah separator "|") as potong,group_concat(bahan.bahan_nama separator "|") as bahan_nama, group_concat(konversi_bahan.bahan_id separator "|") as bahan_id ');
		$this->db->join('konversi_bahan','produk.produk_id = konversi_bahan.produk_id','left');
		$this->db->join('bahan','konversi_bahan.bahan_id = bahan.bahan_id','left');
		$this->db->join('jenis_produk','produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('display_stock_produk', 'display_stock_produk.produk_id = produk.produk_id','left');
		$this->db->group_start();
			$this->db->or_like('jenis_produk_nama',$query);
		$this->db->group_end();
		$this->db->group_by('produk.produk_id');
		$this->db->group_by('display_stock_produk.jumlah');
		return $this->db->get('produk',$length,$start)->result();
	}
	function get_exist_with_id($id,$kode_barang){
	    $this->db->from('produk');
	    $this->db->where('produk_id !=', $id);
	    $this->db->where('produk_kode', $kode_barang);
	    $query = $this->db->count_all_results();
	    if($query>0) {
	      	$exist = 1;
	    } else {
	      	$exist = 0;
	    }
	    return $exist;
	}
}

/* End of file Produk.php */
/* Location: ./application/models/Produk.php */
