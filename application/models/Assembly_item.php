<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Assembly_item extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->table_name = "assembly_item";
    }

}

/* End of file Color.php */
/* Location: ./application/models/Color.php */