<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pos extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name = "penjualan";
	}
	function save(){
		$this->load->model('stock_produk','',true);
		$this->db->trans_begin();
		$lokasi_id = $this->input->post('lokasi_id');
		$data["lokasi_id"] = $this->input->post('lokasi_id');

		$data["tipe_pembayaran_id"] = $this->input->post('tipe_pembayaran_id');
		if(isset($_SESSION["login"]["lokasi_id"])){
			$data["lokasi_id"] = $_SESSION["login"]["lokasi_id"];
		}
		$transaksi = json_decode($this->input->post('transaksi'));
		$potongan = $transaksi->potongan;
		$tambahan = $transaksi->tambahan;
		$total_item = $transaksi->total_item;
		$grand_total = $transaksi->grand_total;
		$terbayar = $transaksi->terbayar;
		$kembalian = $terbayar - $grand_total;
		$data['urutan'] = $this->input->post('urutan');
		$data['no_faktur'] = $this->input->post('no_faktur');
		$data["nama_pelanggan"] = $this->input->post('guest_nama');
		$data["alamat_pelanggan"] = $this->input->post('guest_alamat');
		$data["telepon_pelanggan"] = $this->input->post('guest_telepon');
		$data["urutan"] = $this->input->post('urutan');
		$data["no_faktur"] = $this->input->post('no_faktur');
		$data["tanggal"] = date("Y-m-d");
		$data["pengiriman"] = $this->input->post('pengiriman');
		$data["total"] = $total_item;
		$data["biaya_tambahan"] = $tambahan;
		$data["potongan_akhir"] = $potongan;
		$data["grand_total"] = $grand_total;
		$data["additional_no"] = $this->input->post('additional_no');
		$data["additional_tanggal"] = $this->input->post('additional_tanggal') != "" ? Date("Y-m-d",strtotime($this->input->post('additional_tanggal'))) : "";
		$data["additional_keterangan"] = $this->input->post('additional_keterangan');
		$data["terbayar"] = $terbayar;
		$data["sisa_pembayaran"] = 0;
		$data["log_kasir_id"] = isset($_SESSION['log_pos']['log_kasir_id']) ? $_SESSION['log_pos']['log_kasir_id']:null;
		$data["kembalian"] = $kembalian;
		$jenis_pembayaran = $this->input->post('jenis_pembayaran');
		if($jenis_pembayaran == "kredit"){
			$data['status_pembayaran'] = "Hutang";
		} else {
			$data['status_pembayaran'] = "Lunas";
		}
		$this->db->insert('penjualan', $data);
		$penjualan_id = $this->db->insert_id();
		foreach ($transaksi->item as $key) {

			$total_stock_belanja = $key->jumlah*$key->nilai_konversi;

            $row_stock = $this->stock_produk->stock_by_location_produk_id($lokasi_id,$key->produk_id);
            $jumlah_bahan = $total_stock_belanja;
            foreach ($row_stock as $row) {
                $data = array();
                $jumlah = $row->stock_produk_qty - $jumlah_bahan;
                $stock_out = $jumlah_bahan;
                if($row->stock_produk_qty < $jumlah_bahan){
                    $jumlah = 0;
                    $stock_out = $row->stock_produk_qty;
                }
                $jumlah_bahan =  ($jumlah_bahan-$row->stock_produk_qty);
                $data["stock_produk_qty"] = $jumlah;
                $stock_produk_id = $row->stock_produk_id;
                $updateStok = $this->stock_produk->update_by_id('stock_produk_id',$stock_produk_id,$data);
                if($updateStok){
                    $data = array();
                    $data["tanggal"] = date("Y-m-d");
                    $data["table_name"] = "stock_produk";
                    $data["stock_produk_id"] = $row->stock_produk_id;
                    $data["produk_id"] = $key->produk_id;
                    $data["stock_out"] = $stock_out;
                    $data["stock_in"] = 0;
                    $data["last_stock"] = $this->stock_produk->last_stock($row->produk_id)->result;
                    $data["last_stock_total"] = $this->stock_produk->stock_total()->result;
                    $data["keterangan"] = "Penjualan Produk";
                    $data["method"] = "update";
                    $this->stock_produk->arus_stock_produk($data);
                }
                if($jumlah_bahan <= 0){
                    break;
                }

            }
            $harga_net = $key->harga_eceran;
            if($key->tipe_penjualan == "grosir"){
                $harga_net = $key->harga_grosir;
            }
            $data = array();
            $data["penjualan_id"] = $penjualan_id;
            $data["produk_id"] = $key->produk_id;
            $data["satuan_nama"] = $key->satuan_nama;
            $data["qty"] = $this->string_to_number($key->jumlah);
            $data["harga"] = $this->string_to_number($harga_net);
            $data["ppn_persen"] = "0.1";
            $data["hpp"] = $this->string_to_number($key->hpp);
            $data["sub_total"] = $this->string_to_number($key->subtotal);
            $this->db->insert('penjualan_produk', $data);
		}
		if($jenis_pembayaran == "kredit"){
			$data = array();
			$data['penjualan_id'] = $penjualan_id;
			$temp = strtotime($this->input->post('tenggat_pelunasan'));
			$data['tenggat_pelunasan'] = date("Y-m-d",$temp);
			$this->db->insert('piutang', $data);
			$piutang_id = $this->db->insert_id();
			if($terbayar > 0){
				
			}
		}
		if ($this->db->trans_status() === FALSE){
			$return["result"] = false;
			return $return;
		}

		$this->db->trans_commit();
		$return["result"] = true;
		$return["id"] = $penjualan_id;
		return $return;
	}
	function custom_save(){
		$this->load->model('stock_produk','',true);
		$this->db->trans_begin();
		$data["lokasi_id"] = $this->input->post('lokasi_id');
		$data["tipe_pembayaran_id"] = $this->input->post('tipe_pembayaran_id');
		if(isset($_SESSION["login"]["lokasi_id"])){
			$data["lokasi_id"] = $_SESSION["login"]["lokasi_id"];
		}
		$transaksi = json_decode($this->input->post('transaksi'));
		$potongan = $transaksi->potongan;
		$tambahan = $transaksi->tambahan;
		$total_item = $transaksi->total_item;
		$grand_total = $transaksi->grand_total;
		$terbayar = $transaksi->terbayar;
		$kembalian = $terbayar - $grand_total;
		$data['urutan'] = $this->input->post('urutan');
		$data['no_faktur'] = $this->input->post('no_faktur');
		$data["nama_pelanggan"] = $this->input->post('guest_nama');
		$data["alamat_pelanggan"] = $this->input->post('guest_alamat');
		$data["telepon_pelanggan"] = $this->input->post('guest_telepon');
		$data["urutan"] = $this->input->post('urutan');
		$data["no_faktur"] = $this->input->post('no_faktur');
		$data["tanggal"] = date("Y-m-d");
		$data["pengiriman"] = $this->input->post('pengiriman');
		$data["total"] = $total_item;
		$data["biaya_tambahan"] = $tambahan;
		$data["potongan_akhir"] = $potongan;
		$data["grand_total"] = $grand_total;
		$data["terbayar"] = $terbayar;
		$data["sisa_pembayaran"] = 0;
		$data["additional_no"] = $this->input->post('additional_no');
		$data["additional_tanggal"] = $this->input->post('additional_tanggal') != "" ? Date("Y-m-d",strtotime($this->input->post('additional_tanggal'))) : "";
		$data["additional_keterangan"] = $this->input->post('additional_keterangan');
		$data["log_kasir_id"] = isset($_SESSION['log_pos']['log_kasir_id']) ? $_SESSION['log_pos']['log_kasir_id']:null;
		$data["kembalian"] = $kembalian;
		$jenis_pembayaran = $this->input->post('jenis_pembayaran');
		if($jenis_pembayaran == "kredit"){
			$data['status_pembayaran'] = "Hutang";
		} else {
			$data['status_pembayaran'] = "Lunas";
		}
		$this->db->insert('penjualan', $data);
		$penjualan_id = $this->db->insert_id();
		if($jenis_pembayaran == "kredit"){
			$data = array();
			$data['penjualan_id'] = $penjualan_id;
			$temp = strtotime($this->input->post('tenggat_pelunasan'));
			$data['tenggat_pelunasan'] = date("Y-m-d",$temp);
			$this->db->insert('piutang', $data);
			$piutang_id = $this->db->insert_id();
			if($terbayar > 0){

			}
		}
		if ($this->db->trans_status() === FALSE){
			$return["result"] = false;
			return $return;
		}

		$this->db->trans_commit();
		$return["result"] = true;
		$return["id"] = $penjualan_id;
		return $return;
	}
	function getUrutan(){
		$date = date("Y-m-d");
		$this->db->select('max(urutan) as urutan');
		$this->db->where('tanggal', $date);
		$urutan = $this->db->get('penjualan')->row()->urutan;
		if($urutan == null){
			$urutan = 1;
		} else {
			$urutan++;
		}
		return $urutan;
	}
	function getFakturCode(){
		$date = date("ymd");
		$urutan = $this->getUrutan();
		$lok = "00";
		if(isset($_SESSION['login']['lokasi_id'])){
			$lok = $_SESSION['login']['lokasi_id'];
		}
		$tipe = "PL";
		$faktur = $tipe.'-'.$date.'/'.$lok.'/'.$urutan;
		return $faktur;
	}
	function transaksi_count_all(){
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = penjualan.tipe_pembayaran_id');	
		return $this->db->get('penjualan')->num_rows();
	}
	function transaksi_count_filter($query){
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = penjualan.tipe_pembayaran_id');
		$this->db->group_start();
		$this->db->like('nama_pelanggan', $query, 'BOTH');
		$this->db->or_like('no_faktur', $query, 'BOTH');
		$this->db->or_like('tanggal', $query, 'BOTH');
		$this->db->or_like('lokasi_nama', $query, 'BOTH');
		$this->db->or_like('tipe_pembayaran.tipe_pembayaran_nama', $query, 'BOTH');
		$this->db->group_end();
		$this->db->join('lokasi', 'penjualan.lokasi_id = lokasi.lokasi_id', 'left');
		if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
			$this->db->where('tanggal >=', $this->input->get('tanggal_start'));
		}
		if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
			$this->db->where('tanggal <=', $this->input->get('tanggal_end'));
		}
		if(isset($_GET['no_faktur'])&&$this->input->get('no_faktur')!=""){
			$this->db->like('no_faktur', $this->input->get('no_faktur'));
		}
		if(isset($_GET['lokasi_id'])&&$this->input->get('lokasi_id')!=""){
			$this->db->where('penjualan.lokasi_id', $this->input->get('lokasi_id'));
		}
		if(isset($_GET['tipe_pembayaran_id'])&&$this->input->get('tipe_pembayaran_id')!=""){
			$this->db->where('penjualan.tipe_pembayaran_id', $this->input->get('tipe_pembayaran_id'));
		}	
		$this->db->order_by('penjualan_id', 'desc');
		//echo $this->db->_compile_select();		
		return $this->db->get('penjualan')->num_rows();
	}
	function transaksi_list($start,$length,$query){
		$this->db->select('penjualan.*,lokasi.lokasi_nama,tipe_pembayaran_nama,(penjualan.grand_total - penjualan.biaya_tambahan - sum(penjualan_produk.qty*penjualan_produk.hpp)) as laba, (sum(penjualan_produk.qty*penjualan_produk.hpp)) as total_hpp');
        $this->db->join('penjualan_produk', 'penjualan_produk.penjualan_id = penjualan.penjualan_id');
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = penjualan.tipe_pembayaran_id');
		$this->db->group_start();
		$this->db->like('nama_pelanggan', $query, 'BOTH');
		$this->db->or_like('no_faktur', $query, 'BOTH');
		$this->db->or_like('tanggal', $query, 'BOTH');
		$this->db->or_like('lokasi_nama', $query, 'BOTH');
		$this->db->or_like('tipe_pembayaran.tipe_pembayaran_nama', $query, 'BOTH');
		$this->db->group_end();
		$this->db->join('lokasi', 'penjualan.lokasi_id = lokasi.lokasi_id', 'left');
		if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
			$this->db->where('tanggal >=', $this->input->get('tanggal_start'));
		}
		if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
			$this->db->where('tanggal <=', $this->input->get('tanggal_end'));
		}
		if(isset($_GET['no_faktur'])&&$this->input->get('no_faktur')!=""){
			$this->db->like('no_faktur', $this->input->get('no_faktur'));
		}
		if(isset($_GET['lokasi_id'])&&$this->input->get('lokasi_id')!=""){
			$this->db->where('penjualan.lokasi_id', $this->input->get('lokasi_id'));
		}
		if(isset($_GET['tipe_pembayaran_id'])&&$this->input->get('tipe_pembayaran_id')!=""){
			$this->db->where('penjualan.tipe_pembayaran_id', $this->input->get('tipe_pembayaran_id'));
		}
        $this->db->group_by('penjualan_id');
		$this->db->order_by('penjualan_id', 'desc');
		return $this->db->get('penjualan',$length,$start)->result();
	}
	function transaksi_by_log($log_kasir_id){
		$this->db->select("if(sum(penjualan.grand_total) is null, 0, sum(penjualan.grand_total)) as total");
		$this->db->join('tipe_pembayaran','tipe_pembayaran on penjualan.tipe_pembayaran_id = tipe_pembayaran.tipe_pembayaran_id');
		$this->db->from('penjualan');
		$this->db->where('tipe_pembayaran.kembalian', '1');
		$this->db->where('penjualan.log_kasir_id', $log_kasir_id);
		return $this->db->get()->row();
	}
	function detailTransaksi($id){
		$this->db->select("penjualan_produk.*,produk.* ");
		$this->db->join('produk', 'penjualan_produk.produk_id = produk.produk_id','left');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id','left');
		$this->db->where('penjualan_produk.penjualan_id', $id);
		return $this->db->get('penjualan_produk')->result();
	}
	function laporan_detail_transaksi_count_all(){
		$this->db->join('penjualan_produk', 'penjualan.penjualan_id = penjualan_produk.penjualan_id');
		$this->db->join('produk', 'penjualan_produk.produk_id = produk.produk_id');	
		$this->db->join('lokasi', 'lokasi.lokasi_id = penjualan.lokasi_id');	
		return $this->db->get('penjualan')->num_rows();
	}
	function laporan_detail_transaksi_count_filter($query){
		$this->db->join('penjualan_produk', 'penjualan.penjualan_id = penjualan_produk.penjualan_id');
		$this->db->join('produk', 'penjualan_produk.produk_id = produk.produk_id');	
		$this->db->join('lokasi', 'lokasi.lokasi_id = penjualan.lokasi_id');			
		$this->db->group_start();
			$this->db->like('nama_pelanggan', $query, 'BOTH');
			$this->db->or_like('no_faktur', $query, 'BOTH');
			$this->db->or_like('tanggal', $query, 'BOTH');
			$this->db->or_like('lokasi_nama', $query, 'BOTH');
			$this->db->or_like('produk.produk_kode', $query, 'BOTH');
			$this->db->or_like('produk.produk_nama', $query, 'BOTH');
			$this->db->or_like('produk.harga_eceran', $query, 'BOTH');
			$this->db->or_like('qty', $query, 'BOTH');
		$this->db->group_end();
		if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
			$this->db->where('tanggal >=', $this->input->get('tanggal_start'));
		}
		if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
			$this->db->where('tanggal <=', $this->input->get('tanggal_end'));
		}
		if(isset($_GET['no_faktur'])&&$this->input->get('no_faktur')!=""){
			$this->db->like('no_faktur', $this->input->get('no_faktur'));
		}
		if(isset($_GET['lokasi_id'])&&$this->input->get('lokasi_id')!=""){
			$this->db->where('penjualan.lokasi_id', $this->input->get('lokasi_id'));
		}
		if(isset($_GET['produk_id'])&&$this->input->get('produk_id')!=""){
			$this->db->where('produk.produk_id', $this->input->get('produk_id'));
		}	
		$this->db->order_by('penjualan.penjualan_id', 'desc');
		return $this->db->get('penjualan')->num_rows();
	}
	function laporan_detail_transaksi_list($start,$length,$query){
		$this->db->select("penjualan.*,lokasi.lokasi_nama,produk.produk_kode,produk.produk_nama,penjualan_produk.qty,penjualan_produk.harga,penjualan_produk.sub_total,(penjualan_produk.sub_total - (penjualan_produk.qty*penjualan_produk.hpp)) as laba, (penjualan_produk.qty*penjualan_produk.hpp) as total_hpp");
		$this->db->join('penjualan_produk', 'penjualan.penjualan_id = penjualan_produk.penjualan_id');
		$this->db->join('produk', 'penjualan_produk.produk_id = produk.produk_id');	
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');	
		$this->db->join('lokasi', 'lokasi.lokasi_id = penjualan.lokasi_id');
		$this->db->group_start();
			$this->db->like('nama_pelanggan', $query, 'BOTH');
			$this->db->or_like('no_faktur', $query, 'BOTH');
			$this->db->or_like('tanggal', $query, 'BOTH');
			$this->db->or_like('lokasi_nama', $query, 'BOTH');
			$this->db->or_like('produk.produk_kode', $query, 'BOTH');
			$this->db->or_like('produk.produk_nama', $query, 'BOTH');
			$this->db->or_like('produk.harga_eceran', $query, 'BOTH');
			$this->db->or_like('qty', $query, 'BOTH');
		$this->db->group_end();
		if(isset($_GET['tanggal_start'])&&$this->input->get('tanggal_start')!=""){
			$this->db->where('tanggal >=', $this->input->get('tanggal_start'));
		}
		if(isset($_GET['tanggal_end'])&&$this->input->get('tanggal_end')!=""){
			$this->db->where('tanggal <=', $this->input->get('tanggal_end'));
		}
		if(isset($_GET['no_faktur'])&&$this->input->get('no_faktur')!=""){
			$this->db->like('no_faktur', $this->input->get('no_faktur'));
		}
		if(isset($_GET['lokasi_id'])&&$this->input->get('lokasi_id')!=""){
			$this->db->where('penjualan.lokasi_id', $this->input->get('lokasi_id'));
		}
		if(isset($_GET['produk_id'])&&$this->input->get('produk_id')!=""){
			$this->db->where('produk.produk_id', $this->input->get('produk_id'));
		}	
		$this->db->order_by('penjualan.penjualan_id', 'desc');
		return $this->db->get('penjualan',$length,$start)->result();

	}
    function mostPenjualanDashboardList($search=null){
        $this->db->select('produk.produk_kode, sum(penjualan_produk.qty) as qty, lokasi.lokasi_nama,produk.produk_nama');
        $this->db->join('produk', 'penjualan_produk.produk_id = produk.produk_id');
        $this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
        $this->db->join('penjualan', 'penjualan.penjualan_id = penjualan_produk.penjualan_id');
        $this->db->join('lokasi', 'penjualan.lokasi_id = lokasi.lokasi_id');
        if($search!=null){
            $this->db->where('penjualan.tanggal >=',$search["min_tanggal"]);
            $this->db->where('penjualan.tanggal <=',$search["max_tanggal"]);
            if($search["lokasi_id"]!="All"){
                $this->db->where('penjualan.lokasi_id',$search["lokasi_id"]);
            }
        }
        $this->db->group_by('penjualan_produk.produk_id');
        $this->db->group_by('penjualan.lokasi_id');
        $this->db->order_by('qty', 'desc');
        return $this->db->get('penjualan_produk', 5, 0)->result();
    }
    function mostPenjualanDashboardCount($search=null){
        $this->db->select('produk.produk_kode, sum(penjualan_produk.qty) as qty, lokasi.lokasi_nama');
        $this->db->join('produk', 'penjualan_produk.produk_id = produk.produk_id');
        $this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
        $this->db->join('penjualan', 'penjualan.penjualan_id = penjualan_produk.penjualan_id');
        $this->db->join('lokasi', 'penjualan.lokasi_id = lokasi.lokasi_id');
        if($search!=null){
            $this->db->where('penjualan.tanggal >=',$search["min_tanggal"]);
            $this->db->where('penjualan.tanggal <=',$search["max_tanggal"]);
            if($search["lokasi_id"]!="All"){
                $this->db->where('penjualan.lokasi_id',$search["lokasi_id"]);
            }
        }
        $this->db->group_by('penjualan_produk.produk_id');
        $this->db->group_by('penjualan.lokasi_id');
        $this->db->order_by('qty', 'desc');
        return $this->db->get('penjualan_produk')->num_rows();
    }
    function leastPenjualanDashboardList($search=null){
        $this->db->select('produk.produk_kode, sum(penjualan_produk.qty) as qty, lokasi.lokasi_nama,produk.produk_nama');
        $this->db->join('produk', 'penjualan_produk.produk_id = produk.produk_id');
        $this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
        $this->db->join('penjualan', 'penjualan.penjualan_id = penjualan_produk.penjualan_id');
        $this->db->join('lokasi', 'penjualan.lokasi_id = lokasi.lokasi_id');
        if($search!=null){
            $this->db->where('penjualan.tanggal >=',$search["min_tanggal"]);
            $this->db->where('penjualan.tanggal <=',$search["max_tanggal"]);
            if($search["lokasi_id"]!="All"){
                $this->db->where('penjualan.lokasi_id',$search["lokasi_id"]);
            }
        }
        $this->db->group_by('penjualan_produk.produk_id');
        $this->db->group_by('penjualan.lokasi_id');
        $this->db->order_by('qty', 'asc');
        return $this->db->get('penjualan_produk', 5, 0)->result();
    }
    function leastPenjualanDashboardCount($search=null){
        $this->db->select('produk.produk_kode, sum(penjualan_produk.qty) as qty, lokasi.lokasi_nama');
        $this->db->join('produk', 'penjualan_produk.produk_id = produk.produk_id');
        $this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
        $this->db->join('penjualan', 'penjualan.penjualan_id = penjualan_produk.penjualan_id');
        $this->db->join('lokasi', 'penjualan.lokasi_id = lokasi.lokasi_id');
        if($search!=null){
            $this->db->where('penjualan.tanggal >=',$search["min_tanggal"]);
            $this->db->where('penjualan.tanggal <=',$search["max_tanggal"]);
            if($search["lokasi_id"]!="All"){
                $this->db->where('penjualan.lokasi_id',$search["lokasi_id"]);
            }
        }
        $this->db->group_by('penjualan_produk.produk_id');
        $this->db->group_by('penjualan.lokasi_id');
        $this->db->order_by('qty', 'asc');
        return $this->db->get('penjualan_produk')->num_rows();
    }
    function minTanggalPenjualan(){
        $this->db->select('min(tanggal) as tanggal');
        return $this->db->get('penjualan')->row()->tanggal;
    }
	function totalPenjualan(){
		$this->db->select('sum(penjualan.grand_total) as sales');
		$this->db->from('penjualan');
		return $this->db->get()->row()->sales;
	}
	function penjualanByMonth(){
		$this->db->select('sum(penjualan.grand_total) as sales, concat_ws(" ",MONTH(penjualan.tanggal),YEAR(penjualan.tanggal)) as "label"');
		$this->db->from('penjualan');
		$this->db->group_by('MONTH(penjualan.tanggal)');
		$this->db->group_by('YEAR(penjualan.tanggal)');
		return $this->db->get('')->result();
	}
	function penjualan_by_id($id){
		$this->db->from('penjualan');
		$this->db->where("penjualan_id",$id);
		return $this->db->get()->row();
	}
}

/* End of file Pos.php */
/* Location: ./application/models/Pos.php */
