<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staff extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name = 'staff';
	}
	function staff_list($start,$length,$query){
		$this->db->select('staff.*,staff_lama_kerja.lama_bekerja');
		$this->db->join('staff_lama_kerja', 'staff.staff_id = staff_lama_kerja.staff_id');
		$this->db->like('staff.staff_id', $query, 'BOTH'); 
		$this->db->or_like('staff_nama', $query, 'BOTH'); 
		$this->db->or_like('staff_alamat', $query, 'BOTH'); 
		$this->db->or_like('staff_email', $query, 'BOTH'); 
		$this->db->or_like('staff_status', $query, 'BOTH'); 
		$this->db->or_like('staff_phone_number', $query, 'BOTH');
		$this->db->or_like('staff_lama_kerja.lama_bekerja', $query, 'BOTH');
		$this->db->order_by('staff_id', 'desc');
		return $this->db->get('staff', $length, $start)->result();
	}
	function staff_count_filter($query){
		$this->db->select('staff.*,staff_lama_kerja.lama_bekerja');
		$this->db->join('staff_lama_kerja', 'staff.staff_id = staff_lama_kerja.staff_id');
		$this->db->like('staff.staff_id', $query, 'BOTH'); 
		$this->db->or_like('staff_nama', $query, 'BOTH'); 
		$this->db->or_like('staff_alamat', $query, 'BOTH'); 
		$this->db->or_like('staff_email', $query, 'BOTH'); 
		$this->db->or_like('staff_status', $query, 'BOTH'); 
		$this->db->or_like('staff_phone_number', $query, 'BOTH');
		$this->db->or_like('staff_lama_kerja.lama_bekerja', $query, 'BOTH');
		return $this->db->get('staff')->num_rows();
	}
	function staff_count_all(){
		$this->db->select('staff.*,staff_lama_kerja.lama_bekerja');
		$this->db->join('staff_lama_kerja', 'staff.staff_id = staff_lama_kerja.staff_id');
		return $this->db->get('staff')->num_rows();
	}	
	function staff_by_id($staff_id){
		$this->db->where('staff_id', $staff_id);
		return $this->db->get('staff')->row();
	}
	function is_ready_email($staff_id,$email){
		$this->db->where('staff_email', $email);
		$data = $this->db->get('staff')->row();
		if($data != null){
			if($data->staff_id == $staff_id){
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}
}

/* End of file Staff.php */
/* Location: ./application/models/Staff.php */