<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hutang extends My_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name = "hutang";
	}
    function hutang_count_all(){
        $this->db->join('po_bahan', 'hutang.po_bahan_id = po_bahan.po_bahan_id','left');
        $this->db->join('po_produk', 'hutang.po_produk_id = po_produk.po_produk_id','left');
        $this->db->join('suplier', 'po_bahan.suplier_id = suplier.suplier_id','left');
        $this->db->join('suplier b', 'po_produk.suplier_id = b.suplier_id','left');
        $this->db->join('pembayaran_hutang', 'pembayaran_hutang.hutang_id = hutang.hutang_id', 'left');
        $this->db->where('hutang.status_hutang','Buka' );
        $this->db->group_by('hutang.hutang_id');
        return $this->db->get('hutang')->num_rows();
    }
    function hutang_count_filter($query){
        $this->db->join('po_bahan', 'hutang.po_bahan_id = po_bahan.po_bahan_id','left');
        $this->db->join('po_produk', 'hutang.po_produk_id = po_produk.po_produk_id','left');
        $this->db->join('suplier', 'po_bahan.suplier_id = suplier.suplier_id','left');
        $this->db->join('suplier b', 'po_produk.suplier_id = b.suplier_id','left');
        $this->db->join('pembayaran_hutang', 'pembayaran_hutang.hutang_id = hutang.hutang_id', 'left');
        $this->db->group_start();
        $this->db->like('b.suplier_nama', $query, 'BOTH');
        $this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
        $this->db->or_like('po_bahan_no', $query, 'BOTH');
        $temp = strtotime($query);
        $date = date("Y-m-d",$temp);
        $this->db->or_like('tenggat_pelunasan', $date, 'BOTH');
        $this->db->or_like('tenggat_pelunasan', $query, 'BOTH');
        $this->db->or_like('hutang.status_pembayaran', $query, 'BOTH');
        $this->db->group_end();
        if($this->input->get('po_bahan_no')!=""){
            $this->db->like('po_bahan_no', $this->input->get('po_bahan_no'), 'BOTH');
        }
        if($this->input->get('tenggat_mulai')!=""){
            $this->db->where('tenggat_pelunasan >=', $this->input->get('tenggat_mulai'));
        }
        if($this->input->get('tenggat_end')!=""){
            $this->db->where('tenggat_pelunasan <=', $this->input->get('tenggat_end'));
        }
        if($this->input->get('status_pembayaran')!=""){
            $this->db->where('status_pembayaran', $this->input->get('status_pembayaran'));
        }
        if($this->input->get('suplier_id')!=""){
            $this->db->where('suplier.suplier_id', $this->input->get('suplier_id'));
        }
        $this->db->where('hutang.status_hutang','Buka' );
        $this->db->group_by('hutang.hutang_id');
        return $this->db->get('hutang')->num_rows();
    }
    function hutang_list($start,$length,$query){
        $this->db->select('hutang.*,hutang.status_pembayaran as status_pembayaran_hutang,if(suplier.suplier_nama is null,b.suplier_nama,suplier.suplier_nama) as suplier_nama,'.
            'if(po_bahan_no is null,po_produk_no,po_bahan_no) as po_bahan_no,'.
            'if(po_bahan.grand_total is null,po_produk.grand_total,po_bahan.grand_total) as grand_total,'.
            'if(sum(pembayaran_hutang.jumlah)is null,0,sum(pembayaran_hutang.jumlah)) as terbayar, '.
            'if(po_bahan.po_bahan_id is null,(po_produk.grand_total-(if(sum(pembayaran_hutang.jumlah)is null,0,sum(pembayaran_hutang.jumlah)))),(po_bahan.grand_total-(if(sum(pembayaran_hutang.jumlah)is null,0,sum(pembayaran_hutang.jumlah))))) as sisa,'.
            ' if(po_bahan.status_pembayaran is null,po_produk.status_pembayaran,po_bahan.status_pembayaran) as status_pembayaran');
        $this->db->join('po_bahan', 'hutang.po_bahan_id = po_bahan.po_bahan_id','left');
        $this->db->join('po_produk', 'hutang.po_produk_id = po_produk.po_produk_id','left');
        $this->db->join('suplier', 'po_bahan.suplier_id = suplier.suplier_id','left');
        $this->db->join('suplier b', 'po_produk.suplier_id = b.suplier_id','left');
        $this->db->join('pembayaran_hutang', 'pembayaran_hutang.hutang_id = hutang.hutang_id', 'left');
        $this->db->group_start();
        $this->db->like('b.suplier_nama', $query, 'BOTH');
        $this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
        $this->db->or_like('po_bahan_no', $query, 'BOTH');
        $temp = strtotime($query);
        $date = date("Y-m-d",$temp);
        $this->db->or_like('tenggat_pelunasan', $date, 'BOTH');
        $this->db->or_like('tenggat_pelunasan', $query, 'BOTH');
        $this->db->or_like('hutang.status_pembayaran', $query, 'BOTH');
        $this->db->group_end();
        if($this->input->get('po_bahan_no')!=""){
            $this->db->like('po_bahan_no', $this->input->get('po_bahan_no'), 'BOTH');
        }
        if($this->input->get('tenggat_mulai')!=""){
            $this->db->where('tenggat_pelunasan >=', $this->input->get('tenggat_mulai'));
        }
        if($this->input->get('tenggat_end')!=""){
            $this->db->where('tenggat_pelunasan <=', $this->input->get('tenggat_end'));
        }
        if($this->input->get('status_pembayaran')!=""){
            $this->db->where('status_pembayaran', $this->input->get('status_pembayaran'));
        }
        if($this->input->get('suplier_id')!=""){
            $this->db->where('suplier.suplier_id', $this->input->get('suplier_id'));
        }
        $this->db->where('hutang.status_hutang','Buka' );
        $this->db->group_by('hutang.hutang_id');
        $this->db->order_by('hutang.hutang_id', 'desc');
        return $this->db->get('hutang',$length,$start)->result();
    }
    function hutang_jatuh_count_all(){
        $this->db->join('po_bahan', 'hutang.po_bahan_id = po_bahan.po_bahan_id','left');
        $this->db->join('po_produk', 'hutang.po_produk_id = po_produk.po_produk_id','left');
        $this->db->join('suplier', 'po_bahan.suplier_id = suplier.suplier_id','left');
        $this->db->join('suplier b', 'po_produk.suplier_id = b.suplier_id','left');
        $this->db->join('pembayaran_hutang', 'pembayaran_hutang.hutang_id = hutang.hutang_id', 'left');
        $this->db->where('hutang.status_hutang','Buka' );
        $this->db->where('hutang.status_pembayaran','Belum Lunas' );
        $date_par = Date("Y-m-d");
        $this->db->where('hutang.tenggat_pelunasan <',$date_par );
        $this->db->group_by('hutang.hutang_id');
        return $this->db->get('hutang')->num_rows();
    }
    function hutang_jatuh_count_filter($query){
        $this->db->join('po_bahan', 'hutang.po_bahan_id = po_bahan.po_bahan_id','left');
        $this->db->join('po_produk', 'hutang.po_produk_id = po_produk.po_produk_id','left');
        $this->db->join('suplier', 'po_bahan.suplier_id = suplier.suplier_id','left');
        $this->db->join('suplier b', 'po_produk.suplier_id = b.suplier_id','left');
        $this->db->join('pembayaran_hutang', 'pembayaran_hutang.hutang_id = hutang.hutang_id', 'left');
        $this->db->group_start();
        $this->db->like('b.suplier_nama', $query, 'BOTH');
        $this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
        $this->db->or_like('po_bahan_no', $query, 'BOTH');
        $temp = strtotime($query);
        $date = date("Y-m-d",$temp);
        $this->db->or_like('tenggat_pelunasan', $date, 'BOTH');
        $this->db->or_like('tenggat_pelunasan', $query, 'BOTH');
        $this->db->or_like('hutang.status_pembayaran', $query, 'BOTH');
        $this->db->group_end();
        if($this->input->get('po_bahan_no')!=""){
            $this->db->like('po_bahan_no', $this->input->get('po_bahan_no'), 'BOTH');
        }
        if($this->input->get('tenggat_mulai')!=""){
            $this->db->where('tenggat_pelunasan >=', $this->input->get('tenggat_mulai'));
        }
        if($this->input->get('tenggat_end')!=""){
            $this->db->where('tenggat_pelunasan <=', $this->input->get('tenggat_end'));
        }
        if($this->input->get('status_pembayaran')!=""){
            $this->db->where('status_pembayaran', $this->input->get('status_pembayaran'));
        }
        if($this->input->get('suplier_id')!=""){
            $this->db->where('suplier.suplier_id', $this->input->get('suplier_id'));
        }
        $this->db->where('hutang.status_hutang','Buka' );
        $this->db->where('hutang.status_pembayaran','Belum Lunas' );
        $date_par = Date("Y-m-d");
        $this->db->where('hutang.tenggat_pelunasan <',$date_par );
        $this->db->group_by('hutang.hutang_id');
        return $this->db->get('hutang')->num_rows();
    }
    function hutang_jatuh_list($start,$length,$query){
        $this->db->select('hutang.*,hutang.status_pembayaran as status_pembayaran_hutang,if(suplier.suplier_nama is null,b.suplier_nama,suplier.suplier_nama) as suplier_nama,'.
            'if(po_bahan_no is null,po_produk_no,po_bahan_no) as po_bahan_no,'.
            'if(po_bahan.grand_total is null,po_produk.grand_total,po_bahan.grand_total) as grand_total,'.
            'if(sum(pembayaran_hutang.jumlah)is null,0,sum(pembayaran_hutang.jumlah)) as terbayar, '.
            'if(po_bahan.po_bahan_id is null,(po_produk.grand_total-(if(sum(pembayaran_hutang.jumlah)is null,0,sum(pembayaran_hutang.jumlah)))),(po_bahan.grand_total-(if(sum(pembayaran_hutang.jumlah)is null,0,sum(pembayaran_hutang.jumlah))))) as sisa,'.
            ' if(po_bahan.status_pembayaran is null,po_produk.status_pembayaran,po_bahan.status_pembayaran) as status_pembayaran');
        $this->db->join('po_bahan', 'hutang.po_bahan_id = po_bahan.po_bahan_id','left');
        $this->db->join('po_produk', 'hutang.po_produk_id = po_produk.po_produk_id','left');
        $this->db->join('suplier', 'po_bahan.suplier_id = suplier.suplier_id','left');
        $this->db->join('suplier b', 'po_produk.suplier_id = b.suplier_id','left');
        $this->db->join('pembayaran_hutang', 'pembayaran_hutang.hutang_id = hutang.hutang_id', 'left');
        $this->db->group_start();
        $this->db->like('b.suplier_nama', $query, 'BOTH');
        $this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
        $this->db->or_like('po_bahan_no', $query, 'BOTH');
        $temp = strtotime($query);
        $date = date("Y-m-d",$temp);
        $this->db->or_like('tenggat_pelunasan', $date, 'BOTH');
        $this->db->or_like('tenggat_pelunasan', $query, 'BOTH');
        $this->db->or_like('hutang.status_pembayaran', $query, 'BOTH');
        $this->db->group_end();
        if($this->input->get('po_bahan_no')!=""){
            $this->db->like('po_bahan_no', $this->input->get('po_bahan_no'), 'BOTH');
        }
        if($this->input->get('tenggat_mulai')!=""){
            $this->db->where('tenggat_pelunasan >=', $this->input->get('tenggat_mulai'));
        }
        if($this->input->get('tenggat_end')!=""){
            $this->db->where('tenggat_pelunasan <=', $this->input->get('tenggat_end'));
        }
        if($this->input->get('status_pembayaran')!=""){
            $this->db->where('status_pembayaran', $this->input->get('status_pembayaran'));
        }
        if($this->input->get('suplier_id')!=""){
            $this->db->where('suplier.suplier_id', $this->input->get('suplier_id'));
        }
        $this->db->where('hutang.status_hutang','Buka' );
        $this->db->where('hutang.status_pembayaran','Belum Lunas' );
        $date_par = Date("Y-m-d");
        $this->db->where('hutang.tenggat_pelunasan <',$date_par );
        $this->db->group_by('hutang.hutang_id');
        $this->db->order_by('hutang.hutang_id', 'desc');
        return $this->db->get('hutang',$length,$start)->result();
    }
    function hutang_akan_count_all(){
        $date = new DateTime(date("Y-m-d"));
        $date->add(new DateInterval('P3D'));
        $date_end = $date->format('Y-m-d');

        $this->db->join('po_bahan', 'hutang.po_bahan_id = po_bahan.po_bahan_id','left');
        $this->db->join('po_produk', 'hutang.po_produk_id = po_produk.po_produk_id','left');
        $this->db->join('suplier', 'po_bahan.suplier_id = suplier.suplier_id','left');
        $this->db->join('suplier b', 'po_produk.suplier_id = b.suplier_id','left');
        $this->db->join('pembayaran_hutang', 'pembayaran_hutang.hutang_id = hutang.hutang_id', 'left');
        $this->db->where('hutang.status_hutang','Buka' );
        $this->db->where('hutang.status_pembayaran','Belum Lunas' );
        $date_par = Date("Y-m-d");
        $this->db->where('hutang.tenggat_pelunasan >=',$date_par );
        $this->db->where('hutang.tenggat_pelunasan <=',$date_end );
        $this->db->group_by('hutang.hutang_id');
        return $this->db->get('hutang')->num_rows();
    }
    function hutang_akan_count_filter($query){
        $date = new DateTime(date("Y-m-d"));
        $date->add(new DateInterval('P3D'));
        $date_end = $date->format('Y-m-d');

        $this->db->join('po_bahan', 'hutang.po_bahan_id = po_bahan.po_bahan_id','left');
        $this->db->join('po_produk', 'hutang.po_produk_id = po_produk.po_produk_id','left');
        $this->db->join('suplier', 'po_bahan.suplier_id = suplier.suplier_id','left');
        $this->db->join('suplier b', 'po_produk.suplier_id = b.suplier_id','left');
        $this->db->join('pembayaran_hutang', 'pembayaran_hutang.hutang_id = hutang.hutang_id', 'left');
        $this->db->group_start();
        $this->db->like('b.suplier_nama', $query, 'BOTH');
        $this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
        $this->db->or_like('po_bahan_no', $query, 'BOTH');
        $temp = strtotime($query);
        $date = date("Y-m-d",$temp);
        $this->db->or_like('tenggat_pelunasan', $date, 'BOTH');
        $this->db->or_like('tenggat_pelunasan', $query, 'BOTH');
        $this->db->or_like('hutang.status_pembayaran', $query, 'BOTH');
        $this->db->group_end();
        if($this->input->get('po_bahan_no')!=""){
            $this->db->like('po_bahan_no', $this->input->get('po_bahan_no'), 'BOTH');
        }
        if($this->input->get('tenggat_mulai')!=""){
            $this->db->where('tenggat_pelunasan >=', $this->input->get('tenggat_mulai'));
        }
        if($this->input->get('tenggat_end')!=""){
            $this->db->where('tenggat_pelunasan <=', $this->input->get('tenggat_end'));
        }
        if($this->input->get('status_pembayaran')!=""){
            $this->db->where('status_pembayaran', $this->input->get('status_pembayaran'));
        }
        if($this->input->get('suplier_id')!=""){
            $this->db->where('suplier.suplier_id', $this->input->get('suplier_id'));
        }
        $this->db->where('hutang.status_hutang','Buka' );
        $this->db->where('hutang.status_pembayaran','Belum Lunas' );
        $date_par = Date("Y-m-d");
        $this->db->where('hutang.tenggat_pelunasan >=',$date_par );
        $this->db->where('hutang.tenggat_pelunasan <=',$date_end );
        $this->db->group_by('hutang.hutang_id');
        return $this->db->get('hutang')->num_rows();
    }
    function hutang_akan_list($start,$length,$query){
        $date = new DateTime(date("Y-m-d"));
        $date->add(new DateInterval('P3D'));
        $date_end = $date->format('Y-m-d');

        $this->db->select('hutang.*,hutang.status_pembayaran as status_pembayaran_hutang,if(suplier.suplier_nama is null,b.suplier_nama,suplier.suplier_nama) as suplier_nama,'.
            'if(po_bahan_no is null,po_produk_no,po_bahan_no) as po_bahan_no,'.
            'if(po_bahan.grand_total is null,po_produk.grand_total,po_bahan.grand_total) as grand_total,'.
            'if(sum(pembayaran_hutang.jumlah)is null,0,sum(pembayaran_hutang.jumlah)) as terbayar, '.
            'if(po_bahan.po_bahan_id is null,(po_produk.grand_total-(if(sum(pembayaran_hutang.jumlah)is null,0,sum(pembayaran_hutang.jumlah)))),(po_bahan.grand_total-(if(sum(pembayaran_hutang.jumlah)is null,0,sum(pembayaran_hutang.jumlah))))) as sisa,'.
            ' if(po_bahan.status_pembayaran is null,po_produk.status_pembayaran,po_bahan.status_pembayaran) as status_pembayaran');
        $this->db->join('po_bahan', 'hutang.po_bahan_id = po_bahan.po_bahan_id','left');
        $this->db->join('po_produk', 'hutang.po_produk_id = po_produk.po_produk_id','left');
        $this->db->join('suplier', 'po_bahan.suplier_id = suplier.suplier_id','left');
        $this->db->join('suplier b', 'po_produk.suplier_id = b.suplier_id','left');
        $this->db->join('pembayaran_hutang', 'pembayaran_hutang.hutang_id = hutang.hutang_id', 'left');
        $this->db->group_start();
        $this->db->like('b.suplier_nama', $query, 'BOTH');
        $this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
        $this->db->or_like('po_bahan_no', $query, 'BOTH');
        $temp = strtotime($query);
        $date = date("Y-m-d",$temp);
        $this->db->or_like('tenggat_pelunasan', $date, 'BOTH');
        $this->db->or_like('tenggat_pelunasan', $query, 'BOTH');
        $this->db->or_like('hutang.status_pembayaran', $query, 'BOTH');
        $this->db->group_end();
        if($this->input->get('po_bahan_no')!=""){
            $this->db->like('po_bahan_no', $this->input->get('po_bahan_no'), 'BOTH');
        }
        if($this->input->get('tenggat_mulai')!=""){
            $this->db->where('tenggat_pelunasan >=', $this->input->get('tenggat_mulai'));
        }
        if($this->input->get('tenggat_end')!=""){
            $this->db->where('tenggat_pelunasan <=', $this->input->get('tenggat_end'));
        }
        if($this->input->get('status_pembayaran')!=""){
            $this->db->where('status_pembayaran', $this->input->get('status_pembayaran'));
        }
        if($this->input->get('suplier_id')!=""){
            $this->db->where('suplier.suplier_id', $this->input->get('suplier_id'));
        }
        $this->db->where('hutang.status_hutang','Buka' );
        $this->db->where('hutang.status_pembayaran','Belum Lunas' );
        $date_par = Date("Y-m-d");
        $this->db->where('hutang.tenggat_pelunasan >=',$date_par );
        $this->db->where('hutang.tenggat_pelunasan <=',$date_end );
        $this->db->group_by('hutang.hutang_id');
        $this->db->order_by('hutang.hutang_id', 'desc');
        return $this->db->get('hutang',$length,$start)->result();
    }
    function history_hutang_count_all(){
        $this->db->join('po_bahan', 'hutang.po_bahan_id = po_bahan.po_bahan_id','left');
        $this->db->join('po_produk', 'hutang.po_produk_id = po_produk.po_produk_id','left');
        $this->db->join('suplier', 'po_bahan.suplier_id = suplier.suplier_id','left');
        $this->db->join('suplier b', 'po_produk.suplier_id = b.suplier_id','left');
        $this->db->join('pembayaran_hutang', 'pembayaran_hutang.hutang_id = hutang.hutang_id', 'left');
        $this->db->where('hutang.status_hutang','Tutup' );
        $this->db->group_by('hutang.hutang_id');
        return $this->db->get('hutang')->num_rows();
    }
    function history_hutang_count_filter($query){
        $this->db->join('po_bahan', 'hutang.po_bahan_id = po_bahan.po_bahan_id','left');
        $this->db->join('po_produk', 'hutang.po_produk_id = po_produk.po_produk_id','left');
        $this->db->join('suplier', 'po_bahan.suplier_id = suplier.suplier_id','left');
        $this->db->join('suplier b', 'po_produk.suplier_id = b.suplier_id','left');
        $this->db->join('pembayaran_hutang', 'pembayaran_hutang.hutang_id = hutang.hutang_id', 'left');
        $this->db->group_start();
        $this->db->like('b.suplier_nama', $query, 'BOTH');
        $this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
        $this->db->or_like('po_bahan_no', $query, 'BOTH');
        $temp = strtotime($query);
        $date = date("Y-m-d",$temp);
        $this->db->or_like('tenggat_pelunasan', $date, 'BOTH');
        $this->db->or_like('tenggat_pelunasan', $query, 'BOTH');
        $this->db->or_like('hutang.status_pembayaran', $query, 'BOTH');
        $this->db->group_end();
        if($this->input->get('po_bahan_no')!=""){
            $this->db->like('po_bahan_no', $this->input->get('po_bahan_no'), 'BOTH');
        }
        if($this->input->get('tenggat_mulai')!=""){
            $this->db->where('tenggat_pelunasan >=', $this->input->get('tenggat_mulai'));
        }
        if($this->input->get('tenggat_end')!=""){
            $this->db->where('tenggat_pelunasan <=', $this->input->get('tenggat_end'));
        }
        if($this->input->get('status_pembayaran')!=""){
            $this->db->where('status_pembayaran', $this->input->get('status_pembayaran'));
        }
        if($this->input->get('suplier_id')!=""){
            $this->db->where('suplier.suplier_id', $this->input->get('suplier_id'));
        }
        $this->db->where('hutang.status_hutang','Tutup' );
        $this->db->group_by('hutang.hutang_id');
        return $this->db->get('hutang')->num_rows();
    }
    function history_hutang_list($start,$length,$query){
        $this->db->select('hutang.*,hutang.status_pembayaran as status_pembayaran_hutang,if(suplier.suplier_nama is null,b.suplier_nama,suplier.suplier_nama) as suplier_nama,'.
            'if(po_bahan_no is null,po_produk_no,po_bahan_no) as po_bahan_no,'.
            'if(po_bahan.grand_total is null,po_produk.grand_total,po_bahan.grand_total) as grand_total,'.
            'if(sum(pembayaran_hutang.jumlah)is null,0,sum(pembayaran_hutang.jumlah)) as terbayar, '.
            'if(po_bahan.po_bahan_id is null,(po_produk.grand_total-(if(sum(pembayaran_hutang.jumlah)is null,0,sum(pembayaran_hutang.jumlah)))),(po_bahan.grand_total-(if(sum(pembayaran_hutang.jumlah)is null,0,sum(pembayaran_hutang.jumlah))))) as sisa,'.
            ' if(po_bahan.status_pembayaran is null,po_produk.status_pembayaran,po_bahan.status_pembayaran) as status_pembayaran');
        $this->db->join('po_bahan', 'hutang.po_bahan_id = po_bahan.po_bahan_id','left');
        $this->db->join('po_produk', 'hutang.po_produk_id = po_produk.po_produk_id','left');
        $this->db->join('suplier', 'po_bahan.suplier_id = suplier.suplier_id','left');
        $this->db->join('suplier b', 'po_produk.suplier_id = b.suplier_id','left');
        $this->db->join('pembayaran_hutang', 'pembayaran_hutang.hutang_id = hutang.hutang_id', 'left');
        $this->db->group_start();
        $this->db->like('b.suplier_nama', $query, 'BOTH');
        $this->db->or_like('suplier.suplier_nama', $query, 'BOTH');
        $this->db->or_like('po_bahan_no', $query, 'BOTH');
        $temp = strtotime($query);
        $date = date("Y-m-d",$temp);
        $this->db->or_like('tenggat_pelunasan', $date, 'BOTH');
        $this->db->or_like('tenggat_pelunasan', $query, 'BOTH');
        $this->db->or_like('hutang.status_pembayaran', $query, 'BOTH');
        $this->db->group_end();
        if($this->input->get('po_bahan_no')!=""){
            $this->db->like('po_bahan_no', $this->input->get('po_bahan_no'), 'BOTH');
        }
        if($this->input->get('tenggat_mulai')!=""){
            $this->db->where('tenggat_pelunasan >=', $this->input->get('tenggat_mulai'));
        }
        if($this->input->get('tenggat_end')!=""){
            $this->db->where('tenggat_pelunasan <=', $this->input->get('tenggat_end'));
        }
        if($this->input->get('status_pembayaran')!=""){
            $this->db->where('status_pembayaran', $this->input->get('status_pembayaran'));
        }
        if($this->input->get('suplier_id')!=""){
            $this->db->where('suplier.suplier_id', $this->input->get('suplier_id'));
        }
        $this->db->where('hutang.status_hutang','Tutup' );
        $this->db->group_by('hutang.hutang_id');
        $this->db->order_by('hutang.hutang_id', 'desc');
        return $this->db->get('hutang',$length,$start)->result();
    }
	function hutang_by_id($hutang_id){
		$this->db->select('hutang.*,suplier.suplier_nama,po_produk_no,po_produk.grand_total,if(sum(pembayaran_hutang.jumlah)is null,0,sum(pembayaran_hutang.jumlah)) as terbayar, (po_produk.grand_total-(if(sum(pembayaran_hutang.jumlah)is null,0,sum(pembayaran_hutang.jumlah)))) as sisa');
		$this->db->join('po_produk', 'hutang.po_produk_id = po_produk.po_produk_id');
		$this->db->join('suplier', 'po_produk.suplier_id = suplier.suplier_id');
		$this->db->join('pembayaran_hutang', 'pembayaran_hutang.hutang_id = hutang.hutang_id', 'left');	
		$this->db->group_by('hutang.hutang_id');
		$this->db->where('hutang.hutang_id', $hutang_id);
		return $this->db->get('hutang')->row();
	}
	function pembayaran_hutang_count($hutang_id){
		$this->db->select('pembayaran_hutang.*,tipe_pembayaran.*');
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = pembayaran_hutang.tipe_pembayaran_id');
		$this->db->where('pembayaran_hutang.hutang_id', $hutang_id);
		return $this->db->get('pembayaran_hutang')->num_rows();
	}
	function pembayaran_hutang_filter($hutang_id,$query){
		$this->db->select('pembayaran_hutang.*,tipe_pembayaran.*');
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = pembayaran_hutang.tipe_pembayaran_id');
		$this->db->where('pembayaran_hutang.hutang_id', $hutang_id);
		return $this->db->get('pembayaran_hutang')->num_rows();
	}
	function pembayaran_hutang_list($start,$length,$query,$hutang_id){
		$this->db->select('pembayaran_hutang.*,pembayaran_hutang.jumlah as "old_jumlah",tipe_pembayaran.*');
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = pembayaran_hutang.tipe_pembayaran_id');
		$this->db->order_by('pembayaran_hutang.pembayaran_hutang_id', 'desc');
		$this->db->where('hutang_id', $hutang_id);
		return $this->db->get('pembayaran_hutang',$length,$start)->result();		
	}
	function insert_pembayaran($data){
		return $this->db->insert('pembayaran_hutang', $data);
	}
	function edit_pembayaran($data,$pembayaran_hutang_id)	{
		$this->db->where('pembayaran_hutang_id', $pembayaran_hutang_id);
		return $this->db->update('pembayaran_hutang', $data);
	}
	function delete_pembayaran($pembayaran_hutang_id){
		$this->db->where('pembayaran_hutang_id', $pembayaran_hutang_id);
		return $this->db->delete('pembayaran_hutang');
	}
	function detail_pembayaran($pembayaran_hutang_id){
		$this->db->where('pembayaran_hutang_id', $pembayaran_hutang_id);
		$this->db->from('pembayaran_hutang');
		return $this->db->get()->row();
	}
	function laporan_pembayaran_hutang_all(){
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = pembayaran_hutang.tipe_pembayaran_id');
		$this->db->join('hutang', 'pembayaran_hutang.hutang_id = hutang.hutang_id');
		$this->db->join('po_produk', 'hutang.po_produk_id = po_produk.po_produk_id');
		return $this->db->get('pembayaran_hutang')->num_rows();
	}
	function laporan_pembayaran_hutang_filter($query){
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = pembayaran_hutang.tipe_pembayaran_id');
		$this->db->join('hutang', 'pembayaran_hutang.hutang_id = hutang.hutang_id');
		$this->db->join('po_produk', 'hutang.po_produk_id = po_produk.po_produk_id');
		$this->db->group_start();
			$this->db->like('po_produk.po_produk_no', $query, 'BOTH');
			$this->db->like('tipe_pembayaran.tipe_pembayaran_nama', $query, 'BOTH');
		$this->db->group_end();
		if($this->input->get('po_bahan_no')!=""){
			$this->db->like('po_produk_no', $this->input->get('po_bahan_no'), 'BOTH');
		}
		if($this->input->get('tipe_pembayaran_id')!=""){
			$this->db->where('pembayaran_hutang.tipe_pembayaran_id', $this->input->get('tipe_pembayaran_id'));
		}
		if($this->input->get('tanggal_start')!=""){
			$this->db->where('tanggal >=', $this->input->get('tanggal_start'));
		}
		if($this->input->get('tanggal_end')!=""){
			$this->db->where('tanggal <=', $this->input->get('tanggal_end'));
		}
		$this->db->order_by('pembayaran_hutang.pembayaran_hutang_id', 'desc');
		return $this->db->get('pembayaran_hutang')->num_rows();
	}
	function laporan_pembayaran_hutang_list($start,$length,$query){
		$this->db->select('pembayaran_hutang.*,po_produk.po_produk_no,tipe_pembayaran.tipe_pembayaran_nama');
		$this->db->join('tipe_pembayaran', 'tipe_pembayaran.tipe_pembayaran_id = pembayaran_hutang.tipe_pembayaran_id');
		$this->db->join('hutang', 'pembayaran_hutang.hutang_id = hutang.hutang_id');
		$this->db->join('po_produk', 'hutang.po_produk_id = po_produk.po_produk_id');
		$this->db->group_start();
			$this->db->like('po_produk.po_produk_no', $query, 'BOTH');
			$this->db->like('tipe_pembayaran.tipe_pembayaran_nama', $query, 'BOTH');
		$this->db->group_end();
        if($this->input->get('po_bahan_no')!=""){
            $this->db->like('po_produk_no', $this->input->get('po_bahan_no'), 'BOTH');
        }
		if($this->input->get('tipe_pembayaran_id')!=""){
			$this->db->where('pembayaran_hutang.tipe_pembayaran_id', $this->input->get('tipe_pembayaran_id'));
		}
		if($this->input->get('tanggal_start')!=""){
			$this->db->where('tanggal >=', $this->input->get('tanggal_start'));
		}
		if($this->input->get('tanggal_end')!=""){
			$this->db->where('tanggal <=', $this->input->get('tanggal_end'));
		}
		return $this->db->get('pembayaran_hutang',$length,$start)->result();		
	}
	function totalHutang(){
		$sql = 'SELECT (sum(po_produk.grand_total) - sum(pembayaran_hutang.jumlah)) as hutang FROM hutang INNER JOIN po_produk on hutang.po_produk_id = po_produk.po_produk_id LEFT JOIN pembayaran_hutang on hutang.hutang_id = pembayaran_hutang.hutang_id' ;
		$res = $this->db->query($sql);
		return $res->row()->hutang;
	}	
}

/* End of file Hutang.php */
/* Location: ./application/models/Hutang.php */