<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stock_bahan extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name = "stock_bahan";
	}	
	function stock_bahan_list($start,$length,$query,$bahan_id){
		$this->db->select('stock_bahan.*,lokasi_nama');
		$this->db->join('lokasi', 'lokasi.lokasi_id = stock_bahan.stock_bahan_lokasi_id');
		$this->db->group_start();
		$this->db->like('lokasi.lokasi_nama', $query, 'BOTH'); 
		$this->db->or_like('stock_bahan.stock_bahan_seri', $query, 'BOTH'); 
		$this->db->or_like('stock_bahan.stock_bahan_qty', $query, 'BOTH'); 
		$this->db->group_end();
		$this->db->where('stock_bahan.bahan_id', $bahan_id); 
		$this->db->where('delete_flag', "0");
		if(isset($_SESSION["login"]["lokasi_id"])){
			$this->db->where('stock_bahan.stock_bahan_lokasi_id', $_SESSION["login"]["lokasi_id"]);
		}		
		$this->db->order_by('stock_bahan_id', 'desc');

		return $this->db->get('stock_bahan', $length, $start)->result();
	}
	function stock_bahan_count($bahan_id){
		$this->db->join('lokasi', 'lokasi.lokasi_id = stock_bahan.stock_bahan_lokasi_id');
		$this->db->where('stock_bahan.bahan_id', $bahan_id); 
		if(isset($_SESSION["login"]["lokasi_id"])){
			$this->db->where('stock_bahan.stock_bahan_lokasi_id', $_SESSION["login"]["lokasi_id"]);
		}	
		$this->db->where('delete_flag', "0");	
		return $this->db->get('stock_bahan')->num_rows();
	}
	function stock_bahan_count_filter($bahan_id,$query){
		$this->db->join('lokasi', 'lokasi.lokasi_id = stock_bahan.stock_bahan_lokasi_id');
		$this->db->group_start();
		$this->db->like('lokasi.lokasi_nama', $query, 'BOTH'); 
		$this->db->or_like('stock_bahan.stock_bahan_seri', $query, 'BOTH'); 
		$this->db->or_like('stock_bahan.stock_bahan_qty', $query, 'BOTH'); 
		$this->db->group_end();		
		if(isset($_SESSION["login"]["lokasi_id"])){
			$this->db->where('stock_bahan.stock_bahan_lokasi_id', $_SESSION["login"]["lokasi_id"]);
		}		
		$this->db->where('stock_bahan.bahan_id', $bahan_id); 
		$this->db->where('delete_flag', "0");
		return $this->db->get('stock_bahan')->num_rows();
	}
	function is_bahan_seri_ready($seri,$bahan_id,$stock_bahan_lokasi_id,$stock_bahan_id = null){
		$this->db->from('stock_bahan');
		$this->db->where('stock_bahan_lokasi_id', $stock_bahan_lokasi_id);
		$this->db->where('stock_bahan_seri', $seri);
		$this->db->where('bahan_id', $bahan_id);
		$sql = $this->db->get();
		if($sql->num_rows() > 0){
			$data = $sql->row();
			if ($stock_bahan_id != null && $stock_bahan_id == $data->stock_bahan_id){
				return true;
			} else {
				false;
			}
		} else {
			return true;
		}
	}
	function first_date(){
		$this->db->order_by('tanggal');
		$get = $this->db->get('arus_stock_bahan');
		$date = date("Y-m-d");
		if($get->num_rows()>0){
			$date = $get->row()->tanggal;
		}
		return $date;
	}
	function is_bahan_seri_ready_arr($seri,$bahan_id,$stock_bahan_lokasi_id,$stock_bahan_id = null){
		$this->db->from('stock_bahan');
		$this->db->where('stock_bahan_lokasi_id', $stock_bahan_lokasi_id);
		$this->db->where('stock_bahan_seri', $seri);
		$this->db->where('bahan_id', $bahan_id);
		$sql = $this->db->get();
		if($sql->num_rows() > 0){
			$data = $sql->row();
			if ($stock_bahan_id != null && $stock_bahan_id == $data->stock_bahan_id){
				$result["status"] = true;
				return $result;
			} else {
				$result["status"] = false;
				$result["data"] = $data;
				return $result;
			}
		} else {
			$result["status"] = true;
			return $result;
		}
	}
	function arus_stock_bahan($data){
		return $this->db->insert('arus_stock_bahan', $data);
	}
	function arus_stock_bahan_list($start,$length,$query){
		$this->db->select('arus_stock_bahan.*,bahan.bahan_nama,satuan.satuan_nama,lokasi.lokasi_nama');
		$this->db->join('bahan', 'bahan.bahan_id = arus_stock_bahan.bahan_id');
		$this->db->join('satuan', 'bahan.bahan_satuan_id = satuan.satuan_id');
		$this->db->join('stock_bahan', 'stock_bahan.stock_bahan_id = arus_stock_bahan.stock_bahan_id');
		$this->db->join('lokasi', 'lokasi.lokasi_id = stock_bahan.stock_bahan_lokasi_id');
		if($this->input->get("start_date")!= ""){
			$this->db->where('tanggal >=', $this->input->get('start_date'));
			$this->db->where('tanggal <=', $this->input->get('end_date'));
		}
		if($this->input->get("bahan_id")!= "" && $this->input->get("bahan_id")!= "all"){
			$this->db->where('bahan.bahan_id', $this->input->get('bahan_id'));
		}
		$this->db->group_start();
		$this->db->like('bahan.bahan_nama', $query, 'BOTH');
		$this->db->or_like('satuan.satuan_nama', $query, 'BOTH');
		$this->db->or_like('lokasi.lokasi_nama', $query, 'BOTH');
		$this->db->or_like('arus_stock_bahan.keterangan', $query, 'BOTH');
		$this->db->group_end();
		$this->db->order_by('arus_stock_bahan.tanggal', 'ASC');
		$this->db->order_by('arus_stock_bahan.arus_stock_bahan_id', 'ASC');
		return $this->db->get('arus_stock_bahan',$length, $start)->result();
	}
	function arus_stock_bahan_count(){
		$this->db->join('bahan', 'bahan.bahan_id = arus_stock_bahan.bahan_id');
		$this->db->join('satuan', 'bahan.bahan_satuan_id = satuan.satuan_id');
		$this->db->join('stock_bahan', 'stock_bahan.stock_bahan_id = arus_stock_bahan.stock_bahan_id');
		$this->db->join('lokasi', 'lokasi.lokasi_id = stock_bahan.stock_bahan_lokasi_id');
		return $this->db->get('arus_stock_bahan')->num_rows();
	}
	function arus_stock_bahan_count_filter($query){
		$this->db->join('bahan', 'bahan.bahan_id = arus_stock_bahan.bahan_id');
		$this->db->join('satuan', 'bahan.bahan_satuan_id = satuan.satuan_id');
		$this->db->join('stock_bahan', 'stock_bahan.stock_bahan_id = arus_stock_bahan.stock_bahan_id');
		$this->db->join('lokasi', 'lokasi.lokasi_id = stock_bahan.stock_bahan_lokasi_id');	
		if($this->input->get("start_date")!= ""){
			$this->db->where('tanggal >=', $this->input->get('start_date'));
			$this->db->where('tanggal <=', $this->input->get('end_date'));
		}
		if($this->input->get("bahan_id")!= "" && $this->input->get("bahan_id")!= "all"){
			$this->db->where('bahan.bahan_id', $this->input->get('bahan_id'));
		}
		$this->db->group_start();
		$this->db->like('bahan.bahan_nama', $query, 'BOTH');
		$this->db->or_like('satuan.satuan_nama', $query, 'BOTH');
		$this->db->or_like('lokasi.lokasi_nama', $query, 'BOTH');
		$this->db->or_like('arus_stock_bahan.keterangan', $query, 'BOTH');
		$this->db->group_end();			
		return $this->db->get('arus_stock_bahan')->num_rows();
	}
	function last_stock($bahan_id){
		$this->db->select('sum(stock_bahan.stock_bahan_qty) as result');
		$this->db->from('stock_bahan');
		$this->db->where('bahan_id', $bahan_id);
		$this->db->where('delete_flag', "0");
		return $this->db->get()->row();
	}
	function stock_total(){
		$this->db->select('sum(stock_bahan.stock_bahan_qty) as result');
		$this->db->from('stock_bahan');
		return $this->db->get()->row();
	}
	function stock_by_location_bahan_id($lokasi_id,$bahan_id){
		$this->db->where('stock_bahan_lokasi_id', $lokasi_id);
		$this->db->where('bahan_id', $bahan_id);
		$this->db->where('delete_flag', "0");
		$this->db->where('stock_bahan_qty >', "0");
		return $this->db->get('stock_bahan')->result();
	}
	function transfer_stock(){
		$this->db->trans_begin();
		$qty = $this->string_to_number($this->input->post('stock_bahan_qty')) - $this->string_to_number($this->input->post('qty'));
		$data["stock_bahan_qty"] = $qty;
		$this->db->where('stock_bahan_id', $this->input->post('stock_bahan_id'));
		$update = $this->db->update('stock_bahan', $data);
		$last_stock = $this->last_stock($this->input->post('bahan_id'))->result;
		if($update){
			$data = array();
			$data["tanggal"] = date("Y-m-d");
			$data["table_name"] = "stock_bahan";
			$data["stock_bahan_id"] = $this->input->post('stock_bahan_id');
			$data["bahan_id"] = $this->input->post('bahan_id');
			$data["stock_out"] = $this->string_to_number($this->input->post('qty'));
			$data["stock_in"] = 0;
			$data["last_stock"] =$last_stock;
			$data["last_stock_total"] = $this->stock_total()->result;
			$data["keterangan"] = "Transfer stok bahan";
			$data["method"] = "update";
			$this->db->insert('arus_stock_bahan', $data);
			$data = array();
			$data["tanggal"] = date("Y-m-d");
			$data["bahan_id"] = $this->input->post('bahan_id');
			$data["stock_bahan_id"] = $this->input->post('stock_bahan_id');
			$data["bahan_kode"] = $this->input->post('bahan_kode');
			$data["bahan_nama"] = $this->input->post('bahan_nama');
			$data["history_transfer_qty"] = $this->string_to_number($this->input->post('qty'));
			$data["bahan_seri"] = $this->input->post('stock_bahan_seri');
			$data["histori_lokasi_awal_id"] = $this->input->post('stock_bahan_lokasi_id');
			$data["histori_lokasi_tujuan_id"] = $this->input->post('histori_lokasi_tujuan_id');
			$data["dari"] = $this->input->post('lokasi_nama');
			$data["tujuan"] = $this->input->post('lokasi_tujuan_nama');
			$data["last_stock"] = $last_stock;
			$data["status"] = "Menunggu Konfirmasi";
			$this->db->insert('history_transfer_bahan', $data);
		}
		$stock_bahan_id = $this->input->post('stock_bahan_id');
		if ($this->db->trans_status() === FALSE)
			return FALSE;

		$this->db->trans_commit();
		return TRUE;
	}
	function stock_by_location_list($start,$length,$query,$lokasi_id){
		$this->db->select('bahan.*,satuan.satuan_nama,jenis_bahan.jenis_bahan_nama,stock_bahan.stock_bahan_qty,stock_bahan.stock_bahan_seri,lokasi_nama,stock_bahan.stock_bahan_id,lokasi.lokasi_id,if(sum(stock_bahan.stock_bahan_qty) is null, 0, sum(stock_bahan.stock_bahan_qty)) as "stock"');
		$this->db->join('jenis_bahan', 'bahan.bahan_jenis_id = jenis_bahan.jenis_bahan_id');
		$this->db->join('stock_bahan', 'bahan.bahan_id = stock_bahan.bahan_id');
		$this->db->join('lokasi', 'lokasi.lokasi_id = stock_bahan.stock_bahan_lokasi_id');
		$this->db->join('satuan', 'satuan.satuan_id = bahan.bahan_satuan_id');
		$this->db->group_start();
		$this->db->like('lokasi.lokasi_nama', $query, 'BOTH'); 
		$this->db->or_like('stock_bahan.stock_bahan_seri', $query, 'BOTH'); 
		$this->db->or_like('stock_bahan.stock_bahan_qty', $query, 'BOTH'); 
		$this->db->or_like('bahan.bahan_nama', $query, 'BOTH'); 
		$this->db->or_like('jenis_bahan.jenis_bahan_nama', $query, 'BOTH'); 
		$this->db->group_end();
		if ($lokasi_id!=null){
			$this->db->where('stock_bahan.stock_bahan_lokasi_id', $lokasi_id);
		}
		if(isset($_GET['bahan_id'])){
			$this->db->where('bahan.bahan_id', $this->input->get('bahan_id'));
		}
		if(isset($_GET['jenis_bahan_id'])){
			$this->db->where('jenis_bahan.jenis_bahan_id', $this->input->get('jenis_bahan_id'));
		}
		if(isset($_GET['satuan_id'])){
			$this->db->where('satuan.satuan_id', $this->input->get('satuan_id'));
		}
		$this->db->where('delete_flag', "0");
		$this->db->group_by('bahan.bahan_id');
		$this->db->group_by('lokasi.lokasi_id');
		$this->db->order_by('bahan_id', 'desc');
		return $this->db->get('bahan', $length, $start)->result();
	}
	function stock_by_location_count($lokasi_id){
		$this->db->join('jenis_bahan', 'bahan.bahan_jenis_id = jenis_bahan.jenis_bahan_id');
		$this->db->join('stock_bahan', 'bahan.bahan_id = stock_bahan.bahan_id');
		$this->db->join('lokasi', 'lokasi.lokasi_id = stock_bahan.stock_bahan_lokasi_id');
		$this->db->join('satuan', 'satuan.satuan_id = bahan.bahan_satuan_id');
		if ($lokasi_id!=null){
			$this->db->where('stock_bahan.stock_bahan_lokasi_id', $lokasi_id);
		}
		$this->db->where('delete_flag', "0");
		$this->db->group_by('bahan.bahan_id');
		$this->db->group_by('lokasi.lokasi_id');
		return $this->db->get('bahan')->num_rows();
	}
	function stock_by_location_filter($query,$lokasi_id){
		$this->db->join('jenis_bahan', 'bahan.bahan_jenis_id = jenis_bahan.jenis_bahan_id');
		$this->db->join('stock_bahan', 'bahan.bahan_id = stock_bahan.bahan_id');
		$this->db->join('lokasi', 'lokasi.lokasi_id = stock_bahan.stock_bahan_lokasi_id');
		$this->db->join('satuan', 'satuan.satuan_id = bahan.bahan_satuan_id');
		$this->db->group_start();
		$this->db->like('lokasi.lokasi_nama', $query, 'BOTH'); 
		$this->db->or_like('stock_bahan.stock_bahan_seri', $query, 'BOTH'); 
		$this->db->or_like('stock_bahan.stock_bahan_qty', $query, 'BOTH'); 
		$this->db->or_like('bahan.bahan_nama', $query, 'BOTH'); 
		$this->db->or_like('jenis_bahan.jenis_bahan_nama', $query, 'BOTH'); 
		$this->db->group_end();		
		if ($lokasi_id!=null){
			$this->db->where('stock_bahan.stock_bahan_lokasi_id', $lokasi_id);
		}
		if(isset($_GET['bahan_id'])){
			$this->db->where('bahan.bahan_id', $this->input->get('bahan_id'));
		}
		if(isset($_GET['jenis_bahan_id'])){
			$this->db->where('jenis_bahan.jenis_bahan_id', $this->input->get('jenis_bahan_id'));
		}
		if(isset($_GET['satuan_id'])){
			$this->db->where('satuan.satuan_id', $this->input->get('satuan_id'));
		}
		$this->db->where('delete_flag', "0");
		$this->db->group_by('bahan.bahan_id');
		$this->db->group_by('lokasi.lokasi_id');
		return $this->db->get('bahan')->num_rows();
	}
	function bahan_by_lokasi($lokasi_id,$bahan_id){
		$this->db->where('delete_flag', "0");
		$this->db->where('stock_bahan_lokasi_id', $lokasi_id);
		$this->db->where('bahan_id', $bahan_id);
		return $this->db->get('stock_bahan')->result();
	}
}

/* End of file Stock_stock_bahan.php */
/* Location: ./application/models/Stock_stock_bahan.php */