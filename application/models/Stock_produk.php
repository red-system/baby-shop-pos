<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stock_produk extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name = "stock_produk";
	}	
	function stock_produk_list($start,$length,$query,$produk_id){
		$this->db->select('stock_produk.*,lokasi_nama');
		$this->db->join('lokasi', 'lokasi.lokasi_id = stock_produk.stock_produk_lokasi_id');
		$this->db->group_start();
			$this->db->like('lokasi.lokasi_nama', $query, 'BOTH'); 
			$this->db->or_like('stock_produk.stock_produk_seri', $query, 'BOTH'); 
			$this->db->or_like('stock_produk.stock_produk_qty', $query, 'BOTH'); 
		$this->db->group_end();
		$this->db->where('stock_produk.produk_id', $produk_id);
		$this->db->where('delete_flag', "0"); 
		$this->db->where('stock_produk_qty >', "0"); 
		if(isset($_SESSION["login"]["lokasi_id"])){
			$this->db->where('lokasi_id', $_SESSION["login"]["lokasi_id"]);
		}
		$this->db->order_by('stock_produk_id', 'desc');
		return $this->db->get('stock_produk', $length, $start)->result();
	}
	function stock_produk_count($produk_id){
		$this->db->join('lokasi', 'lokasi.lokasi_id = stock_produk.stock_produk_lokasi_id');
		$this->db->where('stock_produk.produk_id', $produk_id); 
		$this->db->where('delete_flag', "0"); 
		$this->db->where('stock_produk_qty >', "0"); 
		if(isset($_SESSION["login"]["lokasi_id"])){
			$this->db->where('lokasi_id', $_SESSION["login"]["lokasi_id"]);
		}
		return $this->db->get('stock_produk')->num_rows();
	}
	function stock_produk_count_filter($query,$produk_id){
		$this->db->join('lokasi', 'lokasi.lokasi_id = stock_produk.stock_produk_lokasi_id');
		$this->db->group_start();
			$this->db->like('lokasi.lokasi_nama', $query, 'BOTH'); 
			$this->db->or_like('stock_produk.stock_produk_seri', $query, 'BOTH'); 
			$this->db->or_like('stock_produk.stock_produk_qty', $query, 'BOTH'); 
		$this->db->group_end();		
		$this->db->where('stock_produk.produk_id', $produk_id); 
		$this->db->where('delete_flag', "0"); 
		$this->db->where('stock_produk_qty >', "0");
		if(isset($_SESSION["login"]["lokasi_id"])){
			$this->db->where('lokasi_id', $_SESSION["login"]["lokasi_id"]);
		} 
		return $this->db->get('stock_produk')->num_rows();
	}	
	function is_produk_seri_ready($seri,$produk_id,$stock_produk_lokasi_id,$stock_produk_id = null){
		$this->db->from('stock_produk');
		$this->db->where('stock_produk_lokasi_id', $stock_produk_lokasi_id);
		$this->db->where('stock_produk_seri', $seri);
		$this->db->where('produk_id', $produk_id);
		$sql = $this->db->get();
		if($sql->num_rows() > 0){
			$data = $sql->row();
			if ($stock_produk_id != null && $stock_produk_id == $data->stock_produk_id){
				return true;
			} else {
				false;
			}
		} else {
			return true;
		}
	}
	function stock_by_location_list($start,$length,$query,$lokasi_id){
		$this->db->select('produk.*,satuan.satuan_nama,jenis_produk.jenis_produk_nama,stock_produk.stock_produk_qty,stock_produk.stock_produk_seri,stock_produk.hpp,lokasi_nama,stock_produk.stock_produk_id,lokasi.lokasi_id,if(sum(stock_produk.stock_produk_qty) is null,0,sum(stock_produk.stock_produk_qty))as"stock"');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('stock_produk', 'produk.produk_id = stock_produk.produk_id');
		$this->db->join('lokasi', 'lokasi.lokasi_id = stock_produk.stock_produk_lokasi_id');
		$this->db->join('satuan', 'satuan.satuan_id = produk.produk_satuan_id');
		$this->db->group_start();
			$this->db->like('lokasi.lokasi_nama', $query, 'BOTH'); 
			$this->db->or_like('stock_produk.stock_produk_seri', $query, 'BOTH'); 
			$this->db->or_like('stock_produk.stock_produk_qty', $query, 'BOTH');
			$this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH');
			$this->db->or_like('produk.produk_kode', $query, 'BOTH'); 
			$this->db->or_like('produk.produk_nama', $query, 'BOTH');
			$this->db->or_like('satuan.satuan_nama', $query, 'BOTH');
		$this->db->group_end();
		if ($lokasi_id!=null){
			$this->db->where('stock_produk.stock_produk_lokasi_id', $lokasi_id);
		}
		if(isset($_GET['produk_id'])){
			$this->db->where('produk.produk_id', $this->input->get('produk_id'));
		}
		if(isset($_GET['jenis_produk_id'])){
			$this->db->where('jenis_produk.jenis_produk_id', $this->input->get('jenis_produk_id'));
		}
		if(isset($_GET['satuan_id'])){
			$this->db->where('satuan.satuan_id', $this->input->get('satuan_id'));
		}
		$this->db->where('stock_produk.delete_flag', "0");
		$this->db->group_by('produk.produk_id');
		$this->db->group_by('lokasi.lokasi_id');
		$this->db->order_by('produk_id', 'desc');
		//echo $this->db->_compile_select();
		return $this->db->get('produk', $length, $start)->result();
	}
	function stock_by_location_count($lokasi_id){
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('stock_produk', 'produk.produk_id = stock_produk.produk_id');
		$this->db->join('lokasi', 'lokasi.lokasi_id = stock_produk.stock_produk_lokasi_id');
		$this->db->join('satuan', 'satuan.satuan_id = produk.produk_satuan_id');
		if ($lokasi_id!=null){
			$this->db->where('stock_produk.stock_produk_lokasi_id', $lokasi_id);
		}
		$this->db->where('stock_produk.delete_flag', "0");
		$this->db->group_by('produk.produk_id');
		$this->db->group_by('lokasi.lokasi_id');
		return $this->db->get('produk')->num_rows();
	}
	function stock_by_location_filter($query,$lokasi_id){
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('stock_produk', 'produk.produk_id = stock_produk.produk_id');
		$this->db->join('lokasi', 'lokasi.lokasi_id = stock_produk.stock_produk_lokasi_id');
		$this->db->join('satuan', 'satuan.satuan_id = produk.produk_satuan_id');
		$this->db->group_start();
			$this->db->like('lokasi.lokasi_nama', $query, 'BOTH'); 
			$this->db->or_like('stock_produk.stock_produk_seri', $query, 'BOTH'); 
			$this->db->or_like('stock_produk.stock_produk_qty', $query, 'BOTH');
			$this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH');
			$this->db->or_like('produk.produk_kode', $query, 'BOTH'); 
			$this->db->or_like('produk.produk_nama', $query, 'BOTH');
			$this->db->or_like('satuan.satuan_nama', $query, 'BOTH');
		$this->db->group_end();		
		if ($lokasi_id!=null){
			$this->db->where('stock_produk.stock_produk_lokasi_id', $lokasi_id);
		}
		if(isset($_GET['produk_id'])){
			$this->db->where('produk.produk_id', $this->input->get('produk_id'));
		}
		if(isset($_GET['jenis_produk_id'])){
			$this->db->where('jenis_produk.jenis_produk_id', $this->input->get('jenis_produk_id'));
		}
		if(isset($_GET['satuan_id'])){
			$this->db->where('satuan.satuan_id', $this->input->get('satuan_id'));
		}
		$this->db->where('stock_produk.delete_flag', "0");
		$this->db->group_by('produk.produk_id');
		$this->db->group_by('lokasi.lokasi_id');
		return $this->db->get('produk')->num_rows();
	}
	function stock_by_location_filter_print($query,$lokasi_id,$jenis_produk_id,$satuan_id,$produk_id){
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('stock_produk', 'produk.produk_id = stock_produk.produk_id');
		$this->db->join('lokasi', 'lokasi.lokasi_id = stock_produk.stock_produk_lokasi_id');
		$this->db->join('satuan', 'satuan.satuan_id = produk.produk_satuan_id');
		$this->db->group_start();
			$this->db->like('lokasi.lokasi_nama', $query, 'BOTH'); 
			$this->db->or_like('stock_produk.stock_produk_seri', $query, 'BOTH'); 
			$this->db->or_like('stock_produk.stock_produk_qty', $query, 'BOTH');
			$this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH');
			$this->db->or_like('produk.produk_kode', $query, 'BOTH'); 
			$this->db->or_like('produk.produk_nama', $query, 'BOTH');
			$this->db->or_like('satuan.satuan_nama', $query, 'BOTH');
		$this->db->group_end();		
		if ($lokasi_id!=null){
			$this->db->where('stock_produk.stock_produk_lokasi_id', $lokasi_id);
		}
		if($produk_id!=null){
			$this->db->where('produk.produk_id', $this->input->get('produk_id'));
		}
		if($jenis_produk_id!=null){
			$this->db->where('jenis_produk.jenis_produk_id', $this->input->get('jenis_produk_id'));
		}
		if($satuan_id!=null){
			$this->db->where('satuan.satuan_id', $this->input->get('satuan_id'));
		}
		$this->db->where('stock_produk.delete_flag', "0");
		$this->db->group_by('produk.produk_id');
		$this->db->group_by('lokasi.lokasi_id');
		return $this->db->get('produk')->num_rows();
	}
	function stock_by_location_list_print($start,$length,$query,$lokasi_id,$jenis_produk_id,$satuan_id,$produk_id){
		$this->db->select('produk.*,satuan.satuan_nama,jenis_produk.jenis_produk_nama,stock_produk.stock_produk_qty,stock_produk.stock_produk_seri,stock_produk.hpp,lokasi_nama,stock_produk.stock_produk_id,lokasi.lokasi_id,if(sum(stock_produk.stock_produk_qty) is null,0,sum(stock_produk.stock_produk_qty))as"stock"');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('stock_produk', 'produk.produk_id = stock_produk.produk_id');
		$this->db->join('lokasi', 'lokasi.lokasi_id = stock_produk.stock_produk_lokasi_id');
		$this->db->join('satuan', 'satuan.satuan_id = produk.produk_satuan_id');
		$this->db->group_start();
			$this->db->like('lokasi.lokasi_nama', $query, 'BOTH'); 
			$this->db->or_like('stock_produk.stock_produk_seri', $query, 'BOTH'); 
			$this->db->or_like('stock_produk.stock_produk_qty', $query, 'BOTH');
			$this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH');
			$this->db->or_like('produk.produk_kode', $query, 'BOTH'); 
			$this->db->or_like('produk.produk_nama', $query, 'BOTH');
			$this->db->or_like('satuan.satuan_nama', $query, 'BOTH');
		$this->db->group_end();
		if ($lokasi_id!=null){
			$this->db->where('stock_produk.stock_produk_lokasi_id', $lokasi_id);
		}
		if($produk_id!=null){
			$this->db->where('produk.produk_id', $this->input->get('produk_id'));
		}
		if($jenis_produk_id!=null){
			$this->db->where('jenis_produk.jenis_produk_id', $this->input->get('jenis_produk_id'));
		}
		if($satuan_id!=null){
			$this->db->where('satuan.satuan_id', $this->input->get('satuan_id'));
		}
		$this->db->where('stock_produk.delete_flag', "0");
		$this->db->group_by('produk.produk_id');
		$this->db->group_by('lokasi.lokasi_id');
		$this->db->order_by('produk_id', 'desc');
		//echo $this->db->_compile_select();
		return $this->db->get('produk', $length, $start)->result();
	}
	function pos_produk_list($start,$length,$query,$lokasi_id){
	    $this->db->select('produk.*,satuan_id,satuan_kode,satuan_nama,jenis_produk.jenis_produk_nama,lokasi_nama,lokasi_id,display_stock_produk_lokasi.jumlah as stock_produk_qty, barcode as stock_produk_seri, hpp_global as hpp, produk.produk_id as stock_produk_id');
        $this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
        $this->db->join('display_stock_produk_lokasi', 'produk.produk_id = display_stock_produk_lokasi.produk_id');
        $this->db->join('lokasi', 'lokasi.lokasi_id = display_stock_produk_lokasi.stock_produk_lokasi_id');
        $this->db->join('satuan', 'satuan.satuan_id = produk.produk_satuan_id');
        $this->db->group_start();
        $this->db->like('lokasi.lokasi_nama', $query, 'BOTH');
        $this->db->or_like('produk_kode', $query, 'BOTH');
        $this->db->or_like('display_stock_produk_lokasi.jumlah', $query, 'BOTH');
        $this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH');
        $this->db->group_end();
        if ($lokasi_id!=null){
            $this->db->where('display_stock_produk_lokasi.stock_produk_lokasi_id', $lokasi_id);
        }
        if($this->input->get("stock_produk_seri")!=""){
            $this->db->where('barcode', $this->input->get("stock_produk_seri"));
        }
        $this->db->where('display_stock_produk_lokasi.jumlah >', "0");
        $this->db->order_by('produk.produk_id', 'desc');
		return $this->db->get('produk', $length, $start)->result();
	}
	function pos_produk_count($lokasi_id){
        $this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
        $this->db->join('display_stock_produk_lokasi', 'produk.produk_id = display_stock_produk_lokasi.produk_id');
        $this->db->join('lokasi', 'lokasi.lokasi_id = display_stock_produk_lokasi.stock_produk_lokasi_id');
        $this->db->join('satuan', 'satuan.satuan_id = produk.produk_satuan_id');
        if ($lokasi_id!=null){
            $this->db->where('display_stock_produk_lokasi.stock_produk_lokasi_id', $lokasi_id);
        }
        if($this->input->get("stock_produk_seri")!=""){
            $this->db->where('barcode', $this->input->get("stock_produk_seri"));
        }
        $this->db->where('display_stock_produk_lokasi.jumlah >', "0");
		return $this->db->get('produk')->num_rows();
	}
	function pos_produk_filter($query,$lokasi_id){
        $this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
        $this->db->join('display_stock_produk_lokasi', 'produk.produk_id = display_stock_produk_lokasi.produk_id');
        $this->db->join('lokasi', 'lokasi.lokasi_id = display_stock_produk_lokasi.stock_produk_lokasi_id');
        $this->db->join('satuan', 'satuan.satuan_id = produk.produk_satuan_id');
        $this->db->group_start();
        $this->db->like('lokasi.lokasi_nama', $query, 'BOTH');
        $this->db->or_like('produk_kode', $query, 'BOTH');
        $this->db->or_like('display_stock_produk_lokasi.jumlah', $query, 'BOTH');
        $this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH');
        $this->db->group_end();
        if ($lokasi_id!=null){
            $this->db->where('display_stock_produk_lokasi.stock_produk_lokasi_id', $lokasi_id);
        }
        if($this->input->get("stock_produk_seri")!=""){
            $this->db->where('barcode', $this->input->get("stock_produk_seri"));
        }
        $this->db->where('display_stock_produk_lokasi.jumlah >', "0");
		return $this->db->get('produk')->num_rows();
	}			
	function is_produk_seri_ready_arr($seri,$produk_id,$stock_produk_lokasi_id,$stock_produk_id = null){
		$this->db->from('stock_produk');
		$this->db->where('stock_produk_lokasi_id', $stock_produk_lokasi_id);
		$this->db->where('stock_produk_seri', $seri);
		$this->db->where('produk_id', $produk_id);
		$sql = $this->db->get();
		if($sql->num_rows() > 0){
			$data = $sql->row();
			if ($stock_produk_id != null && $stock_produk_id == $data->stock_produk_id){
				$result["status"] = true;
				return $result;
			} else {
				$result["status"] = false;
				$result["data"] = $data;
				return $result;
			}
		} else {
			$result["status"] = true;
			return $result;
		}
	}
	function arus_stock_produk($data){
		return $this->db->insert('arus_stock_produk', $data);
	}
	function first_date(){
		$this->db->order_by('tanggal');
		$get = $this->db->get('arus_stock_produk');
		$date = date("Y-m-d");
		if($get->num_rows()>0){
			$date = $get->row()->tanggal;
		}
		return $date;
	}
	function arus_stock_produk_list($start,$length,$query){
		$this->db->select('arus_stock_produk.*,lokasi.lokasi_nama,stock_produk.stock_produk_seri,produk.produk_nama');
		$this->db->join('produk', 'produk.produk_id = arus_stock_produk.produk_id');
		$this->db->join('jenis_produk', 'jenis_produk.jenis_produk_id = produk.produk_jenis_id');
		$this->db->join('stock_produk', 'stock_produk.stock_produk_id = arus_stock_produk.stock_produk_id');
		$this->db->join('lokasi', 'lokasi.lokasi_id = stock_produk.stock_produk_lokasi_id');
		if($this->input->get("start_date")!= ""){
			$this->db->where('tanggal >=', $this->input->get('start_date'));
			$this->db->where('tanggal <=', $this->input->get('end_date'));
		}
		if($this->input->get("produk_id")!= "" && $this->input->get("produk_id")!= "all"){
			$this->db->where('produk.produk_id', $this->input->get('produk_id'));
		}
		if($this->input->get("lokasi_id")!= "" && $this->input->get("lokasi_id")!= "all"){
			$this->db->where('lokasi.lokasi_id', $this->input->get('lokasi_id'));
		}		
		$this->db->group_start();
			$this->db->or_like('stock_produk.stock_produk_seri', $query, 'BOTH');
			$this->db->or_like('lokasi.lokasi_nama', $query, 'BOTH');
			$this->db->or_like('arus_stock_produk.keterangan', $query, 'BOTH');
			$this->db->or_like('produk.produk_nama', $query, 'BOTH');
		$this->db->group_end();
		$this->db->order_by('arus_stock_produk.tanggal', 'ASC');
		$this->db->order_by('arus_stock_produk.id', 'ASC');
		return $this->db->get('arus_stock_produk',$length, $start)->result();
	}
	function arus_stock_produk_count(){
		$this->db->join('produk', 'produk.produk_id = arus_stock_produk.produk_id');
		$this->db->join('stock_produk', 'stock_produk.stock_produk_id = arus_stock_produk.stock_produk_id');
		$this->db->join('lokasi', 'lokasi.lokasi_id = stock_produk.stock_produk_lokasi_id');
		return $this->db->get('arus_stock_produk')->num_rows();
	}
	function arus_stock_produk_count_filter($query){
		$this->db->join('produk', 'produk.produk_id = arus_stock_produk.produk_id');
		$this->db->join('jenis_produk', 'jenis_produk.jenis_produk_id = produk.produk_jenis_id');
		$this->db->join('stock_produk', 'stock_produk.stock_produk_id = arus_stock_produk.stock_produk_id');
		$this->db->join('lokasi', 'lokasi.lokasi_id = stock_produk.stock_produk_lokasi_id');
		if($this->input->get("start_date")!= ""){
			$this->db->where('tanggal >=', $this->input->get('start_date'));
			$this->db->where('tanggal <=', $this->input->get('end_date'));
		}
		if($this->input->get("produk_id")!= "" && $this->input->get("produk_id")!= "all"){
			$this->db->where('produk.produk_id', $this->input->get('produk_id'));
		}
		if($this->input->get("lokasi_id")!= "" && $this->input->get("lokasi_id")!= "all"){
			$this->db->where('lokasi.lokasi_id', $this->input->get('lokasi_id'));
		}				
		$this->db->group_start();
			$this->db->or_like('stock_produk.stock_produk_seri', $query, 'BOTH');
			$this->db->or_like('lokasi.lokasi_nama', $query, 'BOTH');
			$this->db->or_like('arus_stock_produk.keterangan', $query, 'BOTH');
			$this->db->or_like('produk.produk_nama', $query, 'BOTH');
		$this->db->group_end();			
		return $this->db->get('arus_stock_produk')->num_rows();
	}	
	function last_stock($produk_id){
		$this->db->select('sum(stock_produk.stock_produk_qty) as result');
		$this->db->from('stock_produk');
		$this->db->where('produk_id', $produk_id);
		$this->db->where('delete_flag', "0");
		return $this->db->get()->row();
	}
	function stock_total(){
		$this->db->select('sum(stock_produk.stock_produk_qty) as result');
		$this->db->from('stock_produk');
		return $this->db->get()->row();
	}
	function urutan_seri($key){
		$this->db->like('stock_produk_seri', $key, 'BOTH');
		$sql = $this->db->get('stock_produk');
		if ($sql->num_rows() > 0){
			return $sql->row()->urutan+1;
		} else {
			return 1;
		}
	}
	function transfer_stock(){
		$this->db->trans_begin();
		$qty = (str_replace(",", "", $this->input->post('stock_produk_qty'))) - (str_replace(",", "", $this->input->post('qty')));
		$data["stock_produk_qty"] = $qty;
		$this->db->where('stock_produk_id', $this->input->post('stock_produk_id'));
		$update = $this->db->update('stock_produk', $data);
		$last_stock = $this->last_stock($this->input->post('produk_id'))->result;
		if($update){
			$data = array();
			$data["tanggal"] = date("Y-m-d");
			$data["table_name"] = "stock_produk";
			$data["stock_produk_id"] = $this->input->post('stock_produk_id');
			$data["produk_id"] = $this->input->post('produk_id');
			$data["stock_out"] = str_replace(",", "", $this->input->post('qty'));
			$data["stock_in"] = 0;
			$data["last_stock"] =$last_stock;
			$data["last_stock_total"] = $this->stock_total()->result;
			$data["keterangan"] = "Transfer stok produk";
			$data["method"] = "update";
			$this->db->insert('arus_stock_produk', $data);
			$data = array();
			$data["tanggal"] = date("Y-m-d");
			$data["produk_id"] = $this->input->post('produk_id');
			$data["stock_produk_id"] = $this->input->post('stock_produk_id');
			$data["produk_kode"] = $this->input->post('produk_kode');
			$data["produk_nama"] = $this->input->post('produk_nama');
			$data["history_transfer_qty"] = str_replace(",", "", $this->input->post('qty'));
			$data["produk_seri"] = $this->input->post('stock_produk_seri');
			$data["histori_lokasi_awal_id"] = $this->input->post('stock_produk_lokasi_id');
			$data["histori_lokasi_tujuan_id"] = $this->input->post('histori_lokasi_tujuan_id');
			$data["dari"] = $this->input->post('lokasi_nama');
			$data["tujuan"] = $this->input->post('lokasi_tujuan_nama');
			$data["last_stock"] = $last_stock;
			$data["status"] = "Menunggu Konfirmasi";
			$this->db->insert('history_transfer_produk', $data);
		}
		$stock_produk_id = $this->input->post('stock_produk_id');
		if ($this->db->trans_status() === FALSE)
	      return FALSE;

	    $this->db->trans_commit();
	    return TRUE;
	}

	function produk_by_lokasi($lokasi_id,$produk_id){
		$this->db->where('delete_flag', "0");
		$this->db->where('stock_produk_lokasi_id', $lokasi_id);
		$this->db->where('produk_id', $produk_id);
		return $this->db->get('stock_produk')->result();
	}
	function total_stock_perlokasi($produk_id,$lokasi_id){
		$this->db->select('SUM(stock_produk_qty) as total_stock');
		$this->db->where('produk_id', $produk_id);
		$this->db->where('stock_produk_lokasi_id', $lokasi_id);
		$this->db->where('delete_flag', 0);
		$this->db->from('stock_produk');
		return $this->db->get()->row()->total_stock;
	}
	function produk_item_count(){
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('satuan', 'satuan.satuan_id = produk.produk_satuan_id');
		$this->db->order_by('produk.produk_id', 'desc');
		return $this->db->get('produk')->num_rows();
	}
	function produk_item_filter($query){
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('satuan', 'satuan.satuan_id = produk.produk_satuan_id');
		$this->db->group_start();
			$this->db->like('produk.produk_kode', $query, 'BOTH');
			$this->db->or_like('produk.produk_nama', $query, 'BOTH');
			$this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH');
		$this->db->group_end();
		$this->db->order_by('produk.produk_id', 'desc');
		return $this->db->get('produk')->num_rows();
	}
	function produk_item_list($start,$length,$query){
		$this->db->select('produk.*,satuan.*,jenis_produk.jenis_produk_nama');
		$this->db->join('jenis_produk', 'produk.produk_jenis_id = jenis_produk.jenis_produk_id');
		$this->db->join('satuan', 'satuan.satuan_id = produk.produk_satuan_id');
		$this->db->group_start();
			$this->db->like('produk.produk_kode', $query, 'BOTH');
			$this->db->or_like('produk.produk_nama', $query, 'BOTH');
			$this->db->or_like('jenis_produk.jenis_produk_nama', $query, 'BOTH');
		$this->db->group_end();
		$this->db->order_by('produk.produk_id', 'desc');
		return $this->db->get('produk', $length, $start)->result();
	}
    function stock_by_location_produk_id($lokasi_id,$bahan_id){
        $this->db->where('stock_produk_lokasi_id', $lokasi_id);
        $this->db->where('produk_id', $bahan_id);
        $this->db->where('delete_flag', "0");
        $this->db->where('stock_produk_qty >', "0");
        return $this->db->get('stock_produk')->result();
    }
}

/* End of file Stock_stock_produk.php */
/* Location: ./application/models/Stock_stock_produk.php */
