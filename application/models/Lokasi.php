<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lokasi extends MY_Model {

public function __construct()
	{
		parent::__construct();
		$this->table_name = "lokasi";
	}	
	function lokasi_list($start,$length,$query){
		$this->db->like('lokasi_id', $query, 'BOTH'); 
		$this->db->or_like('lokasi_kode', $query, 'BOTH'); 
		$this->db->or_like('lokasi_nama', $query, 'BOTH'); 
		$this->db->or_like('lokasi_alamat', $query, 'BOTH'); 
		$this->db->or_like('lokasi_tipe', $query, 'BOTH'); 
		$this->db->or_like('lokasi_telepon', $query, 'BOTH'); 
		$this->db->order_by('lokasi_id', 'desc');
		return $this->db->get('lokasi', $length, $start)->result();
	}
	function lokasi_count_filter($query){
		$this->db->like('lokasi_id', $query, 'BOTH'); 
		$this->db->or_like('lokasi_kode', $query, 'BOTH'); 
		$this->db->or_like('lokasi_nama', $query, 'BOTH'); 
		$this->db->or_like('lokasi_alamat', $query, 'BOTH'); 
		$this->db->or_like('lokasi_tipe', $query, 'BOTH'); 
		$this->db->or_like('lokasi_telepon', $query, 'BOTH'); 
		return $this->db->get('lokasi')->num_rows();
	}
	function lokasi_count_all(){
		return $this->db->get('lokasi')->num_rows();
	}	
	function is_ready_kode($lokasi_id,$kode){
		$this->db->where('lokasi_kode', $kode);
		$data = $this->db->get('lokasi')->row();
		if($data != null){
			if($data->lokasi_id == $lokasi_id){
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}
	function all_gudang(){
		$this->db->where('lokasi_tipe', "Gudang");
		$this->db->order_by('lokasi_id', 'desc');
		return $this->db->get('lokasi')->result();
	}
	function all_toko(){
		$this->db->where('lokasi_tipe', "Toko");
		$this->db->order_by('lokasi_id', 'desc');
		return $this->db->get('lokasi')->result();
	}	
}

/* End of file Lokasi.php */
/* Location: ./application/models/Lokasi.php */