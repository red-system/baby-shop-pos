<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->table_name= 'user';
		
	}
	function auth_login($email){
		$this->db->select('user.*,user_role.user_role_name,staff_nama,staff_email,lokasi.lokasi_nama');
		$this->db->where('staff_email', $email);
		$this->db->from('user');
		$this->db->join('user_role', 'user.user_role_id = user_role.user_role_id');
		$this->db->join('staff', 'user.user_staff_id = staff.staff_id');
		$this->db->join('lokasi', 'lokasi.lokasi_id = user.lokasi_id','left');
		return $this->db->get()->row();
	}
	function forgot_request($email,$code,$timeout){
		$data = array("forget_request_email"=>$email,"code"=>$code,"timeout"=>$timeout);
		return $this->db->insert('forget_request', $data);
	}
	function forget_session_check($email,$code){
		$this->db->where('forget_request_email', $email);
		$this->db->where('code', $code);
		$this->db->where('UNIX_TIMESTAMP() < timeout', null, false);
		return $this->db->get('forget_request')->result();
	}
	function change_password($email,$password){
		$sql = "UPDATE user INNER JOIN staff ON user.user_staff_id = staff.staff_id SET user.password = '".$password."' WHERE staff.staff_email = '".$email."'";
		return  $this->db->query($sql);
	}
	function user_by_id($user_id){
		$this->db->where('user_id', $user_id);
		$this->db->select('user.*,staff.staff_nama,staff.staff_email,staff.staff_phone_number,staff.tempat_lahir,staff.tanggal_lahir,staff.staff_alamat,user_role.user_role_name');
		$this->db->join('staff', 'staff.staff_id = user.user_staff_id');
		$this->db->join('user_role', 'user_role.user_role_id = user.user_role_id');
		return $this->db->get('user')->row();
	}
	function is_ready_email($email){
		$this->db->where('email', $email);
		$data = $this->db->get('user')->row();
		if($data != null){
			return false;
		} else {
			return true;
		}
	}
	function update_personal_info($user_id,$user_name,$email,$phone_number){
		$updated_at = date('Y-m-d H:i:s');
		$data = array("user_name"=>$user_name,"email"=>$email,"phone_number"=>$phone_number,"updated_at"=>$updated_at);
		$this->db->where('user_id', $user_id);
		return $this->db->update('user', $data);
	}
	function change_avatar($user_id,$url){
		$this->db->where('user_id', $user_id);
		$updated_at = date('Y-m-d H:i:s');
		$data = array("avatar"=>$url,"updated_at"=>$updated_at);
		return $this->db->update('user', $data);
	}
	function all_role(){
		return $this->db->get('user_role')->result();
	}
	function user_list($start,$length,$query){
		$this->db->select('user.*,staff.staff_nama,staff.staff_email,staff.staff_phone_number,user_role.user_role_name');
		$this->db->join('staff', 'staff.staff_id = user.user_staff_id');
		$this->db->join('user_role', 'user_role.user_role_id = user.user_role_id');
		$this->db->like('staff.staff_id', $query, 'BOTH'); 
		$this->db->or_like('user.user_id', $query, 'BOTH'); 
		$this->db->or_like('staff.staff_nama', $query, 'BOTH'); 
		$this->db->or_like('staff.staff_email', $query, 'BOTH'); 
		$this->db->or_like('staff.staff_phone_number', $query, 'BOTH'); 
		$this->db->or_like('user_role.user_role_name', $query, 'BOTH'); 
		$this->db->order_by('user_id', 'desc');
		return $this->db->get('user', $length, $start)->result();
	}
	function user_count_filter($query){
		$this->db->select('user.*,staff.staff_nama,staff.staff_email,staff.staff_phone_number,user_role.user_role_name');
		$this->db->join('staff', 'staff.staff_id = user.user_staff_id');
		$this->db->join('user_role', 'user_role.user_role_id = user.user_role_id');
		$this->db->like('staff.staff_id', $query, 'BOTH'); 
		$this->db->or_like('user.user_id', $query, 'BOTH'); 
		$this->db->or_like('staff.staff_nama', $query, 'BOTH'); 
		$this->db->or_like('staff.staff_email', $query, 'BOTH'); 
		$this->db->or_like('staff.staff_phone_number', $query, 'BOTH'); 
		$this->db->or_like('user_role.user_role_name', $query, 'BOTH'); 
		return $this->db->get('user')->num_rows();
	}
	function user_count_all(){
		$this->db->select('user.*,staff.staff_nama,staff.staff_email,staff.staff_phone_number,user_role.user_role_name');
		$this->db->join('staff', 'staff.staff_id = user.user_staff_id');
		$this->db->join('user_role', 'user_role.user_role_id = user.user_role_id'); 
		return $this->db->get('user')->num_rows();
	}	
}

/* End of file User.php */
/* Location: ./application/models/User.php */