<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Home';

$route['home/country'] = "Home/country";
$route['home/most'] = "Home/most";
$route['home/least'] = "Home/least";
$route['home/sales'] = "Home/sales";
$route['home/piutang-akan'] = "Home/piutang_akan";
$route['home/piutang-jatuh-tempo'] = "Home/piutang_jatuh";
$route['home/hutang-akan'] = "Home/hutang_akan";
$route['home/hutang-jatuh-tempo'] = "Home/hutang_jatuh";

$route['404_override'] = 'page_not_found';
$route['translate_uri_dashes'] = FALSE;
$route['login/auth'] = "Login/auth";
$route['logout'] = "Login/logout";
$route['forget'] = "Login/forget";
$route['forget/:any/:any'] = 'Login/forget_index';
$route['reset-password'] = "Login/reset_password";

$route['profile'] = "Profile";
$route['profile/personal-info'] = "Profile/personal_info";
$route['profile/change-password'] = "Profile/change_password";
$route['profile/change-avatar'] = "Profile/change_avatar";

$route['user'] = "UserController";
$route['user/list'] = "UserController/list";
$route['user/add'] = "UserController/add";
$route['user/edit'] = "UserController/edit";
$route['user/delete'] = "UserController/delete";

$route['user-role'] = "UserRoleController";
$route['user-role/list'] = "UserRoleController/list";
$route['user-role/add'] = "UserRoleController/add";
$route['user-role/edit'] = "UserRoleController/edit";
$route['user-role/delete'] = "UserRoleController/delete";
$route['user-role/access/:any'] = "RoleAksesController";
$route['user-role/access/edit/:num'] = "RoleAksesController/edit";

$route['staff'] = "StaffController";
$route['staff/list'] = "StaffController/list";
$route['staff/add'] = "StaffController/add";
$route['staff/edit'] = "StaffController/edit";
$route['staff/delete'] = "StaffController/delete";

$route['konversi-bahan'] = "KonversiBahanController";
$route['konversi-bahan/list'] = "KonversiBahanController/list";
$route['konversi-bahan/add'] = "KonversiBahanController/add";
$route['konversi-bahan/edit'] = "KonversiBahanController/edit";
$route['konversi-bahan/delete'] = "KonversiBahanController/delete";

$route['lokasi'] = "LokasiController";
$route['lokasi/list'] = "LokasiController/list";
$route['lokasi/add'] = "LokasiController/add";
$route['lokasi/edit'] = "LokasiController/edit";
$route['lokasi/delete'] = "LokasiController/delete";

$route['assembly/add'] = "AssemblyController/add_index";
$route['assembly/add/save'] = "AssemblyController/add_save";
$route['assembly/edit-assembly/:any'] = "AssemblyController/edit_index";
$route['assembly/edit/save'] = "AssemblyController/edit_save";
$route['assembly/delete-assembly'] = "AssemblyController/delete_assembly_produk";
$route['assembly/delete-stock-perlokasi/:num'] = "AssemblyController/delete_stock_perlokasi";
$route['assembly/produk-list'] = "AssemblyController/produk_list";
$route['assembly/get-unit-satuan'] = "AssemblyController/get_unit_satuan";
$route['assembly/detail-by-id'] = "AssemblyController/assembly_detail_by_id";


$route['konversi-satuan/:any'] = "KonversiSatuanController";
$route['konversi-satuan-list/:any'] = "KonversiSatuanController/list";
$route['konversi-satuan-add'] = "KonversiSatuanController/add";
$route['konversi-satuan-edit'] = "KonversiSatuanController/edit";
$route['konversi-satuan-delete'] = "KonversiSatuanController/delete";

$route['unit/:any'] = "UnitController";
$route['unit-list/:any'] = "UnitController/list";
$route['unit-add'] = "UnitController/add";
$route['unit-edit'] = "UnitController/edit";
$route['unit-delete'] = "UnitController/delete";

$route['tipe-pembayaran'] = "TipePembayaranController";
$route['tipe-pembayaran/list'] = "TipePembayaranController/list";
$route['tipe-pembayaran/add'] = "TipePembayaranController/add";
$route['tipe-pembayaran/edit'] = "TipePembayaranController/edit";
$route['tipe-pembayaran/delete'] = "TipePembayaranController/delete";

$route['jenis-bahan'] = "JenisBahanController";
$route['jenis-bahan/list'] = "JenisBahanController/list";
$route['jenis-bahan/add'] = "JenisBahanController/add";
$route['jenis-bahan/edit'] = "JenisBahanController/edit";
$route['jenis-bahan/delete'] = "JenisBahanController/delete";

$route['jenis-produk'] = "JenisProdukController";
$route['jenis-produk/list'] = "JenisProdukController/list";
$route['jenis-produk/add'] = "JenisProdukController/add";
$route['jenis-produk/edit'] = "JenisProdukController/edit";
$route['jenis-produk/delete'] = "JenisProdukController/delete";
$route['jenis-produk/options'] = "JenisProdukController/options";

$route['satuan'] = "SatuanController";
$route['satuan/list'] = "SatuanController/list";
$route['satuan/add'] = "SatuanController/add";
$route['satuan/edit'] = "SatuanController/edit";
$route['satuan/delete'] = "SatuanController/delete";

$route['suplier'] = "SuplierController";
$route['suplier/list'] = "SuplierController/list";
$route['suplier/add'] = "SuplierController/add";
$route['suplier/edit'] = "SuplierController/edit";
$route['suplier/delete'] = "SuplierController/delete";
$route['suplier/option'] = "SuplierController/options";

$route['produksi'] = "ProduksiController";
$route['produksi/list'] = "ProduksiController/list";
$route['produksi/add'] = "ProduksiController/add";
$route['produksi/utility/:any'] = "ProduksiController/utility";
$route['produksi/save-add'] = "ProduksiController/save_add";
$route['produksi/edit/:any'] = "ProduksiController/edit";
$route['produksi/detail'] = "ProduksiController/detail";
$route['produksi/save-edit'] = "ProduksiController/save_edit";
$route['produksi/delete'] = "ProduksiController/delete";
$route['produksi/produk-list'] = "ProduksiController/produk_list";
$route['produksi/selesai-produksi'] = "ProduksiController/selesai_produksi";
$route['produksi/penerimaan-produksi'] = "ProduksiController/penerimaan_produksi";
$route['history-produksi'] = "ProduksiController/history_index";
$route['history-produksi/list'] = "ProduksiController/history_list";

$route['custom-produksi'] = "CustomProduksiController";
$route['custom-produksi/list'] = "CustomProduksiController/list";
$route['custom-produksi/start/:any'] = "CustomProduksiController/start";
$route['custom-produksi/utility/:any'] = "CustomProduksiController/utility";
$route['custom-produksi/save-start'] = "CustomProduksiController/save_start";
$route['custom-produksi/edit/:any'] = "CustomProduksiController/edit";
$route['custom-produksi/detail'] = "CustomProduksiController/detail";
$route['custom-produksi/save-edit'] = "CustomProduksiController/save_edit";
$route['custom-produksi/delete'] = "CustomProduksiController/delete";
$route['custom-produksi/selesai-produksi'] = "CustomProduksiController/selesai_produksi";
$route['custom-produksi/penerimaan-produksi'] = "CustomProduksiController/penerimaan_produksi";
$route['custom-history-produksi'] = "CustomProduksiController/history_index";
$route['custom-history-produksi/list'] = "CustomProduksiController/history_list";


$route['po-produk'] = "PoProdukController";
$route['po-produk/list'] = "PoProdukController/list";
$route['po-produk/add'] = "PoProdukController/add";
$route['po-produk/utility/:any'] = "PoProdukController/utility";
$route['po-produk/save-add'] = "PoProdukController/save_add";
$route['po-produk/save-start'] = "PoProdukController/save_start";
$route['po-produk/edit/:any'] = "PoProdukController/edit";
$route['po-produk/detail'] = "PoProdukController/detail";
$route['po-produk/save-edit'] = "PoProdukController/save_edit";
$route['po-produk/delete'] = "PoProdukController/delete";
$route['po-produk/selesai-po-produk'] = "PoProdukController/selesai_po-produk";
$route['po-produk/penerimaan-po-produk'] = "PoProdukController/penerimaan_po_produk";
$route['history-po-produk'] = "PoProdukController/history";
$route['history-po-produk/list'] = "PoProdukController/history_list";

$route['guest'] = "GuestController";
$route['guest/list'] = "GuestController/list";
$route['guest/add'] = "GuestController/add";
$route['guest/edit'] = "GuestController/edit";
$route['guest/delete'] = "GuestController/delete";

$route['bahan'] = "BahanController";
$route['bahan/list'] = "BahanController/list";
$route['bahan/add'] = "BahanController/add";
$route['bahan/edit'] = "BahanController/edit";
$route['bahan/delete'] = "BahanController/delete";

$route['stock-bahan/:any'] = "StockBahanController";
$route['stock-bahan/list/:num'] = "StockBahanController/list";
$route['stock-bahan/add/:num'] = "StockBahanController/add";
$route['stock-bahan/edit/:num'] = "StockBahanController/edit";
$route['stock-bahan/delete/:num'] = "StockBahanController/delete";

$route['harga-produk/:any'] = "HargaProdukController";
$route['harga-produk/list/:num'] = "HargaProdukController/list";
$route['harga-produk/add/:num'] = "HargaProdukController/add";
$route['harga-produk/edit/:num'] = "HargaProdukController/edit";
$route['harga-produk/delete/:num'] = "HargaProdukController/delete";

$route['produk'] = "ProdukController";
$route['produk/list'] = "ProdukController/list";
$route['produk/add'] = "ProdukController/add";
$route['produk/edit'] = "ProdukController/edit";
$route['produk/harga/:num'] = "ProdukController/harga";
$route['produk/item_produk/:num'] = "ProdukController/item_produk";
$route['produk/delete'] = "ProdukController/delete";

$route['stock-produk/:any'] = "StockProdukController";
$route['stock-produk/list/:num'] = "StockProdukController/list";
$route['stock-produk/add/:num'] = "StockProdukController/add";
$route['stock-produk/edit/:num'] = "StockProdukController/edit";
$route['stock-produk/delete/:num'] = "StockProdukController/delete";
$route['stock-produk/barcode/:num'] = "StockProdukController/barcode";
$route['stock-produk/print-barcode/:num/:num'] = "StockProdukController/print_barcode";

$route['transfer-bahan'] = "TransferBahanController";
$route['transfer-bahan/list'] = "TransferBahanController/list";
$route['transfer-bahan/stock/:any'] = "TransferBahanController/stock_index";
$route['transfer-bahan/stock/list/:num'] = "TransferBahanController/stock_list";
$route['transfer-bahan/stock/transfer/:num'] = "TransferBahanController/transfer";
$route['konfirmasi-transfer-bahan'] = "KonfirmasiTransferBahanController";
$route['konfirmasi-transfer-bahan/list'] = "KonfirmasiTransferBahanController/list";
$route['konfirmasi-transfer-bahan/confirm'] = "KonfirmasiTransferBahanController/confirm";
$route['history-transfer-bahan'] = "HistoryTransferBahanController";
$route['history-transfer-bahan/list'] = "HistoryTransferBahanController/list";
$route['history-transfer-bahan/pdf'] = "HistoryTransferBahanController/pdf";
$route['history-transfer-bahan/excel'] = "HistoryTransferBahanController/excel";

$route['transfer-produk'] = "TransferProdukController";
$route['transfer-produk/list'] = "TransferProdukController/list";
$route['transfer-produk/stock/:any'] = "TransferProdukController/stock_index";
$route['transfer-produk/stock/list/:num'] = "TransferProdukController/stock_list";
$route['transfer-produk/stock/transfer/:num'] = "TransferProdukController/transfer";
$route['konfirmasi-transfer-produk'] = "KonfirmasiTransferProdukController";
$route['konfirmasi-transfer-produk/list'] = "KonfirmasiTransferProdukController/list";
$route['konfirmasi-transfer-produk/confirm'] = "KonfirmasiTransferProdukController/confirm";
$route['history-transfer-produk'] = "HistoryTransferProdukController";
$route['history-transfer-produk/list'] = "HistoryTransferProdukController/list";
$route['history-transfer-produk/pdf'] = "HistoryTransferProdukController/pdf";
$route['history-transfer-produk/excel'] = "HistoryTransferProdukController/excel";

$route['penyesuaian-bahan'] = "PenyesuaianStockBahanController";
$route['penyesuaian-bahan/list'] = "PenyesuaianStockBahanController/list";
$route['penyesuaian-bahan/stock/:any'] = "PenyesuaianStockBahanController/stock_index";
$route['penyesuaian-bahan/stock/adjust/:num'] = "PenyesuaianStockBahanController/adjust";
$route['history-penyesuaian-bahan'] = "HistoryPenyesuaianBahanController";
$route['history-penyesuaian-bahan/list'] = "HistoryPenyesuaianBahanController/list";
$route['history-penyesuaian-bahan/pdf'] = "HistoryPenyesuaianBahanController/pdf";
$route['history-penyesuaian-bahan/excel'] = "HistoryPenyesuaianBahanController/excel";

$route['penyesuaian-produk'] = "PenyesuaianStockProdukController";
$route['penyesuaian-produk/list'] = "PenyesuaianStockProdukController/list";
$route['penyesuaian-produk/stock/:any'] = "PenyesuaianStockProdukController/stock_index";
$route['penyesuaian-produk/stock/adjust/:num'] = "PenyesuaianStockProdukController/adjust";
$route['history-penyesuaian-produk'] = "HistoryPenyesuaianProdukController";
$route['history-penyesuaian-produk/list'] = "HistoryPenyesuaianProdukController/list";
$route['history-penyesuaian-produk/pdf'] = "HistoryPenyesuaianProdukController/pdf";
$route['history-penyesuaian-produk/excel'] = "HistoryPenyesuaianProdukController/excel";

$route['low-stock-bahan'] = "LowStockBahanController";
$route['low-stock-bahan/list'] = "LowStockBahanController/list";

$route['low-stock-produk'] = "LowStockProdukController";
$route['low-stock-produk/list'] = "LowStockProdukController/list";
$route['low-stock-produk/pdf'] = "LowStockProdukController/pdf";
$route['low-stock-produk/excel'] = "LowStockProdukController/excel";

$route['komposisi-produk'] = "KomposisiProdukController";
$route['komposisi-produk/list'] = "KomposisiProdukController/list";
$route['komposisi-produk/komposisi/:any'] = "KomposisiProdukController/komposisi_index";
$route['komposisi-produk/komposisi/add/:num'] = "KomposisiProdukController/add";
$route['komposisi-produk/komposisi/edit/:num'] = "KomposisiProdukController/edit";
$route['komposisi-produk/komposisi/delete/:num'] = "KomposisiProdukController/delete";
$route['komposisi-produk/komposisi/list/:num'] = "KomposisiProdukController/komposisi_list";

$route['produk-by-location'] = "ProdukByLocation";
$route['produk-by-location/list'] = "ProdukByLocation/list";
$route['produk-by-location/pdf'] = "ProdukByLocation/pdf";
$route['produk-by-location/excel'] = "ProdukByLocation/excel";

$route['bahan-by-location'] = "BahanByLocation";
$route['bahan-by-location/list'] = "BahanByLocation/list";
$route['bahan-by-location/pdf'] = "BahanByLocation/pdf";
$route['bahan-by-location/excel'] = "BahanByLocation/excel";

$route['kartu-stock-bahan'] = "KartuStockBahanController";
$route['kartu-stock-bahan/list'] = "KartuStockBahanController/list";
$route['kartu-stock-bahan/pdf'] = "KartuStockBahanController/pdf";
$route['kartu-stock-bahan/excel'] = "KartuStockBahanController/excel";

$route['kartu-stock-produk'] = "KartuStockProdukController";
$route['kartu-stock-produk/list'] = "KartuStockProdukController/list";
$route['kartu-stock-produk/pdf'] = "KartuStockProdukController/pdf";
$route['kartu-stock-produk/excel'] = "KartuStockProdukController/excel";

$route['pos'] = "POSController";
$route['pos/guest'] = "POSController/guest_list";
$route['pos/utility/:any'] = "POSController/utility";
$route['pos/produk'] = "POSController/produk_list";
$route['pos/delete'] = "RekapitulasiPosController/delete";
$route['pos/save'] = "POSController/save";
$route['pos/save-preaty-cash'] = "POSController/save_preaty_cash";
$route['pos/closing'] = "POSController/closing";
$route['pos/print/:num'] = "POSController/print";
$route['rekapitulasi-pos'] = "RekapitulasiPosController";
$route['rekapitulasi-pos/list'] = "RekapitulasiPosController/list";
$route['rekapitulasi-pos/detail/:num'] = "RekapitulasiPosController/detail";

$route['custom-pos'] = "CustomPOSController";
$route['custom-pos/save'] = "CustomPOSController/save";

$route['hutang'] = "HutangController";
$route['hutang/list'] = "HutangController/list";
$route['hutang/pay/:any'] = "HutangController/pay";
$route['hutang/pay/list/:num'] = "HutangController/pay_list";
$route['hutang/pay/add/:num'] = "HutangController/add";
$route['hutang/pay/edit/:num'] = "HutangController/edit";
$route['hutang/pay/delete/:num'] = "HutangController/delete";
$route['hutang/detail/:num'] = "HutangController/detail";
$route['hutang/posting/:any'] = "HutangController/posting";
$route['history-hutang'] = "HistoryHutangController";
$route['history-hutang/list'] = "HistoryHutangController/list";

$route['piutang'] = "PiutangController";
$route['piutang/list'] = "PiutangController/list";
$route['piutang/pay/:any'] = "PiutangController/pay";
$route['piutang/pay/list/:num'] = "PiutangController/pay_list";
$route['piutang/pay/add/:num'] = "PiutangController/add";
$route['piutang/pay/edit/:num'] = "PiutangController/edit";
$route['piutang/pay/delete/:num'] = "PiutangController/delete";
$route['piutang/detail/:num'] = "PiutangController/detail";
$route['piutang/posting/:any'] = "PiutangController/posting";
$route['history-piutang'] = "HistoryPiutangController";
$route['history-piutang/list'] = "HistoryPiutangController/list";

$route['laporan/:any'] = "LaporanController";
$route['laporan/:any/:any'] = "LaporanController";
$route['laporan/:any/:any/:any'] = "LaporanController";

$route['size'] = "SizeController";
$route['size/list'] = "SizeController/list";
$route['size/add'] = "SizeController/add";
$route['size/edit'] = "SizeController/edit";
$route['size/delete'] = "SizeController/delete";
$route['size/options'] = "SizeController/options";

$route['color'] = "ColorController";
$route['color/list'] = "ColorController/list";
$route['color/add'] = "ColorController/add";
$route['color/edit'] = "ColorController/edit";
$route['color/delete'] = "ColorController/delete";
$route['color/options'] = "ColorController/options";

$route['fabric'] = "FabricController";
$route['fabric/list'] = "FabricController/list";
$route['fabric/add'] = "FabricController/add";
$route['fabric/edit'] = "FabricController/edit";
$route['fabric/delete'] = "FabricController/delete";
$route['fabric/options'] = "FabricController/options";

$route['patern'] = "PaternController";
$route['patern/list'] = "PaternController/list";
$route['patern/add'] = "PaternController/add";
$route['patern/edit'] = "PaternController/edit";
$route['patern/delete'] = "PaternController/delete";
$route['patern/options'] = "PaternController/options";
