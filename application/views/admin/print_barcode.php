<html>
<head>
	<meta charset="utf-8" />
	<title>Print Barcode</title>
	<meta name="description" content="Child datatable from remote data">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
	<script>
        WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
	</script>
	<link href="<?=base_url()?>assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

</head>
<body class="receipt" onload="window.print()">
<section class="sheet" style="padding: 3mm 3mm 3mm 3mm">
	<div class="col-12 row">
	<?php
		$jumlah_cetak = $this->uri->segment(4);
		for ($i = 0; $i<$jumlah_cetak;$i++){
			?>
			<div class="col-2" style="padding: 10px">
				<img src="<?=base_url().'stock-produk/barcode/'.$this->uri->segment(3)?>" style="width: 100%" height="auto">
			</div>
			<?php
		}
	?>
	</div>
</section>
</body>
</html>
