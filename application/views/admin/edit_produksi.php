						<div class="kt-subheader   kt-grid__item" id="kt_subheader">
							<div class="kt-subheader__main">

								<h3 class="kt-subheader__title">Produksi</h3>
								<span class="kt-subheader__separator kt-hidden"></span>
								<div class="kt-subheader__breadcrumbs">
									<a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
									<span class="kt-subheader__breadcrumbs-separator"></span>
									<a href="<?=base_url()?>produksi" class="kt-subheader__breadcrumbs-link">Produksi</a>
									<input type="hidden" id="base_url" name="" value="<?=base_url()?>">
									<input type="hidden" name="" id="no_produk" value="<?=$no_produk?>">
									<input type="hidden" name="" id="no_bahan" value="<?=$no_bahan?>">
								</div>

							</div>
						</div>
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<h3 class="kt-portlet__head-title">
											Edit Produksi
										</h3>
									</div>
								</div>
								<div class="kt-portlet__body">
									<form action="<?=base_url()?>produksi/save-edit" method="post" id="kt_add">
										<div class="modal-body">
											<div class="row">
												<div class="col-md-6">
													<div class="form-group row">
														<label for="example-text-input" class="col-3 col-form-label">Kode Produksi</label>
														<div class="col-9">
															<input type="hidden" name="produksi_kode" value="<?=$produksi["produksi_kode"]?>">
															<input type="hidden" name="produksi_id" value="<?=$id?>">
															<label name="produksi_kode" class="col-form-label"><?=$produksi["produksi_kode"]?></label>
														</div>
													</div>
													<div class="form-group row">
														<label for="example-text-input" class="col-3 col-form-label">Tanggal Mulai <b class="label--required">*</b></label>
														<div class="col-9">
															<input type="text" class="form-control kt-input tanggal" name="tanggal_mulai" placeholder="Dari" autocomplete="off" data-col-index="5" value="<?=$produksi["tanggal_mulai"]?>" required="" />
														</div>
													</div>
													<div class="form-group row">
														<label for="example-text-input" class="col-3 col-form-label">Estimasi Selesai <b class="label--required">*</b></label>
														<div class="col-9">
															<input type="text" class="form-control kt-input tanggal" name="estimasi_selesai" placeholder="Dari" autocomplete="off" data-col-index="5" value="<?=$produksi["estimasi_selesai"]?>" required="" />
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group row">
														<label for="example-text-input" class="col-3 col-form-label">Lokasi Asal Bahan</label>
														<div class="col-9">
															<select class="form-control col-md-12 lokasi-bahan" name="lokasi_bahan_id" required="">
																<option value="">Pilih Lokasi</option>
																<?php
																foreach ($lokasi as $key) {
																	?>
																	<option class="lokasi_option" value="<?=$key->lokasi_id?>" <?=($produksi["lokasi_bahan_id"]==$key->lokasi_id ? "selected" : "")?>><?=$key->lokasi_nama?></option>
																	<?php
																}
																?>
															</select>															
														</div>
													</div>
													<div class="form-group row">
														<label for="example-text-input" class="col-3 col-form-label">Keterangan</label>
														<div class="col-9">
															<textarea class="form-control" rows="3" name="keterangan"><?=$produksi["keterangan"]?></textarea>
														</div>
													</div>															
												</div>
												<div class="col-md-8"><h5><strong>Item</strong></h5></div>
												<div class="col-md-4"><button type="button" class="btn btn-primary pull-right" id="add_item"><i class="flaticon2-plus"></i>&nbsp;Tambah</button></div>
											</div>
											<div class="row">
												<div class="col-md-12" id="item-container">
													<?php
													foreach ($produksi["item"] as $key) {
														?>
														<div class="item-produksi" id="item_<?=$key["produksi_item_id"]?>">
															<div class="row">
																<div class="col-md-6">
																	<div class="form-group row">
																		<label for="example-text-input" class="col-3 col-form-label">Produk <b class="label--required">*</b></label>
																		<div class="col-9">
																			<div class="input-group col-12">
																				<input type="text" class="form-control readonly" name="produk_nama_<?=$key["produksi_item_id"]?>" id="produk_nama_<?=$key["produksi_item_id"]?>" value="<?=$key["produk_nama"]?>" required="" autocomplete="off">
																				<input type="hidden" class="produk_id" id="produk_id_<?=$key["produksi_item_id"]?>" name="item[produk_<?=$key["produksi_item_id"]?>][produk_id]" value="<?=$key["produk_id"]?>" readonly="">
																				<div class="input-group-append">
																					<button class="btn btn-primary produk-search" type="button" data-no="<?=$key["produksi_item_id"]?>"><i class="flaticon-search"></i></button>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="form-group row">
																		<label for="example-text-input" class="col-3 col-form-label">Jumlah<b class="label--required">*</b></label>
																		<div class="col-9">
																			<input type="text" class="form-control input-numeral produk-jumlah" data-no="<?=$key["produksi_item_id"]?>" autocomplete="off" id="produk_jumlah_<?=$key["produksi_item_id"]?>" name="item[produk_<?=$key["produksi_item_id"]?>][jumlah]" value="<?=$key["jumlah"]?>" required="">
																		</div>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group row">
																		<label for="example-text-input" class="col-3 col-form-label">Keterangan</label>
																		<div class="col-9">
																			<textarea class="form-control" rows="3" id="produk_keterangan_<?=$key["produksi_item_id"]?>" name="item[produk_<?=$key["produksi_item_id"]?>][keterangan]"><?=$key["produk_keterangan"]?></textarea></div>
																		</div>
																	</div>
																	<div class="col-md-12">
																		<h5><strong>List Bahan</strong></h5>
																<div id="bahan_container_<?=$key["produksi_item_id"]?>" class="col-12">
																	<?php
																	$i = 0;
																	foreach ($key["item_bahan"] as $eachBahan) {
																	?>
																	<div class="col-12 row">
																		<div class="col-md-6">
																			<div class="form-group row">
																				<input type="hidden" name="item[produk_<?=$key["produksi_item_id"]?>][item_bahan][bahan_<?=$i?>][bahan_id]" id="bahan_id_<?=$i?>" value="26">
																				<label class="col-3 col-form-label">Bahan</label>
																				<label class="col-1 col-form-label">:</label>
																				<label class="col-8 col-form-label" id="bahan_nama_<?=$i?>"><?=$eachBahan["bahan_nama"]?></label>
																			</div>
																		</div>
																		<div class="col-md-6">
																			<div class="form-group row">
																				<input type="hidden" name="item[produk_<?=$key["produksi_item_id"]?>][item_bahan][bahan_<?=$i?>][jumlah_potong]" id="produk_<?=$key["produksi_item_id"]?>_potong_<?=$i?>" value="<?=$eachBahan["potong"]?>">
																				<input type="hidden" class="jumlah_bahan" name="item[produk_<?=$key["produksi_item_id"]?>][item_bahan][bahan_<?=$i?>][jumlah]" id="jumlah_bahan_<?=$i?>">
																				<label class="col-3 col-form-label">Jumlah</label>
																				<label class="col-1 col-form-label">:</label>
																				<label class="col-8 col-form-label" id="produk_<?=$key["produksi_item_id"]?>_bahan_<?=$i?>"><?=$eachBahan["jumlah"]?></label>
																			</div>
																		</div>
																	</div>
																		<?php
																		$i++;
																	}
																	?>
																</div>

															</div>


															<?php
														}
														?>
													</div>
												</div>

											</div>
											<div class="modal-footer">
												<a href="<?=base_url()?>produksi" class="btn-produk-add btn btn-warning btn btn--custom btn--pill btn--icon btn--air">
													<span>
														<i class="la la-angle-double-left"></i>
														<span>Kembali ke Daftar</span>
													</span>
												</a>
												<button id="kt_add_submit" type="submit" class="btn btn-primary">Save changes</button>
											</div>
										</form>
									</div>
								</div>
							</div>
							<div class="modal" id="kt_modal_produk" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLongTitle">Data Produk</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											</button>
										</div>
										<div class="modal-body">
											<div class="row">
												<input type="hidden" id="list_produk" value="<?=base_url()?>produk/list">
												<div class="col-md-12">
													<table class="table table-striped- table-hover table-checkable" id="produk-table">
														<thead>
															<tr>
																<th>Kode Produk</th>
																<th>Nama Produk</th>
																<th>Stok</th>
																<th width="60">Aksi</th>
															</tr>
														</thead>
														<tbody id="produk_child"></tbody>
													</table>												
												</div>
											</div>


										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										</div>
									</div>
								</div>
							</div>
							<div class="modal" id="kt_modal_bahan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLongTitle">Data Bahan</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											</button>
										</div>
										<div class="modal-body">
											<div class="row">
												<input type="hidden" id="list_bahan" value="<?=base_url()?>produksi/utility/list-bahan">
												<div class="col-md-12">
													<table class="table table-striped- table-hover table-checkable" id="bahan-table">
														<thead>
															<tr>
																<th>Kode Bahan</th>
																<th>Nama Bahan</th>
																<th>Stok</th>
																<th width="60">Aksi</th>
															</tr>
														</thead>
														<tbody id="bahan_child"></tbody>
													</table>												
												</div>
											</div>


										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										</div>
									</div>
								</div>
							</div>						

