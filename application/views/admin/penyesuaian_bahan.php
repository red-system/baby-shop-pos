<!-- begin:: Content -->
						<div class="kt-subheader   kt-grid__item" id="kt_subheader">
						    <div class="kt-subheader__main">
						        
						        <h3 class="kt-subheader__title"><?=((isset($bahan)) ? $bahan->bahan_nama : 'Penyesuaian Stock Bahan' )?></h3>
						        	<span class="kt-subheader__separator kt-hidden"></span>
						            <div class="kt-subheader__breadcrumbs">
						                <a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
						                <span class="kt-subheader__breadcrumbs-separator"></span>
						                <a href="<?=base_url()?>penyesuaian-bahan" class="kt-subheader__breadcrumbs-link">Inventori</a>
						                <span class="kt-subheader__breadcrumbs-separator"></span>
						                <a href="<?=base_url()?>penyesuaian-bahan" class="kt-subheader__breadcrumbs-link">Penyesuaian Barang</a>
						                <?php if(isset($bahan)){
						                	?>
						                	<span class="kt-subheader__breadcrumbs-separator"></span>
						                <a href="<?=base_url().'penyesuaian-bahan/stock/'.$this->uri->segment(3)?>" class="kt-subheader__breadcrumbs-link">Stock</a>
						                	<?php
						                } ?>
						                                <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
						            </div>
						                
						    </div>
						</div>

						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
							<div class="row">
								<div class="col-lg-12">
									<div class="kt-portlet kt-portlet--tabs">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-toolbar">
												<ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">
													<li class="nav-item">
														<a class="nav-link <?=(($this->uri->segment(1)=='penyesuaian-bahan') ? 'active' : '')?>" href="<?=base_url()?>penyesuaian-bahan">
															<i class="flaticon-truck"></i> <span class="kt--visible-desktop-inline-block">Penyesuaian Stock</span>
														</a>
													</li>
													<li class="nav-item">
														<a class="nav-link <?=(($this->uri->segment(1)=='history-penyesuaian-bahan') ? 'active' : '')?>" href="<?=base_url()?>history-penyesuaian-bahan">
															<i class="flaticon2-time"></i> <span class="kt--visible-desktop-inline-block">History Penyesuaian</span>
														</a>
													</li>
												</ul>
											</div>
										</div>
										<div class="kt-portlet__body">
											<div class="tab-content">
												<div class="kt-portlet kt-portlet--mobile">
												<input type="hidden" id="base_url" value="<?=base_url()?>" name="">
														<input type="hidden" id="list_url" value="<?=$list_url?>" name="">
														<div style="display: none;" id="table_column"><?=$column?></div>
										<?php if(isset($columnDef)) {  ?>
											<div style="display: none;" id="table_columnDef"><?=$columnDef?></div>
										<?php } ?>
														<div style="display: none;" id="sumColumn"><?=(isset($sumColumn) ? $sumColumn : "")?></div>
														<div style="display: none;" data-width="150" id="table_action"><?=(isset($action) ? $action : "")?></div>
												<div class="kt-portlet__body">

													<!--begin: Search Form -->
													<div class="">
														<div class="row align-items-center">
															<div class="col-xl-8 order-2 order-xl-1">
																<div class="row align-items-center">
																	<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
																		<div class="kt-input-icon kt-input-icon--left">
																			<input type="text" class="form-control" placeholder="Search..." id="generalSearch">
																			<span class="kt-input-icon__icon kt-input-icon__icon--left">
																				<span><i class="la la-search"></i></span>
																			</span>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<?php if($this->uri->segment(1) =="penyesuaian-bahan") {  
														if(isset($bahan)){ ?>
														<table class="datatable table table-striped- table-hover table-checkable" >
															<thead>
																<tr>
																	<th width="30">No</th>
																	<th>Lokasi</th>
																	<th>No seri</th>
																	<th>Jumlah</th>
																	<th width="100">Action</th>
																</tr>
															</thead>
															<tbody id="child_data_ajax"></tbody>
															<tfoot >
																<tr>
																	<td colspan="3" style="text-align: right"><strong>Total</strong> </td>
																	<td ><strong></strong> </td>
																	<td></td>
																</tr>
																
															</tfoot>
														</table>
														<?php } else { ?>
														<table class="datatable table table-striped- table-hover table-checkable" >
															<thead>
																<tr>
																	<th width="30">No</th>
																	<th>Kode Bahan</th>
																	<th>Nama Bahan</th>
																	<th>Jenis Bahan</th>
																	<th>Satuan</th>
																	<th>Global Stok</th>
																	<?=((isset($_SESSION['login']['lokasi_id'])) ? '<th>Jumlah</th>': "")?>
																	<th>Suplier</th><th width="60">Action</th>
																	
																</tr>
															</thead>
															<tbody id="child_data_ajax"></tbody>
															<tfoot >
																<tr>
																	<td colspan="5" style="text-align: right"><strong>Total</strong> </td>
																	<td ><strong></strong> </td>
																	<td></td>
																	<td></td>
																	<?=((isset($_SESSION['login']['lokasi_id'])) ? "<td></td>": "")?>
																</tr>
																
															</tfoot>
														</table>
													<?php } } else { ?>
													<table class="datatable table table-striped- table-hover table-checkable" >
														<thead>
															<tr>
																<th width="30">No</th>
																<th>Tanggal</th>
																<th>Kode Bahan</th>
																<th>Nama Bahan</th>
																<th>Jenis Bahan</th>
																<th>Jumlah Awal</th>
																<th>Jumlah Akhir</th>
																<th>Keterangan</th>
															</tr>
														</thead>
														<tbody id="child_data_ajax"></tbody>
													</table>
													<?php } ?>
												</div>
											</div>
											</div>
										</div>
									</div>
								</div>
								
							</div>
						</div>
						<?php if (isset($bahan)) { ?>
						<div class="modal" id="kt_modal_adjust" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLongTitle">Penyesuaian Stock Bahan</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											</button>
										</div>
										<form action="<?=base_url().'penyesuaian-bahan/stock/adjust/'.$bahan->bahan_id ?>" method="post" id="kt_add_staff_form">
											<div class="modal-body">
												<div class="row">
													<div class="col-md-12">
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Kode Bahan</label>
															<div class="col-9">
																<input type="hidden" name="stock_bahan_id">
				                                            	<input type="hidden" name="bahan_id">
				                                            	<input type="hidden" placeholder="" name="bahan_kode" class="form-control" value="<?=$bahan->bahan_kode?>">
																<label name="suplier_kode" class="col-form-label"><?=$bahan->bahan_kode?></label>
															</div>
														</div>
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Nama Bahan</label>
															<div class="col-9">
																<input type="hidden" placeholder="" name="bahan_nama" class="form-control" value="<?=$bahan->bahan_nama?>">
																<label name="suplier_kode" class="col-form-label"><?=$bahan->bahan_nama?></label>
															</div>
														</div>
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">No Seri</label>
															<div class="col-9">
																<input type="hidden" placeholder="" name="stock_bahan_seri" class="form-control">
																<label name="stock_bahan_seri" class="col-form-label"></label>
															</div>
														</div>
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Jenis Bahan</label>
															<div class="col-9">
																<input type="hidden" placeholder="" name="jenis_bahan_id" class="form-control" value="<?=$bahan->bahan_jenis_id?>">
				                                            	<input type="hidden" placeholder="" name="jenis_bahan_nama" class="form-control" value="<?=$bahan->jenis_bahan_nama?>">
																<label name="stock_bahan_seri" class="col-form-label"><?=$bahan->jenis_bahan_nama?></label>
															</div>
														</div>
														<div class="form-group row">
															<label for="example-text-input" class="col-3 col-form-label">Lokasi</label>
															<div class="col-9">
																<input type="hidden" placeholder="" name="stock_bahan_lokasi_id" class="form-control" value="">
				                                            	<input type="hidden" placeholder="" name="lokasi_nama" class="form-control" value="">
																<label name="lokasi_nama" class="col-form-label"></label>
															</div>
														</div>
				                                        <div class="form-group">
				                                            	<label class="form-control-label ">jumlah <b class="label--required">*</b></label>
				                                            	<input type="text" placeholder="" name="stock_bahan_qty" class="input-numeral form-control" value="" required="">
				                                        </div>
				                                        <div class="form-group">
				                                            	<label class="form-control-label ">Keterangan <b class="label--required">*</b></label>
				                                            	<textarea class="form-control" id="keterangan" rows="3" name="keterangan" required=""></textarea>
				                                        </div>
				                                        
			                                        </div>
											</div>
										</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
												<button id="kt_add_submit" type="submit" class="btn btn-primary">Save changes</button>
											</div>
										</form>
									</div>
								</div>
							</div>
							<?php } ?>