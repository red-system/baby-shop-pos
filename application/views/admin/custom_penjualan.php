
<!-- begin:: Content -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-subheader__main">

		<h3 class="kt-subheader__title">Penjualan Custom</h3>
		<span class="kt-subheader__separator kt-hidden"></span>
		<div class="kt-subheader__breadcrumbs">
			<a href="<?=base_url()?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
			<span class="kt-subheader__breadcrumbs-separator"></span>
			<a href="<?=base_url()?>pos" class="kt-subheader__breadcrumbs-link">POS</a>
			<span class="kt-subheader__breadcrumbs-separator"></span>
			<a href="<?=base_url()?>pos" class="kt-subheader__breadcrumbs-link">Penjualan Custom</a>
			<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
		</div>

	</div>
</div>
<div class="kt-content  kt-grid__item kt-grid__itekt--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-toolbar">
				<ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">
					<li class="nav-item">
						<a class="nav-link <?=(($this->uri->segment(1)=='custom-pos') ? 'active' : '')?>" href="<?=base_url()?>pos">
							<i class="flaticon2-shopping-cart"></i> <span class="kt--visible-desktop-inline-block">Penjualan Custom</span>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link <?=(($this->uri->segment(1)=='rekapitulasi-pos') ? 'active' : '')?>" href="<?=base_url()?>rekapitulasi-pos">
							<i class="flaticon2-line-chart"></i> <span class="kt--visible-desktop-inline-block">Rekapitulasi POS</span>
						</a>
					</li>
				</ul>
				<button style="margin-left: 20px" type="button" class="btn btn-danger" id="btn-closing"><i class="flaticon-close"></i> Closing</button>
			</div>
		</div>
		<form action="<?=base_url()?>custom-pos/save" method="post" id="save-form">
			<div class="kt-portlet__body">

				<div class="row" id="guest-form">

					<div class="col-md-6">
						<div class="form-group row">
							<label for="example-text-input" class="col-2 col-form-label">Pelanggan</label>
							<div class="input-group col-10">
								<input type="text" class="form-control" name="guest_nama" value="Guest">
								<div class="input-group-append">
									<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#kt_modal_search_pelanggan"><i class="flaticon-search"></i></button>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-2 col-form-label">Alamat</label>
							<div class="col-10">
								<input class="form-control" type="text" value="" name="guest_alamat">
							</div>
						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-2 col-form-label">Telepon</label>
							<div class="col-10">
								<input class="form-control" type="text" value="" name="guest_telepon">
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group row">
							<label for="example-text-input" class="col-2 col-form-label">No Faktur </label>
							<div class="input-group col-10">
								<label for="example-text-input" class="col-form-label"> <?=$faktur?> </label>
							</div>
							<input type="hidden" name="no_faktur" value="<?=$faktur?>">
							<input type="hidden" name="urutan" value="<?=$urutan?>">
							<input type="hidden" name="tenggat_pelunasan" id="input_tenggat_pelunasan">
							<input type="hidden" name="jenis_pembayaran" id="input_jenis_pembayaran" value="kas">
							<input type="hidden" class="additional" name="additional_no" id="input_no_additional">
							<input type="hidden" class="additional" name="additional_tanggal" id="input_tanggal_additional">
							<input type="hidden" class="additional" name="additional_keterangan" id="input_keterangan_additional">

						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-2 col-form-label">Pengiriman</label>
							<div class="col-10">
								<div class="kt-radio-inline">
									<label class="kt-radio">
										<input type="radio" name="pengiriman" value="ya">Ya
										<span></span>
									</label>
									<label class="kt-radio">
										<input type="radio" name="pengiriman" checked="" value="tidak">Tidak
										<span></span>
									</label>
								</div>
							</div>
						</div>
						<?php if(!isset($_SESSION['login']['lokasi_id'])) { ?>
							<div class="form-group row">
								<label for="example-text-input" class="col-2 col-form-label">lokasi<b class="label--required">*</b></label>
								<div class="col-10">
									<select class="form-control col-md-12" name="lokasi_id" id="lokasi_id" required="">
										<?php
										foreach ($lokasi as $key) {
											?>
											<option value="<?=$key->lokasi_id?>"><?=$key->lokasi_nama?></option>
											<?php
										}
										?>
									</select>
								</div>
							</div>
						<?php } ?>
						<textarea name="transaksi" id="textTransaksi" style="display: none;"></textarea>
						<input type="hidden" id="input_tipe_pembayaran" name="tipe_pembayaran_id" value="<?=$tipe_pembayaran[0]->tipe_pembayaran_id?>">
					</div>

				</div>
			</div>
		</form>
	</div>
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
			</div>
			<div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">
					<div class="dropdown dropdown-inline">
						<button type="button" class="btn btn-brand btn-icon-sm" id="button_add_trans">
							<i class="flaticon2-plus"></i> Tambah Produk
						</button>
					</div>
				</div>
			</div>
		</div>
		<div class="kt-portlet__body">
			<table class="table table-striped- table-hover table-checkable" >
				<thead>
				<tr>
					<th>Desc Produk</th>
					<th>Note</th>
					<th width="80">QTY</th>
					<th>Harga</th>
					<th>Subtotal</th>
					<th width="150">Aksi</th>
				</tr>
				</thead>
				<tbody id="item_child"></tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-lg-5 offset-lg-7">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__body row">
					<div class="col-lg-12">
						<div class="form-group kt-form__group row">
							<label for="example-text-input" class="col-4 col-form-label">Total</label>
							<div class="col-8 col-form-label">
								<strong><span class="total" id="total-item">0</span></strong>
							</div>
						</div>
						<div class="form-group kt-form__group row">
							<label for="example-text-input" class="col-4 col-form-label">Biaya
								Tambahan</label>
							<div class="col-8">
								<input type="text" name="tambahan" id="tambahan" class="input-numeral form-control" value="0">
							</div>
						</div>
						<div class="form-group kt-form__group row">
							<label for="example-text-input" class="col-4 col-form-label">Disc</label>
							<div class="col-8">
								<input type="text" name="potongan" id="potongan" class="input-numeral form-control" value="0">
							</div>
						</div>
						<div class="form-group kt-form__group row">
							<label for="example-text-input" class="col-4 col-form-label">Ballance</label>
							<div class="col-8 col-form-label">
								<strong><span class="" id="grand-total">0</span></strong>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal" id="kt_modal_add" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Produk Custom</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<form action="<?=base_url()?>produk/add" method="post" id="kt_add_staff_form">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="form-control-label ">Jenis Produk <b class="label--required">*</b></label>
								<div class="typeahead">
									<input class="form-control" style="text-transform:uppercase" id="jenis_produk_add" name="jenis_produk" type="text" dir="ltr">
								</div>
							</div>
							<div class="form-group">
								<label class="form-control-label ">Size <b class="label--required">*</b></label>
								<div class="typeahead">
									<input class="form-control" style="text-transform:uppercase" id="size_add" name="size" type="text" dir="ltr">
								</div>
							</div>
							<div class="form-group">
								<label class="form-control-label ">Color <b class="label--required">*</b></label>
								<div class="typeahead">
									<input class="form-control" style="text-transform:uppercase" id="color_add" name="color" type="text" dir="ltr">
								</div>
							</div>
							<div class="form-group">
								<label class="form-control-label ">Fabric <b class="label--required">*</b></label>
								<div class="typeahead">
									<input class="form-control" style="text-transform:uppercase" id="fabric_add" name="fabric" type="text" dir="ltr">
								</div>
							</div>
							<div class="form-group">
								<label class="form-control-label ">Patern <b class="label--required">*</b></label>
								<div class="typeahead">
									<input class="form-control" style="text-transform:uppercase" id="patern_add" name="patern" type="text" dir="ltr">
								</div>
							</div>

						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="form-control-label ">Satuan <b class="label--required">*</b></label>
								<select class="form-control col-md-12" id="satuan_id_add" name="produk_satuan_id" required="">
									<option value="">Pilih Satuan</option>
									<?php
									foreach ($satuan as $key) {
										?>
										<option value="<?=$key->satuan_id?>"><?=$key->satuan_nama?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="form-group">
								<label class="form-control-label ">Harga Satuan<b class="label--required">*</b></label>
								<input type="text" placeholder="" id="harga_add" name="harga" class="input-numeral form-control" value="0" required="">
							</div>
							<div class="form-group">
								<label class="form-control-label ">Jumlah Pesanan<b class="label--required">*</b></label>
								<input type="text" placeholder="" id="jumlah_add" name="jumlah" class="input-numeral form-control" value="0" required="">
							</div>
							<div class="form-group">
								<label class="form-control-label ">Catatan</label>
								<textarea class="form-control" id="note_add" rows="3" name="note" ></textarea>
							</div>

						</div>
					</div>


				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button id="kt_add_submit" type="button" class="btn btn-primary">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal" id="kt_modal_edit" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Produk Custom</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<form action="<?=base_url()?>produk/add" method="post" id="kt_add_staff_form">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="form-control-label ">Jenis Produk <b class="label--required">*</b></label>
								<div class="typeahead">
									<input class="form-control" autocomplete="off" style="text-transform:uppercase" id="jenis_produk_edit" name="jenis_produk" type="text" dir="ltr">
								</div>
							</div>
							<div class="form-group">
								<label class="form-control-label ">Size <b class="label--required">*</b></label>
								<div class="typeahead">
									<input class="form-control" style="text-transform:uppercase" id="size_edit" name="size" type="text" dir="ltr">
								</div>
							</div>
							<div class="form-group">
								<label class="form-control-label ">Color <b class="label--required">*</b></label>
								<div class="typeahead">
									<input class="form-control" style="text-transform:uppercase" id="color_edit" name="color" type="text" dir="ltr">
								</div>
							</div>
							<div class="form-group">
								<label class="form-control-label ">Fabric <b class="label--required">*</b></label>
								<div class="typeahead">
									<input class="form-control" style="text-transform:uppercase" id="fabric_edit" name="fabric" type="text" dir="ltr">
								</div>
							</div>
							<div class="form-group">
								<label class="form-control-label ">Patern <b class="label--required">*</b></label>
								<div class="typeahead">
									<input class="form-control" style="text-transform:uppercase" id="patern_edit" name="patern" type="text" dir="ltr">
								</div>
							</div>

						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="form-control-label ">Satuan <b class="label--required">*</b></label>
								<select class="form-control col-md-12" id="satuan_id_edit" name="produk_satuan_id" required="">
									<option value="">Pilih Satuan</option>
									<?php
									foreach ($satuan as $key) {
										?>
										<option value="<?=$key->satuan_id?>"><?=$key->satuan_nama?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="form-group">
								<label class="form-control-label ">Harga Satuan<b class="label--required">*</b></label>
								<input type="text" placeholder="" id="harga_edit" name="harga" class="input-numeral form-control" value="0" required="">
							</div>
							<div class="form-group">
								<label class="form-control-label ">Jumlah Pesanan<b class="label--required">*</b></label>
								<input type="text" placeholder="" id="jumlah_edit" name="jumlah" class="input-numeral form-control" value="0" required="">
							</div>
							<div class="form-group">
								<label class="form-control-label ">Catatan</label>
								<textarea class="form-control" id="note_edit" rows="3" name="note" ></textarea>
							</div>

						</div>
					</div>


				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button id="kt_edit_submit" type="button" class="btn btn-primary">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal" id="kt_modal_search_pelanggan"  role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<input type="hidden" id="guest_url" value="<?=base_url()?>pos/guest" name="">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Search Pelanggan</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>

			<div class="modal-body">
				<table class="table table-striped- table-hover table-checkable" id="guest-table">
					<thead>
					<tr>
						<th width="30">No</th>
						<th>Nama Pelanggan</th>
						<th>Alamat</th>
						<th>Telepon</th>
						<th width="60">Aksi</th>

					</tr>
					</thead>
					<tbody id="guest_child"></tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="modal" id="kt_modal_pembayaran"  role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">

			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Pembayaran</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-12">
						<div class="form-group row">
							<label for="example-text-input" class="col-3 col-form-label">Total</label>
							<div class="col-9">
								<label class="col-form-label" id="total-bayar">0</label>
							</div>
						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-3 col-form-label">Tambahan</label>
							<div class="col-9">
								<label name="" class="col-form-label" id="tambahan-bayar">0</label>
							</div>
						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-3 col-form-label">Potongan</label>
							<div class="col-9">
								<label name="" class="col-form-label" id="potongan-bayar">0</label>
							</div>
						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-3 col-form-label">Grand Total</label>
							<div class="col-9">
								<label name="" class="col-form-label" id="grand-bayar">0</label>
							</div>
						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-3 col-form-label">Tipe Pembayaran</label>
							<div class="col-9">
								<select class="form-control col-md-12" name="tipe_pembayaran_id" id="tipe_pembayaran" required="">
									<?php
									foreach ($tipe_pembayaran as $key) {
										?>
										<option class="" value="<?=$key->tipe_pembayaran_id?>" data-additional="<?=$key->additional?>" data-kembalian="<?=$key->kembalian?>" data-jenis="<?=$key->jenis_pembayaran?>"><?=$key->tipe_pembayaran_nama.' '.$key->no_akun?></option>
										<?php
									}
									?>
								</select>
							</div>
						</div>

						<div class="form-group row col-md-12" id="terbayar_container">
							<label for="example-text-input" class="col-3 col-form-label">Terbayar</label>
							<div class="col-9">
								<input type="text" name="terbayar" class="form-control input-numeral readonly" value="0" id="terbayar">
							</div>
						</div>
						<div id="kredit_container" style="display: none;">
							<div class="form-group kt-form__group row">
								<label for="example-text-input" class="col-3 col-form-label">Tenggat Pelunasan<b class="label--required">*</b></label>
								<div class="col-9">
									<input type="text" name="tenggat_pelunasan" id="tenggat_pelunasan" class="form-control tanggal tipe_kredit" required="" autocomplete="off">
								</div>
							</div>
						</div>
						<div id="additional_container" style="display: none">
							<div class="form-group row col-md-12" >
								<label for="example-text-input" class="col-3 col-form-label">No ref / No Check / No BG</label>
								<div class="col-9">
									<input type="text" name="additional_no" class="form-control additional">
								</div>
							</div>
							<div class="form-group row col-md-12" >
								<label for="example-text-input" class="col-3 col-form-label">tanggal</label>
								<div class="col-9">
									<input type="text" name="additional_tanggal" id="additional_tanggal" class="form-control tanggal additional" required="" autocomplete="off">
								</div>
							</div>
							<div class="form-group row col-md-12" >
								<label for="example-text-input" class="col-3 col-form-label">Keterangan</label>
								<div class="col-9">
									<textarea class="form-control additional" name="additional_keterangan"></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="btn-save">Save</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="pos-floating-button">
	<button class="btn btn-success btn-pill btn-lg" id="process-button"><i class="fa fa-arrow-right"></i>&nbsp; Proses</button>
</div>
