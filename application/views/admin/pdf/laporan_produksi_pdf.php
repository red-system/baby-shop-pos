
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Laporan Produksi</title>
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <style type="text/css">
    	.table {
  width: 100%;
  margin-bottom: 1rem;
  color: #212529;
  background-color: transparent;
  border-collapse: collapse; }
  .table th,
  .table td {
    padding: 0.75rem;
    vertical-align: top;
    border-top: 1px solid #ebedf2; }
  .table thead th {
    vertical-align: bottom;
    border-bottom: 2px solid #ebedf2; }
  .table tbody + tbody {
    border-top: 1px solid #ebedf2; }

.table-sm th,
.table-sm td {
  padding: 0.3rem; }

.table-bordered {
  border: 1px solid #ebedf2; }
  .table-bordered th,
  .table-bordered td {
    border: 1px solid #ebedf2; }
  .table-bordered thead th,
  .table-bordered thead td {
    border-bottom-width: 1px; }

.table-borderless th,
.table-borderless td,
.table-borderless thead th,
.table-borderless tbody + tbody {
  border: 0; }

.table-striped tbody tr:nth-of-type(odd) {
  background-color: #f7f8fa; }

.table-hover tbody tr:hover {
  color: #212529;
  background-color: #fafbfc; }
body {
    font-family: 'Poppins';font-size: 12px;
}
    </style>
</head>
<body>
 
<div id="container">
	<table class="datatable table table-borderless">
		<thead>
			<tr>
				<th width="10"></th>
				<th width="42%"></th>
				<th width="50%"></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td rowspan="3"><img src="<?=base_url()?>assets/media/logos/logo-laporan.png" width="65px" height="auto"></td>
				<td><strong><?=$this->config->item('company_name')?></strong><br><?=$this->config->item('company_address')?><br><?=$this->config->item('company_phone')?></td>
				<td style="text-align: right"><strong>Laporan Produksi</strong>
					<br><span>Tanggal Produksi : <?=$mulai_start." s/d ".$mulai_end?></span>
					<br><span>Kode Produksi : <?=$produksi_kode?></span> 
					<br><span>Status Produksi : <?=$status_produksi?></span> 
					<br><span>Tanggal Penerimaan : <?=$penerimaan_start." s/d ".$penerimaan_end?></span>
					<br><span>Status Penerimaan : <?=$status_penerimaan?></span> 
					<br><span>Produk : <?=$produk?></span> 
				</td>
			</tr>
		</tbody>
	</table>
	<table class="datatable table table-striped- table-bordered table-hover table-checkable">
			<thead>
				<tr>
					<th width="30">No</th>
					<th>Kode Produksi</th>
					<th>Tanggal Produksi</th>
					<th>Status Produksi</th>
					<th>Tanggal Penerimaan</th>
					<th>Status Penerimaan</th>
					<th>Produk</th>
					<th>Jumlah</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$sum = 0;
				foreach ($list as $key) {
				$sum +=str_replace(",","",$key->jumlah);
				?>
				<tr>
					<td><?=$key->no?></td>
					<td><?=$key->produksi_kode?></td>
					<td><?=$key->tanggal_mulai?></td>
					<td><?=$key->status_produksi?></td>
					<td><?=$key->tanggal_penerimaan?></td>
					<td><?=$key->status_penerimaan?></td>
					<td><?=$key->produk_nama?></td>
					<td style="text-align: right;"><?=$key->jumlah?></td>
				</tr>
				<?php
			}
			?>
			</tbody>
			<tfoot >
				<tr>
					<td colspan="7" style="text-align: right"><strong>Total</strong> </td>
					<td ><strong><?=number_format($sum)?></strong></td>
				</tr>

			</tfoot>
	</table> 
</div>
 
</body>
</html>