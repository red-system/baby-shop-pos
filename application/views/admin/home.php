<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="margin-top: 15px">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body ">
            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                <div class="row align-items-center">
                    <div class="col-md-12 order-2 order-xl-1 searchForm">
                        <div class="row align-items-center">
                            <div class="col-md-3"></div>
                            <div class="col-md-2 kt-margin-b-20-tablet-and-mobile" style="display: <?=((isset($_SESSION['login']['lokasi_id']))? "none":"block")?>">
                                <div class="kt-form__group kt-form__group--inline">
                                    <div class="kt-form__label">
                                        <label>Lokasi</label>
                                    </div>
                                    <div class="kt-form__control">
                                        <select id="lokasi_id" class="form-control searchInput" data-col-index="3" data-field="lokasi_id">
                                            <?php if(!isset($_SESSION['login']['lokasi_id'])) { ?>
                                                <option value="All" <?=($lokasi_id=='All'?'selected':'')?>>All</option>
                                                <?php foreach ($lokasi as $key) {
                                                    ?>
                                                    <option value="<?=$key->lokasi_id?>" <?=($lokasi_id==$key->lokasi_id?'selected':'')?>><?=$key->lokasi_nama?></option>
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <option value="<?=$_SESSION['login']['lokasi_id']?>"></option>
                                                <?php
                                            } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-form__group kt-form__group--inline">
                                    <div class="kt-form__label">
                                        <label>Tanggal</label>
                                    </div>
                                    <div class="kt-form__control">
                                        <div class="input-daterange input-group" id="kt_datepicker">
                                            <input type="text" class="form-control kt-input searchInput tanggal" id="start_date" name="start_date" placeholder="Dari" autocomplete="off" data-col-index="1" value="<?=$min_tanggal?>" data-field="tanggal_start" />
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
                                            </div>
                                            <input type="text" class="form-control kt-input searchInput tanggal" id="end_date" name="end_date" placeholder="Sampai" autocomplete="off" data-col-index="1" value="<?=$max_tanggal?>" data-field="tanggal_end" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__foot text-center">
            <div class="btn-group btn-group btn-pill btn-group-sm">
                <button type="button" class="btn btn-info akses-filter_data">
                    <i class="la la-search"></i> Filter Data
                </button>

            </div>
        </div>
    </div>
    <div class="col-md-12 row">
        <div class="col-md-6">
            <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-warning">
                <div class="kt-portlet__head ">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Piutang Akan Jatuh Tempo
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <h3></h3>
                    </div>
                </div>
                <div class="kt-portlet__body" style="text-align: center;background-color: white;border-radius: 0px;">
                    <input type="hidden" id="piutang_akan" value="<?=base_url()."home/piutang-akan?start_date=".$min_tanggal.'&end_date='.$max_tanggal.'&lokasi_id='.$lokasi_id?>" name="">
                    <table class="datatable table table-striped- table-hover table-checkable" style="color: #6c7293" id="piutang-akan-table">
                        <thead>
                        <tr>
                            <th width="10">No</th>
                            <th>Pelanggan</th>
                            <th>No Faktur</th>
                            <th>Sisa</th>
                            <th>Tenggat Pelunasan</th>

                        </tr>
                        </thead>
                        <tbody id="child_data_ajax">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-danger">
                <div class="kt-portlet__head ">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Piutang Jatuh Tempo
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <h3></h3>
                    </div>
                </div>
                <div class="kt-portlet__body" style="text-align: center;background-color: white;border-radius: 0px;">
                    <input type="hidden" id="piutang_jatuh" value="<?=base_url()."home/piutang-jatuh-tempo?start_date=".$min_tanggal.'&end_date='.$max_tanggal.'&lokasi_id='.$lokasi_id?>" name="">
                    <table class="datatable table table-striped- table-hover table-checkable" style="color: #6c7293" id="piutang-jatuh-table">
                        <thead>
                        <tr>
                            <th width="10">No</th>
                            <th>Pelanggan</th>
                            <th>No Faktur</th>
                            <th>Sisa</th>
                            <th>Tenggat Pelunasan</th>

                        </tr>
                        </thead>
                        <tbody id="child_data_ajax">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 row">
        <div class="col-md-6">
            <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-warning">
                <div class="kt-portlet__head ">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Hutang Akan Jatuh Tempo
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <h3></h3>
                    </div>
                </div>
                <div class="kt-portlet__body" style="text-align: center;background-color: white;border-radius: 0px;">
                    <input type="hidden" id="hutang_akan" value="<?=base_url()."home/hutang-akan?start_date=".$min_tanggal.'&end_date='.$max_tanggal.'&lokasi_id='.$lokasi_id?>" name="">
                    <table class="datatable table table-striped- table-hover table-checkable" style="color: #6c7293" id="hutang-akan-table">
                        <thead>
                        <tr>
                            <th width="10">No</th>
                            <th>Suplier</th>
                            <th>No PO Produk</th>
                            <th>Sisa</th>
                            <th>Tenggat Pelunasan</th>

                        </tr>
                        </thead>
                        <tbody id="child_data_ajax">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-danger">
                <div class="kt-portlet__head ">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Hutang Jatuh Tempo
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <h3></h3>
                    </div>
                </div>
                <div class="kt-portlet__body" style="text-align: center;background-color: white;border-radius: 0px;">
                    <input type="hidden" id="hutang_jatuh" value="<?=base_url()."home/hutang-jatuh-tempo?start_date=".$min_tanggal.'&end_date='.$max_tanggal.'&lokasi_id='.$lokasi_id?>" name="">
                    <table class="datatable table table-striped- table-hover table-checkable" style="color: #6c7293" id="hutang-jatuh-table">
                        <thead>
                        <tr>
                            <th width="10">No</th>
                            <th>Suplier</th>
                            <th>No PO Produk</th>
                            <th>Sisa</th>
                            <th>Tenggat Pelunasan</th>

                        </tr>
                        </thead>
                        <tbody id="child_data_ajax">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 row">
        <div class="col-md-4">
            <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-danger">
                <div class="kt-portlet__head ">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Sales - <?=date("d/m/Y",strtotime($min_tanggal))." - ".date("d/m/Y")?>
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <h3></h3>
                    </div>
                </div>
                <div class="kt-portlet__body" style="text-align: center;">
                    <h2>Rp. <?=$sales?></h2>								</div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-brand">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Hutang Keseluruhan
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body" style="text-align: center;">
                    <h2>Rp. <?=$hutang?></h2>								</div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-warning">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Piutang Keseluruhan
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body" style="text-align: center;">
                    <h2>Rp. <?=$piutang?></h2>								</div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">

            <!--begin::Portlet-->
            <div class="kt-portlet kt-portlet--tab">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
												<span class="kt-portlet__head-icon kt-hidden">
													<i class="la la-gear"></i>
												</span>
                        <h3 class="kt-portlet__head-title">
                            Sales Chart
                            <textarea style="display: none;" id="chart-data"><?=json_encode($chartData)?></textarea>
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div id="kt_morris_1" style="height:500px;"></div>
                </div>
            </div>

            <!--end::Portlet-->
        </div>
    </div>
    <div class="col-md-12 row">
        <div class="col-md-4">
            <div class="kt-portlet ">
                <div class="kt-portlet__head">
                    <input type="hidden" id="most_url" value="<?=base_url()."home/most?start_date=".$min_tanggal.'&end_date='.$max_tanggal.'&lokasi_id='.$lokasi_id?>" name="">
                    <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="flaticon-notes"></i>
										</span>
                        <h3 class="kt-portlet__head-title">
                            Produk Paling Laku
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <table class="datatable table table-striped- table-hover table-checkable" style="color: #6c7293" id="most-table">
                        <thead>
                        <tr>
                            <th width="30">Kode Produk</th>
                            <th>Nama Produk</th>
                            <th style="text-center">Jumlah</th>
                            <th>Lokasi</th>

                        </tr>
                        </thead>
                        <tbody id="child_data_ajax">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="kt-portlet ">
                <div class="kt-portlet__head">
                    <input type="hidden" id="least_url" value="<?=base_url()."home/least?start_date=".$min_tanggal.'&end_date='.$max_tanggal.'&lokasi_id='.$lokasi_id?>" name="">
                    <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="flaticon-notes"></i>
										</span>
                        <h3 class="kt-portlet__head-title">
                            Produk Kurang Laku
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <table class="datatable table table-striped- table-hover table-checkable" style="color: #6c7293" id="least-table">
                        <thead>
                        <tr>
                            <th width="30">Kode Produk</th>
                            <th>Nama Produk</th>
                            <th style="text-center">Jumlah</th>
                            <th>Lokasi</th>

                        </tr>
                        </thead>
                        <tbody id="child_data_ajax">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="kt-portlet ">
                <input type="hidden" id="country_url" value="<?=base_url()."home/country?start_date=".$min_tanggal.'&end_date='.$max_tanggal.'&lokasi_id='.$lokasi_id?>" name="">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="flaticon-notes"></i>
										</span>
                        <h3 class="kt-portlet__head-title">
                            Visitor
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-actions">
                            <a href="#" class="btn btn-outline-light btn-pill btn-sm btn-icon btn-icon-md">
                                <i class="flaticon2-lock"></i>
                            </a>
                            <a href="#" class="btn btn-outline-light btn-pill btn-sm btn-icon btn-icon-md">
                                <i class="flaticon2-download-symbol"></i>
                            </a>
                            <a href="#" class="btn btn-outline-light btn-pill btn-sm btn-icon btn-icon-md">
                                <i class="flaticon2-rocket-1"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <table class="datatable table table-striped- table-hover table-checkable" id="guest-table">
                        <thead>
                        <tr>
                            <th>Country</th>
                            <th style="text-center">Jumlah</th>
                        </tr>
                        </thead>
                        <tbody id="child_data_ajax"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

					