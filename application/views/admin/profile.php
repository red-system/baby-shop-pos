<!-- begin:: Content -->
                        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                            
                        </div>
                        <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_profile">

                           <div class="row">
                               <div class="col-md-4">
                                   <div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
										
										<div class="kt-portlet__body">
											<div class="kt-profile-pic">
												<img src="<?=base_url().$_SESSION['login']['avatar']?>" style='height:auto;width: 40%;' class="account_avatar">
											</div>
											<h5 style="text-align: center;margin-top: 20px"><b class="account_name"><?=$user->staff_nama?></b></h5>
											<span style="text-align: center;"><?=$user->user_role_name?></span>
                                            <span style="text-align: center;"><?=$user->staff_email?></span>
                                            <span style="text-align: center;"><?=$user->staff_phone_number?></span>											
										</div>
									</div>
                               </div>
                               <div class="col-md-8">
                               		<div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
										<div class="kt-portlet__body">
											<ul class="nav nav-tabs  nav-tabs-line" role="tablist">
												<li class="nav-item">
													<a class="nav-link active" data-toggle="tab" href="#kt_tabs_1_1" role="tab">Personal Info</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" data-toggle="tab" href="#kt_tabs_1_2" role="tab">Change Avatar</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" data-toggle="tab" href="#kt_tabs_1_3" role="tab">Change Password</a>
												</li>
											</ul>
											<div class="tab-content">
												<div class="tab-pane active" id="kt_tabs_1_1" role="tabpanel" style="padding: 10px">
													<form role="form" action="<?=base_url()?>profile/personal-info" id="edit_personal_form" method="post">
                                                                <div class="form-group">
                                                                    <label class="control-label">Name</label>
                                                                    <input type="text" placeholder="John" name="user_name" class="form-control" value="<?=$user->staff_nama?>" readonly="" id="personal_name"> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Email</label>
                                                                    <input type="text" placeholder="Doe" name="email" class="form-control" value="<?=$user->staff_email?>" readonly="" id="personal_email"> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Mobile Number</label>
                                                                    <input type="tel" placeholder="+1 646 580 DEMO (6284)" name="phone_number" class="form-control" value="<?=$user->staff_phone_number?>" readonly="" id="personal_phone"> 
                                                                </div>
                                                                <div class="form-group row">
                                                                    <div class="form-group col-6">
                                                                        <label class="control-label">Tempat lahir</label>
                                                                        <div class="col-md-12">
                                                                            <input type="text"  name="tempat_lahir" class="form-control" value="<?=$user->tempat_lahir?>" id="tempat_lahir" readonly=""> 
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group col-6">
                                                                        <label class="control-label">Tanggal Lahir <b class="label--required">*</b></label>
                                                                        <div class="col-md-12">
                                                                            <input type="text" placeholder="21-01-1987" name="tanggal_lahir" class="form-control tanggal" value="<?=$user->tanggal_lahir?>"  id="tanggal_lahir">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                   <label class="control-label">Alamat</label>
                                                                    <textarea class="form-control" id="staff_alamat" rows="3" name="staff_alamat" readonly=""><?=$user->staff_alamat?></textarea>
                                                                </div>
                                                                <div class="form-group" class="" id="personal_edit_group" style="display: none;">
                                                                	<button id="kt_personal_info_submit" class="btn btn-primary pull-right" style="margin: 5px"><i class="fa fa-save"></i> Save</button>
                                                                	<button type="button" class="btn btn-danger pull-right" style="margin: 5px" id="cancel_personal_btn"><i class="fa fa-window-close"></i> Cancel</button>
                                                                </div>
                                                                <div class="form-group" class="" id="personal_cancel_group" >
                                                                    <button type="button" class="btn btn-warning pull-right" style="margin: 5px" id="edit_personal_btn"><i class="fa fa-edit"></i> Edit</button>
                                                                </div>
                                                            </form>
												</div>
												<div class="tab-pane" id="kt_tabs_1_2" role="tabpanel" style="padding: 10px">
													<form role="form" action="<?=base_url()?>profile/change-avatar" id="edit_avatar_form" method="post" enctype='multipart/form-data'>
                                                                <div class="form-group">
                                                                    <div class="fileinput fileinput-exists" data-provides="fileinput">
                                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                            <img src="<?=base_url()?>assets/media/no_image.png" alt="" id="displayImg" width="200" height="150"> </div>
                                                                        <div class="form-group">
                                                                            <label>File Browser</label>
                                                                            <div></div>
                                                                            <div class="custom-file">
                                                                                <input type="file" class="custom-file-input" name="avatar" accept="image/x-png,image/gif,image/jpeg" id="customFile">
                                                                                <label class="custom-file-label selected" for="customFile" id="labelCustomFile"></label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="margin-top-10">
                                                                    <button id="kt_avatar_submit" class="btn btn-primary pull-right" style="margin: 5px"><i class="fa fa-save"></i> Save</button>
                                                                    
                                                                </div>
                                                            </form>
												</div>
												<div class="tab-pane" id="kt_tabs_1_3" role="tabpanel" style="padding: 10px">
                                                            <form role="form" action="<?=base_url()?>profile/change-password" id="edit_password_form" method="post">
                                                                <div class="form-group">
                                                                    <label class="control-label">Current Password</label>
                                                                    <input type="password" name="old_pass" id="old_pass" class="form-control"> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">New Password</label>
                                                                    <input type="password" name="new_pass" id="new_pass" class="form-control"> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Re-type New Password</label>
                                                                    <input type="password" name="re_pass" id="re_pass" class="form-control"> </div>
                                                                <div class="margin-top-10">
                                                                    <button id="kt_password_submit" class="btn btn-primary pull-right" style="margin: 5px"><i class="fa fa-save"></i> Save</button>
                                                                </div>
                                                            </form>
                                                </div>
											</div>
										</div>
									</div>
                               </div>
                           </div>
                        </div>

                    
