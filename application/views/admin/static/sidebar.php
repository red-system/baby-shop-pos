
<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
    <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
        <ul class="kt-menu__nav ">
            <?php
            $parent='';
            $html = '';
            $sub_menu = '';
            $menu_akses = $this->menu_akses;
            $menu_akses = json_decode($menu_akses,true);
            foreach ($this->side_menu as $key) {
                $text = '<li class="kt-menu__item balioz-menu '.(($page == $key->menu_kode) ? 'kt-menu__item--open kt-menu__item--here' : '').'" aria-haspopup="true"><a href="'.base_url().$key->url.'" class="kt-menu__link "><span class="kt-menu__link-icon">'.$this->icons[$key->icon].'</span><span class="kt-menu__link-text">'.$key->menu_nama.'</span>'.(isset($this->badge_parrent[$key->menu_kode])?$this->badge_parrent[$key->menu_kode]:'').'</a></li>';
                if($menu_akses[$key->menu_kode]['akses_menu']){
                    if($parent!= $key->menu_kode){
                        if($html != '' ){
                            $text2 = '</ul></div></li>';
                            $html .=$text2;
                            echo $html;
                            $html = '';
                        }
                        if($key->sub_menu_nama!=null ){
                            $text = '<li class="kt-menu__item  kt-menu__item--submenu '.(($parrent == $key->menu_kode) ? 'kt-menu__item--open kt-menu__item--here' : '').'" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">'.$this->icons[$key->icon].'</span><span class="kt-menu__link-text">'.$key->menu_nama.'</span>'.(isset($this->badge_parrent[$key->menu_kode])?$this->badge_parrent[$key->menu_kode]:'').'<i class="kt-menu__ver-arrow la la-angle-right"></i></a>
													<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
														<ul class="kt-menu__subnav">';
                            if($menu_akses[$key->menu_kode][$key->sub_menu_kode]['akses_menu']){
                                $text .='<li class="kt-menu__item '.(($page == $key->sub_menu_kode) ? 'kt-menu__item--open kt-menu__item--here' : '').'" data-check="123" aria-haspopup="true"><a href="'.base_url().$key->sub_url.'" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">'.$key->sub_menu_nama.'</span> '.(isset($this->badge_sub[$key->sub_menu_kode])?$this->badge_sub[$key->sub_menu_kode]:'').'</a></li>';
                            }
                            $html .=$text;

                        } else {
                            echo $text;
                        }
                        $parent = $key->menu_kode;

                    } else {
                        if($menu_akses[$key->menu_kode][$key->sub_menu_kode]['akses_menu']){
                            $text2 = '<li class="kt-menu__item '.(($page == $key->sub_menu_kode) ? 'kt-menu__item--open kt-menu__item--here' : '').'" data-check="123" aria-haspopup="true"><a href="'.base_url().$key->sub_url.'" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">'.$key->sub_menu_nama.'</span>'.(isset($this->badge_sub[$key->sub_menu_kode])?$this->badge_sub[$key->sub_menu_kode]:'').'</a></li>';

                            $html .=$text2;
                        }

                    }
                } else {
                    if($html != '' ){
                        $text2 = '</ul></div></li>';
                        $html .=$text2;
                        echo $html;
                        $html = '';
                    }
                }
            }
            if($html != '' ){
                $text2 = '</ul></div></li>';
                $html .=$text2;
                echo $html;
                $html = '';
            }
            ?>
        </ul>
    </div>
</div>
</div>

