<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Metronic | Error Page - 1</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
                google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
                active: function() {
                    sessionStorage.fonts = true;
                }
            });
        </script>
		<link href="<?=config_item('base_url')?>assets/app/custom/error/error-v1.default.css" rel="stylesheet" type="text/css" />
		<link href="<?=config_item('base_url')?>assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
		<link href="<?=config_item('base_url')?>assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
		<link href="<?=config_item('base_url')?>assets/demo/default/skins/header/base/light.css" rel="stylesheet" type="text/css" />
		<link href="<?=config_item('base_url')?>assets/demo/default/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
		<link href="<?=config_item('base_url')?>assets/demo/default/skins/brand/dark.css" rel="stylesheet" type="text/css" />
		<link href="<?=config_item('base_url')?>assets/demo/default/skins/aside/dark.css" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" href="<?=config_item('base_url')?>assets/media/logos/favicon.ico" />
	</head>
	<body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
		<div class="kt-grid kt-grid--ver kt-grid--root">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid  kt-error-v1" style="background-image: url(<?=config_item('base_url')?>assets/media//error/bg1.jpg);">
				<div class="kt-error-v1__container">
					<h1 class="kt-error-v1__number">404</h1>
					<p class="kt-error-v1__desc">
						OOPS! Something went wrong here
					</p>
				</div>
			</div>
		</div>
		<script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#5d78ff",
						"dark": "#282a3c",
						"light": "#ffffff",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
						"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
					}
				}
			};
		</script>
		<script src="<?=config_item('base_url')?>assets/vendors/general/jquery/dist/jquery.js" type="text/javascript"></script>
		<script src="<?=config_item('base_url')?>assets/vendors/general/popper.js/dist/umd/popper.js" type="text/javascript"></script>
		<script src="<?=config_item('base_url')?>assets/vendors/general/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="<?=config_item('base_url')?>assets/vendors/general/js-cookie/src/js.cookie.js" type="text/javascript"></script>
		<script src="<?=config_item('base_url')?>assets/vendors/general/moment/min/moment.min.js" type="text/javascript"></script>
		<script src="<?=config_item('base_url')?>assets/vendors/general/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
		<script src="<?=config_item('base_url')?>assets/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
		<script src="<?=config_item('base_url')?>assets/vendors/general/sticky-js/dist/sticky.min.js" type="text/javascript"></script>
		<script src="<?=config_item('base_url')?>assets/vendors/general/wnumb/wNumb.js" type="text/javascript"></script>
		<script src="<?=config_item('base_url')?>assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
		<script src="<?=config_item('base_url')?>assets/app/bundle/app.bundle.js" type="text/javascript"></script>
	</body>
</html>